 

 
  
  
   
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
  </ol>
  
  <div class="carousel-inner">
   
    <div class="carousel-item height-90vh padding-y-80 active">
     <div class="bg-absolute" data-dark-overlay="5" style="background:url(website/images/slider1.png) no-repeat"></div>
       
    </div>
   
    <div class="carousel-item height-90vh padding-y-80">
      <div class="bg-absolute" data-dark-overlay="5" style="background:url(website/images/slider2.png) no-repeat"></div>
       
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <i class="ti-angle-left iconbox bg-black-0_5 hover:primary"></i>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <i class="ti-angle-right iconbox bg-black-0_5 hover:primary"></i>
  </a>
</div>
    
    

<section class="padding-y-100 bg-light">
  <div class="container">
    <div class="row">
    <div class="col-12 text-center text-white mb-5">
        <h3 style='color:black;'>
          Browse SPEED’s Online Microcourses by Category
        </h3>
        <div class="width-3rem height-4 rounded bg-primary mx-auto"></div>
      </div>
    </div>
    <div class="col-12">
       <div class="owl-carousel arrow-on-hover"
         data-space="30"
         data-arrow="true"
         >
         

         <?php for($i=0;$i<count($categoryList);$i++) { 

         ?> 
        <div class="card text-gray height-100p shadow-v1">
         <div class="position-relative">
           <a href="">
             <img class="card-img-top" style='height:200px;' src="website/images/<?php echo $categoryList[$i]->image;?>" style="height:200px;" alt="">
           </a>
         </div>
          <div class="card-body">
            <a href="course/index/<?php echo $categoryList[$i]->id;?>" class="h6" style="text-transform: uppercase;">
              <?php echo $categoryList[$i]->name;?>
            </a>
          </div>
        </div>

      <?php } ?>
         
      
       
       </div>
     </div>
  </div> <!-- END container-->
</section>



<section class="paddingTop-50 paddingBottom-100 bg-light-v2">
  <div class="container">
   
    <div class="row marginTop-50">
      <div class="col-md-6 my-2">
        <h3>
          EARLY CHILDHOOD EDUCATION
        </h3>
      </div>
      <div class="col-md-6 my-2 text-md-right">
        <a href="#" class="btn btn-outline-primary">All Courses</a>
      </div>
      <div class="col-12 mt-2">
        <div class="owl-carousel arrow-on-hover" 
        data-items="4"
        data-state-outer-class="py-4"
        data-arrow="true"
        data-autoplay="false"
        data-space="30"
        data-loop="true">


        <?php for($j=0;$j<count($firstCourseList);$j++) { ?>
         
          <div class="card text-gray height-100p shadow-v2">
            <a href="">
              <img class="card-img-top" style='height:200px;' src="website/images/<?php echo $firstCourseList[$j]->file;?>" alt="">
            </a>
            <div class="p-4">
              <a href="<?php echo BASE_PATH;?>coursedetails/<?php echo $firstCourseList[$j]->id;?>" class="h6" style='height:59px;'>
                <?php echo $firstCourseList[$j]->name;?>
              </a>
            </div>
            <div class="media border-top p-4 align-items-center justify-content-between">
              <h4 class="h5 mb-0">
              <span class="text-primary">RM <?php echo $firstCourseList[$j]->amount;?></span>
              </h4>
              <a href="#" class="btn btn-opacity-primary iconbox iconbox-sm" 
                data-container="body" 
                data-toggle="tooltip" 
                data-placement="top" 
                data-skin="light" 
                title="Add to wishlist">
                <i class="ti-heart"></i>
              </a>
            </div>
         </div>
          <?php } ?> 
         
         


        </div>
      </div>
    </div> <!-- END row-->
    
    <div class="row mt-5 marginTop-50">
      <div class="col-md-6 my-2">
        <h3>
          KURSUS ASUHAN PERMATA Courses
        </h3>
      </div>
      <div class="col-md-6 my-2 text-md-right">
        <a href="#" class="btn btn-outline-primary">All Courses</a>
      </div>
      <div class="col-12 mt-2">
        <div class="owl-carousel arrow-on-hover" 
        data-items="4"
        data-arrow="true"
        data-state-outer-class="py-4"
        data-autoplay="false"
        data-space="30"
        data-loop="true">
         
           
        <?php for($j=0;$j<count($secondCourseList);$j++) { ?>
         
          <div class="card text-gray height-100p shadow-v2">
            <a href="">
              <img class="card-img-top" style='height:200px;' src="website/images/<?php echo $secondCourseList[$j]->file;?>" alt="">
            </a>
            <div class="p-4">
              <a href="<?php echo BASE_PATH;?>coursedetails/<?php echo $secondCourseList[$j]->id;?>" class="h6" style='height:59px;'>
                <?php echo $secondCourseList[$j]->name;?>
              </a>
            </div>
            <div class="media border-top p-4 align-items-center justify-content-between">
              <h4 class="h5 mb-0">
              <span class="text-primary">RM <?php echo $secondCourseList[$j]->amount;?></span>
              </h4>
              <a href="#" class="btn btn-opacity-primary iconbox iconbox-sm" 
                data-container="body" 
                data-toggle="tooltip" 
                data-placement="top" 
                data-skin="light" 
                title="Add to wishlist">
                <i class="ti-heart"></i>
              </a>
            </div>
         </div>
          <?php } ?> 
         

           

         

         
        </div>
      </div>
    </div> <!-- END row-->
    
    <div class="row mt-5 marginTop-50">
      <div class="col-md-6 my-2">
        <h3>
          MANAGEMENT AND LEADERSHIP Courses
        </h3>
      </div>
      <div class="col-md-6 my-2 text-md-right">
        <a href="#" class="btn btn-outline-primary">All Courses</a>
      </div>
      <div class="col-12 mt-2">
        <div class="owl-carousel arrow-on-hover" 
        data-items="4"
        data-arrow="true"
        data-state-outer-class="py-4"
        data-autoplay="false"
        data-space="30"
        data-loop="true">
         
           <?php for($j=0;$j<count($thirdCourseList);$j++) { ?>
         
          <div class="card text-gray height-100p shadow-v2">
              <img class="card-img-top" style='height:200px;' src="website/images/<?php echo $thirdCourseList[$j]->file;?>" alt="">
            <div class="p-4">
              <a href="<?php echo BASE_PATH;?>coursedetails/<?php echo $thirdCourseList[$j]->id;?>" class="h6" style='height:59px;'>
                <?php echo $thirdCourseList[$j]->name;?>
              </a>
            </div>
            <div class="media border-top p-4 align-items-center justify-content-between">
              <h4 class="h5 mb-0">
              <span class="text-primary">RM <?php echo $thirdCourseList[$j]->amount;?></span>
              </h4>
              <a href="#" class="btn btn-opacity-primary iconbox iconbox-sm" 
                data-container="body" 
                data-toggle="tooltip" 
                data-placement="top" 
                data-skin="light" 
                title="Add to wishlist">
                <i class="ti-heart"></i>
              </a>
            </div>
         </div>
          <?php } ?> 
        </div>
      </div>
    </div> <!-- END row-->
  </div> <!-- END container-->
</section>   <!-- END section-->
 
   

   
 
<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> 

<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="website/js/vendors.bundle.js"></script>
    <script src="website/js/scripts.js"></script>
  </body>
</html>