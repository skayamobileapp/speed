 
<section class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 mr-auto mt-4">
        <div class="border border-light p-5">
          <img class="w-100" src="<?php echo BASE_PATH;?>website/images/<?php echo $courseDetails->file;?>" alt="">
        </div>
      </div>
      <div class="col-lg-6 mt-4">
        <h2><?php echo $courseDetails->name;?></h2>
        <ul class="list-inline">
          <li class="list-inline-item pr-3 border-right">
            <ul class="list-unstyled ec-review-rating font-size-12">
              <li class="active"><i class="fas fa-star"></i></li>
              <li class="active"><i class="fas fa-star"></i></li>
              <li class="active"><i class="fas fa-star"></i></li>
              <li class="active"><i class="fas fa-star"></i></li>
              <li class="active"><i class="fas fa-star"></i></li>
            </ul>
          </li>
          <li class="list-inline-item">3 customer reviews</li>
        </ul>
        <h4 class="mb-3">
          <span class="text-primary">RM <?php echo $courseDetails->amount;?></span>
          <span class="text-gray"><s>RM 1000</s></span>
        </h4>
        <p><i class="fas fa-check-circle text-success mr-2"></i>Available on stock</p>
        <div class="mb-2">
          <h4>Key Features</h4>
          <ul class="list-unstyled list-style-icon list-icon-bullet mt-3">
            <li>The dos and don'ts of storing passwords in a database</li>
            <li>Exchange Rates and the Currency Conversion Tool</li>
            <li>Building a Web Messenger with Microservices</li>
            <li>Extending TempMessenger with a User Authentication Microservice</li>
          </ul>
        </div>
        
        <div class="d-md-flex">
          <button class="btn btn-primary mb-2 mr-3" onclick='buynow(<?php echo $courseDetails->id;?>)'>Buy Now</button>
        </div>
      </div>
    </div> <!-- END row-->
  </div> <!-- END container-->
</section>







<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> 

<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  </body>
</html>

<script>
   function buynow(id)
    {
        $.get("/coursedetails/tempbuynow/"+id, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>login";
         });
    }

  </script>