
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Register</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="#"> Register</a>
        </li>
       
      </ol>
     </div>
   </div>
  </div> 
</div>




<form method="POST" action="<?php echo base_url(); ?>register/index">

<section class="padding-y-10">
  <div class="container">
   <div class="row">
        <div class="col-md-12 order-md-1">
          <h4 class="mb-3">Billing address</h4>
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">First name</label>
                <input type="text" class="form-control" id="firstName" name='firstName' placeholder="" value="" required="required">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Last name</label>
                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="" value="" required="">
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">NRIC / Passport</label>
                <input type="text" class="form-control" id="NRIC" name="NRIC" placeholder="" value="" required="required">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              
            </div>

             <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="" value="" required="required" onblur="checkduplicate()">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">PAssword</label>
                <input type="text" class="form-control" id="password" name="password" placeholder="" value="" required="">
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>


            <div class="mb-3">
              
            </div>

            <div class="mb-3">
              <label for="address">Address</label>
              <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" required="">
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="mb-3">
              <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
              <input type="text" class="form-control" id="address2" name="address2" placeholder="Apartment or suite">
            </div>

            <div class="row">
              <div class="col-md-5 mb-3">
                <label for="country">Country</label>
                <select class="custom-select d-block w-100" id="country" name="country" required="">
                  <option value="">Choose...</option>
                  <option>United States</option>
                </select>
                <div class="invalid-feedback">
                  Please select a valid country.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="state">State</label>
                <select class="custom-select d-block w-100" id="state" name="state" required="">
                  <option value="">Choose...</option>
                  <option>California</option>
                </select>
                <div class="invalid-feedback">
                  Please provide a valid state.
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="zip">Zip</label>
                <input type="text" class="form-control" id="zip"  name="zip" placeholder="" required="">
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>
            </div>
            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
        </div>
      </div>
  </div> <!-- END container-->
</section>

</form>



<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> 

<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>

<script>
function checkduplicate() {
  var email = $("#email").val();
        $.get("/register/duplicateemail/"+email, function(data, status){
             if(data=='1') {
              alert("Email already exist please login and proceed further");
               $("#email").val(' ');
             }
         });
}
</script>
  </body>
</html>