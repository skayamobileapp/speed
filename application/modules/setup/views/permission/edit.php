<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Edit Permission</h3>
            </div>


    <form id="form_main" action="" method="post">

        <div class="form-container">

            <h4 class="form-group-title">Permission Details</h4>

        
                <div class="row">

                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Permission<span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $permission->code; ?>">
                          </div>
                        </div>
                      </div>

                       <div class="col-lg-6">
                          <div class="form-group row align-items-center">
                            <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($permission->status == 1){
                                    echo "checked=checked";
                                } ?> >
                                <label class="custom-control-label" for="customRadioInline1">Active</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($permission->status == 0){
                                    echo "checked=checked";
                                } ?> >
                                <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                              </div>
                            </div>
                          </div>
                        </div>

                  </div>      

                  <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Description</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" id="description" name="description" ><?php echo $permission->description; ?></textarea>
                          </div>
                        </div>
                    </div>
                    
                  </div>      


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <a href="../list" class="btn btn-link">Cancel</a>
                  </div>

                </div> 

            </div> 

    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

   $(document).ready(function() {
        $("#form_main").validate({
            rules: {
              code: {
                    required: true
                },
                description : {
                  required: true
                }
            },
            messages: {
              code: {
                    required: "<p class='error-text'>Permission required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>