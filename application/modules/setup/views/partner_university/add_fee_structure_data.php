<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Parter University Fee Structure Details</h3>
        </div>



			<?php
	        if($partnerUniversity)
	        {
	        ?>

	        <br>

	        <div class="form-container">
	            <h4 class="form-group-title">Partner University, Aggrement & Fee Details</h4>
	            <div class='data-list'>
	                <div class='row'> 
	                    <div class='col-sm-6'>
	                        <dl>
	                            <dt>University Name :</dt>
	                            <dd><?php echo ucwords($partnerUniversity->name);?></dd>
	                        </dl>
	                        <dl>
	                            <dt>Short Name :</dt>
	                            <dd><?php echo $partnerUniversity->short_name ?></dd>
	                        </dl>
	                        <?php
					        if($partnerUniversityAggrement)
					        {
					        ?>

					        <dl>
	                            <dt>Aggrement Name :</dt>
	                            <dd><?php echo ucwords($partnerUniversityAggrement->name);?></dd>
	                        </dl>
	                        <dl>
	                            <dt>Start Date :</dt>
	                            <dd><?php echo date('d-m-Y', strtotime($partnerUniversityAggrement->start_date)); ?></dd>
	                        </dl>
					        <?php
					    	}
					        ?>

					        <?php
					        if($partnerUniversityAggrementFeeMaster)
					        {
					        ?>

					        <dl>
	                            <dt>Fee Structure Name :</dt>
	                            <dd><?php echo ucwords($partnerUniversityAggrementFeeMaster->name);?></dd>
	                        </dl>

	                        <dl>
	                            <dt>Fee Structure Code :</dt>
	                            <dd><?php echo ucwords($partnerUniversityAggrementFeeMaster->code);?></dd>
	                        </dl>
	                        
					        <?php
					    	}
					        ?>
	                                                 
	                    </div>        
	                    
	                    <div class='col-sm-6'>                           
	                        <dl>
	                            <dt>University Code :</dt>
	                            <dd><?php echo $partnerUniversity->code ?></dd>
	                        </dl>
	                        <dl>
	                            <dt>Name (Optional Language) :</dt>
	                            <dd><?php echo $partnerUniversity->name_in_malay; ?></dd>
	                        </dl>
	                        
	                        <?php
					        if($partnerUniversityAggrement)
					        {
					        ?>

					        <dl>
	                            <dt>Reminder Months :</dt>
	                            <dd><?php echo $partnerUniversityAggrement->reminder_months ?></dd>
	                        </dl>
	                        <dl>
	                            <dt>End Date :</dt>
	                            <dd><?php echo date('d-m-Y', strtotime($partnerUniversityAggrement->end_date)); ?></dd>
	                        </dl>

					        <?php
					    	}
					        ?>

					        <?php
					        if($partnerUniversityAggrementFeeMaster)
					        {
					        ?>

					        <dl>
	                            <dt>Fee Structure (Optional Name) :</dt>
	                            <dd><?php echo $partnerUniversityAggrementFeeMaster->name_optional_language ?></dd>
	                        </dl>
	                        
					        <?php
					    	}
					        ?>

	                    </div>
	                </div>
	            </div>
	        </div>




	        <?php
	        }

	        ?>

		<br>
            


            <form id="form_aggrement" action="" method="post" enctype="multipart/form-data">


	        <div class="form-container">
	            <h4 class="form-group-title">Partner Installment / Per Semester Details</h4>



	            <div class="row">

	            		    <div class="col-sm-4">
	                        <div class="form-group">
	                            <label>Fee Type <span class='error-text'>*</span></label>
	                            <select name="is_installment" id="is_installment" class="form-control" onchange="showInstallments(this.value)">
	                                <option value="">Select</option>
	                                <option value="0">Per Semester</option>
	                                <option value="1">Installment</option>
	                            </select>
	                        </div>
	                    </div>


	                    <div class="col-sm-4">
                          <div class="form-group">
                              <label>Currency <span class='error-text'>*</span></label>
                              <select name="id_currency" id="id_currency" class="form-control" disabled="true">
                                  <option value="">Select</option>
                                  <?php
                                  if (!empty($currencyList))
                                  {
                                      foreach ($currencyList as $record)
                                      {?>
                                          <option value="<?php echo $record->id;?>"
                                           <?php
                                           if($partnerUniversityAggrement->id_currency == $record->id)
                                           {
                                            echo 'selected';
                                           }
                                           ?> 
                                          ><?php echo $record->code . " - " . $record->name;?>
                                          </option>
                                  <?php
                                      }
                                  }
                                  ?>
                              </select>
                          </div>
                      </div>



                      <div class="col-sm-4" id="view_triger_installments">
                          <div class="form-group">
                                <label>Trigger / Total Installments <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="installments" name="installments">
                          </div>
                      </div>      
             



                      <div class="col-sm-4" id="view_id_fee_structure_trigger" style="display: none">
                            <div class="form-group">
                                <label>Trigger Fee On <span class='error-text'>*</span></label>
                                <select name="pu_id_fee_structure_trigger" id="pu_id_fee_structure_trigger" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($getFeeStructureTriggerList))
                                    {
                                        foreach ($getFeeStructureTriggerList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                            ><?php echo $record->name;?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                      </div>



	                    <div class="col-sm-4" id="view_fee_item" style="display: none">
                            <div class="form-group">
                                <label>Fee Item <span class='error-text'>*</span></label>
                                <select name="id_fee_item" id="id_fee_item" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($feeSetupList))
                                    {
                                        foreach ($feeSetupList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                            ><?php echo $record->code . " - " . $record->name;?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4" id="view_amount">
                            <div class="form-group">
                                <label>Amount <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="amount" name="amount">
                                <input type="hidden" class="form-control" id="id_programme" name="id_programme" value="<?php echo $partnerUniversityAggrementFeeMaster->id_programme; ?>">
                                <input type="hidden" class="form-control" id="id_intake" name="id_intake" value="<?php echo $partnerUniversityAggrementFeeMaster->id_intake; ?>">
                            </div>
                        </div>


	              
	            </div>


	        </div>

	        <div class="button-block clearfix">
	            <div class="bttn-group">
	                <button type="submit" class="btn btn-primary btn-lg">Save</button>
	                <a href="<?php echo '../../../addProgramAggrementInfo/' . $id_aggrement . '/' . $id_partner_university; ?>" class="btn btn-link">Cancel</a>
	            </div>
	        </div>

                  


                </form>


               <!--  <div class="row">
                    <div id="view_aggrement_data"></div>
                </div>
    -->


                <?php

                if(!empty($feeStructureListSearch))
                {
                    ?>
                    <br>

                    <div class="form-container">
                            <h4 class="form-group-title">Fee Structure List</h4>

                        

                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
						                        <th>Fee Item</th>
                                    <th>Fee Type</th>
                                  	<th>Trigger / Total Installments</th>
                                    <th>Trigger On</th>
                                  	<th>Currency</th>
                                  	<th>Amount</th>
            						            <th>Created On</th>
            						            <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                 $i=1;
                                  foreach ($feeStructureListSearch as $record)
                                 { ?>
                                    <tr>
                                    <td><?php echo $i ?></td>					                
					                          <td><?php echo $record->fee_code . " - " . $record->fee_name ?></td>
                                    <td><?php
                                      if($record->is_installment == 1)
                                      {
                                        echo 'Installment'; 
                                      }
                                      elseif($record->is_installment == 0)
                                      {
                                        echo 'Per Semester';
                                      }?>
                                        
                                    </td>
                                    <td><?php echo $record->installments ?></td>
                                    <td><?php echo $record->trigger_name ?></td>
                                    <td><?php echo $record->currency_code ?></td>
                                    <td><?php echo $record->amount ?></td>

					                          <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td>
					                          <td class="text-center">

                                      <?php
                                      if($record->is_installment == 1)
                                      {
                                        ?>
                                      
                                      <a onclick="viewFeeByTrainingCenter(<?php echo $record->id_fee_structure; ?>)" title="View">View</a>

                                      <?php
                                      }
                                      ?>
                                    </td>

                                    </tr>
                                  <?php
                                  $i++;
                              } 
                              ?>
                                </tbody>
                            </table>
                          </div>

                        </div>




                <?php
                
                }
                 ?>








        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Program Landscape</h4> -->
              </div>

              <div class="modal-body">

                <br>

                <form id="form_four" action="" method="post">



                <div class="form-container">
                    <h4 class="form-group-title"> Fee Structure Installment / Per Semester Details</h4>

                    <div class="row">



                        <input type="hidden" class="form-control" id="trigger_id_fee_structure" name="trigger_id_fee_structure" readonly>
                        <input type="hidden" class="form-control" id="trigger_id_fee_structure_master" name="trigger_id_fee_structure_master" readonly>

                        <input type="hidden" class="form-control" id="trigger_id_training_center" name="trigger_id_training_center" readonly>
                        <input type="hidden" class="form-control" id="trigger_id_program_landscape" name="trigger_id_program_landscape" readonly>






                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Trigger On
                                  <!-- Hided This Due To Change In Flow As 2 Fields Required type and Total -->
                                 <!-- <?php echo $getProgrammeLandscapeLocal->program_landscape_type; ?> -->
                                 <span class='error-text'>*</span></label>
                                <select name="trigger_id_semester" id="trigger_id_semester" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        for ($i=1; $i <= 8
                                          // $getProgrammeLandscapeLocal->total_semester
                                           ; $i++)
                                        {?>
                                    <option value="<?php echo $i;  ?>">
                                        <?php echo $i;?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                              <div class="form-group">
                                  <label>Fee Item <span class='error-text'>*</span></label>
                                  <select name="trigger_id_fee_item" id="trigger_id_fee_item" class="form-control">
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($feeSetupList))
                                      {
                                          foreach ($feeSetupList as $record)
                                          {?>
                                              <option value="<?php echo $record->id;?>"
                                              ><?php echo $record->code . " - " . $record->name;?>
                                              </option>
                                      <?php
                                          }
                                      }
                                      ?>
                                  </select>
                              </div>
                          </div>


                          <div class="col-sm-3">
                              <div class="form-group">
                                  <label>Trigger Fee On <span class='error-text'>*</span></label>
                                  <select name="id_fee_structure_trigger" id="id_fee_structure_trigger" class="form-control">
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($getFeeStructureTriggerList))
                                      {
                                          foreach ($getFeeStructureTriggerList as $record)
                                          {?>
                                              <option value="<?php echo $record->id;?>"
                                              ><?php echo $record->name;?>
                                              </option>
                                      <?php
                                          }
                                      }
                                      ?>
                                  </select>
                              </div>
                          </div>



                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Amount <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="triggering_amount" name="triggering_amount">
                            </div>
                        </div>


                      </div>


                      <div class="row">

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Total Installments / Semester <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="triggering_installment_nos" name="triggering_installment_nos" readonly>
                            </div>
                        </div>


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Currency <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="triggering_data_currency" name="triggering_data_currency" readonly>
                            </div>
                        </div>

                    </div>


                </div>

            	</form>

              <div class="modal-footer">
                  <button type="button" class="btn btn-default" onclick="saveInstallmentData()">Add</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>








                <div class="form-container">
                        
                <div class="row">
                    <div id='view_model'>
                    </div>
                </div>



                </div>

            </div>
            </div>

          </div>
        </div>
    



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



<script type="text/javascript">

    $('select').select2();


    $( function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });


    function getProgrammeByEducationLevelId(id_education_level)
    {
      // alert(id_education_level);
      if(id_education_level != '')
        {
            $.get("/setup/partnerUniversity/getProgrammeByEducationLevelId/"+id_education_level, function(data, status)
            {  
                $("#view_programme").html(data);
                $("#view_programme").show();
            });
        }
    }


    function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/setup/partnerUniversity/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
               }
            });


     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/setup/partnerUniversity/getIntakesTo',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake_to").html(result);
               }
            });


      var tempPR = {};
        tempPR['id_program'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/setup/partnerUniversity/getLearningModeByProgramId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_learning_mode").html(result);
                $("#view_learning_mode").show();
                // $("#dummy_learning_mode").hide();


                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });


            var id_program = $("#id_programme").val();

            $.get("/setup/partnerUniversity/getSchemeByProgramId/"+id_program, function(data, status){
                $("#view_program_scheme").html(data);
                $("#view_program_scheme").show();
            });
    }

    function showInstallments(is_installment)
    {
      // alert(is_installment);
      if(is_installment == 0)
      {
        $('#view_amount').show();
        $('#view_fee_item').show();
        $('#view_id_fee_structure_trigger').show();
        $('#view_triger_installments').hide();

      }else
      {

        $('#view_amount').hide();
        $('#view_fee_item').hide();
        $('#view_id_fee_structure_trigger').hide();
        $('#view_triger_installments').show();
      }
    }

    function saveAggrementData()
    {
        if($('#form_aggrement').valid())
        {

            $('#form_aggrement').submit();
        }
    }



    function viewFeeByTrainingCenter(id_fee_structure)
    {
      // alert(id_fee_structure);
      var tempPR = {};

        tempPR['id_fee_structure_master'] = '<?php echo $id_fee_structure_master; ?>';
        tempPR['id_fee_structure'] = id_fee_structure;

            $.ajax(
            {
               url: '/setup/partnerUniversity/getTrainingCenterFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // if(result == 0)
                // {
                //   alert('Duplicate Training Center Data Not Allowed');
                // }
                // alert(result);

                $("#view_model").html(result);

                var installment_amount_selected = $("#installment_amount_selected").val();
                var installment_nos = $("#installment_nos").val();
                var id_fee_structure = $("#id_fee_structure").val();     
                var id_training_center = $("#id_data_training_center").val();   
                var id_program_landscape = $("#id_data_program_landscape").val();
                var data_currency = $("#data_currency").val();


                $("#triggering_amount").val(installment_amount_selected);
                $("#triggering_installment_nos").val(installment_nos);
                $("#trigger_id_fee_structure").val(id_fee_structure);
                $("#trigger_id_fee_structure_master").val(id_program_landscape);
                $("#trigger_id_training_center").val(id_training_center);
                $("#trigger_id_program_landscape").val(id_program_landscape);
                $("#triggering_data_currency").val(data_currency);

                $('#myModal').modal('show');
                // window.location.reload();
               }
            });
    }


    function saveInstallmentData()
    {

      if($('#form_four').valid())
        {
          var tempPR = {};

          tempPR['id_fee_item'] = $("#trigger_id_fee_item").val();
          tempPR['id_fee_structure_trigger'] = $("#id_fee_structure_trigger").val();
          tempPR['id_fee_structure'] = $("#trigger_id_fee_structure").val();
          tempPR['id_fee_structure_master'] = $("#trigger_id_fee_structure_master").val();
          tempPR['amount'] = $("#triggering_amount").val();
          tempPR['id_training_center'] = $("#trigger_id_training_center").val();
          tempPR['id_semester'] = $("#trigger_id_semester").val();
          tempPR['id_program_landscape'] = $("#trigger_id_program_landscape").val();

            $.ajax(
            {
               url: '/setup/partnerUniversity/saveInstallmentData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
          }
    }



    function deleteSemesterDataByIdFeeStructureTrainingCenter(id_fee_training_center)
    {
      $.ajax(
            {
               url: '/setup/partnerUniversity/deleteSemesterDataByIdFeeStructureTrainingCenter/'+id_fee_training_center,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }




    $(document).ready(function() {
        $("#form_aggrement").validate({
            rules: {
                name: {
                  required: true
                },
                code: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_education_level: {
                  required: true
                },
                id_programme: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_student: {
                    required: true
                },
                 id_course_registered_landscape: {
                    required: true
                },
                 id_learning_mode: {
                    required: true
                },
                 id_program_has_scheme: {
                    required: true
                },
                 is_installment: {
                    required: true
                },
                 id_fee_item: {
                    required: true
                },
                 amount: {
                    required: true
                },
                installments: {
                	required: true
                },
                pu_id_fee_structure_trigger: {
                  required: true
                },
                id_currency: {
                  required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Intake Required</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Semester Required</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Student Required</p>",
                },
                id_course_registered_landscape: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_learning_mode: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                },
                id_program_has_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                is_installment: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                },
                installments: {
                    required: "<p class='error-text'>Installments / No Of Semesters Required</p>",
                },
                pu_id_fee_structure_trigger: {
                    required: "<p class='error-text'>Select Trigger Fee</p>",
                },
                id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_four").validate({
            rules: {
                trigger_id_semester: {
                    required: true
                },
                trigger_id_fee_item: {
                  required: true
                },
                triggering_amount: {
                  required: true
                },
                id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                trigger_id_semester: {
                    required: "<p class='error-text'>Select Triggering Semester</p>",
                },
                trigger_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                triggering_amount: {
                    required: "<p class='error-text'>Amount Reuired</p>",
                },
                id_fee_structure_trigger: {
                    required: "<p class='error-text'>Select Reigger On</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
