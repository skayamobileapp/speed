<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Parter University Fee Structure Main</h3>
        </div>



			<?php
	        if($partnerUniversity)
	        {
	        ?>

	        <br>

	        <div class="form-container">
	            <h4 class="form-group-title">Partner University & Aggrement Details</h4>
	            <div class='data-list'>
	                <div class='row'> 
	                    <div class='col-sm-6'>
	                        <dl>
	                            <dt>University Name :</dt>
	                            <dd><?php echo ucwords($partnerUniversity->name);?></dd>
	                        </dl>
	                        <dl>
	                            <dt>Short Name :</dt>
	                            <dd><?php echo $partnerUniversity->short_name ?></dd>
	                        </dl>
	                        <?php
					        if($partnerUniversityAggrement)
					        {
					        ?>

					        <dl>
	                            <dt>Aggrement Name :</dt>
	                            <dd><?php echo ucwords($partnerUniversityAggrement->name);?></dd>
	                        </dl>
	                        <dl>
	                            <dt>Start Date :</dt>
	                            <dd><?php echo date('d-m-Y', strtotime($partnerUniversityAggrement->start_date)); ?></dd>
	                        </dl>
	                        <dl>
	                            <dt>Currency :</dt>
	                            <dd><?php echo ucwords($partnerUniversityAggrement->currency_name);?></dd>
	                        </dl>
					        <?php
					    	}
					        ?>
	                                                 
	                    </div>        
	                    
	                    <div class='col-sm-6'>                           
	                        <dl>
	                            <dt>University Code :</dt>
	                            <dd><?php echo $partnerUniversity->code ?></dd>
	                        </dl>
	                        <dl>
	                            <dt>Name (Optional Language) :</dt>
	                            <dd><?php echo $partnerUniversity->name_in_malay; ?></dd>
	                        </dl>
	                        <?php
					        if($partnerUniversityAggrement)
					        {
					        ?>

					        <dl>
	                            <dt>Reminder Months :</dt>
	                            <dd><?php echo $partnerUniversityAggrement->reminder_months ?></dd>
	                        </dl>
	                        <dl>
	                            <dt>End Date :</dt>
	                            <dd><?php echo date('d-m-Y', strtotime($partnerUniversityAggrement->end_date)); ?></dd>
	                        </dl>

					        <?php
					    	}
					        ?>

	                    </div>
	                </div>
	            </div>
	        </div>




	        <?php
	        }

	        ?>


        <!-- div class="m-auto text-center">
		    <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
		</div>
		<div class="clearfix">

		    <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
		        <li role="presentation" class="active" ><a href="#tab_one" class="nav-link border rounded text-center"
		                aria-controls="tab_one" aria-selected="true"
		                role="tab" data-toggle="tab">Partner University Details</a>
		        </li>

		        <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
		                aria-controls="tab_two" role="tab" data-toggle="tab">Aggrement Details</a>
		        </li>

		    </ul>

		    
		    <div class="tab-content offers-tab-content">

		        <div role="tabpanel" class="tab-pane active" id="tab_one">
		            <div class="col-12 mt-4">


		            	<?php
				        if($partnerUniversity)
				        {
				        ?>

				        <br>

				        <div class="form-container">
				            <h4 class="form-group-title">Partner University Details</h4>
				            <div class='data-list'>
				                <div class='row'> 
				                    <div class='col-sm-6'>
				                        <dl>
				                            <dt>University Name :</dt>
				                            <dd><?php echo ucwords($partnerUniversity->name);?></dd>
				                        </dl>
				                        <dl>
				                            <dt>Short Name :</dt>
				                            <dd><?php echo $partnerUniversity->short_name ?></dd>
				                        </dl>
				                        <dl>
				                            <dt>Address 1 :</dt>
				                            <dd><?php echo $partnerUniversity->address1; ?></dd>
				                        </dl>
				                        <dl>
				                            <dt>City :</dt>
				                            <dd><?php echo $partnerUniversity->city ?></dd>
				                        </dl>  
				                        <dl>
				                            <dt>Country :</dt>
				                            <dd><?php echo $partnerUniversity->country ?></dd>
				                        </dl>
				                        <dl>
				                            <dt>Billing To :</dt>
				                            <dd><?php echo $partnerUniversity->billing_to ?></dd>
				                        </dl>                            
				                    </div>        
				                    
				                    <div class='col-sm-6'>                           
				                        <dl>
				                            <dt>University Code :</dt>
				                            <dd><?php echo $partnerUniversity->code ?></dd>
				                        </dl>
				                        <dl>
				                            <dt>Name (Optional Language) :</dt>
				                            <dd><?php echo $partnerUniversity->name_in_malay; ?></dd>
				                        </dl>
				                        <dl>
				                            <dt>Address 2 :</dt>
				                            <dd><?php echo $partnerUniversity->address2; ?></dd>
				                        </dl>
				                        <dl>
				                            <dt>Zipcode :</dt>
				                            <dd><?php echo $partnerUniversity->zipcode; ?></dd>
				                        </dl>
				                        <dl>
				                            <dt>State :</dt>
				                            <dd><?php echo $partnerUniversity->state ?></dd>
				                        </dl>  
				                    </div>
				                </div>
				            </div>
				        </div>




				        <?php
				        }

				        ?>


		            </div>

		        </div>



		        <div role="tabpanel" class="tab-pane" id="tab_two">
		            <div class="col-12 mt-4">



		            	<?php

				        if($partnerUniversityAggrement)
				        {
				        ?>

				        <br>

					        <div class="form-container">
					            <h4 class="form-group-title">Aggrement Details</h4>
					            <div class='data-list'>
					                <div class='row'> 
					                    <div class='col-sm-6'>
					                        <dl>
					                            <dt>Aggrement Name :</dt>
					                            <dd><?php echo ucwords($partnerUniversityAggrement->name);?></dd>
					                        </dl>
					                        <dl>
					                            <dt>Start Date :</dt>
					                            <dd><?php echo date('d-m-Y', strtotime($partnerUniversityAggrement->start_date)); ?></dd>
					                        </dl>
					                        <dl>
					                            <dt>Currency :</dt>
					                            <dd><?php echo $partnerUniversityAggrement->currency_name ?></dd>
					                        </dl>                            
					                    </div>        
					                    
					                    <div class='col-sm-6'>                           
					                        <dl>
					                            <dt>Reminder Months :</dt>
					                            <dd><?php echo $partnerUniversityAggrement->reminder_months ?></dd>
					                        </dl>
					                        <dl>
					                            <dt>End Date :</dt>
					                            <dd><?php echo date('d-m-Y', strtotime($partnerUniversityAggrement->end_date)); ?></dd>
					                        </dl>
					                        <dl>
					                            <dt>MOA FILE :</dt>
					                            <dd>
					                            	<a href="<?php echo '/assets/images/' . $partnerUniversityAggrement->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $partnerUniversityAggrement->file; ?>)" title="<?php echo $partnerUniversityAggrement->file; ?>">View</a></dd>
					                        </dl> 
					                    </div>
					                </div>
					            </div>
					        </div>



				        <?php
				        }
				        ?>


		            </div>

		        </div>



		    </div>

		</div> -->



		<br>
            


            <form id="form_aggrement" action="" method="post" enctype="multipart/form-data">


                <div class="form-container">
	            <h4 class="form-group-title">Fee Structure Main</h4>

	            <div class="row">


	            		<div class="col-sm-4">
	                        <div class="form-group">
	                            <label>Code <span class='error-text'>*</span></label>
	                            <input type="text" class="form-control" id="code" name="code">
	                        </div>
	                    </div>

	                    <div class="col-sm-4">
	                        <div class="form-group">
	                            <label>Name <span class='error-text'>*</span></label>
	                            <input type="text" class="form-control" id="name" name="name">
	                        </div>
	                    </div>


	                    <div class="col-sm-4">
	                        <div class="form-group">
	                            <label>Name In Other Language </label>
	                            <input type="text" class="form-control" id="name_optional_language" name="name_optional_language">
	                        </div>
	                    </div>

	            </div>

	            <div class="row">

	                <div class="col-sm-4">
	                  <div class="form-group">
	                     <label>Education Level <span class='error-text'>*</span></label>
	                     <select name="id_education_level" id="id_education_level" class="form-control" onchange="getProgrammeByEducationLevelId(this.value)">
	                        <option value="">Select</option>
	                        <?php
	                           if (!empty($degreeTypeList))
	                           {
	                             foreach ($degreeTypeList as $record)
	                             {
	                                    ?>
	                        <option value="<?php echo $record->id;  ?>">
	                           <?php echo $record->name;  ?>
	                        </option>
	                        <?php
	                            }
	                          }
	                           ?>
	                     </select>
	                  </div>
	                </div>


	                
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <label>Programme <span class='error-text'>*</span></label>
	                        <span id="view_programme">
	                          <select class="form-control" id='id_programme' name='id_programme'>
	                            <option value=''></option>
	                          </select>
	                        </span>
	                    </div>
	                </div>
	                
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <label>From Intake <span class='error-text'>*</span></label>
	                        <span id="view_intake">
	                          <select class="form-control" id='id_intake' name='id_intake'>
	                            <option value=''></option>
	                          </select>
	                        </span>
	                    </div>
	                </div>

	            </div>

	            <div class="row">

	            	<div class="col-sm-4">
	                    <div class="form-group">
	                        <label>To Intake </label>
	                        <span id="view_intake_to">
	                          <select class="form-control" id='id_intake_to' name='id_intake_to'>
	                            <option value=''></option>
	                          </select>
	                        </span>
	                    </div>
	                </div>
	                
	               
	                <div class="col-sm-4">
	                    <div class="form-group">
	                        <label>Learning Mode <span class='error-text'>*</span></label>
	                        <span id="view_learning_mode">
	                          <select class="form-control" id='id_learning_mode' name='id_learning_mode'>
	                            <option value=''></option>
	                          </select>
	                        </span>
	                    </div>
	                </div>

	            

	                <div class="col-sm-4">
	                  <div class="form-group">
	                     <label>Program Scheme Mode <span class='error-text'>*</span></label>
	                     <span id="view_program_scheme">
	                          <select class="form-control" id='id_program_has_scheme' name='id_program_has_scheme'>
	                            <option value=''></option>
	                          </select>

	                     </span>
	                  </div>
	                </div>


	            </div>

	            <div class="row">


            		<div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>  



	            </div>


	        </div>


	        <!-- <div class="form-container">
	            <h4 class="form-group-title">Partner Installment / Per Semester Details</h4>



	            <div class="row">

	            		<div class="col-sm-4">
	                        <div class="form-group">
	                            <label>Fee Type <span class='error-text'>*</span></label>
	                            <select name="is_installment" id="is_installment" class="form-control" onchange="showInstallments(this.value)">
	                                <option value="">Select</option>
	                                <option value="0">Per Semester</option>
	                                <option value="1">Installment</option>
	                            </select>
	                        </div>
	                    </div>


	                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Currency <span class='error-text'>*</span></label>
                                <select name="id_currency" id="id_currency" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($currencyList))
                                    {
                                        foreach ($currencyList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                            ><?php echo $record->code . " - " . $record->name;?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


	                    <div class="col-sm-4" id="view_fee_item" style="display: none">
                            <div class="form-group">
                                <label>Fee Item <span class='error-text'>*</span></label>
                                <select name="id_fee_item" id="id_fee_item" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($feeSetupList))
                                    {
                                        foreach ($feeSetupList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                            ><?php echo $record->code . " - " . $record->name;?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4" id="view_amount">
                            <div class="form-group">
                                <label>Amount <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="amount" name="amount">
                            </div>
                        </div>





                        <div class="col-sm-4">
	                        <div class="form-group">
	                              <label>No. Of Semesters / Installments To Charge <span class='error-text'>*</span></label>
	                              <input type="number" class="form-control" id="installments" name="installments">
	                        </div>
	                    </div>      
             
	              
	            </div>


	        </div> -->

	        <div class="button-block clearfix">
	            <div class="bttn-group">
	                <button type="submit" class="btn btn-primary btn-lg">Save</button>
	                <a href="<?php echo '../../addAggrementInfo/' . $id_partner_university; ?>" class="btn btn-link">Cancel</a>
	            </div>
	        </div>

                  


                </form>


               <!--  <div class="row">
                    <div id="view_aggrement_data"></div>
                </div>
    -->


                <?php

                if(!empty($feeStructureMasterListSearch))
                {
                    ?>
                    <br>

                    <div class="form-container">
                            <h4 class="form-group-title">Fee Structure Main List</h4>

                        

                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
						            <th>Code</th>
						            <th>Name</th>
						            <th>Education Level</th>
						            <th>Programme Name</th>
						            <th>Programme Code</th>
						            <th>From Intake</th>
						            <th>Learning Mode</th>
						            <th>Scheme</th>
						            <!-- <th>Fee Item</th>
                                    <th>Fee Type</th>
                                  	<th>Semesters / Installments To Charge</th>
                                  	<th>Currency</th>
                                  	<th>Amount</th> -->
						            <th>Created On</th>
						            <th>Status</th>
						            <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                 $i=1;
                                  foreach ($feeStructureMasterListSearch as $record)
                                 { ?>
                                    <tr>
                                    <td><?php echo $i ?></td>
					                <td><?php echo $record->code ?></td>
					                <td><?php echo $record->name ?></td>
					                <td><?php echo $record->education_level ?></td>
					                <td><?php echo $record->program ?></td>
					                <td><?php echo $record->program_code ?></td>
					                <td><?php echo $record->intake_year . " - " . $record->intake; ?></td>
					                <td><?php echo $record->mode_of_program . " - " . $record->mode_of_study; ?></td>
					                <td><?php echo $record->scheme_code . " - " . $record->scheme_name; ?></td>
					                
					                <!-- <td><?php echo $record->fee_code . " - " . $record->fee_name ?></td>
                                      <td><?php
                                      if($record->is_installment == 1)
                                      {
                                        echo 'Installment'; 
                                      }
                                      elseif($record->is_installment == 0)
                                      {
                                        echo 'Per Semester';
                                      }?></td>
                                      <td><?php echo $record->installments ?></td>
                                      <td><?php echo $record->currency_code . " - " . $record->currency_name ?></td>
                                      <td><?php echo $record->amount ?></td> -->

					                <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td>
					                <td><?php if ($record->status == '1') {
					            			echo "Active";
					            		} else {
					            			echo "In-Active";
					            		}
					            		?></td>
					                <td class="text-center">

                                      <a href="<?php echo '../../addFeeStructureData/' . $record->id . '/' . $id_aggrement . '/' . $id_partner_university; ?>" title="Add Fee Structure">Fee</a>

                                      <!-- <?php if($record->is_installment == 1)
                                      {
                                        ?>
                                      |
                                      
                                      <a onclick="viewFeeByTrainingCenter(<?php echo $record->id; ?>)" title="View">View</a>

                                      <?php
                                      }
                                      ?> -->
                                    </td>

                                     </tr>
                                  <?php
                              } 
                              ?>
                                </tbody>
                            </table>
                          </div>

                        </div>




                <?php
                
                }
                 ?>








        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Program Landscape</h4> -->
              </div>

              <div class="modal-body">

                <br>

                <form id="form_four" action="" method="post">



                <div class="form-container">
                    <h4 class="form-group-title"> Installment Details</h4>

                    <div class="row">



                        <input type="hidden" class="form-control" id="trigger_id_fee_structure" name="trigger_id_fee_structure" readonly>
                        <input type="hidden" class="form-control" id="trigger_id_fee_structure_master" name="trigger_id_fee_structure_master" readonly>

                        <input type="hidden" class="form-control" id="trigger_id_training_center" name="trigger_id_training_center" readonly>
                        <input type="hidden" class="form-control" id="trigger_id_program_landscape" name="trigger_id_program_landscape" readonly>






                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Installment Trigger On
                                  <!-- Hided This Due To Change In Flow As 2 Fields Required type and Total -->
                                 <!-- <?php echo $getProgrammeLandscapeLocal->program_landscape_type; ?> -->
                                 <span class='error-text'>*</span></label>
                                <select name="trigger_id_semester" id="trigger_id_semester" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        for ($i=1; $i <= 8
                                          // $getProgrammeLandscapeLocal->total_semester
                                           ; $i++)
                                        {?>
                                    <option value="<?php echo $i;  ?>">
                                        <?php echo $i;?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                              <div class="form-group">
                                  <label>Fee Item <span class='error-text'>*</span></label>
                                  <select name="trigger_id_fee_item" id="trigger_id_fee_item" class="form-control">
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($feeSetupList))
                                      {
                                          foreach ($feeSetupList as $record)
                                          {?>
                                              <option value="<?php echo $record->id;?>"
                                              ><?php echo $record->code . " - " . $record->name;?>
                                              </option>
                                      <?php
                                          }
                                      }
                                      ?>
                                  </select>
                              </div>
                          </div>



                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Amount <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="triggering_amount" name="triggering_amount">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Total Installment <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="triggering_installment_nos" name="triggering_installment_nos" readonly>
                            </div>
                        </div>


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Currency <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="triggering_data_currency" name="triggering_data_currency" readonly>
                            </div>
                        </div>

                        




                    </div>


                </div>

            	</form>

              <div class="modal-footer">
                  <button type="button" class="btn btn-default" onclick="saveInstallmentData()">Add</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>








                <div class="form-container">
                        
                <div class="row">
                    <div id='view_model'>
                    </div>
                </div>



                </div>

            </div>
            </div>

          </div>
        </div>
    



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



<script type="text/javascript">

    $('select').select2();


    $( function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });


    function getProgrammeByEducationLevelId(id_education_level)
    {
      // alert(id_education_level);
      if(id_education_level != '')
        {
            $.get("/setup/partnerUniversity/getProgrammeByEducationLevelId/"+id_education_level, function(data, status)
            {  
                $("#view_programme").html(data);
                $("#view_programme").show();
            });
        }
    }


    function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/setup/partnerUniversity/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
               }
            });


     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/setup/partnerUniversity/getIntakesTo',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake_to").html(result);
               }
            });


      var tempPR = {};
        tempPR['id_program'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/setup/partnerUniversity/getLearningModeByProgramId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_learning_mode").html(result);
                $("#view_learning_mode").show();
                // $("#dummy_learning_mode").hide();


                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });


            var id_program = $("#id_programme").val();

            $.get("/setup/partnerUniversity/getSchemeByProgramId/"+id_program, function(data, status){
                $("#view_program_scheme").html(data);
                $("#view_program_scheme").show();
            });
    }

    function showInstallments(is_installment)
    {
      // alert(is_installment);
      if(is_installment == 0)
      {
        $('#view_amount').show();
        $('#view_fee_item').show();
      }else
      {

        $('#view_amount').hide();
        $('#view_fee_item').hide();
      }
    }

    function saveAggrementData()
    {
        if($('#form_aggrement').valid())
        {

            $('#form_aggrement').submit();
        }
    }



    function viewFeeByTrainingCenter(id_fee_structure_master)
    {
      // alert(id_fee_structure_master);
      var tempPR = {};

        tempPR['id_fee_structure_master'] = id_fee_structure_master;
            $.ajax(
            {
               url: '/setup/partnerUniversity/getTrainingCenterFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // if(result == 0)
                // {
                //   alert('Duplicate Training Center Data Not Allowed');
                // }
                // alert(result);

                $("#view_model").html(result);

                var installment_amount_selected = $("#installment_amount_selected").val();
                var installment_nos = $("#installment_nos").val();
                var id_fee_structure = $("#id_fee_structure").val();     
                var id_training_center = $("#id_data_training_center").val();   
                var id_program_landscape = $("#id_data_program_landscape").val();
                var data_currency = $("#data_currency").val();





                $("#triggering_amount").val(installment_amount_selected);
                $("#triggering_installment_nos").val(installment_nos);
                $("#trigger_id_fee_structure").val(id_fee_structure);
                $("#trigger_id_fee_structure_master").val(id_fee_structure_master);
                $("#trigger_id_training_center").val(id_training_center);
                $("#trigger_id_program_landscape").val(id_program_landscape);
                $("#triggering_data_currency").val(data_currency);

                $('#myModal').modal('show');
                // window.location.reload();
               }
            });
    }


    function saveInstallmentData()
    {

      if($('#form_four').valid())
        {
          var tempPR = {};

          tempPR['id_fee_item'] = $("#trigger_id_fee_item").val();
          tempPR['id_fee_structure_master'] = $("#trigger_id_fee_structure_master").val();
          tempPR['amount'] = $("#triggering_amount").val();
          tempPR['id_training_center'] = $("#trigger_id_training_center").val();
          tempPR['id_semester'] = $("#trigger_id_semester").val();
          tempPR['id_program_landscape'] = $("#trigger_id_program_landscape").val();

            $.ajax(
            {
               url: '/setup/partnerUniversity/saveInstallmentData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
          }
    }



    function deleteSemesterDataByIdFeeStructureTrainingCenter(id_fee_training_center)
    {
      $.ajax(
            {
               url: '/setup/partnerUniversity/deleteSemesterDataByIdFeeStructureTrainingCenter/'+id_fee_training_center,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }




    $(document).ready(function() {
        $("#form_aggrement").validate({
            rules: {
                name: {
                  required: true
                },
                code: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_education_level: {
                  required: true
                },
                id_programme: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_student: {
                    required: true
                },
                 id_course_registered_landscape: {
                    required: true
                },
                 id_learning_mode: {
                    required: true
                },
                 id_program_has_scheme: {
                    required: true
                },
                 is_installment: {
                    required: true
                },
                 id_fee_item: {
                    required: true
                },
                 amount: {
                    required: true
                },
                installments: {
                	required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Intake Required</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Semester Required</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Student Required</p>",
                },
                id_course_registered_landscape: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_learning_mode: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                },
                id_program_has_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                is_installment: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                },
                installments: {
                    required: "<p class='error-text'>Installments / No Of Semesters Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_four").validate({
            rules: {
                trigger_id_semester: {
                    required: true
                },
                trigger_id_fee_item: {
                  required: true
                }
            },
            messages: {
                trigger_id_semester: {
                    required: "<p class='error-text'>Select Triggering Semester</p>",
                },
                trigger_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
