<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Parter University Aggrement Info</h3>
        </div>


        <br>


        <div class="topnav">
          <a href="<?php echo '../edit/' . $id; ?>" title="Partner University Info">Parter University Info</a> | 
          <a href="<?php echo '../addTrainingCenterInfo/' . $id; ?>" title="Trainnig Center Info">Traing Centers Info</a> | 
          <a href="<?php echo '../addAggrementInfo/' . $id; ?>" title="Aggrement Info" style="background: #aaff00">Aggrement Info</a>
        </div>

        <br>



            


            <form id="form_aggrement" action="" method="post" enctype="multipart/form-data">


                <br>

                <div class="form-container">
                    <h4 class="form-group-title">MOA Aggrement Details</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Name <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="moa_name" name="moa_name" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>MOA Start Date <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="moa_start_date" name="moa_start_date" autocomplete="off">
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>MOA End Date <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="moa_end_date" name="moa_end_date" autocomplete="off">
                            </div>
                        </div>

                        

                    </div>

                    <div class="row">

                        
                        
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Currency <span class='error-text'>*</span></label>
                                <select name="moa_id_currency" id="moa_id_currency" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($currencyList))
                                    {
                                        foreach ($currencyList as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>



                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Reminer (Months) <span class='error-text'>*</span></label>
                                <select name="moa_reminder_months" id="moa_reminder_months" class="form-control">
                                    <option value="">Select</option>
                                    <?php

                                    for ($i=1; $i <= 12; $i++)
                                    {
                                        ?>
                                    <option value="<?php echo $i;  ?>">
                                        <?php echo $i;?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>MOA File <span class='error-text'>*</span></label>
                                <input type="file" class="form-control" id="moa_file" name="moa_file">
                                <input type="hidden" class="form-control" id="btn_submit" name="btn_submit" value="4">
                            </div>
                        </div>


                    </div>

                </div>


                <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="button" class="btn btn-primary btn-lg" name="btn_submit" value="4" onclick="saveAggrementData()">Add</button>
                        <a href="../list" class="btn btn-link">Back</a>
                    </div>
                </div>

                  


                </form>


               <!--  <div class="row">
                    <div id="view_aggrement_data"></div>
                </div>
    -->


                <?php

                if(!empty($getPartnerUniversityAggrementList))
                {
                    ?>
                    <br>

                    <div class="form-container">
                            <h4 class="form-group-title">MOA Aggrement List</h4>

                        

                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
                                     <th>Name</th>
                                     <th>MOA Start Date</th>
                                     <th>MOA End Date</th>
                                     <th>Reminder (Months)</th>
                                     <th>Currency</th>
                                     <th class="text-center">MOA File</th>
                                     <!-- <th class="text-center">Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                  for($i=0;$i<count($getPartnerUniversityAggrementList);$i++)
                                 { ?>
                                    <tr>
                                    <td><?php echo $i+1;?></td>
                                    <td><?php echo $getPartnerUniversityAggrementList[$i]->name;?></td>               
                                    <td><?php echo date('d-m-Y', strtotime($getPartnerUniversityAggrementList[$i]->start_date));?></td>
                                    <td><?php echo date('d-m-Y', strtotime($getPartnerUniversityAggrementList[$i]->end_date));?></td>
                                    <td><?php echo $getPartnerUniversityAggrementList[$i]->reminder_months;?></td>               
                                    <td><?php echo $getPartnerUniversityAggrementList[$i]->currency_code . " - " . $getPartnerUniversityAggrementList[$i]->currency_name;?></td>               
                                    <td class="text-center">

                                        <a href="<?php echo '/assets/images/' . $getPartnerUniversityAggrementList[$i]->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $getPartnerUniversityAggrementList[$i]->file; ?>)" title="<?php echo $getPartnerUniversityAggrementList[$i]->file; ?>">View</a>
                                    </td>

                                    <!-- <td class="text-center">
                                        <a href="<?php echo '../addProgramAggrementInfo/' . $getPartnerUniversityAggrementList[$i]->id . '/' . $id; ?>" title="Add Program Info" >Program</a>
                                    </td> -->
 
                                    <!-- <td class="text-center">
                                        <a onclick="deleteMoaAggrement(<?php echo $getPartnerUniversityAggrementList[$i]->id; ?>)">Delete</a>
                                    </td> -->

                                     </tr>
                                  <?php
                              } 
                              ?>
                                </tbody>
                            </table>
                          </div>

                        </div>




                <?php
                
                }
                 ?>







    



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



<script type="text/javascript">

    $('select').select2();


    $( function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });

    function saveAggrementData()
    {
        if($('#form_aggrement').valid())
        {

            $('#form_aggrement').submit();
        }
    }



    function deleteMoaAggrement(id)
    {
         $.ajax(
            {
               url: '/setup/partnerUniversity/deleteMoaAggrement/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    // window.location.reload();
                    location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_aggrement").validate({
            rules: {
                moa_start_date: {
                    required: true
                },
                moa_end_date: {
                    required: true
                },
                moa_file: {
                    required: true
                },
                moa_name: {
                    required: true
                },
                moa_id_currency: {
                    required: true
                },
                moa_reminder_months: {
                    required: true
                }
            },
            messages: {
                moa_start_date: {
                    required: "<p class='error-text'>Select MOA Start Date</p>",
                },
                moa_end_date: {
                    required: "<p class='error-text'>Select MOA End Date</p>",
                },
                moa_file: {
                    required: "<p class='error-text'>Select MOA File</p>",
                },
                moa_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                moa_id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                moa_reminder_months: {
                    required: "<p class='error-text'>Select Reminer Months</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    
</script>
