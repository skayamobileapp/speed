<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <!-- <div class="page-title clearfix">
            <h3>Welcome : Module SETUP</h3>
        </div> -->
        
        <!-- <div class="row">
            <div class="col-sm-3">
                <div class="stats-col">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_approval_icon.svg" />Application Pending Approvals
                    </div>
                    <div class="count">
                        03
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="stats-col pending-acceptance">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_acceptance_icon.svg" />Application Pending Acceptance
                    </div>
                    <div class="count">
                        00
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>            
            <div class="col-sm-3">
                <div class="stats-col active-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/active_students_icon.svg" />Active Students
                    </div>
                    <div class="count">
                        295
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="stats-col inactive-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/inactive_students_icon.svg" />Inactive Students
                    </div>
                    <div class="count">
                        734
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>                                    
        </div> -->

        <hr/>
        <div class="row">            
            <div class="col-sm-3 col-lg-2">
                <a href="/setup/welcome" class="dashboard-menu"><span class="icon"></span>System Setup</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/cm/welcome" class="dashboard-menu curriculm-management"><span class="icon"></span>Course Management</a>
            </div>   
            <div class="col-sm-3 col-lg-2">
                <a href="/finance/welcome" class="dashboard-menu partners-management"><span class="icon"></span>Finance</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/records/welcome" class="dashboard-menu records"><span class="icon"></span>Records</a>
            </div>
        </div>

        <footer class="footer-wrapper">
            <p>&copy; 2020 All rights, reserved</p>
        </footer>

    </div>
</div>