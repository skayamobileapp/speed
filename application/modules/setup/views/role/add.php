<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Role</h3>
            </div>


    <form id="form_main" action="" method="post">

        <div class="form-container">

            <h4 class="form-group-title">Role Details</h4>

        
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Role<span class="text-danger">*</span></label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="role" name="role">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group row align-items-center">
                      <label class="col-sm-4 col-form-label">Status<span class="text-danger">*</span></label>
                      <div class="col-sm-8">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                          <label class="custom-control-label" for="customRadioInline1">Active</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                          <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>



                <div class="row">

                  <div class="col-lg-12">
                    <div class="form-group row">
                      <label class="col-lg-2 col-form-label">Permissions<span class="text-danger">*</span></label>
                      <div class="col-lg-8">
                        <select name="permissions[]" id="permissions" multiple="multiple" class="form-control">
                          <?php
                          if (!empty($permissions)) {
                            foreach ($permissions as $record) {
                          ?>
                              <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->description;  ?>
                              </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <a href="list" class="btn btn-link">Cancel</a>
                  </div>

                </div> 

            </div> 

    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

   $(document).ready(function() {
        $("#form_main").validate({
            rules: {
              role: {
                    required: true
                },
                permissions : {
                  required: true
                }
            },
            messages: {
              role: {
                    required: "<p class='error-text'>Role Required</p>",
                },
                permissions: {
                    required: "<p class='error-text'>Permissions Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>