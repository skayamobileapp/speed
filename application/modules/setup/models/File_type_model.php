<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class File_type_model extends CI_Model
{
    function fileTypeList()
    {
        $this->db->select('*');
        $this->db->from('file_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function fileTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('file_type');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFileType($id)
    {
        $this->db->select('*');
        $this->db->from('file_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewFileType($data)
    {
        $this->db->trans_start();
        $this->db->insert('file_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editFileType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('file_type', $data);
        return TRUE;
    }
}

