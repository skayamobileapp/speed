<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Examset_model extends CI_Model
{

    function examsetListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('examset as c');
        if (!empty($data['name'])) {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%'  or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getExamset($id)
    {
        $this->db->select('c.*');
        $this->db->from('examset as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewExamset($data)
    {
        $this->db->trans_start();
        $this->db->insert('examset', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editExamset($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('examset', $data);

        return $result;
    }

    function deleteExamset($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('examset', $data);
        return $this->db->affected_rows();
    }
}
