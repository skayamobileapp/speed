<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Staff_model extends CI_Model
{
    function staffList()
    {
        $this->db->select('s.*, d.name as department');
        $this->db->from('staff as s');
        $this->db->join('department as d','s.id_department = d.id');
        $this->db->order_by("s.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function staffListSearch($data)
    {
        $this->db->select('s.*, d.code as department_code, d.name as department');
        $this->db->from('staff as s');
        $this->db->join('department as d','s.id_department = d.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(s.name  LIKE '%" . $data['name'] . "%' or s.ic_no  LIKE '%" . $data['name'] . "%' or s.mobile_number  LIKE '%" . $data['name'] . "%' or s.staff_id  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['id_department']))
        {
            $this->db->where('s.id_department', $data['id_department']);
        }
        $this->db->order_by("s.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function getStaff($id)
    {
        $this->db->select('s.*, d.name as department');
        $this->db->from('staff as s');
        $this->db->join('department as d','s.id_department = d.id');
        $this->db->where('s.id', $id);
        $this->db->order_by("s.name", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getDepartmentByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFacultyProgramListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('faculty_program');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getCountryByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function checkStaffDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('mobile_number', $data['mobile_number']);
        $this->db->or_where('ic_no', $data['ic_no']);
        $this->db->or_where('staff_id', $data['staff_id']);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewStaff($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStaff($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('staff', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($countryId, $countryInfo)
    {
        $this->db->where('id', $countryId);
        $this->db->update('staff', $countryInfo);
        return $this->db->affected_rows();
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_staff_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_staff_has_course', $data);
        return TRUE;
    }

    function getTempStaff($id_session)
    {
        $this->db->select('a.*, c.name');
        $this->db->from('temp_staff_has_course as a');
        $this->db->join('course as c', 'a.id_course = c.id');        
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffCourse($id)
    {
        $this->db->select('a.*, c.name as coursename');
        $this->db->from('staff_has_course as a');
        $this->db->join('course as c', 'a.id_course = c.id');        
        $this->db->where('a.id_staff', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('temp_staff_has_course');
    }

    function deleteCourseData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('staff_has_course');
    }

    function deleteTempDataBySession($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_staff_has_course');
    }

    function addNewStaffCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSalutation($id)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('course as a');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}