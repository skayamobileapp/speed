<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Tos extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tos_model');
        $this->load->model('question_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('tos.list') == 1) {
            $this->loadAccessRestricted();
        } else {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['tosList'] = $this->tos_model->tosListSearch($formData);
            $this->global['pageTitle'] = 'Speed Management System : TOS List';
            $this->global['pageCode'] = 'tos.list';
            $this->loadViews("tos/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('tos.add') == 1) {
            $this->loadAccessRestricted();
        } else {

            $user_id = $this->session->userId;

            if ($this->input->post()) {

                $name = $this->security->xss_clean($this->input->post('name'));
                $question_count = $this->security->xss_clean($this->input->post('question_count'));
                $id_pool = implode(',', $this->security->xss_clean($this->input->post('id_pool')));
                $status = $this->security->xss_clean($this->input->post('status'));
                $main_data = array(
                    'name' => $name,
                    'question_count' => $question_count,
                    'id_pool' => $id_pool,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $questionList = json_decode($this->security->xss_clean($this->input->post('questionList')));
                $selectedQuestions = $this->security->xss_clean($this->input->post('selectedQuestions'));
                $detail_data = array();
                for ($i = 0; $i < count($questionList); $i++) {
                    $row = array();
                    $row['id_course'] = $questionList[$i]->id_course;
                    $row['id_pool'] = $questionList[$i]->id_pool;
                    $row['id_topic'] = $questionList[$i]->id_topic;
                    $row['id_bloom_taxonomy'] = $questionList[$i]->id_bloom_taxonomy;
                    $row['id_difficult_level'] = $questionList[$i]->id_difficult_level;
                    $row['questions_available'] = $questionList[$i]->QuestionCount;
                    $row['questions_selected'] = $selectedQuestions[$i];
                    $row['status'] = $status;
                    if ((int)$selectedQuestions[$i] != 0) {
                        array_push($detail_data, $row);
                    }
                }

                $result = $this->tos_model->addNewTos($main_data, $detail_data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Tos added successfully');
                } else {
                    $this->session->set_flashdata('error', 'Tos add failed');
                }
                redirect('/setup/tos/list');
            }

            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $this->global['pageCode'] = 'tos.add';
            $this->global['pageTitle'] = 'Speed Management System : Add TOS';
            $this->loadViews("tos/add", $this->global, $data, NULL);
        }
    }
    function edit($id = NULL)
    {
        if ($this->checkAccess('tos.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/tos/list');
            }

            $user_id = $this->session->userId;

            if ($this->input->post()) {
                $question_count = $this->security->xss_clean($this->input->post('question_count'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $name = $this->security->xss_clean($this->input->post('name'));

                $main_data = array(
                    'id' => $id,
                    'question_count' => $question_count,
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                $tosdetails = $this->tos_model->getTosDetails($id);
                $selectedQuestions = $this->security->xss_clean($this->input->post('selectedQuestions'));
                $detail_data = array();
                for ($i = 0; $i < count($tosdetails); $i++) {
                    $row = array();
                    $row['id_course'] = $tosdetails[$i]->id_course;
                    $row['id_pool'] = $tosdetails[$i]->id_pool;
                    $row['id_topic'] = $tosdetails[$i]->id_topic;
                    $row['id_bloom_taxonomy'] = $tosdetails[$i]->id_bloom_taxonomy;
                    $row['id_difficult_level'] = $tosdetails[$i]->id_difficult_level;
                    $row['questions_available'] = $tosdetails[$i]->questions_available;
                    $row['id_details'] = $tosdetails[$i]->id_details;
                    $row['questions_selected'] = $selectedQuestions[$i];
                    $row['status'] = $status;
                    if ((int)$selectedQuestions[$i] == 0) {
                        $row['status'] = 0;
                    }
                    array_push($detail_data, $row);
                }

                $result = $this->tos_model->editTos($main_data, $detail_data);

                if ($result) {
                    $this->session->set_flashdata('success', 'TOS edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'TOS edit failed');
                }
                redirect('/setup/tos/list');
            }

            $data['tos'] = $this->tos_model->getTos($id);
            $data['tosdetails'] = $this->tos_model->getTosDetails($id);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $this->global['pageCode'] = 'tos.edit';
            $this->global['pageTitle'] = 'Speed Management System : Edit TOS';
            $this->loadViews("tos/edit", $this->global, $data, NULL);
        }
    }
}
