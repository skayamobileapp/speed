<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Programme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['staffList'] = $this->programme_model->staffListByStatus('1');
            // $data['programmeList'] = $this->programme_model->programmeList();

            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['programmeList'] = $this->programme_model->programmeListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : Program List';
            $this->loadViews("programme/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('programme.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {                

                 // For file validation from 36 to 44 , file size validation

                // echo "<Pre>";print_r($this->input->post());exit;
                




                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $total_cr_hrs = $this->security->xss_clean($this->input->post('total_cr_hrs'));
                $graduate_studies = $this->security->xss_clean($this->input->post('graduate_studies'));
                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_scheme = $this->security->xss_clean($this->input->post('id_scheme'));
                $internal_external = $this->security->xss_clean($this->input->post('internal_external'));
                $foundation = $this->security->xss_clean($this->input->post('foundation'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $is_apel = $this->security->xss_clean($this->input->post('is_apel'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $mode = $this->security->xss_clean($this->input->post('mode'));


            
                $data = array(
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
                    'total_cr_hrs' => $total_cr_hrs,
                    'graduate_studies' => $graduate_studies,
                    'foundation' => $foundation,
                    'id_award' => $id_award,
                    'id_education_level' => $id_education_level,
                    'id_scheme' => $id_scheme,
                    'is_apel' => $is_apel,
                    'mode' => $mode,
                    'internal_external' => $internal_external,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $inserted_id = $this->programme_model->addNewProgrammeDetails($data);

                $temp_details = $this->programme_model->getTempProgrammeHasDean($id_session);
                 for($i=0;$i<count($temp_details);$i++)
                 {
                    // echo "<Pre>";print_r($temp_details[$i]);exit;

                    $id_staff = $temp_details[$i]->id_staff;
                    $id_programme = $temp_details[$i]->id_programme;
                    $effective_start_date = $temp_details[$i]->effective_start_date;

                     $detailsData = array(
                        'id_programme' => $inserted_id,
                        'id_staff' => $id_staff,
                        // 'id_programme' => $id_programme,
                        'effective_start_date' => date('Y-m-d',strtotime($effective_start_date)),
                    );
                    // echo "<Pre>";print_r($detailsData);exit;
                    $result = $this->programme_model->addNewProgrammeHasDean($detailsData);
                 }

                //  $temp_details = $this->programme_model->getTempProgrammeHasCourse($id_session);
                //  for($i=0;$i<count($temp_details);$i++)
                //  {
                //     // echo "<Pre>";print_r($temp_details[$i]);exit;

                //     $id_course = $temp_details[$i]->id_course;
                //     $type = $temp_details[$i]->type;

                //      $detailsData = array(
                //         'id_programme' => $inserted_id,
                //         'id_course' => $id_course,
                //         // 'id_programme' => $id_programme,
                //         'type' => $type,
                //     );
                //     // echo "<Pre>";print_r($detailsData);exit;
                //     $result = $this->programme_model->addNewProgrammeHasCourse($detailsData);
                //  }

                //  $temp_scheme_details = $this->programme_model->getTempProgrammeHasScheme($id_session);
                //  for($i=0;$i<count($temp_scheme_details);$i++)
                //  {
                //     // echo "<Pre>";print_r($temp_details[$i]);exit;

                //     $id_scheme = $temp_scheme_details[$i]->id_scheme;

                //      $detailsData = array(
                //         'id_program' => $inserted_id,
                //         'id_scheme' => $id_scheme
                //     );
                //     // echo "<Pre>";print_r($detailsData);exit;
                //     $result = $this->programme_model->addNewProgrammeHasScheme($detailsData);
                //  }

                // $this->programme_model->deleteTempData($id_session);
                // $this->programme_model->deleteTempCourseData($id_session);
                // $this->programme_model->deleteTempProgramHasSchemeBySession($id_session);

                redirect('/setup/programme/edit/'. $inserted_id);
            }
            else
            {

                $this->programme_model->deleteTempProgHasDeanDataBySession($id_session);
                // $this->programme_model->deleteTempData($id_session);
                // $this->programme_model->deleteTempCourseData($id_session);
                // $this->programme_model->deleteTempProgramHasSchemeBySession($id_session);
            }

            $data['staffList'] = $this->programme_model->staffListByStatus('1');
            $data['courseList'] = $this->programme_model->courseListByStatus('1');
            $data['awardList'] = $this->programme_model->awardListByStatus('1');
            $data['schemeList'] = $this->programme_model->schemeListByStatus('1');
            $data['educationLevelList'] = $this->programme_model->educationLevelListByStatus('1');
            

            // echo "<Pre>";print_r($data['partnerCategoryList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Add Programme';
            $this->loadViews("programme/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('programme.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/programme/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $total_cr_hrs = $this->security->xss_clean($this->input->post('total_cr_hrs'));
                $graduate_studies = $this->security->xss_clean($this->input->post('graduate_studies'));
                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_scheme = $this->security->xss_clean($this->input->post('id_scheme'));
                $is_apel = $this->security->xss_clean($this->input->post('is_apel'));
                $internal_external = $this->security->xss_clean($this->input->post('internal_external'));
                $foundation = $this->security->xss_clean($this->input->post('foundation'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $mode = $this->security->xss_clean($this->input->post('mode'));

                // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
            
                $data = array(
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
                    'total_cr_hrs' => $total_cr_hrs,
                    'graduate_studies' => $graduate_studies,
                    'foundation' => $foundation,
                    'id_award' => $id_award,
                    'id_education_level' => $id_education_level,
                    'id_scheme' => $id_scheme,
                    'is_apel' => $is_apel,
                    'mode' => $mode,
                    'internal_external' => $internal_external,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'id_partner_university'=>$id_partner_university
                );

                // echo "<Pre>";print_r($data);exit;
                $result = $this->programme_model->editProgrammeDetails($data,$id);
                // $inserted_id = $id;
                // $temp_details = $this->programme_model->getTempProgrammeHasDean($id_session);
                //  for($i=0;$i<count($temp_details);$i++)
                //  {
                //     // echo "<Pre>";print_r($temp_details[$i]);exit;

                //     $id_staff = $temp_details[$i]->id_staff;
                //     $id_programme = $temp_details[$i]->id_programme;
                //     $effective_start_date = $temp_details[$i]->effective_start_date;

                //      $detailsData = array(
                //         'id_programme' => $inserted_id,
                //         'id_staff' => $id_staff,
                //         // 'id_programme' => $id_programme,
                //         'effective_start_date' => $effective_start_date,
                //     );
                //     // echo "<Pre>";print_r($detailsData);exit;
                //     $result = $this->programme_model->addNewProgrammeHasDean($detailsData);
                //  }

                //  $temp_details = $this->programme_model->getTempProgrammeHasCourse($id_session);
                //  for($i=0;$i<count($temp_details);$i++)
                //  {
                //     // echo "<Pre>";print_r($temp_details[$i]);exit;

                //     $id_course = $temp_details[$i]->id_course;
                //     $type = $temp_details[$i]->type;

                //      $detailsData = array(
                //         'id_programme' => $inserted_id,
                //         'id_course' => $id_course,
                //         // 'id_programme' => $id_programme,
                //         'type' => $type,
                //     );
                //     // echo "<Pre>";print_r($detailsData);exit;
                //     $result = $this->programme_model->addNewProgrammeHasCourse($detailsData);
                //  }
                //  $this->programme_model->deleteTempData($id_session);
                // $this->programme_model->deleteTempCourseData($id_session);
                redirect('/setup/programme/list');
            }
            $data['id_programme'] = $id;
            $data['staffList'] = $this->programme_model->staffListByStatus('1');
            $data['programmeHasDeanList'] = $this->programme_model->getProgrammeHasDean($id);
            $data['programmeHasCourseList'] = $this->programme_model->getProgrammeHasCourse($id);
            $data['awardList'] = $this->programme_model->awardListByStatus();
            $data['schemeList'] = $this->programme_model->schemeListByStatus('1');
            $data['programTypeList'] = $this->programme_model->programTypeListByStatus('1');
            // $data['schemeList'] = $this->programme_model->schemeListByStatus('1');
            $data['educationLevelList'] = $this->programme_model->educationLevelListByStatus('1');





            $data['partnerList'] = $this->programme_model->partnerUniversityListSearch();
            

            $data['programmeHasDeanList'] = $this->programme_model->getProgrammeHasDean($id);
            $data['programmeLearningModeList'] = $this->programme_model->programmeLearningModeList($id);
            $data['programmeSchemeList'] = $this->programme_model->programmeSchemeList($id);
            $data['programmeMajoringList'] = $this->programme_model->programmeMajoringList($id);
            $data['programmeMinoringList'] = $this->programme_model->programmeMinoringList($id);
            $data['programmeConcurrentList'] = $this->programme_model->programmeConcurrentList($id);
            $data['programmeAcceredationList'] = $this->programme_model->programmeAcceredationList($id);
            $data['programmeObjectiveList'] = $this->programme_model->programmeObjectiveList($id);


            $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
            
            // echo "<Pre>";print_r($data['programmeDetails']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Programme';
            $this->loadViews("programme/edit", $this->global, $data, NULL);
        }
    }

     function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;
        $tempData['id_session'] = $id_session;
        $tempData['effective_start_date'] = date('Y-m-d',strtotime($tempData['effective_start_date']));
        
            $inserted_id = $this->programme_model->addTempDetails($tempData);
        // }
        $data = $this->displaytempdata();
        
        echo $data;
    }

    function tempaddcourse()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;
        // echo "<Pre>";print_r($tempData);exit;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->programme_model->updateTempCourseDetails($tempData,$id);
        }
        else
        {

            unset($tempData['id']);
            $inserted_id = $this->programme_model->addTempCourseDetails($tempData);
        }
        $data = $this->displaytempCoursedata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_model->getTempProgrammeHasDean($id_session); 

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Staff Name</th>
                    <th>Effective Date</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $staff = $temp_details[$i]->salutation . " - " . $temp_details[$i]->staff;
                    $effective_start_date = $temp_details[$i]->effective_start_date;
                    $effective_start_date = date('d-m-Y',strtotime($effective_start_date));
                    $j = $i+1;
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>
                            <td>$effective_start_date</td>                            
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function displaytempCoursedata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_model->getTempProgrammeHasCourse($id_session); 
        // echo "<Pre>";print_r($details);exit;
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Course Name</th>
                    <th class='text-centre'>Action</th>
                </tr>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $name = $temp_details[$i]->courseName;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$name</td>            
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempCourseData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</table>";
        return $table;
    }

    function tempDetailsDataAdd()
    {
        
        $id_session = $this->session->my_session_id;

        $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        $effective_start_date = $this->security->xss_clean($this->input->post('effective_start_date'));
             
        // echo "<Pre>";  print_r($effective_start_date);exit;
        $data = array(
            'id_session' => $id_session,
            'id_staff' => $id_staff,
            'effective_start_date' => $effective_start_date
        );   
        $inserted_id = $this->programme_model->addNewTempProgrammeHasDean($data);

        $temp_details = array(
                'id' => $inserted_id,
                'id_staff' => $id_staff,
                'effective_start_date' => date('Y-m-d',strtotime($effective_start_date))
            );

        $temp_details = $this->programme_model->getTempProgrammeHasDean($id_session);
        // echo "<Pre>";  print_r($temp_details);exit;

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Sl. No</th>
                    <th>Staff Name</th>
                    <th>Effective Date</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $id = $temp_details[$i]->id;
                    $staff = $temp_details[$i]->salutation . " - " . $temp_details[$i]->staff;
                    $effective_start_date = $temp_details[$i]->effective_start_date;

                    $table .= "
                <tr>
                    <td>$i+1</td>
                    <td>$staff</td>
                    <td>$effective_start_date</td>
                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }

    function add_new_dean_on_edit()
    {
        // echo "<Pre>";print_r("jk");exit;
        $id_programme= $_POST['id_programme'];
        $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        $effective_start_date = $this->security->xss_clean($this->input->post('effective_start_date'));

         $detailsData = array(
            'id_programme' => $id_programme,
            'id_staff' => $id_staff,
            // 'id_programme' => $id_programme,
            'effective_start_date' => date('Y-m-d',strtotime($effective_start_date)),
        );
        // echo "<Pre>";print_r($detailsData);exit;
        $result = $this->programme_model->addNewProgrammeHasDean($detailsData);
        // $data['programmeHasDeanList'] = $this->programme_has_dean_model->getProgrammeHasDeanByProgrammeId($id);
        redirect($_SERVER['HTTP_REFERER']);

    }

    function delete_programme_has_dean()
    {
        $id = $this->input->get('id');
        // echo "<Pre>";print_r($id);exit;

       $this->programme_model->deleteProgrammeHasDean($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_programme_has_course()
    {
        $id = $this->input->get('id');
        // echo "<Pre>";print_r($id);exit;

       $this->programme_model->deleteProgrammeHasCourse($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function tempedit($id){
        $data = $this->programme_model->getTempDetails($id);
        echo json_encode($data);

    }

    function tempDelete($id)
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->programme_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    }

    function tempCourseDelete($id)
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->programme_model->deleteTempCourseData($id);
        $data = $this->displaytempCoursedata();
        echo $data; 
    }

    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $data['id_staff'] =  $tempData['id_staff'];
        $data['effective_start_date'] =  date('Y-m-d',strtotime($tempData['effective_start_date']));
        $data['id_programme'] =  $tempData['id'];
        $inserted_id = $this->programme_model->addNewProgrammeHasDean($data);

        if($inserted_id)
        {
            echo "success";exit;
        }
        
        //  $temp_details = $this->programme_model->getProgrammeHasDean($tempData['id']);
        // if(!empty($temp_details))
        // {

        // // echo "<Pre>";print_r($temp_details);exit;
        // $table = "<table  class='table' id='list-table'>
        //           <tr>
        //             <th>Sl. No</th>
        //             <th>Staff Name</th>
        //             <th>Effective Start Date</th>
        //             <th>Action</th>
        //         </tr>";
        //             for($i=0;$i<count($temp_details);$i++)
        //             {
        //             $id = $temp_details[$i]->id;
        //             $staff_name = $temp_details[$i]->salutation . " - " .$temp_details[$i]->staff;
        //             $effective_start_date = $temp_details[$i]->effective_start_date;
        //             $effective_start_date = date('d-m-Y',strtotime($effective_start_date));
        //             $j = $i+1;
        //                 $table .= "
        //                 <tr>
        //                     <td>$j</td>
        //                     <td>$staff_name</td>                         
        //                     <td>$effective_start_date</td>                         
        //                     <td>
        //                         <span onclick='deleteStaffDetailData($id)'>Delete</a>
        //                     <td>
        //                 </tr>";
        //             }
        // $table.= "</table>";

        // }
        // else
        // {
        //     $table="";
        // }

        // echo $table;           
    }

    function deleteStaffDetailData($id_details)
    {
        $inserted_id = $this->programme_model->deleteProgrammeHasDean($id_details);
        
        echo "Success"; 
    }

    function tempSchemeAdd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;
        $tempData['id_session'] = $id_session;
        
        $inserted_id = $this->programme_model->addTempProgramHasScheme($tempData);
        
        $data = $this->displayTempSchemeData();
        
        echo $data;        
    }


    function displayTempSchemeData()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_model->getTempProgrammeHasScheme($id_session); 

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Scheme</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $scheme = $temp_details[$i]->scheme_code . " - " . $temp_details[$i]->scheme_name;
                    $j = $i+1;
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$scheme</td>                           
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempProgramHasScheme($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempProgramHasScheme($id)
    {
        $inserted_id = $this->programme_model->deleteTempProgramHasScheme($id);
        $data = $this->displayTempSchemeData();
        echo $data; 
    }




    function directSchemeAdd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['min_duration'] = round($tempData['min_duration']);
        $tempData['max_duration'] = round($tempData['max_duration']);
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->addNewProgrammeHasScheme($tempData);

        if($inserted_id)
        {
            echo "success";
        }     
    }

    function saveProgramSchemeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->addNewProgramSchemeDetails($tempData);

        if($inserted_id)
        {
            echo "success";
        }     
    }

    function saveMajorData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->addNewProgrammeHasMajor($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function saveMinorData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->addNewProgrammeHasMinor($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function saveConcurrentData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->addNewProgrammeHasConcurrent($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function saveAcceredationData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['acceredation_dt'] = date('Y-m-d', strtotime($tempData['acceredation_dt']));
        $tempData['valid_from'] = date('Y-m-d', strtotime($tempData['valid_from']));
        $tempData['valid_to'] = date('Y-m-d', strtotime($tempData['valid_to']));
        $tempData['approval_dt'] = date('Y-m-d', strtotime($tempData['approval_dt']));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->addNewProgrammeHasAcceredation($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function saveProgramObjectiveData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveProgramObjectiveData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }











    function deleteProgrammeHasScheme($id_details)
    {
        $inserted_id = $this->programme_model->deleteProgrammeHasScheme($id_details);
        
        echo "Success"; 
    }

    function deleteProgramSchemeDetails($id)
    {
        $deleted_id = $this->programme_model->deleteProgramSchemeDetails($id);
        
        echo "Success"; 
    }


    function deleteMajorDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteMajorDetails($id_details);
        
        echo "Success"; 
    }



    function deleteMinorDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteMinorDetails($id_details);
        
        echo "Success"; 
    }



    function deleteCuncurrentDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteCuncurrentDetails($id_details);
        
        echo "Success"; 
    }



    function deleteAcceredationDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteAcceredationDetails($id_details);
        
        echo "Success"; 
    }


    function deleteProgramObjectiveDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteProgramObjectiveDetails($id_details);
        
        echo "Success"; 
    }
}