<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseLearningObjective extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_learning_objective_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('learningobjective.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['courseLearningObjectiveList'] = $this->course_learning_objective_model->courseLearningObjectiveListSearch($formData);

            $this->global['pageTitle'] = 'Speed Management System : Course Learning Objective List';
            $this->global['pageCode'] = 'learningobjective.list';
            $this->loadViews("course_learning_objective/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('learningobjective.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $user_id
                );
                
                $result = $this->course_learning_objective_model->addNewCourseLearningObjective($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Course Learning  created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Course Learning creation failed');
                }
                redirect('/setup/courseLearningObjective/list');
            }
            $this->global['pageCode'] = 'learningobjective.add';
            $this->global['pageTitle'] = 'Speed Management System : Add Course Learning Objective';
            $this->loadViews("course_learning_objective/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('learningobjective.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/courseLearningObjective/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $user_id
                );
                
                $result = $this->course_learning_objective_model->editCourseLearningObjective($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Course Learning Objective edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Course Learning Objective edit failed');
                }
                redirect('/setup/courseLearningObjective/list');
            }

            $data['courseLearningObjective'] = $this->course_learning_objective_model->getCourseLearningObjective($id);
            $this->global['pageCode'] = 'learningobjective.edit';
            $this->global['pageTitle'] = 'Speed Management System : Edit Course Learning Objective';
            $this->loadViews("course_learning_objective/edit", $this->global, $data, NULL);
        }
    }
}
