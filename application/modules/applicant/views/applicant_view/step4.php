<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Documents Upload Details</h1>
  </div>
  
  <div class="page-container">
    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">
   <div class="">
      <div id="wizard" class="wizard">
         <div class="wizard__content">
            <header class="wizard__header">
               <div class="wizard__steps">
                  <nav class="steps">
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step1" class="step__text">Profile Details</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -start"></div>
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step2" class="step__text">Contact Information</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step3" class="step__text">Program Interest</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step4" class="step__text">Document Upload</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step5" class="step__text">Discount Information</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step6" class="step__text">Declaration Form</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>
            </header>





            <div class="panels">
               <div class="paneld">



                  <div class="form-container">

                    <h4 class="form-group-title">Documents Upload</h4>


                     <div class="row">
                          
                        <div id='doc'>
                        </div>

              
                     </div>





                      <?php

                      if(!empty($applicantUploadedFiles))
                      {
                          ?>
                          <br>

                          <div class="form-container">
                                  <h4 class="form-group-title">Document Uploaded Details</h4>

                              

                                <div class="custom-table">
                                  <table class="table">
                                      <thead>
                                          <tr>
                                          <th>Sl. No</th>
                                           <th>Name</th>
                                           <th style="text-align: center;">File</th>
                                           <th style="text-align: center;">Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                           <?php
                                       $total = 0;
                                        for($i=0;$i<count($applicantUploadedFiles);$i++)
                                       { ?>
                                          <tr>
                                          <td><?php echo $i+1;?></td>
                                          <td><?php echo $applicantUploadedFiles[$i]->document_name;?></td>
                                          <td class="text-center">

                                              <a href="<?php echo '/assets/images/' . $applicantUploadedFiles[$i]->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $applicantUploadedFiles[$i]->file; ?>)" title="<?php echo $applicantUploadedFiles[$i]->file; ?>">View</a>
                                          </td>
                                          <td class="text-center">
                                              <a onclick="deleteApplicantUploadedDocument(<?php echo $applicantUploadedFiles[$i]->id; ?>)">Delete</a>
                                          </td>

                                           </tr>
                                        <?php
                                    } 
                                    ?>
                                      </tbody>
                                  </table>
                                </div>

                              </div>

                      <?php
                      
                      }
                       ?>





                  </div>


               </div>
            </div>

            <div class="wizard__footer">
               <button class="btn btn-primary">Previous</button>

               <button class="btn btn-link mr-3">Cancel</button>
               <button class="btn btn-primary next" type="submit">Save & Continue</button>
            </div>

         </div>
         <h2 class="wizard__congrats-message">
            Congratulations!!
         </h2>
      </div>
      <footer class="footer-wrapper">
         <p>&copy; 2019 All rights, reserved</p>
      </footer>
   </div>
   </div>
   </div>      
</form>
  </div>
</main>





      


<script type="text/javascript">

  $('select').select2();
  
  function getCourseByCategory(id)
  {
      $.get("/applicant/applicant/getCourseByCategory/"+id, function(data, status){
     
          $("#view_course").html(data);
      });
  }


  function getDocumentByProgramme(id)
  {
      if(id != '')
      {

          $.get("/applicant/applicant/getDocumentByProgramme/"+id, function(data, status){
         
              if(data != '')
              {
                  $("#doc").html(data);
                  // $("#view_document").show();
              }else
              {
                  // $("#view_document").hide();
                  alert('No Records Defined To Upload');
              }

          });
      }
  }

  function deleteApplicantUploadedDocument(id)
  {
      if(id != '')
      {
          $.get("/applicant/applicant/deleteApplicantUploadedDocument/"+id, function(data, status)
          {
            alert('Document File Deleted Successfully');
            window.location.reload();
          });
      }    
  }




    
    $(document).ready(function()
    {

      var id_category = "<?php echo $getApplicantDetails->id_category;?>";


      // getDocumentByProgramme(id_category);

        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                 is_submitted: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                passport: {
                    required: true
                },
                program_scheme: {
                    required: true
                },
                 alumni_discount: {
                    required: true
                },
                alumni_name: {
                    required: true
                },
                alumni_email: {
                    required: true
                },
                alumni_nric: {
                    required: true
                },
                id_program_scheme: {
                    required: true
                },
                id_branch: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Level</p>",
                },
                passport: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                alumni_discount: {
                    required: "<p class='error-text'>Select Alumni Discount Applicable </p>",
                },
                alumni_name: {
                    required: "<p class='error-text'>Alumni Name Required</p>",
                },
                alumni_email: {
                    required: "<p class='error-text'>Alumni Email Required </p>",
                },
                alumni_nric: {
                    required: "<p class='error-text'>Alumni NRIC Required</p>",
                },
                id_program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
  });




    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });

</script>

 <link rel="stylesheet" href="<?php echo BASE_PATH; ?>/assets/css/wizard.css">
