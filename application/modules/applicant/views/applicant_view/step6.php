<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Documents Upload Details</h1>
  </div>
  
  <div class="page-container">
    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">
   <div class="">
      <div id="wizard" class="wizard">
         <div class="wizard__content">
            <header class="wizard__header">
               <div class="wizard__steps">
                  <nav class="steps">
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step1" class="step__text">Profile Details</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -start"></div>
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step2" class="step__text">Contact Information</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step3" class="step__text">Program Interest</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step4" class="step__text">Document Upload</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step5" class="step__text">Discount Information</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step6" class="step__text">Declaration Form</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>
            </header>





            <div class="panels">
               <div class="paneld">



                  <div class="clearfix">
                            <!-- Form Container Starts-->
                          <div id="view_requirements">
                          </div>
                          <div class="form-container">
                             <h4 class="form-group-title">Decleration</h4>
                             <br>
                             <h4 class="modal-title">Agree To Terms & Condition To Submit The Application</h4>
                             <br>
                             <p>
                                1. These terms and conditions represent an agreement between the University of Edinburgh ("University") and you, a prospective student. By accepting the University's offer of a place on a programme, you accept these terms and conditions in full, which along with your offer and the University's rules, regulations, policies and procedures and the most recently published prospectus (as applicable), form the contract between you and the University in relation to your studies at the University as amended from time to time pursuant to Clause 1.3 (the "Contract"). 
                             </p>
                             <p>
                                2.    If you have any questions or concerns about these terms and conditions, please contact the University's Student Recruitment and Admission Office:
                             </p>
                             <p>Requirements
                             </p>
                             <br>
                             &emsp;<input type="checkbox" id="is_submitted" name="is_submitted" value="1" >&emsp;
                             By Checking This Button I accept the <u><a onclick="showModel()">Terms and Conditions</a> <span class='error-text'>*</span></u>
                          </div>
                          <div class="button-block clearfix">
                             <div class="bttn-group">
                                <!--  <p align="right"> -->
                                
                                <button  style="position: absolute; right: 0;" class="button" type="button" class="btn btn-primary btn-lg" onclick="validateSubmission()">Submit Application >></button>
                                <br>
                                <br>
                                <br>
                                
                                <!-- </p> -->
                             </div>
                          </div>              
                        </div> 


               </div>
            </div>

            <div class="wizard__footer">
               <button class="btn btn-primary">Previous</button>

               <!-- <button class="btn btn-link mr-3">Cancel</button> -->
               <!-- <button class="btn btn-primary next" type="submit">Save & Continue</button> -->
            </div>

         </div>
         <h2 class="wizard__congrats-message">
            Congratulations!!
         </h2>
      </div>
      <footer class="footer-wrapper">
         <p>&copy; 2019 All rights, reserved</p>
      </footer>
   </div>
   </div>
   </div>      
</form>
  </div>
</main>





      


<script type="text/javascript">

  $('select').select2();


   function validateSubmission()
    {
        if($('#form_applicant').valid())
        {
            $('#form_applicant').submit();
        }
    }
    
    $(document).ready(function()
    {

     var id_category = "<?php echo $getApplicantDetails->id_category;?>";


            // $.get("/applicant/applicant/getIndividualEntryRequirement/"+id_category, function(data, status)
            // {
            //   // alert(id);
            //     $("#view_requirements").html(data);
            //     $("#view_requirements").show();
            // });   

        $("#form_applicant").validate({
            rules: {
                 is_submitted: {
                    required: true
                }
            },
            messages: {
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
  });




    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });

</script>

 <link rel="stylesheet" href="<?php echo BASE_PATH; ?>/assets/css/wizard.css">
