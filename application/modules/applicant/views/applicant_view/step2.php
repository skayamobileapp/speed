<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Profile Details</h1>
  </div>
  
  <div class="page-container">
    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">
   <div class="">
      <div id="wizard" class="wizard">
         <div class="wizard__content">
            <header class="wizard__header">
               <div class="wizard__steps">
                  <nav class="steps">
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step1" class="step__text">Profile Details</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -start"></div>
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step2" class="step__text">Contact Information</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step3" class="step__text">Program Interest</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step4" class="step__text">Document Upload</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step5" class="step__text">Discount Information</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step6" class="step__text">Declaration Form</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>
            </header>





            <div class="panels">
               <div class="paneld">



                  <div class="form-container">
                             <h4 class="form-group-title">Mailing Address</h4>
                             <div class="row">
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Mailing Address 1 <span class='error-text'>*</span></label>
                                      <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $getApplicantDetails->mail_address1 ?>">
                                   </div>
                                </div>
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Mailing Address 2 </label>
                                      <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $getApplicantDetails->mail_address2 ?>">
                                   </div>
                                </div>
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Mailing Country <span class='error-text'>*</span></label>
                                      <select name="mailing_country" id="mailing_country" class="form-control" onchange="getStateByCountry(this.value)">
                                         <option value="">Select</option>
                                         <?php
                                            if (!empty($countryList))
                                            {
                                                foreach ($countryList as $record)
                                                {?>
                                         <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                               if($getApplicantDetails->mailing_country==$record->id)
                                                   {
                                                       echo "selected";
                                                   }?>
                                            >
                                            <?php echo $record->name;?>
                                         </option>
                                         <?php
                                            }
                                            }
                                            ?>
                                      </select>
                                   </div>
                                </div>
                             </div>
                             <div class="row">
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Mailing State <span class='error-text'>*</span></label>
                                      <span id='view_mailing_state'>
                                           <select name="mailing_state" id="mailing_state" class="form-control">
                                            <option value=''>Select</option>
                                           </select>

                                      </span>
                                   </div>
                                </div>
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Mailing City <span class='error-text'>*</span></label>
                                      <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $getApplicantDetails->mailing_city ?>">
                                   </div>
                                </div>
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Mailing Zipcode <span class='error-text'>*</span></label>
                                      <input type="number" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $getApplicantDetails->mailing_zipcode ?>">
                                   </div>
                                </div>
                             </div>
                          </div>


                          
                          <div class="form-container">
                             <h4 class="form-group-title">Permanent Address</h4>
                             <div class="row">
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Permanent Address 1 <span class='error-text'>*</span></label>
                                      <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getApplicantDetails->permanent_address1 ?>">
                                   </div>
                                </div>
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Permanent Address 2 </label>
                                      <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getApplicantDetails->permanent_address2 ?>">
                                   </div>
                                </div>
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Permanent Country <span class='error-text'>*</span></label>
                                      <select name="permanent_country" id="permanent_country" class="form-control" onchange="getStateByCountryPermanent(this.value)">
                                         <option value="">Select</option>
                                         <?php
                                            if (!empty($countryList))
                                            {
                                                foreach ($countryList as $record)
                                                {?>
                                         <option value="<?php echo $record->id;  ?>"
                                            <?php if($getApplicantDetails->permanent_country==$record->id){ echo "selected"; } ?>
                                            >
                                            <?php echo $record->name;?>
                                         </option>
                                         <?php
                                            }
                                            }
                                            ?>
                                      </select>
                                   </div>
                                </div>
                             </div>
                             <div class="row">
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Permanent State <span class='error-text'>*</span></label>
                                      <span id='view_permanent_state'>
                                           <select name="permanent_state" id="permanent_state" class="form-control">
                                            <option value=''>Select</option>
                                           </select>
                                      </span>
                                   </div><br/><br/>
                                </div>
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Permanent City <span class='error-text'>*</span></label>
                                      <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getApplicantDetails->permanent_city ?>">
                                   </div><br/><br/>
                                </div>
                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Permanent Zipcode <span class='error-text'>*</span></label>
                                      <input type="number" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getApplicantDetails->permanent_zipcode ?>">
                                   </div><br/><br/>
                                </div>
                             </div>
                          </div>


               </div>
            </div>

            <div class="wizard__footer">
               <button class="btn btn-primary">Previous</button>

               <button class="btn btn-link mr-3">Cancel</button>
               <button class="btn btn-primary next" type="submit">Save & Continue</button>
            </div>

         </div>
         <h2 class="wizard__congrats-message">
            Congratulations!!
         </h2>
      </div>
      <footer class="footer-wrapper">
         <p>&copy; 2019 All rights, reserved</p>
      </footer>
   </div>
   </div>
   </div>      
</form>
  </div>
</main>





      


<script type="text/javascript">

  function getStateByCountry(id)
  {
      $.get("/applicant/applicant/getStateByCountry/"+id, function(data, status)
      {
          $("#view_mailing_state").html(data);
      });
  }



  function getStateByCountryPermanent(id)
  {
      $.get("/applicant/applicant/getStateByCountryPermanent/"+id, function(data, status)
      {
          $("#view_permanent_state").html(data);
      });
  }
  


    
    $(document).ready(function() {
        $('select').select2();

        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                 is_submitted: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                passport: {
                    required: true
                },
                program_scheme: {
                    required: true
                },
                 alumni_discount: {
                    required: true
                },
                alumni_name: {
                    required: true
                },
                alumni_email: {
                    required: true
                },
                alumni_nric: {
                    required: true
                },
                id_program_scheme: {
                    required: true
                },
                id_branch: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Level</p>",
                },
                passport: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                alumni_discount: {
                    required: "<p class='error-text'>Select Alumni Discount Applicable </p>",
                },
                alumni_name: {
                    required: "<p class='error-text'>Alumni Name Required</p>",
                },
                alumni_email: {
                    required: "<p class='error-text'>Alumni Email Required </p>",
                },
                alumni_nric: {
                    required: "<p class='error-text'>Alumni NRIC Required</p>",
                },
                id_program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
  });




    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });

</script>

 <link rel="stylesheet" href="<?php echo BASE_PATH; ?>/assets/css/wizard.css">
