    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">

            <div class="main-container clearfix">
                <div class="page-title clearfix">
                    <h3>Pagsfasafse Title</h3>                    
                </div>
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                        <header class="wizard__header">
                        <div class="wizard__steps">
                          <nav class="steps">
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step1" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step2" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step3" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step4" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step5" class="step__text">Discount Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step6" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
    
                      <div class="panels">
                        <div class="paneld">
                          



  <div class="form-container" id="view_is_intake_sibbling_discount">

    <div id="view_intake_discounts"></div>


   <h4 class="form-group-title">Sibbling Discount Details</h4>
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <p>Do you have sibbling/s studying with university? <span class='error-text'>*</span></p>
            <label class="radio-inline">
            <input type="radio" name="sibbling_discount" id="sd1" value="Yes" onclick="showSibblingFields()" <?php if($getApplicantDetails->sibbling_discount=='Yes'){ echo "checked";}?>><span class="check-radio"></span> Yes
            </label>
            <label class="radio-inline">
            <input type="radio" name="sibbling_discount" id="sd2" value="No" onclick="hideSibblingFields()" <?php if($getApplicantDetails->sibbling_discount=='No'){ echo "checked";}?>><span class="check-radio"></span> No
            </label>                              
         </div>
      </div>
   </div>
   <div class="row" id="sibbling">
      <div class="col-sm-4">
         <div class="form-group">
            <label>Name <span class='error-text'>*</span></label>
             <input type="text" id="sibbling_name" name="sibbling_name" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_name ?>"
               >
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group">
            <label>NRIC <span class='error-text'>*</span></label>
             <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_nric ?>">
         </div>
      </div>
   </div>
</div>

<div class="form-container" id="view_is_intake_employee_discount">
   <h4 class="form-group-title">Employee Discount Details</h4>
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <p>Do you eligible for Employee discount <span class='error-text'>*</span></p>
            <label class="radio-inline">
            <input type="radio" name="employee_discount" id="ed1" value="Yes" onclick="showEmployeeFields()" <?php if($getApplicantDetails->employee_discount=='Yes'){ echo "checked";}?>><span class="check-radio"></span> Yes
            </label>
            <label class="radio-inline">
            <input type="radio" name="employee_discount" id="ed2" value="No" onclick="hideEmployeeFields()" <?php if($getApplicantDetails->employee_discount=='No'){ echo "checked";}?>><span class="check-radio"></span> No
            </label>                              
         </div>
      </div>
   </div>
   <div class="row" id="employee">
      <div class="col-sm-4">
         <div class="form-group">
            <label>Name <span class='error-text'>*</span></label>
             <input type="text" id="employee_name" name="employee_name" class="form-control" value="<?php echo $employeeDiscountDetails->employee_name ?>">
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group">
            <label>NRIC <span class='error-text'>*</span></label>
            <input type="text" id="employee_nric" name="employee_nric" class="form-control" value="<?php echo $employeeDiscountDetails->employee_nric ?>">
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group">
            <label>Designation <span class='error-text'>*</span></label>
            <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_designation ?>">
         </div>
      </div>
   </div>
   
</div>
<div class="form-container" id="view_is_intake_alumni_discount">
   <h4 class="form-group-title">Alumni Discount Details</h4>
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <p>Do you eligible for Alumni discount <span class='error-text'>*</span></p>
            <label class="radio-inline">
            <input type="radio" name="alumni_discount" id="ed1" value="Yes" onclick="showAlumniFields()" <?php if($getApplicantDetails->alumni_discount=='Yes'){ echo "checked";}?>><span class="check-radio"></span> Yes
            </label>
            <label class="radio-inline">
            <input type="radio" name="alumni_discount" id="ed2" value="No" onclick="hideAlumniFields()" <?php if($getApplicantDetails->alumni_discount=='No'){ echo "checked";}?>><span class="check-radio"></span> No
            </label>                              
         </div>
      </div>
   </div>
    <div class="row" id="alumni">
      <div class="col-sm-4">
         <div class="form-group">
            <label>Alumni Name <span class='error-text'>*</span></label>
            <input type="text" id="alumni_name" name="alumni_name" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_name ?>">
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group">
            <label>Alumni Email Id <span class='error-text'>*</span></label>
            <input type="email" id="alumni_email" name="alumni_email" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_email ?>">
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group">
            <label>Alumni NRIC <span class='error-text'>*</span></label>
            <input type="text" id="alumni_nric" name="alumni_nric" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_nric ?>">
         </div>
      </div>
   </div>         
        </div>    
      </div>

      <div class="wizard__footer">
        <button class="btn btn-primary next" type="submit">Save & Continue</button>
      </div>
    </div>

    <h2 class="wizard__congrats-message">
      Congratulations!!
    </h2>
  </div>
<footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>        
</div>
    </div>      
    </form>

 <script type="text/javascript">
    
    $(document).ready(function() {


      var id_intake = "<?php echo $getApplicantDetails->id_intake;?>";

      // alert(id_intake);

        $.get("/applicant/applicant/getIntakeDetails/"+id_intake, function(data, status){
           // alert(data);
                if(data != '')
                {
                     // $("#view_intake_discounts").hide();
                    $("#view_intake_discounts").html(data);

                    $("#view_is_intake_employee_discount").hide();
                    $("#view_is_intake_sibbling_discount").hide();
                    $("#view_is_intake_alumni_discount").hide();

                    var is_intake_alumni_discount = $("#is_intake_alumni_discount").val();
                    var is_intake_employee_discount = $("#is_intake_employee_discount").val();
                    var is_intake_sibbling_discount = $("#is_intake_sibbling_discount").val();
            // alert(is_intake_alumni_discount);
            // alert(is_intake_employee_discount);
            // alert(is_intake_sibbling_discount);

                  // alert(student_allotment_count);
                    if(is_intake_alumni_discount == 1)
                    {
                         $("#view_is_intake_alumni_discount").show();
                    }

                    if(is_intake_employee_discount == 1)
                    {
                         $("#view_is_intake_employee_discount").show();
                    }

                    if(is_intake_sibbling_discount == 1)
                    {
                         $("#view_is_intake_sibbling_discount").show();
                    }
                 }

            });

        
        $('select').select2();

        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                 is_submitted: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                passport: {
                    required: true
                },
                program_scheme: {
                    required: true
                },
                 alumni_discount: {
                    required: true
                },
                alumni_name: {
                    required: true
                },
                alumni_email: {
                    required: true
                },
                alumni_nric: {
                    required: true
                },
                id_program_scheme: {
                    required: true
                },
                id_branch: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Level</p>",
                },
                passport: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                alumni_discount: {
                    required: "<p class='error-text'>Select Alumni Discount Applicable </p>",
                },
                alumni_name: {
                    required: "<p class='error-text'>Alumni Name Required</p>",
                },
                alumni_email: {
                    required: "<p class='error-text'>Alumni Email Required </p>",
                },
                alumni_nric: {
                    required: "<p class='error-text'>Alumni NRIC Required</p>",
                },
                id_program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
  });


    function showSibblingFields(){
            $("#sibbling").show();
    }

    function hideSibblingFields(){
            $("#sibbling").hide();
    }

    function showEmployeeFields(){
            $("#employee").show();
    }

    function hideEmployeeFields(){
            $("#employee").hide();
    }

    function showAlumniFields(){
            $("#alumni").show();
    }

    function hideAlumniFields(){
            $("#alumni").hide();
    }


</script>