<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Profile Details</h1>
  </div>
  
  <div class="page-container">
    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">
   <div class="">
      <div id="wizard" class="wizard">
         <div class="wizard__content">
            <header class="wizard__header">
               <div class="wizard__steps">
                  <nav class="steps">
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step1" class="step__text">Profile Details</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -start"></div>
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step -completed">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step2" class="step__text">Contact Information</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step3" class="step__text">Program Interest</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step4" class="step__text">Document Upload</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step5" class="step__text">Discount Information</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                     <div class="step">
                        <div class="step__content">
                           <p class="step__number"></p>
                           <a href="step6" class="step__text">Declaration Form</a>
                           <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                              <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                              <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                           </svg>
                           <div class="lines">
                              <div class="line -background"></div>
                              <div class="line -progress"></div>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>
            </header>





            <div class="panels">
               <div class="paneld">



                  <div class="form-container">

                    <h4 class="form-group-title">Program Interest</h4>


                     <div class="row">
                          
                          <div class="col-sm-4">
                             <div class="form-group">
                                <label>Category <span class='error-text'>*</span></label>
                                <select name="id_category" id="id_category" class="form-control" onchange="getCourseByCategory(this.value)">
                                   <option value="">Select</option>
                                   <?php
                                      if (!empty($categoryList))
                                      {
                                          foreach ($categoryList as $record)
                                          {?>
                                   <option value="<?php echo $record->id;  ?>"
                                      <?php 
                                         if($getApplicantDetails->id_category==$record->id)
                                             {
                                                 echo "selected";
                                             }?>
                                      >
                                      <?php echo $record->name;?>
                                   </option>
                                   <?php
                                      }
                                      }
                                      ?>
                                </select>
                             </div>
                          </div>
                            
                          <div class="col-sm-4">
                             <div class="form-group">
                                <label>Module <span class='error-text'>*</span></label>
                                <span id='view_course'>
                                     <select name="id_course" id="id_course" class="form-control">
                                      <option value=''>Select</option>
                                     </select>

                                </span>
                             </div>
                          </div>



              
                     </div>


                     <div class="row">
                        <div id='view_fee_details'>
                        </div>
                     </div>
                  </div>


               </div>
            </div>

            <div class="wizard__footer">
               <button class="btn btn-primary">Previous</button>

               <button class="btn btn-link mr-3">Cancel</button>
               <button class="btn btn-primary next" type="submit">Save & Continue</button>
            </div>

         </div>
         <h2 class="wizard__congrats-message">
            Congratulations!!
         </h2>
      </div>
      <footer class="footer-wrapper">
         <p>&copy; 2019 All rights, reserved</p>
      </footer>
   </div>
   </div>
   </div>      
</form>
  </div>
</main>





      


<script type="text/javascript">

  $('select').select2();
  
  function getCourseByCategory(id)
  {
      $.get("/applicant/applicant/getCourseByCategory/"+id, function(data, status){
     
          $("#view_course").html(data);
      });
  }

  function getFeeStructureByData()
  {
    if($('#id_category').val() != '' && $('#id_course').val() != '')
    {

    var tempPR = {};
    tempPR['id_category'] = $("#id_category").val();
    tempPR['id_course'] = $("#id_course").val();
        $.ajax(
        {
           url: '/applicant/applicant/getFeeStructureByData',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            $("#view_fee_details").html(result);

            // var total_master = $("#calculated_total_amount").val();
            // $("#total_amount").val(total_master);
           }
        });
    }
  }


    
    $(document).ready(function()
    {
      var id_category = "<?php echo $getApplicantDetails->id_category;?>";

      if(id_category!='')
      {
         $.get("/applicant/applicant/getCourseByCategory/"+id_category, function(data, status)
            {
                var id_course = "<?php echo $getApplicantDetails->id_course;?>";

                $("#view_course").html(data);
                $("#id_course").find('option[value="'+id_course+'"]').attr('selected',true);
                $('select').select2();
            });


       }



        $("#form_applicant").validate({
            rules: {
                id_category: {
                    required: true
                },
                 id_course: {
                    required: true
                }
            },
            messages: {
                id_category: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
  });




    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });

</script>

 <link rel="stylesheet" href="<?php echo BASE_PATH; ?>/assets/css/wizard.css">
