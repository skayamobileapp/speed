 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $applicant_name; ?>
                  <small class="d-block"><?php echo $email_id; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
             
              <li class="nav-item">
                <a class="nav-link collapsed"  data-toggle="collapse" href="#collapseExamApplication" role="button">
                  <i class="fa fa-list-alt"></i>
                  <span>Applicant</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>

                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('applicant.edit','applicant.password_change'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseExamApplication">
                  <li class="nav-item">
                    <a href="/applicant/applicant/step1" class="nav-link <?php if(in_array($pageCode,array('applicant.edit'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Application</span>                      
                    </a>
                  </li>

                  <!-- <li class="nav-item">
                    <a href="/applicant/applicant/changePasssword" class="nav-link <?php if(in_array($pageCode,array('applicant.password_change'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Password Change</span>                      
                    </a>
                  </li> -->

                </ul>
              </li>  
                
          </ul>         
        </div>
      </nav>
    </div>
  </div>