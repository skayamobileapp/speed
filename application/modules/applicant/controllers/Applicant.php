<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Applicant extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_model');
        $this->isApplicantLoggedIn();
        error_reporting(0);
        $this->global['pageCode'] = 'applicant.edit';

    }

    function index()
    {
        $this->welcome();
    }


    function welcome()
    {
        // $formData['name'] = $this->security->xss_clean($this->input->post('name'));

        // $data['searchParam'] = $formData;

        $this->global['pageTitle'] = 'Speed : Applicant Portal';
        $this->loadViews("applicant_view/v", $this->global, NULL, NULL);
    }


    function getcustomreplay($idmessage,$idapplicant)
    {





        switch($idmessage){
            case '1' :
                       $message = "  <p class='cb-help-text'>1</p>
                       <p class='cb-answer'>Your application status is under review, It may take 48hours to know the final status '<strong>0</strong>' :For main menu</p>";
                       break;
case '2' :
                       $message = "  <p class='cb-help-text'>2</p><p class='cb-answer'>Fee for the program will be RM 1500 <br/>For more information Type '<strong>0</strong>' :For main menu</p> ";
                       break;
case '3' :
                       $message = "  <p class='cb-help-text'>3</p><p class='cb-answer'>Temporary Offer Letter will be sent to your eail id.  <br/> For more information Type '<strong>0</strong>' :For main menu</p>";
                       break;
case '4' :
                       $message = " <p class='cb-help-text'>4</p><p class='cb-answer'>Semester will start from Oct 4th 2020 <br/> For more information Type '<strong>0</strong>' :For main menu</p>";
                       break;

case '9' :
                       $message = " <p class='cb-answer'>Thank your for your time, our representative will call back to you, you can also email us at info@gmail.com</p>
                    <p class='cb-help-text'>For more information Type '<strong>0</strong>' :For main menu</p> ";
                       break;
       

case '0' :
                       $message = "<p><ul class='cb-help'>
                        <li>Type '<strong>1</strong>' : For Application Status</li>
                        <li>Type '<strong>2</strong>' : For Fee Amount</li>
                        <li>Type '<strong>3</strong>' : For Offer Letter</li>
                        <li>Type '<strong>4</strong>' : To know the start date of the semester</li>
                        <li>Type '<strong>9</strong>' : To get a call from the representative</li>
                    </ul></p> ";
                       break;                                                                                   

        }

        echo $message;
        exit;

    }

    function edit()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            if($_POST['fileid'] != '')
            {

            // echo "<Pre>"; print_r($this->input->post());exit();


            for($f=0;$f<count($_POST['fileid']);$f++)
            {
                // echo "<Pre>";
                // print_r($_POST[$f]);exit();

                if($_FILES['file']['name'][$f])
                {
                    $fileArray = array();
                    $fileArray['id_document'] = $_POST['fileid'][$f];
                    $fileArray['file_name'] = $_FILES['file']['name'][$f];
                    $fileArray['id_applicant'] = $id;
                    $result = $this->applicant_model->addFileDownload($fileArray);
                }
            }

                
            }
            
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $passport = $this->security->xss_clean($this->input->post('passport'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $religion = $this->security->xss_clean($this->input->post('religion'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $id_race = $this->security->xss_clean($this->input->post('id_race'));
            $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
            $id_program = $this->security->xss_clean($this->input->post('id_program'));
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));

            $sibbling_discount = $this->security->xss_clean($this->input->post('sibbling_discount'));
            $alumni_discount = $this->security->xss_clean($this->input->post('alumni_discount'));
            $employee_discount = $this->security->xss_clean($this->input->post('employee_discount'));
            $id_program_scheme = $this->security->xss_clean($this->input->post('id_program_scheme'));


            $is_submitted = $this->security->xss_clean($this->input->post('is_submitted'));
            $is_hostel = $this->security->xss_clean($this->input->post('is_hostel'));
            $id_degree_type = $this->security->xss_clean($this->input->post('id_degree_type'));
            $id_branch = $this->security->xss_clean($this->input->post('id_branch'));
            $program_scheme = $this->security->xss_clean($this->input->post('program_scheme'));

            $entry = $this->security->xss_clean($this->input->post('entry'));

            if($is_submitted == '')
            {
                $is_submitted = 0;
            }

            // echo "p" . $id_program . "- I". $id_intake;exit();
            // echo "S" . $is_submitted;exit();


            $salutationInfo = $this->applicant_model->getSalutation($salutation);


            $data = array(

                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'email_id' => $email_id,
                'contact_email' => $contact_email,
                'password' => $password,
                'nric' => $nric,
                'passport' => $passport,
                'gender' => $gender,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'religion' => $religion,
                'nationality' => $nationality,
                'id_race' => $id_race,
                'id_program' => $id_program,
                'id_intake' => $id_intake,
                'mail_address1' => $mail_address1,
                'mail_address2' => $mail_address2,
                'mailing_country' => $mailing_country,
                'mailing_state' => $mailing_state,
                'mailing_city' => $mailing_city,
                'mailing_zipcode' => $mailing_zipcode,
                'permanent_address1' => $permanent_address1,
                'permanent_address2' => $permanent_address2,
                'permanent_country' => $permanent_country,
                'permanent_state' => $permanent_state,
                'permanent_city' => $permanent_city,
                'permanent_zipcode' => $permanent_zipcode,
                'sibbling_discount' => $sibbling_discount,
                'employee_discount' => $employee_discount,
                'alumni_discount' => $alumni_discount,
                'is_updated' => 1,
                'is_submitted' => $is_submitted,
                'is_hostel' => 1,
                'id_degree_type' => $id_degree_type,
                'id_program_scheme' => $id_program_scheme,
                'id_branch' => $id_branch,
                'updated_by' => $id_user
            );

            $data['pathway'] = 'DIRECT ENTRY';
            
            if($entry == 1)
            {
                $data['is_apeal_applied'] = 0;
                $data['pathway'] = 'APEL';
            }

            $data['id_program_requirement'] = $entry;


            if($is_submitted == '1')
            {
                $get_program_scheme = $this->applicant_model->getProgramScheme($id_program_scheme);

                $data['program_scheme'] = $get_program_scheme->mode_of_program . " - " . $get_program_scheme->mode_of_study;
                $data['submitted_date'] = date('Y-m-d H:i:s');


                // $data['id_program_requirement'] = $entry;
            }

            // echo "<Pre>"; print_r($data);exit();

            // $checkDuplicate = $this->applicant_model->checkDuplicateApplicant($data,$id);
            // echo "<pre>"; print_r($checkDuplicate);exit();
            // if($checkDuplicate)
            // {
            //     echo "Entered Profile E-Mail / Phone / NRIC Already Exist";exit();
            // }



            if($sibbling_discount == 'Yes')
            {
                $data['is_sibbling_discount'] = '0';
            }
            elseif($sibbling_discount == 'No')
            {
                $data['is_sibbling_discount'] = '3';
            }


            if($employee_discount == 'Yes')
            {
                $data['is_employee_discount'] = '0';
            }
            elseif($employee_discount == 'No')
            {
                $data['is_sibbling_discount'] = '3';
            }


            if($alumni_discount == 'Yes')
            {
                $data['is_alumni_discount'] = '0';
            }
            elseif($alumni_discount == 'No')
            {
                $data['is_alumni_discount'] = '3';
            }

            $sibbling_name = $this->security->xss_clean($this->input->post('sibbling_name'));
            $sibbling_nric = $this->security->xss_clean($this->input->post('sibbling_nric'));

                 $sibbileData = array(
                    'id_applicant' => $id,
                    'sibbling_name' => $sibbling_name,
                    'sibbling_nric' => $sibbling_nric,
                );

            $checkSibblingDicount = $this->applicant_model->getSibblingDiscountByApplicantId($id);

            if($checkSibblingDicount)
            {
                $result = $this->applicant_model->editSibblingDetails($sibbileData, $id);
            }
            else
            {
                // $sibbileData['id_applicant'] = $id;
                if($sibbling_discount == 'Yes')
                {
                    $result = $this->applicant_model->addNewSibblingDiscount($sibbileData);
                }
            }

                

            $employee_name = $this->security->xss_clean($this->input->post('employee_name'));
            $employee_nric = $this->security->xss_clean($this->input->post('employee_nric'));
            $employee_designation = $this->security->xss_clean($this->input->post('employee_designation'));

            $employData = array(
                    'id_applicant' => $id,
                    'employee_name' => $employee_name,
                    'employee_nric' => $employee_nric,
                    'employee_designation' => $employee_designation
                );
            
            $checkEmployeeDicount = $this->applicant_model->getEmployeeDiscountApplicantId($id);

            if($checkEmployeeDicount)
            {
                $result = $this->applicant_model->editEmployeeDetails($employData, $id);
            }
            else
            {
                if($employee_discount == 'Yes')
                {

                // $employData['id_applicant'] = $id;
                    $result = $this->applicant_model->addNewEmployeeDiscount($employData);
                }
            }




            $alumni_name = $this->security->xss_clean($this->input->post('alumni_name'));
            $alumni_nric = $this->security->xss_clean($this->input->post('alumni_nric'));
            $alumni_email = $this->security->xss_clean($this->input->post('alumni_email'));

            $alumniData = array(
                    'id_applicant' => $id,
                    'alumni_name' => $alumni_name,
                    'alumni_nric' => $alumni_nric,
                    'alumni_email' => $alumni_email
                );
            

            $checkAlumniDicount = $this->applicant_model->getAlumniDiscountApplicantId($id);

            // echo "<Pre>"; print_r($checkAlumniDicount);exit;
            if($checkAlumniDicount)
            {
                $result = $this->applicant_model->editAlumniDetails($alumniData, $id);
            }
            else
            {
                if($alumni_discount == 'Yes')
                {

                // $employData['id_applicant'] = $id;
                    $result = $this->applicant_model->addNewAlumniDiscount($alumniData);
                }
            }


            // $result = $this->applicant_model->editEmployeeDetails($employData, $id);

            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/edit');
        }

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
                // echo "<Pre>"; print_r($data);exit;


        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');
        }
        else
        {
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programList();
            $data['countryList'] = $this->applicant_model->countryListByStatus('1');
            $data['stateList'] = $this->applicant_model->stateList();
            $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
            $data['raceList'] = $this->applicant_model->raceListByStatus('1');
            $data['religionList'] = $this->applicant_model->religionListByStatus('1');
            $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
            $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');

            $data['sibblingDiscountDetails'] = $this->applicant_model->getSibblingDiscountByApplicantId($id);

            if(!empty($data['sibblingDiscountDetails']))
            {
                if($data['sibblingDiscountDetails']->sibbling_status != 'Pending')
                {
                    $data['sibblingDiscountDetails'] = $this->applicant_model->getApplicantSibblingDiscountDetails($id);
                }
            }
            $data['employeeDiscountDetails'] = $this->applicant_model->getEmployeeDiscountApplicantId($id);
            if(!empty($data['employeeDiscountDetails']))
            {

                if($data['employeeDiscountDetails']->employee_status != 'Pending')
                {
                    $data['employeeDiscountDetails'] = $this->applicant_model->getApplicantEmployeeDiscountDetails($id);
                }
            }
            $data['alumniDiscountDetails'] = $this->applicant_model->getAlumniDiscountApplicantId($id);
            if(!empty($data['alumniDiscountDetails']))
            {

                if($data['alumniDiscountDetails']->alumni_status != 'Pending')
                {
                    $data['alumniDiscountDetails'] = $this->applicant_model->getApplicantAlumniDiscountDetails($id);
                }
            }
            

                // echo "<Pre>"; print_r($data);exit;


            $this->global['pageTitle'] = 'Campus Management System : Edit Applicant';
            $this->loadViews("applicant_view/edit", $this->global, $data, NULL);
        }
    }

    function step1()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
       

        if($this->input->post())
        {
                      
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $passport = $this->security->xss_clean($this->input->post('passport'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $religion = $this->security->xss_clean($this->input->post('religion'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $id_race = $this->security->xss_clean($this->input->post('id_race'));
           
            $salutationInfo = $this->applicant_model->getSalutation($salutation);


            $data = array(

                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'email_id' => $email_id,
                'contact_email' => $contact_email,
                'password' => $password,
                'nric' => $nric,
                'passport' => $passport,
                'gender' => $gender,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'religion' => $religion,
                'is_updated' => 1,
                'is_hostel' => 1,
                'nationality' => $nationality,
                'id_race' => $id_race,
                
            );

            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/step2');
        }
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');

        }


        // $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        // $data['stateList'] = $this->applicant_model->stateList();
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1'); 

        // $data['categoryList'] = $this->applicant_model->categoryListByStatus('1');
 
        $this->global['pageTitle'] = 'Campus Management System : Edit Applicant';            
        $this->loadViews("applicant_view/step1", $this->global, $data, NULL);
    }
    function step2()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
           if($this->input->post())
        {
                      
            
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
            $salutationInfo = $this->applicant_model->getSalutation($salutation);


            $data = array(

               
                'mail_address1' => $mail_address1,
                'mail_address2' => $mail_address2,
                'mailing_country' => $mailing_country,
                'mailing_state' => $mailing_state,
                'mailing_city' => $mailing_city,
                'mailing_zipcode' => $mailing_zipcode,
                'permanent_address1' => $permanent_address1,
                'permanent_address2' => $permanent_address2,
                'permanent_country' => $permanent_country,
                'permanent_state' => $permanent_state,
                'permanent_city' => $permanent_city,
                'permanent_zipcode' => $permanent_zipcode
            );

            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/step3');
        }
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);


        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');
        }


        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        // $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        // $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        // $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');  

        $this->global['pageTitle'] = 'Campus Management System : Edit Applicant';            

        $this->loadViews("applicant_view/step2", $this->global, $data, NULL);
    }


    function step3()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');
        }

        // $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        // $data['stateList'] = $this->applicant_model->stateList();
        // $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        // $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        // $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');
        $data['categoryList'] = $this->applicant_model->categoryListByStatus('1');


        $this->global['pageTitle'] = 'Campus Management System : Edit Applicant';  

       if($this->input->post())
        {
            $id_category = $this->security->xss_clean($this->input->post('id_category'));
            $id_course = $this->security->xss_clean($this->input->post('id_course'));
            $id_fee_structure = $this->security->xss_clean($this->input->post('id_fee_structure'));

            $data = array(
                'id_category' => $id_category,
                'id_course' => $id_course,
                // 'id_fee_structure' => $id_fee_structure
            );
            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/step6');



        }

        $this->loadViews("applicant_view/step3", $this->global, $data, NULL);
    }
    function step4()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit();
            redirect('/applicant/applicant/step5');

            if($_POST['fileid'] != '')
            {



            for($f=0;$f<count($_POST['fileid']);$f++)
            {
                // echo "<Pre>";
                // print_r($_POST[$f]);exit();

                if($_FILES['file']['name'][$f])
                {

                    $certificate_name = $_FILES['file']['name'][$f];
                    $certificate_size = $_FILES['file']['size'][$f];
                    $certificate_tmp =$_FILES['file']['tmp_name'][$f];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Certificate');

                    $file = $this->uploadFile($certificate_name,$certificate_tmp,'Certificate');



                    $fileArray = array();

                    if($file)
                    {
                        $fileArray['file'] = $file;
                    }

                    $fileArray['id_document'] = $_POST['fileid'][$f];
                    $fileArray['file_name'] = $_FILES['file']['name'][$f];
                    $fileArray['id_applicant'] = $id;
                    $result = $this->applicant_model->addFileDownload($fileArray);
                }
            }

            redirect('/applicant/applicant/step5');
                
            }
        }
         $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');
        }

        // $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);

        $this->loadViews("applicant_view/step4", $this->global, $data, NULL);
    }


    function step5()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
        if($this->input->post())
        {


            $sibbling_discount = $this->security->xss_clean($this->input->post('sibbling_discount'));
            $alumni_discount = $this->security->xss_clean($this->input->post('alumni_discount'));
            $employee_discount = $this->security->xss_clean($this->input->post('employee_discount'));
            $data = array(
                'sibbling_discount' => $sibbling_discount,
                'employee_discount' => $employee_discount,
                'alumni_discount' => $alumni_discount
            );


             if($sibbling_discount == 'Yes')
            {
                $data['is_sibbling_discount'] = '0';
            }
            elseif($sibbling_discount == 'No')
            {
                $data['is_sibbling_discount'] = '3';
            }


            if($employee_discount == 'Yes')
            {
                $data['is_employee_discount'] = '0';
            }
            elseif($employee_discount == 'No')
            {
                $data['is_sibbling_discount'] = '3';
            }


            if($alumni_discount == 'Yes')
            {
                $data['is_alumni_discount'] = '0';
            }
            elseif($alumni_discount == 'No')
            {
                $data['is_alumni_discount'] = '3';
            }

            $sibbling_name = $this->security->xss_clean($this->input->post('sibbling_name'));
            $sibbling_nric = $this->security->xss_clean($this->input->post('sibbling_nric'));

                 $sibbileData = array(
                    'id_applicant' => $id,
                    'sibbling_name' => $sibbling_name,
                    'sibbling_nric' => $sibbling_nric,
                );

            $checkSibblingDicount = $this->applicant_model->getSibblingDiscountByApplicantId($id);

            if($checkSibblingDicount)
            {
                $result = $this->applicant_model->editSibblingDetails($sibbileData, $id);
            }
            else
            {
                // $sibbileData['id_applicant'] = $id;
                if($sibbling_discount == 'Yes')
                {
                    $result = $this->applicant_model->addNewSibblingDiscount($sibbileData);
                }
            }

                

            $employee_name = $this->security->xss_clean($this->input->post('employee_name'));
            $employee_nric = $this->security->xss_clean($this->input->post('employee_nric'));
            $employee_designation = $this->security->xss_clean($this->input->post('employee_designation'));

            $employData = array(
                    'id_applicant' => $id,
                    'employee_name' => $employee_name,
                    'employee_nric' => $employee_nric,
                    'employee_designation' => $employee_designation
                );
            
            $checkEmployeeDicount = $this->applicant_model->getEmployeeDiscountApplicantId($id);

            if($checkEmployeeDicount)
            {
                $result = $this->applicant_model->editEmployeeDetails($employData, $id);
            }
            else
            {
                if($employee_discount == 'Yes')
                {

                // $employData['id_applicant'] = $id;
                    $result = $this->applicant_model->addNewEmployeeDiscount($employData);
                }
            }




            $alumni_name = $this->security->xss_clean($this->input->post('alumni_name'));
            $alumni_nric = $this->security->xss_clean($this->input->post('alumni_nric'));
            $alumni_email = $this->security->xss_clean($this->input->post('alumni_email'));

            $alumniData = array(
                    'id_applicant' => $id,
                    'alumni_name' => $alumni_name,
                    'alumni_nric' => $alumni_nric,
                    'alumni_email' => $alumni_email
                );
            

            $checkAlumniDicount = $this->applicant_model->getAlumniDiscountApplicantId($id);

            // echo "<Pre>"; print_r($checkAlumniDicount);exit;
            if($checkAlumniDicount)
            {
                $result = $this->applicant_model->editAlumniDetails($alumniData, $id);
            }
            else
            {
                if($alumni_discount == 'Yes')
                {

                // $employData['id_applicant'] = $id;
                    $result = $this->applicant_model->addNewAlumniDiscount($alumniData);
                }
            }


            // $result = $this->applicant_model->editEmployeeDetails($employData, $id);

            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/step6');

        }

            $data['sibblingDiscountDetails'] = $this->applicant_model->getSibblingDiscountByApplicantId($id);

            if(!empty($data['sibblingDiscountDetails']))
            {
                if($data['sibblingDiscountDetails']->sibbling_status != 'Pending')
                {
                    $data['sibblingDiscountDetails'] = $this->applicant_model->getApplicantSibblingDiscountDetails($id);
                }
            }
            $data['employeeDiscountDetails'] = $this->applicant_model->getEmployeeDiscountApplicantId($id);
            if(!empty($data['employeeDiscountDetails']))
            {

                if($data['employeeDiscountDetails']->employee_status != 'Pending')
                {
                    $data['employeeDiscountDetails'] = $this->applicant_model->getApplicantEmployeeDiscountDetails($id);
                }
            }
            $data['alumniDiscountDetails'] = $this->applicant_model->getAlumniDiscountApplicantId($id);
            if(!empty($data['alumniDiscountDetails']))
            {

                if($data['alumniDiscountDetails']->alumni_status != 'Pending')
                {
                    $data['alumniDiscountDetails'] = $this->applicant_model->getApplicantAlumniDiscountDetails($id);
                }
            }
            

         $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

         $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');
        }

         // echo "<Pre>";print_r($data['getApplicantDetails']);exit();

        $this->loadViews("applicant_view/step5", $this->global, $data, NULL);
    }

    function step6()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            $is_submitted = $this->security->xss_clean($this->input->post('is_submitted'));
            $entry = $this->security->xss_clean($this->input->post('entry'));

            $data['is_submitted'] = $is_submitted;

            // $data['pathway'] = 'DIRECT ENTRY';
            
            // if($entry == 1)
            // {
            //     $data['is_apeal_applied'] = 0;
            //     $data['pathway'] = 'APEL';
            // }

            // $data['id_program_requirement'] = $entry;

            if($is_submitted == '1')
            {
                $data['submitted_date'] = date('Y-m-d H:i:s');
                // $applicant_data = $this->applicant_model->getApplicantDetailsById($id);

                // $id_program_scheme = $applicant_data->id_program_scheme;
                // $id_program_has_scheme = $applicant_data->id_program_has_scheme;
                // $id_intake = $applicant_data->id_intake;
                // $id_program = $applicant_data->id_program;


                // $get_program_scheme = $this->applicant_model->getProgramScheme($id_program_scheme);
                // $program_landscape = $this->applicant_model->getProgramLandscape($id_intake, $id_program, $id_program_scheme,$id_program_has_scheme);





                // $data['program_scheme'] = $get_program_scheme->mode_of_program . " - " . $get_program_scheme->mode_of_study;
                // $data['id_program_landscape'] = $program_landscape->id;



                // $data['id_program_requirement'] = $entry;
            }

            // echo "<Pre>";print_r($data);exit();


            
            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/view');
            // $this->view();

           
        }

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');
        }

        
        $this->loadViews("applicant_view/step6", $this->global, $data, NULL);
    }

    function chat()
    {
        $id = $this->session->id_applicant;


        $data['applicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);


        $this->global['pageTitle'] = 'Campus Management System : Change Password';
        $this->loadViews("applicant_view/chat", $this->global, $data, NULL);
    }

    function changePasssword()
    {
        $id = $this->session->id_applicant;


        if($this->input->post())
        {
            $password = $this->security->xss_clean($this->input->post('password'));

            $data = array(
                    'password' => md5($password)
                );

            $result = $this->applicant_model->editApplicantDetails($data,$id);
        }

        $data['applicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);


        $this->global['pageTitle'] = 'Campus Management System : Change Password';
        $this->loadViews("applicant_view/change_password", $this->global, $data, NULL);
    }

    function view()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        // $data['nationalityList'] = $this->applicant_model->nationalityList();
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['categoryList'] = $this->applicant_model->categoryListByStatus('1');
        $data['courseList'] = $this->applicant_model->courseListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        // $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetails($id);

        // echo "<Pre>";print_r($data['getProgramDetails']);exit;
        $this->global['pageTitle'] = 'Campus Management System : View Applicant';
        $this->loadViews("applicant_view/view", $this->global, $data, NULL);
    }

    function generateTempOfferLetter()
    {
        $base_url = $_SERVER['HTTP_HOST'];
        $id = $this->session->id_applicant;

        $this->getMpdfLibrary();
        
        $applicant_information = $this->applicant_model->getApplicantInformation($id);

        $organisation = $this->applicant_model->getOrganisation();

        $applicantNAme = $applicant_information->full_name;
        $program_name = $applicant_information->program_name;
        $intake_name = $applicant_information->intake_name;
        $nric = $applicant_information->nric;

        $mail_address1 = $applicant_information->mail_address1;
        $mail_address2 = $applicant_information->mail_address2;
        $mailing_city = $applicant_information->mailing_city;
        $mailing_zipcode = $applicant_information->mailing_zipcode;
        $nric = $applicant_information->nric;
        $created_dt_tm = $applicant_information->created_dt_tm;


        $mode_of_program = $applicant_information->mode_of_program;
        $mode_of_study = $applicant_information->mode_of_study;

        $branchname = $applicant_information->branchname;

        $id_branch = $applicant_information->id_branch;

        if($id_branch == 1)
        {
            $branchname = $organisation->short_name . " - " . $organisation->name;
        }




            $templateResult = $this->applicant_model->gettemplate('1');
            $message = $templateResult->message;


        
                     $message = str_replace("@studentname",$applicantNAme,$message);
                     $message = str_replace("@program",$program_name,$message);
                     $message = str_replace("@nric",$nric,$message);
                     $message = str_replace("@intake",$intake_name,$message);

                     $message = str_replace("@mail_address1",$mail_address1,$message);
                     $message = str_replace("@mail_address2",$mail_address2,$message);
                     $message = str_replace("@mailing_city",$mailing_city,$message);
                     $message = str_replace("@mailing_zipcode",$mailing_zipcode,$message);


                     $message = str_replace("@mode_of_program",$mode_of_program,$message);
                     $message = str_replace("@mode_of_study",$mode_of_study,$message);


                     $message = str_replace("@branchname",$branchname,$message);
                      $message = str_replace("@created_dt_tm",$mailing_zipcode,$message);

                     // print_r($message);exit;


                // include("/home/camsedu/public_html/assets/mpdf/vendor/autoload.php");
                //include("/var/www/html/college/assets/mpdf/vendor/autoload.php");
                //  require_once __DIR__ . '/vendor/autoload.php';
            $mpdf=new \Mpdf\Mpdf(); 

            $mpdf->SetHeader("<table>
                              <tr>
                        <td>Campus Management System
                               </td>
                              </tr>
                              </table>");

            $mpdf->SetFooter('<div>Campus Management System</div>');
            // echo $file_data;exit;

            $file_data = $message;

            $mpdf->WriteHTML($file_data);
            $mpdf->Output('OFFER_LETTER_' . $applicantNAme . '_' . $nric .'.pdf', 'D');
            // $mpdf->Output('Offer_letter.pdf', 'D');
            exit;
    }

    function submitApplication()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            $is_submitted = $this->security->xss_clean($this->input->post('is_submitted'));

            $data = array(
                'is_submitted' => 0,
                'updated_by' => $id_user
            );

            $result = $this->applicant_model->editApplicantDetails($data,$id);
        }

        redirect('/applicant/applicant/edit');
    }

    function editProgram()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            $formData = $this->input->post();

           
            $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
            $id_program = $this->security->xss_clean($this->input->post('id_program'));

            $data = array(
                'id_program' => $id_program,
                'id_intake' => $id_intake,
                'updated_by' => $id_user
            );
            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/edit');
        }
        $data['getApplicantDetails'] = $this->applicant_model->getApplicant($id);
        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            $this->view();
        }
        else
        {
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programList();        
            $this->global['pageTitle'] = 'Campus Management System : View Applicant';
            $this->loadViews("applicant_view/edit_program", $this->global, $data, NULL);
            // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
        }
    }

   
    function getCourseByCategory($id)
    {
        $results = $this->applicant_model->getCourseByCategory($id);

        // echo "<Pre>"; print_r($programme_data);exit;
     //    $table="   
     //        <script type='text/javascript'>
     //             $('select').select2();
     //         </script>
     // ";

        // <select name='id_course' id='id_course' class='form-control' onchange='getFeeStructureByData()'>
        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_course' id='id_course' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $code = $results[$i]->code;
        $table.="<option value=" . $id . ">" . $code . " - " . $name .
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }



    function getStateByCountry($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='mailing_state' id='mailing_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    function getStateByCountryPermanent($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
        <script type='text/javascript'>
             $('select').select2();
         </script>
         ";

            $table.="
            <select name='permanent_state' id='permanent_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function getDocumentByProgramme($id_programme)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->applicant_model->getDocumentByProgrammeId($id_programme);
            // echo "<Pre>"; print_r($intake_data);exit;


            $table="<div class='row'>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;

            $table.="<div class='col-sm-3'>
                      <label>$name <span class='error-text'>*</span></label>
                      <input type='file' name='file[]'>
                      <input type='hidden' name='fileid[]' value='$id' />
                     </div>";
            }
            $table.="</div>";

            echo $table;
    }


    function getIntakeDetails($id_intake)
    {
        $intake_data = $this->applicant_model->getIntakeDetails($id_intake);
        // echo "<Pre>"; print_r($intake_data);exit;

        $is_alumni_discount = $intake_data->is_alumni_discount;
        $is_employee_discount = $intake_data->is_employee_discount;
        $is_sibbling_discount = $intake_data->is_sibbling_discount;


        $table = "

        <input type='hidden' name='is_intake_alumni_discount' id='is_intake_alumni_discount' value='$is_alumni_discount' />
        <input type='hidden' name='is_intake_employee_discount' id='is_intake_employee_discount' value='$is_employee_discount' />
        <input type='hidden' name='is_intake_sibbling_discount' id='is_intake_sibbling_discount' value='$is_sibbling_discount' />";
        
        echo $table;


    }


    function getFeeStructureByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $student_data = $this->applicant_model->getFeeStructureByData($tempData);
        echo "<Pre>"; print_r($student_data);exit;

        $student_name = $student_data->full_name;
        $student_nric = $student_data->nric;
        $email = $student_data->email_id;
        $nric = $student_data->nric;
        $intake_name = $student_data->intake_name;
        $phone = $student_data->phone;
        $programme_name = $student_data->programme_name;


        $table  = "



         <h4 class='sub-title'>Student Details</h4>

            <div class='data-list'>
                <div class='row'>

                    <div class='col-sm-6'>
                        <dl>
                            <dt>Student Name :</dt>
                            <dd>$student_name</dd>
                        </dl>
                        <dl>
                            <dt>Student Email :</dt>
                            <dd>$email</dd>
                        </dl>
                        <dl>
                            <dt>Student NRIC :</dt>
                            <dd>$nric</dd>
                        </dl>
                    </div>        
                    
                    <div class='col-sm-6'>
                        <dl>
                            <dt>Intake :</dt>
                            <dd>
                                $intake_name
                            </dd>
                        </dl>
                        <dl>
                            <dt>Programme :</dt>
                            <dd>$programme_name</dd>
                        </dl>
                        <dl>
                            <dt>Phone No.</dt>
                            <dd>$phone</dd>
                        </dl>
                    </div>

                </div>
            </div>
            <br>";
    }


    function getIndividualEntryRequirement($id_program)
    {
        $programEntryRequirementList = $this->applicant_model->programEntryRequirementList($id_program);
        $programDetails = $this->applicant_model->programDetails($id_program);


        $table = '';


        
        // echo "<Pre>"; print_r($table);exit;

        if(!empty($programEntryRequirementList))
        {
            $table.= '
             <div class="form-container">
                        <h4 class="form-group-title">Program Requirement Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                 <th>Requirement</th>
                                </tr>
                            </thead>
                            <tbody>';


                          if($programDetails->is_apel == 1)
                            {
                            $table.= "
                             <tr>
                                <td><input type='radio' name='entry' value=1> Appel Entry</td>
                                 </tr>
                            ";

                            }


                             $total = 0;
                              for($i=0;$i<count($programEntryRequirementList);$i++)
                             { 
                                $data="";
                                $j = $i+1;

                                if($programEntryRequirementList[$i]->age == 1)
                                {
                                    $data.= "Age Min. " . $programEntryRequirementList[$i]->min_age . " Year, Max. " . $programEntryRequirementList[$i]->max_age . " Year" ;
                                    $and = ", AND ";
                                }
                                if($programEntryRequirementList[$i]->education == 1)
                                {
                                    $data.= $and . "Education Requirement is : " 
                                    // . $programEntryRequirementList[$i]->qualification_code  .  " - "
                                     . $programEntryRequirementList[$i]->qualification_name ;
                                    $and = " ,  AND ";

                                }
                                if($programEntryRequirementList[$i]->work_experience == 1)
                                {
                                    $data.= $and . "Work Experience is : " . $programEntryRequirementList[$i]->work_code . " - " . $programEntryRequirementList[$i]->work_name . ", With Min. Experience Of " . $programEntryRequirementList[$i]->min_work_experience . " Years";
                                    $and = " ,  AND ";
                                    
                                }
                                if($programEntryRequirementList[$i]->other == 1)
                                {
                                    $data.= $and . "Other Requirements Description : " . $programEntryRequirementList[$i]->other_description. " .";
                                }

                                $identry = $programEntryRequirementList[$i]->id;

                            // print_r($identry);exit;



                                $table.="
                                <tr>
                                <td><input type='radio' name='entry' value='$identry'> $data</td>
                                 </tr>";


                          } 
                        $table.='
                            </tbody>
                        </table>
                      </div>

                    </div> ';

        }else
        {
            $table = '<p> No Requirements Available</p>';
        }

        print_r($table);exit;


    }


    function checkFeeStructure()
    {
        $id_session = $this->session->session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $result = $this->applicant_model->checkFeeStructure($tempData);
        
        echo $result;exit;
    
    }

    function deleteApplicantUploadedDocument($id)
    {
        $deleted = $this->applicant_model->deleteApplicantUploadedDocument($id);
        echo "success";exit;
    }


    function logout()
    {
        $sessionArray = array('id_applicant'=> '',                    
                    'applicant_name'=> '',
                    'email_id'=> '',
                    'nric'=> '',
                    'applicant_last_login'=>  '',
                    'isApplicantLoggedIn' => FALSE
            );
     $this->session->set_userdata($sessionArray);
     $this->isApplicantLoggedIn();
    }
}
