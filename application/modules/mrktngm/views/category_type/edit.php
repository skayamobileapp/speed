<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Category Type</h3>
        </div>
        <form id="form_category" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Course Type Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category Type <span class='error-text'>*</span></label><br>
                        <label class="radio-inline">
                              <input type="radio" name="category_name" value="Local" <?php if($categoryTypeDetails->category_name == 'Local'){ echo "checked";} ?>><span class="check-radio"></span> Local
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="category_name" value="International" <?php if($categoryTypeDetails->category_name == 'International'){ echo "checked";} ?>><span class="check-radio"></span> International
                            </label>
                    </div>
                </div>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_programgrade").validate({
            rules: {
                name: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>