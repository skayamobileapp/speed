<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Category Type</h3>
        </div>
        <form id="form_category" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Category Type Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category Type <span class='error-text'>*</span></label><br>
                            <label class="radio-inline">
                              <input type="radio" name="category_name" value="Local"><span class="check-radio"></span> Local
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="category_name" value="International"><span class="check-radio"></span> International
                            </label>
                    </div>
                </div>
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_category").validate({
            rules: {
                category_name: {
                    required: true
                }
            },
            messages: {
                category_name: {
                    required: "<p class='error-text'>Category Name required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
