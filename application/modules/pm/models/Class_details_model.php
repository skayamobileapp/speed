<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Class_details_model extends CI_Model
{
    function classList()
    {
        $this->db->select('*');
        $this->db->from('class_details');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getClassDetails($schoolId)
    {
        $this->db->select('*');
        $this->db->from('class_details');
        $this->db->where('id', $schoolId);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewClassDetails($schoolInfo)
    {
        $this->db->trans_start();
        $this->db->insert('class_details', $schoolInfo);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editClassDetails($schoolDetails, $schoolDetailsId)
    {
        $this->db->where('id', $schoolDetailsId);
        $this->db->update('class_details', $schoolDetails);
        return TRUE;
    }
    
    function deleteClassDetails($countryId, $countryInfo)
    {
        $this->db->where('countryId', $countryId);
        $this->db->update('subject_details', $countryInfo);
        return $this->db->affected_rows();
    }
}

