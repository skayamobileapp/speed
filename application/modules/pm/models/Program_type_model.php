<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Program_type_model extends CI_Model
{
    function programTypeList()
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramType($id)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgramType($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgramType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_type', $data);
        return TRUE;
    }
}

