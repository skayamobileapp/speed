<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class School_details_model extends CI_Model
{
    function schoolDetailsList()
    {
        $this->db->select('sd.*');
        $this->db->from('school_details as sd');
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

    function getSchoolDetails($schoolId)
    {
        $this->db->select('*');
        $this->db->from('school_details');
        $this->db->where('id', $schoolId);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSchoolDetails($schoolInfo)
    {
        $this->db->trans_start();
        $this->db->insert('school_details', $schoolInfo);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSchoolDetails($schoolDetails, $schoolDetailsId)
    {
        $this->db->where('id', $schoolDetailsId);
        $this->db->update('school_details', $schoolDetails);
        return TRUE;
    }
    
    function deleteSchoolDetails($countryId, $countryInfo)
    {
        $this->db->where('countryId', $countryId);
        $this->db->update('subject_details', $countryInfo);
        return $this->db->affected_rows();
    }

    function selectStateListList()
    {
        $this->db->select('*');
        $this->db->from('state');
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function selectCityList($value='')
    {
        if ($value == '')
        {
            $this->db->select('*');
            $this->db->from('state');
         $query = $this->db->get();
         $result = $query->result();
        }
        return $result;
    }
}

  