<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
                    
            <h3>Edit Course Requisite </h3>
        </div>
        <form id="form_semester" action="" method="post">


            <h4 class="sub-title">Course Details</h4>

                <div class="data-list">
                    <div class="row">
    
                        <div class="col-sm-6">
                            <dl>
                                <dt>Course Name :</dt>
                                <dd><?php echo $courseDetails->name;?></dd>
                            </dl>
                        </div>        
                        
                        <div class="col-sm-6">
                            <dl>
                                <dt>Course Code :</dt>
                                <dd><?php echo $courseDetails->code;?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>

        <div class="form-container">
        <h4 class="form-group-title">Add Course Requisite</h4>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Requisite Type <span class='error-text'>*</span></label>
                            <select class="form-control" name="requisite_type" id="requisite_type" onchange="show(this)" style=" widows: 260px">
                                <option value="">Select</option>
                                <option value="Pass with Grade">Pass with Grade</option>
                                <option value="Total Credit Hours">Total Credit Hours</option>
                                <option value="Co-requisite">Co-requisite</option>
                            </select>
                        </div>
                    </div>  


                    <div class="col-sm-4" style="display: none;" id="co_requisite">
                        <div class="form-group">
                            <label>Select Course <span class='error-text'>*</span></label>
                            <select name="id_course" id="id_course" class="form-control selitemIcon"  style=" width: 396px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($courseList))
                                {
                                    foreach ($courseList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>  
                

                    <div class="col-sm-4" style="display: none;" id="min_pass">
                        <div class="form-group">
                            <label>Minimum Pass Grade <span class='error-text'>*</span></label>
                            <select class="form-control" name="min_pass_grade"  style=" width: 360px">
                               <option value="">Select</option>
                                <!-- <option value="Excellent">Excellent</option>
                                <option value="High Distinction">High Distinction</option>
                                <option value="Distinction">Distinction</option>
                                <option value="Very Good">Very Good</option>
                                <option value="Good">Good</option> -->
                                <?php
                                if (!empty($gradeList))
                                {
                                    foreach ($gradeList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->description;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>

                            </select>
                        </div>
                    </div>  


                    <div class="col-sm-4" style="display: none;" id="min_total">
                        <div class="form-group">
                            <label>Min Total Credit Hours Completed <span class='error-text'>*</span></label>
                            <input type="number" name="min_total_credit" class="form-control" value="0">
                        </div>
                    </div>    
                </div>
            </div>


             <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-light btn-lg">Add</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>

        <br>


    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Requisite Type</th>
            <th>Course</th>
            <th>Minimum Pass Grade</th>
            <th>Min Total Credit Hours Completed</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($courseData))
          {
            $i=1;
            foreach ($courseData as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->requisite_type ?></td>
                <td><?php echo $record->coursename ?></td>
                <!-- <td><?php echo $record->coursecode . " - " .  $record->coursename ?></td> -->
                <td><?php echo $record->min_pass_grade ?></td>
                <td><?php echo $record->min_total_credit ?></td>
                <td class="text-center"><?php echo anchor('setup/courseMaster/delete_course?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<style type="text/css">
    .error{
        color: red;
    }
</style>

<script>
    $(document).ready(function()
    {
        $("#form_semester").validate(
        {
            rules:
            {
                requisite_type:
                {
                    required: true
                },
                id_course:
                {
                    required: true
                },
                min_pass_grade:
                {
                    required: true
                },
                min_total_credit:
                {
                    required: true
                }
            },
            messages:
            {
                requisite_type:
                {
                    required: "<p class='error-text'>Select Requisite Type</p>",
                },
                id_course:
                {
                    required: "<p class='error-text'>Select Course</p>",
                },
                min_pass_grade:
                {
                    required: "<p class='error-text'>Min. Pass Grade Required</p>",
                },
                min_total_credit:
                {
                    required: "<p class='error-text'>Min. Total Credit Hours Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
    function show(){
        var value = $("#requisite_type").val();
        if (value =="Pass with Grade"){
            $("#min_pass").show();
        }
        else{
            $("#min_pass").hide();
        }

        if (value =="Total Credit Hours"){
            $("#min_total").show();
        }
        else{
            $("#min_total").hide();
        }

        if (value =="Co-requisite"){
            $("#co_requisite").show();
        }
        else{
            $("#co_requisite").hide();
        }

        
    }
</script>