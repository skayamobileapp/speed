<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Programme Has Courses</h3>
        </div>
        <form id="form_programme_has_course" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Progamme Has Courses Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_programme)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program Landscape <span class='error-text'>*</span></label>
                        <select name="id_programme_landscape" id="id_programme_landscape" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeLandscapeList))
                            {
                                foreach ($programmeLandscapeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_programme_landscape)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_semester)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_course)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Type <span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control">
                             <option value="">Select</option>
                            <option value="<?php echo "$programmeLandscapeDetails->type";?>"
                                <?php 
                                if ($programmeLandscapeDetails->type == 'Long')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Long";  ?>
                            </option>

                            <option value="<?php echo "$programmeLandscapeDetails->type";?>"
                                <?php 
                                if ($programmeLandscapeDetails->type == 'Short')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Short";  ?>
                            </option>
                        </select>
                    </div>
                </div>

                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($programmeLandscapeDetails->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($programmeLandscapeDetails->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function()
    {
        $("#form_programme_has_course").validate(
        {
            rules:
            {
                id_programme:
                {
                    required: true
                },
                id_programme_landscape:
                {
                    required: true
                },
                id_semester: {
                    required: true
                },
                id_course: {
                    required: true
                },
                type: {
                    required: true
                }
            },
            messages:
            {
                id_programme:
                {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_programme_landscape:
                {
                    required: "<p class='error-text'>Select Program Landscape</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_course:
                {
                    required: "<p class='error-text'>Select Course</p>",
                },
                type:
                {
                    required: "<p class='error-text'>Select Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>