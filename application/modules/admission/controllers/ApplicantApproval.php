<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplicantApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_approval_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('applicant.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            // $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));

            $formData['applicant_status'] = 'Draft';

            $data['categoryList'] = $this->applicant_approval_model->categoryListByStatus('1');
            $data['courseList'] = $this->applicant_approval_model->courseListByStatus('1');

 
            $data['applicantList'] = $this->applicant_approval_model->applicantListForApproval($formData);
            $data['searchParam'] = $formData;



            $this->global['pageTitle'] = 'Campus Management System : Applicant Approval';
            $this->global['pageCode'] = 'application.approval_list';
            //print_r($subjectDetails);exit;
            $this->loadViews("applicant_approval/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('applican.approve') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/applicantApproval/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;

                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'applicant_status' => $applicant_status,
                    'reason' => $reason,
                    'updated_dt_tm' => date('Y-m-d H:i:s'),
                    'approved_by' => $id_user
                );
                $result = $this->applicant_approval_model->editApplicantDetails($data,$id);
                if ($applicant_status == "Approved")
                {
                    // $insert_id =  $this->applicant_approval_model->addNewStudent($id);
                    // if($insert_id)
                    // {
                        // $this->applicant_approval_model->addStudentProfileDetail($insert_id);

                        // echoPavan "<Pre>";print_r($insert_id);exit;
                        // $this->applicant_approval_model->createNewMainInvoiceForStudent($id,'4');
                    // }
                }
                redirect('/admission/applicantApproval/list');
            }

            $data['categoryList'] = $this->applicant_approval_model->categoryListByStatus('1');
            $data['courseList'] = $this->applicant_approval_model->courseListByStatus('1');
            $data['countryList'] = $this->applicant_approval_model->countryListByStatus('1');
            $data['stateList'] = $this->applicant_approval_model->stateList();
            $data['raceList'] = $this->applicant_approval_model->raceListByStatus('1');
            $data['salutationList'] = $this->applicant_approval_model->salutationListByStatus('1');
            $data['religionList'] = $this->applicant_approval_model->religionListByStatus('1');


            $data['getApplicantDetails'] = $this->applicant_approval_model->getApplicantDetailsById($id);


            // $data['applicantUploadedFiles'] = $this->applicant_approval_model->getApplicantUploadedFiles($id);

            // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
            
            $this->global['pageTitle'] = 'Campus Management System : Edit Applicant Approval';
            $this->global['pageCode'] = 'application.approve';

            $this->loadViews("applicant_approval/edit", $this->global, $data, NULL);
        }
    }
}
