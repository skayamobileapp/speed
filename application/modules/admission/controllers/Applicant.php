<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Applicant extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('applicatoin.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));

            // $formData['applicant_status'] = 'Draft';

            $data['categoryList'] = $this->applicant_model->categoryListByStatus('1');
            $data['courseList'] = $this->applicant_model->courseListByStatus('1');

 
            $data['applicantList'] = $this->applicant_model->applicantListForApproval($formData);
            $data['searchParam'] = $formData;



            $this->global['pageTitle'] = 'Speed Management System : Applicant Approval';
            $this->global['pageCode'] = 'application.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("applicant/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('applicatoin.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/applicant/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;

                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'applicant_status' => $applicant_status,
                    'reason' => $reason,
                    'approved_by' => $id_user
                );
                $result = $this->applicant_model->editApplicantDetails($data,$id);
                if ($applicant_status == "Approved")
                {
                    // Hided On 13-09-2020 This Hided Because Need To Know Complete Flow Of Invoice Generation
                    //$this->applicant_model->createNewMainInvoiceForStudent($id,$id);
                    

                    // Hided On 08-08-2020 FOr New Student Registration This Move Shifted To Registration->Student->approvalList
                    // $insert_id =  $this->applicant_model->addNewStudent($id);
                    // if($insert_id)
                    // {
                        // $this->applicant_model->addStudentProfileDetail($insert_id);

                        // echoPavan "<Pre>";print_r($insert_id);exit;
                        // $this->applicant_model->createNewMainInvoiceForStudent($id,'4');
                    // }
                }
                redirect('/admission/applicant/list');
            }

            $data['categoryList'] = $this->applicant_model->categoryListByStatus('1');
            $data['courseList'] = $this->applicant_model->courseListByStatus('1');
            $data['countryList'] = $this->applicant_model->countryListByStatus('1');
            $data['stateList'] = $this->applicant_model->stateList();
            $data['raceList'] = $this->applicant_model->raceListByStatus('1');
            $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');
            $data['religionList'] = $this->applicant_model->religionListByStatus('1');


            $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);


            // $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);

            // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
            
            $this->global['pageTitle'] = 'Speed Management System : Edit Applicant Approval';
            $this->global['pageCode'] = 'application.view';

            $this->loadViews("applicant/edit", $this->global, $data, NULL);
        }
    }
}
