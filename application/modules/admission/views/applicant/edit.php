<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Application View</h1>

        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

        
    </div>



            <?php
            if($getApplicantDetails->is_submitted == 1 && $getApplicantDetails->applicant_status =='Draft')
            {
            
            ?>

            <?php
            
            ?>


            <div class="page-title clearfix">
                <a href="/applicant/applicant/generateTempOfferLetter" target="_blank" class="btn btn-link btn-back">
                    Download Offer Letter >>></a>
            </div>



            <br>
               
               <h4 style="text-align: center;">Application Submitted Successfully On <?php echo date('d-m-Y h: i: s A', strtotime($getApplicantDetails->submitted_date)); ?></h4>


            <?php
            }
            elseif($getApplicantDetails->applicant_status =='Migrated')
            {
                    
            ?>
                

            <?php
            }
            elseif($getApplicantDetails->applicant_status =='Approved')
            {
            ?>
                <div class="page-title clearfix">
                    <a href="../../studentLogin" target="_blank" class="btn btn-link btn-back"><?php echo '" Congratulation  '. $getApplicantDetails->full_name .'   "' ?> Your Applicant Application Approved <br> Login To Student Portal >>></a>
                </div>


                <h4 style="text-align: center;">Application Approved  On <?php echo date('d-m-Y H:i', strtotime($getApplicantDetails->approved_dt_tm)); ?> . </h4>
            <?php
            }
            ?>



            


<form id="form_applicant" action="" method="post" enctype="multipart/form-data">


            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Profile Details</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center"
                            aria-controls="program_scheme" role="tab" data-toggle="tab">Contact Information</a>
                    </li>

                    <li role="presentation"><a href="#program_majoring" class="nav-link border rounded text-center"
                            aria-controls="program_majoring" role="tab" data-toggle="tab">Program Interest</a>
                    </li>

                    <li role="presentation"><a href="#program_minoring" class="nav-link border rounded text-center"
                            aria-controls="program_minoring" role="tab" data-toggle="tab">Document Upload</a>
                    </li>

                    <!-- <li role="presentation"><a href="#program_concurrent" class="nav-link border rounded text-center"
                            aria-controls="program_concurrent" role="tab" data-toggle="tab">Discount Information</a>
                    </li> -->
                    <li role="presentation"><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" role="tab" data-toggle="tab">Decleration Form</a>
                    </li>
                    <li role="presentation"><a href="#tab_approval" class="nav-link border rounded text-center"
                            aria-controls="tab_approval" role="tab" data-toggle="tab">Approval Details</a>
                    </li>
                    
                </ul>


            





            
                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">






            <div class="form-container">
                <h4 class="form-group-title">Profile Details</h4>  

                          
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Salutation <span class='error-text'>*</span></label>
                            <select name="salutation" id="salutation" class="form-control" disabled>
                                <?php
                                if (!empty($salutationList)) {
                                    foreach ($salutationList as $record) {
                                ?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php if($getApplicantDetails->salutation==$record->id)
                                            {
                                                echo "selected=selected";
                                            }
                                            ?>
                                            >
                                            <?php echo $record->name;  ?>        
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type Of Nationality <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nationality" name="nationality" value="<?php echo $getApplicantDetails->nationality ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>" readonly>
                        </div>
                    </div>

                </div>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Martial Status <span class='error-text'>*</span></label>
                            <select class="form-control" id="martial_status" name="martial_status" disabled>
                                <option value="">SELECT</option>
                                <option value="Single" <?php if($getApplicantDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                <option value="Married" <?php if($getApplicantDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                <option value="Divorced" <?php if($getApplicantDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Race <span class='error-text'>*</span></label>
                            <select name="id_race" id="id_race" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($raceList))
                                {
                                    foreach ($raceList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->id_race)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Religion <span class='error-text'>*</span></label>
                            <select name="religion" id="religion" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($religionList))
                                {
                                    foreach ($religionList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->religion)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>    
                    
                </div>


                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" readonly>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Password <span class='error-text'>*</span></label>
                            <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone ?>" readonly>
                        </div>
                    </div>


                </div>


                <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender <span class='error-text'>*</span></label>
                            <select class="form-control" id="gender" name="gender" disabled>
                                <option value="">SELECT</option>
                                <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                                <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                            </select>
                        </div>
                    </div>
                    

                    


                   <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone ?>" readonly>
                        </div>
                    </div> -->


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label> Race <span class='error-text'>*</span></label>
                            <select name="id_race" id="id_race" class="form-control" disabled>
                                <option value="">Select</option>
                                <option value="<?php echo $getApplicantDetails->id_race;?>"
                                    <?php 
                                    if ($getApplicantDetails->id_race == 'Bhumiputra')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "Bhumiputra";  ?>
                                </option>

                                <option value="<?php echo $getApplicantDetails->id_race;?>"
                                    <?php 
                                    if ($getApplicantDetails->id_race == 'International')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "International";  ?>
                                </option>
                            </select>
                        </div>
                    </div> -->

                   


                  <!--   
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->id_program==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> -->


                   <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <p>Do you Wish To Apply For Hostel Accomodation <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_hostel" id="sd1" value="1" <?php if($getApplicantDetails->is_hostel=='1'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_hostel" id="sd2" value="0" <?php if($getApplicantDetails->is_hostel=='0'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div> -->

                </div>

                <div class="row">

                    



                   <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label>
                            <select name="id_intake" id="id_intake" class="form-control selitemIcon" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($intakeList))
                                {
                                    foreach ($intakeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->id_intake==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> -->

                     
                    
                </div>
            </div>  



                         



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">


                            <br>

                            


                            <div class="form-container">
                                <h4 class="form-group-title">Mailing Address</h4>

                                <div class="row">
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Address 1 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $getApplicantDetails->mail_address1 ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Address 2 </label>
                                            <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $getApplicantDetails->mail_address2 ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Country <span class='error-text'>*</span></label>
                                            <select name="mailing_country" id="mailing_country" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_country==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing State <span class='error-text'>*</span></label>
                                            <select name="mailing_state" id="mailing_state" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($stateList))
                                                {
                                                    foreach ($stateList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_state==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing City <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $getApplicantDetails->mailing_city ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $getApplicantDetails->mailing_zipcode ?>" readonly>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            

                            <div class="form-container">
                                <h4 class="form-group-title">Permanent Address</h4>

                                <div class="row">
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Address 1 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getApplicantDetails->permanent_address1 ?>" readonly>
                                        </div>
                                
                                    </div><div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getApplicantDetails->permanent_address2 ?>" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Country <span class='error-text'>*</span></label>
                                            <select name="permanent_country" id="permanent_country" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_country==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent State <span class='error-text'>*</span></label>
                                            <select name="permanent_state" id="permanent_state" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($stateList))
                                                {
                                                    foreach ($stateList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_state==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent City <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getApplicantDetails->permanent_city ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getApplicantDetails->permanent_zipcode ?>" readonly>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="program_majoring">
                        <div class="mt-4">




                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Program Interests</h4>


                                <div class="row">

                                     <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Category <span class='error-text'>*</span></label>
                                            <select name="id_category" id="id_category" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($categoryList))
                                                {
                                                    foreach ($categoryList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_category)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                          <div class="form-group">
                                              <label>Course <span class='error-text'>*</span></label>
                                              <select name="id_course" id="id_course" class="form-control" disabled="true">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($courseList))
                                                  {
                                                      foreach ($courseList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;  ?>"
                                                              <?php 
                                                              if($record->id == $getApplicantDetails->id_course)
                                                              {
                                                                  echo "selected=selected";
                                                              } ?>>
                                                              <?php echo $record->code . " - " .$record->name;  ?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>




                            </div>


                        </div>




                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="program_minoring">
                        <div class="mt-4">

                            <br>

                            <!-- <div class="form-container" id="view_document" style="display: none">
                                <h4 class="form-group-title">Documents To Upload</h4> -->
                             
                                <div id='doc'>
                                </div>



                              <?php

                              if(!empty($applicantUploadedFiles))
                              {
                                  ?>
                                  <br>

                                  <div class="form-container">
                                          <h4 class="form-group-title">Document Uploaded Details</h4>

                                      

                                        <div class="custom-table">
                                          <table class="table">
                                              <thead>
                                                  <tr>
                                                  <th>Sl. No</th>
                                                   <th>Name</th>
                                                   <th style="text-align: center;">File</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                   <?php
                                               $total = 0;
                                                for($i=0;$i<count($applicantUploadedFiles);$i++)
                                               { ?>
                                                  <tr>
                                                  <td><?php echo $i+1;?></td>
                                                  <td><?php echo $applicantUploadedFiles[$i]->document_name;?></td>
                                                  <td class="text-center">

                                                      <a href="<?php echo '/assets/images/' . $applicantUploadedFiles[$i]->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $applicantUploadedFiles[$i]->file; ?>)" title="<?php echo $applicantUploadedFiles[$i]->file; ?>">View</a>
                                                  </td>

                                                   </tr>
                                                <?php
                                            } 
                                            ?>
                                              </tbody>
                                          </table>
                                        </div>

                                      </div>

                              <?php
                              
                              }
                               ?>



                            <!-- </div> -->







                        </div>
                    
                    </div>







                    <div role="tabpanel" class="tab-pane" id="program_concurrent">
                        
                        <div class="mt-4">

                        <br>

        <!-- <div class="form-container">
                <h4 class="form-group-title">Discount Information</h4>

        <div id="view_intake_discounts">
                </div>

                <br>


            <div class="form-container" id="view_is_intake_sibbling_discount">
                <h4 class="form-group-title">Sibbling Discount Details</h4> 


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Do you have sibbling/s studying with university? <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                        <input type="radio" id="sd1" name="sibbling_discount" value="Yes" disabled="disabled" <?php if($getApplicantDetails->sibbling_discount=='Yes'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="sd2" name="sibbling_discount" value="No" disabled="disabled" <?php if($getApplicantDetails->sibbling_discount=='No'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                        </label>
                    </div>
                </div>
            </div>


            
           <?php
            if($getApplicantDetails->sibbling_discount=='Yes')
            {
                ?>

                <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" id="sibbling_name" name="sibbling_name" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_name ?>" readonly="readonly">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NRIC <span class='error-text'>*</span></label>
                        <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_nric ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_status ?>" readonly="readonly">
                    </div>
                </div> 
            </div> 

                <?php
                if($sibblingDiscountDetails->sibbling_status=='Reject')
                {
                ?>

               <div class="row">
                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Reject Reason <span class='error-text'>*</span></label>
                                <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $sibblingDiscountDetails->reason ?>" readonly="readonly">
                            </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected By <span class='error-text'>*</span></label>
                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $sibblingDiscountDetails->user_name ?>" readonly="readonly">
                            </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected On <span class='error-text'>*</span></label>
                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($sibblingDiscountDetails->rejected_on)) ?>" readonly="readonly">
                            </div>
                    </div>
                </div>


                <?php
                }
                elseif($sibblingDiscountDetails->sibbling_status=='Approved')
                {
                    ?>
                    

                 <div class="row">

                     <div class="col-sm-4">
                            <div class="form-group">
                                <label>Approved By <span class='error-text'>*</span></label>
                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $sibblingDiscountDetails->user_name ?>" readonly="readonly">
                            </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Approved On <span class='error-text'>*</span></label>
                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $sibblingDiscountDetails->rejected_on ?>" readonly="readonly">
                            </div>
                    </div>

                    
                </div> 

                    <?php
                }
                ?> 

            <?php
            }
             ?>

         </div>




             <br>

        <div class="form-container" id="view_is_intake_employee_discount">
                <h4 class="form-group-title">Employee Discount Details</h4> 


            
            <div class="row">      
                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Do you eligible for Employee discount <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="employee_discount" id="ed1" value="Yes" disabled="disabled" <?php if($getApplicantDetails->employee_discount=='Yes'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="employee_discount" id="ed2" value="No" disabled="disabled" <?php if($getApplicantDetails->employee_discount=='No'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                        </label>                              
                    </div>                         
                </div>
            </div>


             <?php
            if($getApplicantDetails->employee_discount=='Yes')
            {
                ?>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" id="employee_name" name="employee_name" class="form-control" value="<?php echo $employeeDiscountDetails->employee_name ?>" readonly="readonly">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NRIC <span class='error-text'>*</span></label>
                        <input type="text" id="employee_nric" name="employee_nric" class="form-control" value="<?php echo $employeeDiscountDetails->employee_nric ?>" readonly="readonly">
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Designation <span class='error-text'>*</span></label>
                        <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_designation ?>" readonly="readonly">
                    </div>
                </div>
            </div> 


           <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_status ?>" readonly="readonly">
                    </div>
                </div>
            </div>


                <?php
                if($employeeDiscountDetails->employee_status=='Reject')
                {
                ?>

                <div class="row">

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected Reason <span class='error-text'>*</span></label>
                                <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $employeeDiscountDetails->reason ?>" readonly="readonly">
                            </div>
                    </div>

                     <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected By <span class='error-text'>*</span></label>
                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $employeeDiscountDetails->user_name ?>" readonly="readonly">
                            </div>
                    </div>
                </div> 


                <div class="row">

                     <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected On <span class='error-text'>*</span></label>
                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($employeeDiscountDetails->rejected_on)) ?>" readonly="readonly">
                            </div>
                    </div>


                </div> 


                <?php
                }
                elseif($employeeDiscountDetails->employee_status=='Approved')
                {
                    ?>

                <div class="row">

                     <div class="col-sm-4">
                            <div class="form-group">
                                <label>Approved By <span class='error-text'>*</span></label>
                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $employeeDiscountDetails->user_name ?>" readonly="readonly">
                            </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Approved On <span class='error-text'>*</span></label>
                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $employeeDiscountDetails->rejected_on ?>" readonly="readonly">
                            </div>
                    </div>

                    
                </div> 

                    <?php
                }
                ?> 

            <?php
            }
             ?>
             
        



         </div>
















                                <br>


                <div class="form-container" id="view_is_intake_alumni_discount">
                <h4 class="form-group-title">Alumni Discount Details</h4> 


                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Do you eligible for Alumni discount <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="alumni_discount" id="ed1" value="Yes" onclick="showAlumniFields()" <?php if($getApplicantDetails->alumni_discount=='Yes'){ echo "checked";}?> disabled><span class="check-radio" ></span> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="alumni_discount" id="ed2" value="No" onclick="hideAlumniFields()" <?php if($getApplicantDetails->alumni_discount=='No'){ echo "checked";}?>  disabled><span class="check-radio"></span> No
                                            </label>                              
                                        </div>                         
                                    </div>
                                </div>



                                <div class="row" id="alumni" style="display: none;">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni Name <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_name" name="alumni_name" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni Email Id <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_email" name="alumni_email" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_nric" name="alumni_nric" class="form-control">
                                        </div>
                                    </div>

                                </div>
                               

                               <?php
                                if($getApplicantDetails->alumni_discount=='Yes')
                                {
                                    if($getApplicantDetails->is_alumni_discount=='0')
                                    {
                                    ?>

                                <div class="row">
                                   
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_name" name="alumni_name" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_name ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email Id <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_email" name="alumni_email" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_email ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_nric" name="alumni_nric" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_nric ?>" readonly>
                                        </div>
                                    </div>
                                    
                                </div> 

                                <?php
                                    }
                                    else
                                    {
                                        ?>

                                <div class="row">
                                   
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_name" name="alumni_name" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_name ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email Id <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_email" name="alumni_email" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_email ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_nric" name="employee_nric" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_nric ?>" readonly>
                                        </div>
                                    </div>
                                    
                                </div> 

                                <div class="row">

                                   <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Status <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_status ?>" readonly>
                                        </div>
                                    </div>
                                </div>

                                    <?php
                                    }
                                    ?>


                                <?php
                                    if($alumniDiscountDetails->alumni_status=='Reject')
                                    {
                                    ?>

                                    <div class="row">

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected Reason <span class='error-text'>*</span></label>
                                                    <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $alumniDiscountDetails->reason ?>" readonly="readonly">
                                                </div>
                                        </div>

                                         <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected By <span class='error-text'>*</span></label>
                                                    <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $alumniDiscountDetails->user_name ?>" readonly="readonly">
                                                </div>
                                        </div>

                                         <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected On <span class='error-text'>*</span></label>
                                                    <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($alumniDiscountDetails->rejected_on)) ?>" readonly="readonly">
                                                </div>
                                        </div>

                                    </div> 




                                    <?php
                                    }
                                    elseif($alumniDiscountDetails->alumni_status=='Approved')
                                    {
                                        ?>

                                    <div class="row">

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Approved By <span class='error-text'>*</span></label>
                                                    <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $alumniDiscountDetails->user_name ?>" readonly="readonly">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Approved On <span class='error-text'>*</span></label>
                                                    <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $alumniDiscountDetails->rejected_on ?>" readonly="readonly">
                                                </div>
                                        </div>
                                    
                                    </div> 



                                <?php
                                }
                                 ?>
                                

                            <?php
                                
                            }
                             ?>

                        </div>





                  <div id="view_download">
                    
                  </div>

            </div> -->












                             



                    
                        </div>

                    </div>




                <div role="tabpanel" class="tab-pane" id="tab_one">
                    <div class="mt-4">



                        <div class="form-container">
                                    
                                <h4 class="form-group-title">Decleration</h4>

                                <br>



                                <h4 class="modal-title">Agree To Terms & Condition To Submit The Application</h4>


                                <br>



                                    <p>
                                     1. These terms and conditions represent an agreement between the University of Edinburgh ("University") and you, a prospective student. By accepting the University's offer of a place on a programme, you accept these terms and conditions in full, which along with your offer and the University's rules, regulations, policies and procedures and the most recently published prospectus (as applicable), form the contract between you and the University in relation to your studies at the University as amended from time to time pursuant to Clause 1.3 (the "Contract"). 
                                    </p>

                                    <p>

                                     2.    If you have any questions or concerns about these terms and conditions, please contact the University's Student Recruitment and Admission Office:
                                    </p>  

                                    <p>Requirements
                                    </p>  

                                   


                                    <br>

                                    &emsp;<input type="checkbox" id="is_submitted" name="is_submitted" value="1"
                                    <?php
                                    if($getApplicantDetails->is_submitted==1)
                                    { echo 'checked'; } ?> disabled>&emsp;
                                    By Checking This Button I accept the <u><a onclick="showModel()">Terms and Conditions</a> <span class='error-text'>*</span></u>



                            </div>



                        <br>



                        <div class="form-container">
                            <h4 class="form-group-title">Program Requirement Details</h4>
                         

                            <div class="row">

                                
                        
                 




                     <?php
                        if (!empty($programEntryRequirementList))
                        {
                            foreach ($programEntryRequirementList as $record)
                            {
                                ?>
                                    <br>
                                      <input type="radio" name="entry" id="entry" 
                                      <?php
                                      if($record->id == $getApplicantDetails->id_program_requirement)
                                    {
                                        echo 'checked';
                                    }
                                      ?>
                                      disabled>
                                    
                                <?php 
                                 $and = '';

                                if($record->age == 1)
                                {
                                    echo "Age Min. " . $record->min_age . " Year, Max. " . $record->max_age . " Year" ;
                                    $and = ", AND <br>";
                                }
                                if($record->education == 1)
                                {
                                    echo $and . "Education Requirement is : " 
                                    // . $programEntryRequirementList[$i]->qualification_code  .  " - "
                                     . $record->qualification_name ;
                                    $and = " ,  AND <br>";

                                }
                                if($record->work_experience == 1)
                                {
                                    echo $and . "Work Experience is : " . $record->work_code . " - " . $record->work_name . ", With Min. Experience Of " . $record->min_work_experience . " Years";
                                    $and = " ,  AND <br>";
                                    
                                }
                                if($record->other == 1)
                                {
                                    echo $and . "Other Requirements Description : " . $record->other_description. " .";
                                }
                               ?>
                                           


                           <?php
                            }
                        }
                        ?>

                                
                        </div>

                    </div>







                    </div>
                
                </div>















                <div role="tabpanel" class="tab-pane" id="tab_approval">
                
                    <div class="mt-4">

                        <form id="form_applicant" action="" method="post">



                            <div class="form-container">
                                <h4 class="form-group-title">Approval Details</h4> 

                                    <div class="row">

                                                    
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <p>Applicant Status Approval <span class='error-text'>*</span></p>
                                                <label class="radio-inline">
                                                <input type="radio" id="sd1" name="applicant_status" value="Approved" <?php if($getApplicantDetails->applicant_status=='Approved'){ echo "checked";}?> ><span class="check-radio"></span> Approve
                                            </label>
                                            <label class="radio-inline">

                                                <input type="radio" id="sd2" name="applicant_status" value="Draft" <?php if($getApplicantDetails->applicant_status=='Draft'){ echo "checked";}?>><span class="check-radio"></span> Draft
                                            </label>
                                            <label class="radio-inline">

                                                <input type="radio" id="sd2" name="applicant_status" value="Rejected" <?php if($getApplicantDetails->applicant_status=='Rejected'){ echo "checked";}?>><span class="check-radio"></span> Reject
                                            </label>
                                            </div>
                                        </div>

                                         <div class="col-sm-4" id="view_reject" style="display: none">
                                            <div class="form-group">
                                                <label>Reason <span class='error-text'>*</span></label>
                                                <input type="text" id="reason" name="reason" class="form-control">
                                            </div>
                                        </div>

                                     </div>

                                </div>


                           


                        </form>







                    </div>

                </div>










                </div>






                


        </div>


    </form>





     <!--  <div class="chat-bot-container" style="display: ;">
          <div class="cb-header">
              Hi, <strong><?php print_r($_SESSION['applicant_name']);?></strong> <br />We are happy to help you,
          </div>
          <ul class="cb-help">
              <li>Type '<strong>1</strong>' : For Application Status</li>
              <li>Type '<strong>2</strong>' : For Fee Amount</li>
              <li>Type '<strong>3</strong>' : For Offer Letter</li>
              <li>Type '<strong>4</strong>' : To know the start date of the semester</li>
              <li>Type '<strong>9</strong>' : To get a call from the representative</li>
          </ul>
         
          <div id='attach'></div>
          <div class="form-group cb-form">
              <input type="text" class="form-control" placeholder="Type here..." name='message' id='message' value=''>
              <button class="btn btn-primary" type="button" onclick='validateData()'>Start</button>
            </div>                                   
      </div> -->




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer> 
      






</main>


<script>

   $('select').select2();

  function getDocumentByProgramme(id)
    {
      // alert(id);
        $.get("/applicant/applicant/getDocumentByProgramme/"+id, function(data, status)
        {
          // alert(data);
       
            if(data != '')
            {
                // $("#doc_upload").html(data);
                // $("#view_document").show();
            }else
            {
                // $("#view_document").hide();
                alert('No Records Defined To Upload For The Selected Program');
            }
        });
    }


    $(document).ready(function() {

        var id_program = <?php echo $getApplicantDetails->id_program; ?>;

        if(id_program != '')
        {
          getDocumentByProgramme(id_program);
        }

        var id_intake = $("#id_intake").val();
        $.get("/applicant/applicant/getIntakeDetails/"+id_intake, function(data, status)
        {
                    // alert(data);
                    if(data != '')
                    {
                         // $("#view_intake_discounts").hide();
                        $("#view_intake_discounts").html(data);

                        $("#view_is_intake_employee_discount").hide();
                        $("#view_is_intake_sibbling_discount").hide();
                        $("#view_is_intake_alumni_discount").hide();

                        var is_intake_alumni_discount = $("#is_intake_alumni_discount").val();
                        var is_intake_employee_discount = $("#is_intake_employee_discount").val();
                        var is_intake_sibbling_discount = $("#is_intake_sibbling_discount").val();
                // alert(is_intake_alumni_discount);
                // alert(is_intake_employee_discount);
                // alert(is_intake_sibbling_discount);

                      // alert(student_allotment_count);
                        if(is_intake_alumni_discount == 1)
                        {
                             $("#view_is_intake_alumni_discount").show();
                        }

                        if(is_intake_employee_discount == 1)
                        {
                             $("#view_is_intake_employee_discount").show();
                        }

                        if(is_intake_sibbling_discount == 1)
                        {
                             $("#view_is_intake_sibbling_discount").show();
                        }
                     }
                 

            });


         $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                 is_submitted: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                passport: {
                    required: true
                },
                program_scheme: {
                    required: true
                },
                 alumni_discount: {
                    required: true
                },
                alumni_name: {
                    required: true
                },
                alumni_email: {
                    required: true
                },
                alumni_nric: {
                    required: true
                },
                id_program_scheme: {
                    required: true
                },
                id_branch: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Level</p>",
                },
                passport: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                alumni_discount: {
                    required: "<p class='error-text'>Select Alumni Discount Applicable </p>",
                },
                alumni_name: {
                    required: "<p class='error-text'>Alumni Name Required</p>",
                },
                alumni_email: {
                    required: "<p class='error-text'>Alumni Email Required </p>",
                },
                alumni_nric: {
                    required: "<p class='error-text'>Alumni NRIC Required</p>",
                },
                id_program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    function validateData() {
        if($("#message").val()=='') {
           $("#error-textfordisplay").text('Enter some messgae');
        }
        if($("#message").val()!='0' && $("#message").val()!='1' && 
           $("#message").val()!='2' && 
           $("#message").val()!='3' && 
           $("#message").val()!='4' && 
           $("#message").val()!='9' ) {
           $("#error-textfordisplay").text('Please enter valid input');
          
        } else {
            messageid = $("#message").val();
            id_applicant = <?php echo $_SESSION['id_applicant'];?>;
             $.get("/applicant/applicant/getcustomreplay/"+messageid+"/"+id_applicant, function(data, status){
           
                $("#attach").append(data);
            });
        }

        
    }

    function submitApp()
    {
        $('#id_intake').prop('disabled', false);
        $('#id_program').prop('disabled', false);
        if($('#form_applicant').valid())
        {    
            $('#form_applicant').submit();
        }
    }

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });
</script>