 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
             
              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('application.list','application.add','application.edit','application.view','currency_rate_setup.list','currency_rate_setup.add'))){  ?>
                  <?php 
                  }
                  else
                  { 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>"  data-toggle="collapse" href="#collapseExamApplication" role="button">
                  <i class="fa fa-list-alt"></i>
                  <span>Applications</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>

                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('application.list','application.add','application.edit','application.view','application.approval_list','application.approve'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseExamApplication">
                  <li class="nav-item">
                    <a href="/admission/applicantApproval/list" class="nav-link <?php if(in_array($pageCode,array('application.approval_list','application.approve'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Applicaton Approval</span>                      
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/admission/applicant/list" class="nav-link <?php if(in_array($pageCode,array('application.list','application.edit','application.view'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Application Summary</span>                      
                    </a>
                  </li>                  

                </ul>
              </li>  
              
              <!-- <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('account_code.list','account_code.add','account_code.edit','bank_registration.list','bank_registration.add','bank_registration.edit','learningobjective.list','learningobjective.add','learningobjective.edit','taxonomy.list','taxonomy.add','taxonomy.edit','difficultylevel.list','difficultylevel.add','difficultylevel.edit','question.list','question.add','question.edit'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-file-text"></i>
                  <span>General Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('account_code.list','account_code.add','account_code.edit','bank_registration.list','bank_registration.add','bank_registration.edit','payment_type.list','payment_type.add','payment_type.edit','fee_category.list','fee_category.add','fee_category.edit','fee_setup.list','fee_setup.add','fee_setup.edit','fee_structure.list','fee_structure.add','fee_structure.edit'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                  <li class="nav-item">
                    <a href="/finance/accountCode/list" class="nav-link <?php if(in_array($pageCode,array('account_code.list','account_code.add','account_code.edit',))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Account Code</span>                      
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/finance/bankRegistration/list" class="nav-link <?php if(in_array($pageCode,array('bank_registration.list','bank_registration.add','bank_registration.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Bank Registration</span>                      
                    </a>
                  </li>  

                   <li class="nav-item">
                    <a href="/finance/paymentType/list" class="nav-link <?php if(in_array($pageCode,array('payment_type.list','payment_type.add','payment_type.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Payment Type</span>                      
                    </a>
                  </li>  

                   <li class="nav-item">
                    <a href="/finance/feeCategory/list" class="nav-link <?php if(in_array($pageCode,array('fee_category.list','fee_category.add','fee_category.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Fee Category</span>                      
                    </a>
                  </li>                  

                   <li class="nav-item">
                    <a href="/finance/feeSetup/list" class="nav-link <?php if(in_array($pageCode,array('fee_setup.list','fee_setup.add','fee_setup.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Fee Setup</span>                      
                    </a>
                  </li>
                  
                  <li class="nav-item">
                    <a href="/finance/feeStructure/list" class="nav-link <?php if(in_array($pageCode,array('fee_structure.list','fee_structure.add','fee_structure.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Fee Structure</span>                      
                    </a>
                  </li>
                </ul>
              </li>     -->            
                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>