<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Applicant_model extends CI_Model
{
    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }
   

    function categoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', 1);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

     function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

     function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    
    function getApplicantDetailsById($id)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function applicantListForApproval($applicantList)
    {

        $this->db->select('a.*, cat.name as category_name, c.code as course_code, c.name as course_name');
        $this->db->from('applicant as a');
        $this->db->join('category as cat', 'a.id_category = cat.id','left');
        $this->db->join('course as c', 'a.id_course = c.id','left');

        if($applicantList['first_name'])
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['email_id'])
        {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        
        if($applicantList['nric'])
        {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['id_category'])
        {
            $this->db->where('a.id_category', $applicantList['id_category']);
            
        }
        if($applicantList['id_course'])
        {
            $this->db->where('a.id_course', $applicantList['id_course']);
        }
        if($applicantList['applicant_status'])
        {
            $this->db->where('a.applicant_status', $applicantList['applicant_status']);
        }
        
        // $likeCriteria = "(a.is_sibbling_discount  != '0' and a.is_employee_discount  != '0' and a.is_alumni_discount  != '0')";
        // $this->db->where($likeCriteria);

        // $this->db->where('a.applicant_status', $applicantList['applicant_status']);
        $this->db->where('a.is_updated', '1');
        $this->db->where('a.email_verified', '1');
        $this->db->where('a.is_submitted', '1');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }
}