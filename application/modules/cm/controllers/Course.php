<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Course extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('course.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['courseList'] = $this->course_model->courseListSearch($formData);

            $this->global['pageTitle'] = 'Speed Management System : Module List';
            $this->global['pageCode'] = 'course.list';
            $this->loadViews("course/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('course.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                if($_FILES['file']['name'])
                {


                $certificate_name = $_FILES['file']['name'];
                $certificate_size = $_FILES['file']['size'];
                $certificate_tmp =$_FILES['file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'File');

                $file = $this->uploadFile($certificate_name,$certificate_tmp,'File');


                }



            
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $months = $this->security->xss_clean($this->input->post('months'));
                $best_selling = $this->security->xss_clean($this->input->post('best_selling'));
                $best_trending = $this->security->xss_clean($this->input->post('best_trending'));
                $id_category = $this->security->xss_clean($this->input->post('id_category'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_category' => $id_category,
                    'name' => $name,
                    'code' => $code,
                    'months' => $months,
                    'best_selling' => $best_selling,
                    'best_trending' => $best_trending,
                    'status' => $status,
                    'created_by' => $user_id
                );

                if($file)
                {
                    $data['file'] = $file;
                }
                
                $result = $this->course_model->addNewCourse($data);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Module created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Module creation failed');
                }
                redirect('cm/course/list');
            }
            $data['categoryList'] = $this->course_model->categoryListByStatus('1');
           
            $this->global['pageCode'] = 'course.add';
            $this->global['pageTitle'] = 'Speed Management System : Add Module';
            $this->loadViews("course/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('course.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/course/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                if($_FILES['file']['name'])
                {


                $certificate_name = $_FILES['file']['name'];
                $certificate_size = $_FILES['file']['size'];
                $certificate_tmp =$_FILES['file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'File');

                $file = $this->uploadFile($certificate_name,$certificate_tmp,'File');


                }



                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $months = $this->security->xss_clean($this->input->post('months'));
                $best_selling = $this->security->xss_clean($this->input->post('best_selling'));
                $best_trending = $this->security->xss_clean($this->input->post('best_trending'));
                $id_category = $this->security->xss_clean($this->input->post('id_category'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_category' => $id_category,
                    'name' => $name,
                    'code' => $code,
                    'months' => $months,
                    'best_selling' => $best_selling,
                    'best_trending' => $best_trending,
                    'status' => $status,
                    'created_by' => $user_id
                );
                
                if($file)
                {
                    $data['file'] = $file;
                }


                $result = $this->course_model->editCourse($data,$id);
                if ($result)
                {
                    $this->session->set_flashdata('success', 'Module edited successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Module edit failed');
                }
                redirect('/cm/course/list');
            }
            $data['categoryList'] = $this->course_model->categoryListByStatus('1');

            $data['courseDetails'] = $this->course_model->getCourse($id);

            $this->global['pageCode'] = 'course.list';
            $this->global['pageTitle'] = 'Speed Management System : Edit Module';
            
            $this->loadViews("course/edit", $this->global, $data, NULL);
        }
    }
}
