<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Topic extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('topic_model');
        $this->load->model('question_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('topic.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));

            $data['searchParam'] = $formData;
            $data['topicList'] = $this->topic_model->topicListSearch($formData);
            $data['courseList'] = $this->question_model->courseListByStatus('1');

            $this->global['pageTitle'] = 'Speed Management System : Topic List';
            $this->global['pageCode'] = 'topic.list';
            $this->loadViews("topic/list", $this->global, $data, NULL);
        }
    }
     
    function add()
    {
        if ($this->checkAccess('topic.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'id_course' => $id_course,
                    'status' => $status,
                    'created_by' => $user_id
                );
                
                $result = $this->topic_model->addNewTopic($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Topic created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Topic creation failed');
                }
                redirect('/setup/topic/list');
            }
            $data['courseList'] = $this->question_model->courseListByStatus('1');

            $this->global['pageCode'] = 'topic.add';
            $this->global['pageTitle'] = 'Speed Management System : Add Topic';
            $this->loadViews("topic/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('topic.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/topic/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'id_course' => $id_course,
                    'status' => $status,
                    'updated_by' => $user_id
                );
                
                $result = $this->topic_model->editTopic($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Topic edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Topic edit failed');
                }
                redirect('/setup/topic/list');
            }

            $data['topic'] = $this->topic_model->getTopic($id);
            // $data['courseList'] = $this->question_model->topicListByCourse($data['topic']->id_course);
            $data['courseList'] = $this->question_model->courseListByStatus('1');

            $this->global['pageCode'] = 'topic.edit';
            $this->global['pageTitle'] = 'Speed Management System : Edit Topic';
            $this->loadViews("topic/edit", $this->global, $data, NULL);
        }
    }
}
