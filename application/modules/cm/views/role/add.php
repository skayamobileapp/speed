<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Add Role</h1>
    <a href='list' class="btn btn-link ml-auto"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
  </div>

  <form id="form_unit" action="" method="post">

    <div class="page-container">

      <div>
        <h4 class="form-title">Role details</h4>
      </div>
      <div class="form-container">


        <div class="row">
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Role<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="role" name="role">
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row align-items-center">
              <label class="col-sm-4 col-form-label">Status<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                  <label class="custom-control-label" for="customRadioInline1">Active</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                  <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">

          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-lg-2 col-form-label">Permissions<span class="text-danger">*</span></label>
              <div class="col-lg-8">
                <select name="permissions[]" id="permissions" multiple="multiple" class="form-control">
                  <?php
                  if (!empty($permissions)) {
                    foreach ($permissions as $record) {
                  ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->description;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="button-block clearfix">
          <div class="bttn-group">
            <button class="btn btn-primary">Save</button>
            <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
            <!-- <button class="btn btn-link">Cancel</button> -->
          </div>
        </div>


      </div>
    </div>
  </form>
</main>

<script>
  $(document).ready(function() {
    $("#form_unit").validate({
      rules: {
        role: {
          required: true
        }
      },
      messages: {
        role: {
          required: "<p class='error-text'>Role required</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
  });
</script>
<script type="text/javascript">
  $('#permissions').select2({
    placeholder: "Select the permissions",
    allowClear: true,
    multiple:true
  });

  function reloadPage() {
    window.location.reload();
  }
</script>