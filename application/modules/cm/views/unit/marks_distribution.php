<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Assessment to Module Objective</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>

        <div class="page-container">

          <div>
            <h4 class="form-title">Module Objective details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo $unit->code; ?>" readonly>
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $unit->name; ?>" readonly>
                        </div>
                      </div>
                    </div>

                </div>



                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Category <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <select name="id_category" id="id_category" class="form-control" disabled>
                          <option value="">Select</option>
                          <?php
                          if (!empty($categoryList))
                          {
                              foreach ($categoryList as $record)
                              {?>
                           <option value="<?php echo $record->id;  ?>"
                            <?php 
                            if($record->id == $unit->id_category)
                            {
                                echo 'selected';
                            }
                            ?>
                            >
                              <?php echo $record->name;?>
                           </option>
                          <?php
                              }
                          }
                          ?>
                        </select>
                        </div>
                      </div>
                    </div> 


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Module <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <span id='view_course' >
                           <select name="id_course" id="id_course" class="form-control" disabled>
                            <option value=''>Select</option>
                           </select>
                          </span>
                        </div>
                      </div>
                    </div> 


                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($unit->status == 1){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($unit->status == 0){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                    

                </div>

                      


                  
                 

            </div>                                
        </div>










      <br>



      <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Assessment details</h4>
          </div>

            <div class="form-container">


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Module</label>
                        <div class="col-sm-8">
                          <select name="module" id="module" class="form-control">
                          <option value="">Select</option>
                          <option value="Quiz">Quiz</option>
                          <option value="Assignment">Assignment</option>
                          <option value="Project">Project</option>
                        </select>
                        </div>
                      </div>
                    </div> 


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Max. Marks <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="max_marks" name="max_marks">
                        </div>
                      </div>
                    </div>  

                </div>



                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Pass Marks <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="pass_marks" name="pass_marks">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Pass Compulsary <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="is_pass_compulsary" class="custom-control-input" value="1" checked="checked">
                            <label class="custom-control-label" for="customRadioInline1">Yes</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="is_pass_compulsary" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadioInline2">No</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div> 



                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
                  </div>

                </div> 

            </div>                                
        </div>

      </form>


      <br>


      <?php

        if(!empty($unitMarksDistribution))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Assessment Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Module</th>
                            <th>Max. marks</th>
                            <th>Pass Marks</th>
                            <th>Pass Compulsary</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($unitMarksDistribution);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $unitMarksDistribution[$i]->module; ?></td>
                            <td><?php echo $unitMarksDistribution[$i]->max_marks; ?></td>
                            <td><?php echo $unitMarksDistribution[$i]->pass_marks; ?></td>
                            <td><?php if($unitMarksDistribution[$i]->is_pass_compulsary == 1)
                            {
                             echo 'Yes'; 
                            }else
                            {
                              echo 'No';
                            }
                             ?></td>
                            <td class="text-center">
                            <a onclick="deleteMarksDistribution(<?php echo $unitMarksDistribution[$i]->id; ?>)">Delete</a>
                            </td>
                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>












</main>

<script>

  $('select').select2();


    function reloadPage()
    {
      window.location.reload();
    }


  function getCourseByCategory(id)
  {
      $.get("/cm/unit/getCourseByCategory/"+id, function(data, status){
     
          $("#view_course").html(data);
      });
  }


  function deleteMarksDistribution(id)
  {
      $.get("/cm/unit/deleteMarksDistribution/"+id, function(data, status)
      {
        window.location.reload();
      });
  }




    $(document).ready(function()
    {
      var id_category = "<?php echo $unit->id_category;?>";

      if(id_category!='')
      {
         $.get("/cm/unit/getCourseByCategory/"+id_category, function(data, status)
            {
                var id_course = "<?php echo $unit->id_course;?>";

                $("#view_course").html(data);
                $("#id_course").find('option[value="'+id_course+'"]').attr('selected',true);
                $('select').select2();
            });


       }

        $("#form_main").validate({
            rules: {
                module: {
                    required: true
                },
                max_marks: {
                    required: true
                },
                pass_marks:{
                  required: true
                },
                is_pass_compulsary:{
                  required: true
                },
                file:{
                  required: true
                }
            },
            messages: {
                module: {
                    required: "<p class='error-text'>Select Module</p>",
                },
                max_marks: {
                    required: "<p class='error-text'>Max. Marks Required</p>",
                },
                pass_marks: {
                  required: "<p class='error-text'>Pass Marks Required</p>",
                },
                is_pass_compulsary: {
                  required: "<p class='error-text'>Select Pass status</p>",
                },
                file: {
                  required: "<p class='error-text'>Select File To Upload</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>