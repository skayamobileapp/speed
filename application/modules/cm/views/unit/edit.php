<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Edit (Unit) Module Registration</h3>
            </div>


    <form id="form_main" action="" method="post">

        <div class="form-container">

            <h4 class="form-group-title">(Unit) Module Registration Details</h4>

        
                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo $unit->code; ?>">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $unit->name; ?>">
                        </div>
                      </div>
                    </div>

                </div>



                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Category <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <select name="id_category" id="id_category" class="form-control" onchange="getCourseByCategory(this.value)">
                          <option value="">Select</option>
                          <?php
                          if (!empty($categoryList))
                          {
                              foreach ($categoryList as $record)
                              {?>
                           <option value="<?php echo $record->id;  ?>"
                            <?php 
                            if($record->id == $unit->id_category)
                            {
                                echo 'selected';
                            }
                            ?>
                            >
                              <?php echo $record->name;?>
                           </option>
                          <?php
                              }
                          }
                          ?>
                        </select>
                        </div>
                      </div>
                    </div> 


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Module <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <span id='view_course' >
                           <select name="id_course" id="id_course" class="form-control">
                            <option value=''>Select</option>
                           </select>
                          </span>
                        </div>
                      </div>
                    </div> 


                </div>


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($unit->status == 1){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($unit->status == 0){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>      


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <a href="../list" class="btn btn-link">Back</a>
                  </div>

                </div> 

            </div> 

    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

     $('select').select2();


    function reloadPage()
    {
      window.location.reload();
    }


  function getCourseByCategory(id)
  {
      $.get("/cm/unit/getCourseByCategory/"+id, function(data, status){
     
          $("#view_course").html(data);
      });
  }




    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                id_category:{
                  required: true
                },
                id_course:{
                  required: true
                },
                file:{
                  required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_category: {
                  required: "<p class='error-text'>Select Category</p>",
                },
                id_course: {
                  required: "<p class='error-text'>Select Module</p>",
                },
                file: {
                  required: "<p class='error-text'>Select File To Upload</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>