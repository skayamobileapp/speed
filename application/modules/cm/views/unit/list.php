<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Module Objective List</h3>
    <a href="add" class="btn btn-primary ml-auto">+ Add Module Objective</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">

                  <div class="col-lg-6">
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Search</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Code / Name" value="<?php echo $searchParam['name'] ?>">
                      </div>
                    </div>
                  </div>
                  
              </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

      <div class="custom-table">

        <?php
        if ($this->session->flashdata('success')) {
        ?>
          <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php
        }
        if ($this->session->flashdata('error')) {
        ?>
          <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php
        }
        ?>


    
        <table class="table" id="list-table">
            <thead>
                 <tr>
                  <th>Sl. No</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Course</th>
                  <!-- <th>View</th> -->
                  <th class="text-center">Status</th>
                  <th>Add assessement</th>
                  <th style="text-align: center;">Action</th>
                </tr>
            </thead>
           <tbody>
                <?php
                if (!empty($unitList)) {
                  $i=1;
                  foreach ($unitList as $record) {
                ?>
                    <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $record->code; ?></td>
                    <td><?php echo $record->name; ?></td>
                    <td><?php echo $record->category_name; ?></td>
                    <td><?php echo $record->course_code . " - " . $record->course_name; ?></td>
                    <!-- <td>
                      <?php 
                      if($record->file)
                      {
                        ?>

                        <a href="<?php echo '/assets/images/' . $record->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->file; ?>)" title="<?php echo $record->file; ?>">  View                                  
                        </a>

                        <?php
                      }
                      ?>
                     

                    </td> -->
                    <td style="text-align: center;">
                      <?php if ($record->status == '1')
                      {
                        echo "Active";
                      } else {
                        echo "In-Active";
                      }
                      ?>
                    </td>
                    <td> <a href="<?php echo 'markDistribution/' . $record->id; ?>" class="btn" type="button" data-toggle="tooltip" data-placement="top" title="Marks Distribution">
                        <i class="fa fa-plus" aria-hidden="true">
                        </i>
                      </a></td>
                      |
                    <td class="text-center">

                      <a href="<?php echo 'edit/' . $record->id; ?>" class="btn" type="button" data-toggle="tooltip" data-placement="top" title="Edit">
                        <i class="fa fa-pencil-square-o" aria-hidden="true">
                        </i>
                      </a>

                      
                     


                    </td>
                    </tr>
                <?php
                $i++;
                  }
                }
                ?>
              </tbody>
        </table>
    </div>             
 </div>
</div>

<script>

    $('select').select2();
  
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>