<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Dashboard extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->studentId =  $this->session->userdata['id_student'];
        error_reporting(0);
    }

    function index()
    {

        $id_session = session_id();

        $id_student = $this->studentId;

        if(!$this->studentId)
        {
            redirect('/index');
        }

        $data['listOfCourses'] = $this->dashboard_model->getCourses($id_session);

        if(count($data['listOfCourses'])>0)
        {
            redirect('/profile/dashboard/checkout');
        }



        if($this->input->post())
        {
        
            // echo "<Pre>";print_r($this->input->post());exit;

            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $address = $this->security->xss_clean($this->input->post('address'));
            $address2 = $this->security->xss_clean($this->input->post('address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('country'));
            $permanent_state = $this->security->xss_clean($this->input->post('state'));
            $city = $this->security->xss_clean($this->input->post('city'));
            $zipcode = $this->security->xss_clean($this->input->post('zipcode'));

            // $id_location = $this->security->xss_clean($this->input->post('id_location'));
            // $exam_type = $this->security->xss_clean($this->input->post('exam_type'));
            // $email = $this->security->xss_clean($this->input->post('email'));
            // $user_name = $this->security->xss_clean($this->input->post('user_name'));
            // $password = $this->security->xss_clean($this->input->post('password'));
            // $status = $this->security->xss_clean($this->input->post('status'));


            $salutation_data = $this->dashboard_model->getSalutation($salutation);


            $data = array(
                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutation_data->name . ". " . $first_name . " " . $last_name,
                'nationality' => $nationality,
                'gender' => $gender,
                'mail_address1' => $address,
                'mail_address2' => $address2,
                'mailing_country' => $permanent_country,
                'mailing_state' => $permanent_state,
                'mailing_city' => $city,
                'mailing_zipcode' => $zipcode,
                // 'email_id' => $email,
                // 'password' => $password,
                'nric' => $nric,
                'status' => 1,
                'created_by' => $user_id
            );
            // echo "<Pre>"; print_r($data);exit;

            $result = $this->dashboard_model->updateStudent($data,$id_student);
            redirect('/profile/dashboard/course');
        }




       

        $data['countryList'] = $this->dashboard_model->countryListByStatus('1');       
        $data['salutationList'] = $this->dashboard_model->salutationListByStatus('1');       


        $data['student'] = $this->dashboard_model->getStudent($id_student);
        
        // echo "<Pre>";print_r($data['student']);exit;
        
        
        $this->global['pageTitle'] = 'Speed Management System : Exam Has Question List';
        $this->loadViews("dashboard/index", $this->global, $data, NULL);
    }

    function course()
    {

        $data['courseList'] = $this->dashboard_model->getCoursesByStudent($this->studentId);
            $this->loadViews("dashboard/course", $this->global, $data, NULL);
    }

    function soa()
    {

           $data['invoiceList'] = $this->dashboard_model->getInvoices($this->studentId);
        
           $data['receiptList'] = $this->dashboard_model->getReceipt($this->studentId);

            $this->global['pageTitle'] = 'Speed Management System : Exam Has Question List';
            $this->loadViews("dashboard/soa", $this->global, $data, NULL);
    }

     function checkout() {

       $id_session = session_id();

        $data['listOfCourses'] = $this->dashboard_model->getCourses($id_session);
        
        $this->loadViews('dashboard/checkout',$this->global,$data,NULL);

    }


    

    public function payment() {

        $this->global = '';
        $data = '';
        $this->loadViews('dashboard/payment',$this->global,$data,NULL);

    }

    public function success(){

        $id_session = session_id();
        $listOfCourses = $this->dashboard_model->getCourses($id_session);
        for($k=0;$k<count($listOfCourses);$k++) {
            $getCateogryId = $this->dashboard_model->getCoursesById($listOfCourses[$k]->id_course);
            $data['id_course'] = $listOfCourses[$k]->id_course;
            $data['id_student'] = $this->studentId;
            $data['id_category'] = $getCateogryId->id_category;
            $idinvoiceNumber = $this->generateInvoice($data);


            $receipt['invoice_id'] = $idinvoiceNumber;
            $this->generateReceipt($receipt);

            $this->dashboard_model->deletefromtempsuccess($id_session);
        }

        $this->global = '';
        $data = '';
        $this->loadViews('dashboard/success',$this->global,$data,NULL);


    }

    function deletetemp($id) {
         $student_list_data = $this->dashboard_model->deletefromtemp($id);
        if($student_list_data) {
           $return =  "1";            
        } else {
            $return =  "0";
        }
        echo $return;exit;
    }


    function getStateByCountry($id_country)
    {
        $results = $this->dashboard_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="
            <script type='text/javascript'>
                 $('select').select2();
            </script>";

        $table.="
        <select name='state' id='state' class='custom-select d-block w-100' required=''>
            <option value=''>Choose...</option>
            ";

        for($i=0;$i<count($results);$i++)
        {
            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";
        }

        $table.="

        </select>";

        echo $table;
        exit;
    }

    function invoice()
    {
        $data = $this->security->xss_clean($this->input->post('tempData'));
        $this->generateInvoice($data);
    }

    function generateInvoice($data)
    {
        // echo "<Pre>";print_r($data);exit;
        if($data == '')
        {
            echo 'No Parameters Passed To Generate the Invoice';
        }
        else
        {
            $id_category = $data['id_category'];
            $id_course = $data['id_course'];
            $id_student = $data['id_student'];

            $fee_structure = $this->dashboard_model->getFeestructureByData($data);

            $course = $this->dashboard_model->getCourseByData($data);

            if(empty($course))
            {
                echo 'No Module Found';exit;
            }

            if($fee_structure)
            {
                $fee_structure_details = $this->dashboard_model->getFeestructureDetailsByIdFeeStructureMain($fee_structure->id);

                if($fee_structure_details)
                {
                    $invoice_number = $this->dashboard_model->generateMainInvoiceNumber();

                    $invoice_data = array(
                        'id_category' => $id_category,
                        'id_course' => $id_course,
                        'id_student' => $id_student,
                        'type' => 'Student',
                        'currency' => 1,
                        'status' => 1,
                        'invoice_number' => $invoice_number
                    );

                    $id_invoice = $this->dashboard_model->addInvoice($invoice_data);

                    if($id_invoice)
                    {
                        $total_amount = 0;

                        foreach ($fee_structure_details as $detail)
                        {
                            $detail_data = array(
                                'id_main_invoice' => $id_invoice,
                                'id_fee_item' => $detail->id_fee_item,
                                'amount' => $detail->amount,
                                'price' => $detail->amount,
                                'quantity' => 1,
                            );
                        
                            $id_invoice_details = $this->dashboard_model->addInvoiceDetails($detail_data);
                            $total_amount = $total_amount + $detail->amount;
                        }

                        $months = "+". $course->months ." months";
                        $expiry_date = date("Y-m-d", strtotime($months));

                        // $expiry_date = date(("Y-m-d") . $months);


                        $data['id_invoice'] = $id_invoice;
                        $data['status'] = 1;
                        $data['amount'] = $total_amount;
                        $data['expiry_date'] = $expiry_date;

                        // echo "<Pre>"; print_r($id_invoice);exit();

                        $added_student_has_course = $this->dashboard_model->addStudentHasCourse($data);

                        $update_invoice['total_amount'] = $total_amount;
                        $update_invoice['invoice_total'] = $total_amount;
                        $update_invoice['balance_amount'] = $total_amount;

                        $id_updated_invoice = $this->dashboard_model->editInvoice($update_invoice,$id_invoice);
                    
                        // echo "<Pre>"; print_r($id_updated_invoice);exit();

                        return $id_invoice ;

                    }

                }
                else
                {
                    echo 'No Fee Structure Is Defined For Entered Data';exit;
                }


            }
            else
            {
                echo 'No Fee Structure Is Defined For Entered Data';exit;
            }


        }
    }


    function generateReceipt($data)
    {
        // echo "<Pre>";print_r($data);exit;
        if($data == '')
        {
            echo 'No Parameters Passed To Generate the Receipt';
        }
        else
        {
            $id_invoice = $data['invoice_id'];

            $main_invoice = $this->dashboard_model->getInvoice($id_invoice);

            if($main_invoice)
            {
               

                    // $fee_structure_details = $this->dashboard_model->getFeestructureDetailsByIdFeeStructureMain($fee_structure->id);

                    $receipt_number = $this->dashboard_model->generateReceiptNumber();

                    $invoice_data = array(
                        'id_category' => $main_invoice->id_category,
                        'id_course' => $main_invoice->id_course,
                        'id_student' => $main_invoice->id_student,
                        'type' => $main_invoice->type,
                        'currency' => $main_invoice->currency,
                        'status' => 1,
                        'receipt_number' => $receipt_number,
                        'receipt_amount' => $main_invoice->invoice_total
                    );

                    $id_receipt = $this->dashboard_model->addReceipt($invoice_data);

                    if($id_receipt)
                    {
                        $total_amount = 0;

                        $detail_data = array(
                            'id_receipt' => $id_receipt,
                            'id_main_invoice' => $id_invoice,
                            'invoice_amount' => $main_invoice->invoice_total,
                            'paid_amount' => $main_invoice->invoice_total,
                            'status' => 1,
                        );
                    
                        $id_invoice_details = $this->dashboard_model->addReceiptDetails($detail_data);
                        


                        $update_invoice['balance_amount'] = 0;
                        $update_invoice['paid_amount'] = $main_invoice->invoice_total;

                        $id_updated_invoice = $this->dashboard_model->editInvoice($update_invoice,$id_invoice);

                        echo "Receipt Generated : " . $receipt_number;
                    }
                

            }
            else
            {
                echo 'No Main Invoice Found For The Entered Invoice';
            }

            return 1;


        }

    }
}