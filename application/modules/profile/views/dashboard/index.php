  
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Profile Details</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="../dashboard/index"> Profile Details</a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>
<form id="form_main" method="POST" action="<?php echo base_url(); ?>profile/dashboard/index">

<section class="padding-y-10">
  <div class="container">

   <div class="row">
        <div class="col-md-12 order-md-1">


            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="salutation">Salutation</label>
                <select class="custom-select d-block w-100" id="salutation" name="salutation" required="">
                  <option value="">Choose...</option>
                  <?php
                  if (!empty($salutationList))
                  {
                      foreach ($salutationList as $record)
                      {?>
                   <option value="<?php echo $record->id;  ?>"
                    <?php
                    if($student->salutation == $record->id)
                    {
                      echo "selected";
                    }
                    ?>
                    >
                      <?php echo $record->name;?>
                   </option>
                  <?php
                      }
                  }
                  ?>
                </select>
                <div class="invalid-feedback">
                  Select a valid Salutation.
                </div>
              </div>



              <div class="col-md-6 mb-3">
                <label for="first_name">First name</label>
                <input type="text" class="form-control" id="first_name" name='first_name' placeholder="" required="required" value="<?php echo $student->first_name ?>">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>


            </div>

            <div class="row">


              <div class="col-md-6 mb-3">
                <label for="last_name">Last name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="" required="" value="<?php echo $student->last_name ?>">
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>

            


              <div class="col-md-6 mb-3">
                <label for="nric">NRIC / Passport</label>
                <input type="text" class="form-control" id="nric" name="nric" placeholder=""  required="required" value="<?php echo $student->nric ?>">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>


            </div>

            <div class="row">


              <div class="col-md-6 mb-3">
                <label for="nationality">Nationality</label>
                <select class="custom-select d-block w-100" id="nationality" name="nationality" required="">
                  <option value="">Choose...</option>
                  <?php
                  if (!empty($countryList))
                  {
                      foreach ($countryList as $record)
                      {?>
                   <option value="<?php echo $record->id;  ?>"
                    <?php
                    if($student->nationality == $record->id)
                    {
                      echo "selected";
                    }
                    ?>
                    >
                      <?php echo $record->name;?>
                   </option>
                  <?php
                      }
                  }
                  ?>
                </select>
                <div class="invalid-feedback">
                  Please select a valid country.
                </div>
              </div>

              
              
            

              <div class="col-md-6 mb-3">
                <label for="gender">Gender</label>
                <select class="custom-select d-block w-100" id="gender" name="gender" required="">
                  <option value="">Choose...</option>
                  <option value="Male"
                  <?php
                  if($student->gender == 'Male')
                  {
                    echo "selected";
                  }
                  ?>
                  >Male</option>
                  <option value="Female"
                  <?php
                  if($student->gender == 'Female')
                  {
                    echo "selected";
                  }
                  ?>
                  >Female</option>
                </select>
                <div class="invalid-feedback">
                  Please select a valid Gender.
                </div>
              </div>

              


            </div>

            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder=""  required="required" value="<?php echo $student->email ?>" readonly>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>



              <div class="col-md-6 mb-3">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder=""  required=""  value="<?php echo $student->password ?>" readonly>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>

          

            </div>


            <div class="mb-3">
              
            </div>

            <div class="mb-3">
              <label for="address">Address</label>
              <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" required="" value="<?php echo $student->mail_address1 ?>">
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="mb-3">
              <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
              <input type="text" class="form-control" id="address2" name="address2" placeholder="Apartment or suite" value="<?php echo $student->mail_address2 ?>">
            </div>

            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="country">Country</label>
                <select class="custom-select d-block w-100" id="country" name="country" required="" onchange="getStateByCountry(this.value)">
                  <option value="">Choose...</option>
                  <?php
                  if (!empty($countryList))
                  {
                      foreach ($countryList as $record)
                      {?>
                   <option value="<?php echo $record->id;  ?>"
                    <?php
                    if($student->mailing_country == $record->id)
                    {
                      echo "selected";
                    }
                    ?>
                    >
                      <?php echo $record->name;?>
                   </option>
                  <?php
                      }
                  }
                  ?>
                </select>
                <div class="invalid-feedback">
                  Please select a valid country.
                </div>
              </div>

              <div class="col-md-6 mb-3">
                <label for="state">State</label>
                <span id="view_state">
                  <select class="custom-select d-block w-100" id="state" name="state" required="">
                    <option value="">Choose...</option>
                  </select>
                </span>
                <div class="invalid-feedback">
                  Please provide a valid state.
                </div>
              </div>

            </div>

            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="city">City</label>
                <input type="text" class="form-control" id="city"  name="city" placeholder="" required="" value="<?php echo $student->mailing_city ?>">
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>

              <div class="col-md-6 mb-3">
                <label for="zip">Zip</label>
                <input type="number" class="form-control" id="zipcode"  name="zipcode" placeholder="" required="" value="<?php echo $student->mailing_zipcode ?>">
                <div class="invalid-feedback">
                  City required.
                </div>
              </div>

            </div>
            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Updated</button>
        </div>
      </div>
  </div> <!-- END container-->
</section>

</form>




<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> <!-- END site-footer -->


<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  </body>
</html>

<script>

  $(document).ready(function()
  {

    var id_country = <?php echo $student->mailing_country ?>;

    // alert(id_country);

    if(id_country > 0)
    {
       $.get("/profile/dashboard/getStateByCountry/"+id_country, function(data, status){
       
            $("#view_state").html(data);

            var state = "<?php echo $student->mailing_state;?>";

            // alert(state);

            $("#state").find('option[value="'+state+'"]').attr('selected',true);
            $('select').select2();


        });
    }

  });



  function getStateByCountry(id)
    {
        $.get("/profile/dashboard/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }


  function buynow(id)
  {
      var id = 1;
      $.get("/coursedetails/tempbuynow/"+id, function(data, status){
           console.log(data);
           parent.location='../login';
      });
  }

  </script>