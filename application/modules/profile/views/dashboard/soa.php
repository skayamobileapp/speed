
  
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>SOA Details</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="../dashboard/soa"> Statement Of Account</a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>
<section class="padding-y-10 border-bottom border-light">
  <div class="container">
    <div class="row">
      <div class="col-12">
       <div class="card p-4 shadow-v3">
       <ul class="nav tab-state-primary mb-3" role="tablist">
         <li class="nav-item m-1">
           <a class="nav-link border rounded text-center active" data-toggle="tab" href="#Tabs_99-1" role="tab" aria-selected="true">
            <i class="ti-receipt mr-1"></i>
             Invoice
           </a>
         </li>
         <li class="nav-item m-1">
           <a class="nav-link border rounded text-center" data-toggle="tab" href="#Tabs_99-2" role="tab" aria-selected="true">
            <i class="ti-receipt mr-1"></i>
             Receipt
           </a>
         </li>
        
       </ul>
       
        <div class="tab-content">
          <div class="tab-pane fade show active" id="Tabs_99-1" role="tabpanel">
             <div class="row">
                <div class="col-lg-10 mx-auto">
                  <div class="table-responsive my-4">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col" class="font-weight-semiBold">Sl No</th>
                          <th scope="col">Invoice Number</th>
                          <th scope="col">Invoice Date</th>
                          <th scope="col">Invoice Amount</th>
                          <th scope="col">Category Name</th>

                          <th scope="col">Course Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php for($i=0;$i<count($invoiceList);$i++) {?>
                        <tr>
                                                    <td><?php echo $i+1;?></td>

                          <th scope="row" class="font-weight-semiBold"><?php echo $invoiceList[$i]->invoice_number;?></th>
                          <td><?php echo date('d-m-Y H:i:s',strtotime($invoiceList[$i]->date_time));?></td>
                          <td><?php echo $invoiceList[$i]->invoice_total;?></td>
                          <td><?php echo $invoiceList[$i]->categoryname;?></td>
                          <td><?php echo $invoiceList[$i]->coursename;?></td>

                        </tr>
                      <?php } ?>
                        
                      </tbody>
                    </table>
                  </div>        
                </div>      
              </div> <!-- END row-->
              </div>
          <div class="tab-pane fade" id="Tabs_99-2" role="tabpanel">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                  <div class="table-responsive my-4">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col" class="font-weight-semiBold">Sl No</th>
                          <th scope="col">Receipt Number</th>
                          <th scope="col">Receipt Date</th>
                          <th scope="col">Receipt Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php for($i=0;$i<count($receiptList);$i++) {?>
                        <tr>
                                                    <td><?php echo $i+1;?></td>

                          <th scope="row" class="font-weight-semiBold"><?php echo $receiptList[$i]->receipt_number;?></th>
                          <td><?php echo date('d-m-Y H:i:s',strtotime($receiptList[$i]->receipt_date));?></td>
                          <td><?php echo $receiptList[$i]->receipt_amount;?></td>

                        </tr>
                      <?php } ?>
                        
                      </tbody>
                    </table>
                  </div>        
                </div>      
              </div> <!-- END row-->
          </div>
         
        </div> <!-- END tab-content-->
      </div> <!-- END card -->
      </div> <!-- END col-12 -->    
    </div> <!-- END row-->
  </div> <!-- END container-->
</section> <!-- END section-->
    