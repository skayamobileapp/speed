<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    
    <!-- Title-->
    <title>Complete education theme for School, College, University, E-learning</title>
    
    <!-- SEO Meta-->
    <meta name="description" content="Education theme by EchoTheme">
    <meta name="keywords" content="HTML5 Education theme, responsive HTML5 theme, bootstrap 4, Clean Theme">
    <meta name="author" content="education">
    
    <!-- viewport scale-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
            
    <!-- Favicon and Apple Icons-->
    <link rel="icon" type="image/x-icon" href="<?php echo BASE_PATH;?>website/img/favicon/favicon.ico">
    <link rel="shortcut icon" href="<?php echo BASE_PATH;?>website/img/favicon/114x114.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo BASE_PATH;?>website/img/favicon/96x96.png">
    
    
    <!--Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700%7CWork+Sans:400,500">
    
    
    <!-- Icon fonts -->
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/fonts/fontawesome/css/all.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/fonts/themify-icons/css/themify-icons.css">
    
    
    <!-- stylesheet-->    
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/css/vendors.bundle.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/css/style.css">
    
  </head>
  
  <body>
   
  

  <nav class="ec-nav sticky-top bg-white">
    <header class="site-header bg-dark text-white-0_5" style="background-color: #f1f3f7!important;color:#2f3984 !important;">        
    <div class="container">
      <div class="row align-items-center justify-content-between mx-0">
        <ul class="list-inline d-none d-lg-block mb-0">
          <li class="list-inline-item mr-3">
           <div class="d-flex align-items-center">
            <i class="ti-email mr-2"></i>
            <a href="mailto:support@educati.com">support@educati.com</a>
           </div>
          </li>
          <li class="list-inline-item mr-3">
           <div class="d-flex align-items-center">
            <i class="ti-headphone mr-2"></i>
            <a href="tel:+8801740411513">+8801740411513</a>
           </div>
          </li>
        </ul>
      
        <ul class="list-inline mb-0">
          <li class="list-inline-item mr-0 p-md-3 p-2 border-right border-left border-white-0_1">
            <a href="#">Welcome <?php echo $this->session->userdata['student_name']; ?></a>
          </li>
          <li class="list-inline-item mr-0 p-md-3 p-2 border-right border-white-0_1">
            <a href="/index/logout">Logout</a>
          </li>
        </ul>
      </div> <!-- END END row-->
    </div> <!-- END container-->
  </header>

  <div class="container">
    <div class="navbar p-0 navbar-expand-lg">
     <div class="navbar-brand">
        <a class="logo-default" href="/"><img alt="" src="<?php echo BASE_PATH;?>website/images/SPEED.svg"></a>
      </div>
      <span aria-expanded="false" class="navbar-toggler ml-auto collapsed" data-target="#ec-nav__collapsible" data-toggle="collapse">
        <div class="hamburger hamburger--spin js-hamburger">
          <div class="hamburger-box">
            <div class="hamburger-inner"></div>
          </div>
        </div>
      </span>
       <div class="collapse navbar-collapse when-collapsed" id="ec-nav__collapsible">
        <ul class="nav navbar-nav ec-nav__navbar ml-auto">

            <li class="nav-item nav-item__has-megamenu megamenu-col-2">
              <a class="nav-link" href="index">Profile</a>
            </li>

             <li class="nav-item nav-item__has-megamenu megamenu-col-2">
              <a class="nav-link" href="course">Course  </a>
            </li>

            <li class="nav-item nav-item__has-megamenu megamenu-col-2">
              <a class="nav-link" href="#">Assessment</a>
            </li>

            <li class="nav-item nav-item__has-megamenu megamenu-col-2">
              <a class="nav-link" href="soa">Finance</a>
            </li>
  <li class="nav-item nav-item__has-megamenu megamenu-col-2">
              <a class="nav-link" href="#" data-toggle="dropdown">Portfolio</a>
            </li>


        </ul>
      </div>    
    </div>
  </div> <!-- END container-->    
  </nav> <!-- END ec-nav -->  