<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Course</h3>
        </div>

        
        <form id="form_semester" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Course Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language</label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language">
                    </div>
                </div>
            </div>

            <div class="row">

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Type <span class='error-text'>*</span></label>
                        <select name="id_course_type" id="id_course_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseTypeList))
                            {
                                foreach ($courseTypeList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Description <span class='error-text'>*</span></label>
                        <select name="id_course_description" id="id_course_description" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseDescriptionList))
                            {
                                foreach ($courseDescriptionList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Faculty Program <span class='error-text'>*</span></label>
                        <select name="id_faculty_program" id="id_faculty_program" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($facultyProgramList))
                            {
                                foreach ($facultyProgramList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code ." - ". $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Hours <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="credit_hours" name="credit_hours">
                    </div>
                </div>
                



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department</label>
                        <select name="id_department" id="id_department" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentList))
                            {
                                foreach ($departmentList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>  

            </div>

        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">

    $('select').select2();

    $(document).ready(function()
    {
        $("#form_semester").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                code:
                {
                    required: true
                },
                credit_hours:
                {
                    required: true
                },
                id_faculty_program:
                {
                    required: true
                },
                id_course_type:
                {
                    required: true
                },
                id_course_description:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                credit_hours:
                {
                    required: "<p class='error-text'>Credit Hours Required</p>",
                },
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                },
                id_course_type:
                {
                    required: "<p class='error-text'>Select Course Type</p>",
                },
                id_course_description:
                {
                    required: "<p class='error-text'>Select Course Description</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>