<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Publish Assesment Result Schedule</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Publish Assesment Result Schedule Details</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control" onchange="getCoursesByProgramIdNIntakeId()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control" onchange="getIntakeByProgramme(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" autocomplete="false" placeholder="Select" id="dummy_intake">
                        <span id="view_intake"></span>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" autocomplete="false" placeholder="Select" id="dummy_course">
                        <span id="view_course"></span>
                    </div>
                </div>
                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" autocomplete="false">
                    </div>
                </div>

                
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );



    function getIntakeByProgramme(id)
    {
        if(id != '')
        {

        $.get("/examination/publishAssesmentResultDate/getIntakeByProgramme/"+id,
            function(data, status)
            {
                $("#dummy_intake").hide();
                $("#view_intake").html(data);
            }
            );
        }
        else
        {
            $("#view_intake").hide();
            $("#dummy_intake").show();
            $("#id_intake").val('');
        }
    }


    function getCoursesByProgramIdNIntakeId()
    {

        var id_intake = $("#id_intake").val();
        var id_programme = $("#id_program").val();
        var id_semester = $("#id_semester").val();
        // alert(id_student);
        if (id_programme != '' && id_intake != '' && id_semester != '')
        {
            $.ajax(
            {
               url: '/examination/publishAssesmentResultDate/getCoursesByProgramIdNIntakeId',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_program': id_programme,
                'id_semester': id_semester,
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#dummy_course").hide();                
                  $("#view_course").show();                
                  $("#view_course").html(result);
                
               }
            }); 
        }
        else
        {
            $("#view_course").hide();
            $("#dummy_course").show();
            // $("#id_course").val('');
        }    
    }




    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_course: {
                    required: true
                },
                 date_time: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Result Exam Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>