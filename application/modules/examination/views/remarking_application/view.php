<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_receipt" action="" method="post">
        <div class="page-title clearfix">
            <h3>Add Student Remarking Remarks</h3>
        </div>

        <h4 class='sub-title'>Student Profile</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentData->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentData->email_id; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <!-- <dl>
                                <dt>Semester :</dt>
                                <dd><?php echo $studentData->semester_code . " - " . $studentData->semester_name ?></dd>
                            </dl> -->
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $studentData->programme_code . " - " . $studentData->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentData->intake_name; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>




    <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Student Marks</h4>
            
            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade Obtailned <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="grade_ob" name="grade_ob" readonly="readonly" value="<?php echo $marksEntry->grade;?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="marks_o" name="marks_o" readonly="readonly" value="<?php echo $marksEntry->total_obtained_marks;?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Result <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="result_o" name="result_o" value="<?php echo $marksEntry->result;?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">


                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="status_o" name="status_o" value="<?php
                            if($marksEntry->status == '1')
                            {
                                echo "Approved";
                            }
                            elseif($marksEntry->status == '2')
                            {
                                echo "Rejected";
                            }
                            else
                            {
                                echo "Pending";
                            }

                            ?>"
                             readonly="readonly">
                        </div>
                </div>

             <?php
            if($marksEntry->updated=='0')
                {
                ?>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Grade <span class='error-text'>*</span></label>
                        <select name="grade" id="grade" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($gradeList))
                            {
                                foreach ($gradeList as $record)
                                {?>
                             <option value="<?php echo $record->name;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

             <?php
            }
                ?>               

            </div>

        </div>

         <div class="button-block clearfix">
                <div class="bttn-group">

                 <?php
            if($marksEntry->updated=='0')
                {
                ?>

                     <button type="submit" class="btn btn-primary btn-lg">Submit</button>
             <?php
                }
                ?>

                    <a href="../remarkingList" class="btn btn-link">Back</a>
                </div>
            </div>


        
            
    

        <hr/>

        <div class="form-container">
            <h4 class="form-group-title">Student Marks Details</h4>

                <div class="row">
                 <div class="custom-table">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Sl. No </th>
                            <th>Component </th>
                            <th>Max. Marks </th>
                            <th>Previous Obtained Mark</th>
                            <th>Result</th>
                            <th>New Mmarks</th>
                            <!-- <th>Action</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $total_marks = 0;
                          if (!empty($marksEntryDetails)) {
                            $i=1;
                            foreach ($marksEntryDetails as $record) {
                          ?>
                              <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $record->component_code . " - " . $record->component_name; ?></td>
                                
                                <td><?php echo $record->max_marks ?></td>
                                <td><?php echo $record->obtained_marks ?></td>
                                <td><?php echo $record->result ?></td>
                                <td>
                                    <input type="number" name="obtained_marks[]">
                                </td>
                                <td><input type='checkbox' id='id_details[]' name='id_details[]' class='check' value='<?php echo $record->id; ?>'></td>

                              </tr>
                          <?php
                          $i++;
                          $total_marks = $total_marks + $record->obtained_marks;
                            }
                          }
                          ?>
                          <tr >
                        <td bgcolor="" colspan="3" style="text-align: right;"><b>Total : </b></td>
                        <td bgcolor=""><b><?php echo $total_marks ?></b></td>
                        <td bgcolor=""></td>
                      </tr>


                        </tbody>
                      </table>
                    </div>
            
                </div>

        </div>






           
        </form>
         



           




            
    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">

    function validateData()
    {
        if($('#form_main').valid())
        {
            $('#form_main').submit();

        }
    }

    $('select').select2();


     $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                grade: {
                    required: true
                }
            },
            messages: {
                grade: {
                    required: "<p class='error-text'>Select Grade</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>