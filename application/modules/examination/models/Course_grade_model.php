<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_grade_model extends CI_Model
{
    function courseGradeListSearch($data)
    {
        $this->db->select('cg.*, p.name as program, c.name as course, g.name as grade');
        $this->db->from('course_grade as cg');
        $this->db->join('programme as p', 'cg.id_program = p.id');
        $this->db->join('course as c', 'cg.id_course = c.id');
        $this->db->join('grade as g', 'cg.id_grade = g.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('cg.id_program', $data['id_programme']);
        }
        if ($data['id_course'] != '')
        {
            $this->db->where('cg.id_course', $data['id_course']);
        }
        if ($data['id_grade'] != '')
        {
            $this->db->where('cg.id_grade', $data['id_grade']);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function courseList()
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getProgrammeListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function gradeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getCourseGradeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('course_grade');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCourseGrade($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_grade', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCourseGradeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_grade', $data);
        return TRUE;
    }
}

