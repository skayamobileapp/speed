<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_calender_model extends CI_Model
{
    function examCalenderListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, s.name as semester_name, s.code as semester_code');
        $this->db->from('exam_calender as a');
        $this->db->join('semester as s', 'a.id_semester = s.id');
        if ($data['id_semester'] != '')
        {
            $this->db->where('a.id_semester', $data['id_semester']);
        }
        if ($data['activity'] != '')
        {
            $this->db->where('a.activity', $data['activity']);
        }
         $query = $this->db->get();
         $result = $query->result();  

         return $result;
    }

    function getExamCalender($id)
    {
        $this->db->select('*');
        $this->db->from('exam_calender');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addExamCalender($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_calender', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamCalender($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('exam_calender', $data);
        return TRUE;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}