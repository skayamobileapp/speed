<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_marks_entry_model extends CI_Model
{

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function gradeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }
    
   

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as program_code, p.name as program_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('scholarship_education_level as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


    function getStudentByStudent($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code, adv.ic_no, adv.name as advisor');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('scholarship_education_level as qs', 's.id_degree_type = qs.id');
        $this->db->join('staff as adv', 's.id_advisor = adv.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentIdForPassport($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function studentListSearch($applicantList)
    {
        $this->db->select('a.*, i.name as intake, p.code as program_code, p.name as program');
        $this->db->from('student as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        if($applicantList['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['applicant_status']) {
            $likeCriteria = "(a.applicant_status  LIKE '%" . $applicantList['applicant_status'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('a.applicant_status !=', 'Graduated');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    

    function semesterResultList($data)
    {
        $this->db->select('ssr.*, sem.name as semester_name, sem.code as semester_code, stu.full_name as student_name, stu.nric, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code');
        $this->db->from('student_semester_result as ssr');
        $this->db->join('student as stu', 'ssr.id_student = stu.id');
        $this->db->join('semester as sem', 'ssr.id_semester = sem.id');
        $this->db->join('intake as i', 'ssr.id_intake = i.id');
        $this->db->join('programme as p', 'ssr.id_program = p.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('ssr.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('ssr.id_intake', $data['id_intake']);
        }
        if ($data['id_semester'] != '')
        {
            $this->db->where('ssr.id_semester', $data['id_semester']);
        }
        if ($data['id_student'] != '')
        {
            // echo "<Pre>";print_r($status);exit();
            $this->db->where('ssr.id_student', $data['id_student']);
        }
        if ($data['status'] != '')
        {
            // echo "<Pre>";print_r($status);exit();
            $this->db->where('ssr.status', $data['status']);
        }
        $this->db->order_by("ssr.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgrammeListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getIntakeListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getSemesterListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getApprovalList($formData){



        $this->db->select('er.*,p.name as programme_name, p.code as programme_code,stu.full_name as student_name, stu.nric,sem.name as semester_name, sem.code as semester_code,i.year as intake_year, i.name as intake_name');
        $this->db->from('student_semester_course_details as er');
        $this->db->join('student_semester_result as ssr', 'ssr.id = er.id_student_semester_result');
        $this->db->join('programme as p', 'p.id = ssr.id_program');
        $this->db->join('student as stu', 'ssr.id_student = stu.id');
        $this->db->join('semester as sem', 'ssr.id_semester = sem.id');
        $this->db->join('intake as i', 'ssr.id_intake = i.id');

        if ($formData['id_program'] != '')
        {
            $this->db->where('ssr.id_program', $formData['id_program']);
        }
        if ($formData['id_intake'] != '')
        {
            $this->db->where('ssr.id_intake', $formData['id_intake']);
        }
        if ($formData['id_semester'] != '')
        {
            $this->db->where('ssr.id_semester', $formData['id_semester']);
        }
        if ($formData['id_student'] != '')
        {
            $this->db->where('ssr.id_student', $formData['id_student']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;

    }

    function searchStudentList($data)
    {
        $this->db->select('DISTINCT(stu.id) as id, stu.full_name, stu.nric');
        $this->db->from('exam_registration as er');
        $this->db->join('student as stu', 'er.id_student = stu.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_programme', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
        // if ($data['id_semester'] != '')
        // {
        //     $this->db->where('er.id_semester', $data['id_semester']);
        // }
        // if ($data['status'] != '')
        // {
        //     // echo "<Pre>";print_r($status);exit();
        //     $this->db->where('re.status', $data['status']);
        // }
        $this->db->order_by("stu.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function searchSemesterByStudentList($id_student)
    {
        $this->db->select('er.*, sem.id, sem.name as semester_name, sem.code as semester_code');
        $this->db->from('exam_registration as er');
        $this->db->join('semester as sem', 'er.id_semester = sem.id');
        $this->db->where('er.id_student', $id_student);
        $this->db->order_by("sem.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSemesterByStudentId($id_student)
    {
        $this->db->select('DISTINCT(sem.id) as id, sem.name as semester_name, sem.code as semester_code');
        $this->db->from('exam_registration as er');
        $this->db->join('semester as sem', 'er.id_semester = sem.id');
        $this->db->where('er.id_student', $id_student);
        $this->db->order_by("sem.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    
    function getStudentInfo($data)
    {
        // echo "<Pre>";print_r($data);exit;
        $this->db->select('stu.id as id_student, stu.full_name, stu.nric, sem.id as id_semester, sem.name as semester_name, sem.code as semester_code');
        $this->db->from('exam_registration as er');
        $this->db->join('student as stu', 'er.id_student = stu.id');
        $this->db->join('semester as sem', 'er.id_semester = sem.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_programme', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
        if ($data['id_semester'] != '')
        {
            $this->db->where('er.id_semester', $data['id_semester']);
        }
        if ($data['id_student'] != '')
        {
            // echo "<Pre>";print_r($status);exit();
            $this->db->where('er.id_student', $data['id_student']);
        }
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    
     function editStudentChangeStatus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_semester_course_details', $data);
        return TRUE;
    }


    function getCourse($data)
    {
        // echo "<Pre>";print_r($data);exit;
        $this->db->select('er.id as id_exam_registration, co.*');
        $this->db->from('exam_registration as er');
        $this->db->join('course as co', 'er.id_course = co.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_programme', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
        if ($data['id_semester'] != '')
        {
            $this->db->where('er.id_semester', $data['id_semester']);
        }
        if ($data['id_student'] != '')
        {
            $this->db->where('er.id_student', $data['id_student']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCourseForMarksEntry($data)
    {
        // echo "<Pre>";print_r($data);exit;
        $this->db->select('er.id as id_exam_registration, co.*');
        $this->db->from('exam_registration as er');
        $this->db->join('course as co', 'er.id_course = co.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_programme', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
        if ($data['id_semester'] != '')
        {
            $this->db->where('er.id_semester', $data['id_semester']);
        }
        if ($data['id_student'] != '')
        {
            $this->db->where('er.id_student', $data['id_student']);
        }
        $this->db->where('er.is_bulk_withdraw', '0');
        $this->db->where('er.is_semester_result', '0');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function addNewMarksEntry($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_semester_result', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewMarksEntryByCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_semester_course_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;        
    }


    function getMarksEntry($id)
    {
        $this->db->select('ssr.*, sem.name as semester_name, sem.code as semester_code, stu.full_name as student_name, stu.nric, stu.email_id, stu.phone, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code, gra.name as grade');
        $this->db->from('student_semester_result as ssr');
        $this->db->join('student as stu', 'ssr.id_student = stu.id');
        $this->db->join('intake as i', 'ssr.id_intake = i.id');
        $this->db->join('programme as p', 'ssr.id_program = p.id');
        $this->db->join('semester as sem', 'ssr.id_semester = sem.id','left');
        $this->db->join('grade as gra', 'ssr.grade = gra.id','left');
        $this->db->where('ssr.id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function getMarksEntryDetailsByMasterId($id_master)
    {
         $this->db->select('ssr.*, c.name as course_name, c.code as course_code, gra.name as grade');
        $this->db->from('student_semester_course_details as ssr');
        $this->db->join('course as c', 'ssr.id_course = c.id');
        $this->db->join('grade as gra', 'ssr.grade = gra.id');
        $this->db->where('ssr.id_student_semester_result', $id_master);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getGradeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function editStudentMarksEntry($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_semester_result', $data);
        return TRUE;
    }

    function updateExamRegistration($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('exam_registration', $data);
        return TRUE;
    }


    function getLandscapeListByProgramIdNIntakeId($data)
    {
        $this->db->select('a.*');
        $this->db->from('programme_landscape as a');
        $this->db->where('a.id_programme', $data['id_programme']);
        $this->db->where('a.id_intake', $data['id_intake']);
         $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function programmeLandscapeListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('programme_landscape as a');
        $this->db->where('a.status', $status);
         $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }































    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('programme_landscape as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function getProgramLandscapeCourses($data)
    {
        $this->db->select('DISTINCT(c.id) as id, c.name , c.code, a.id as id_course_registered');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('pl.status', '1');
        $this->db->where('a.id_intake', $data['id_intake']);
         $this->db->where('pl.id_programme', $data['id_programme']);
         $this->db->where('a.id_program_landscape', $data['id_programme_landscape']);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getStudentListForCourseRegisteredStudent($data)
    {
        $this->db->select('DISTINCT(s.id) as id, s.*, i.name as intake, p.code as program_code, p.name as program, ihs.id as id_course_registration');
        $this->db->from('course_registration as ihs');
        $this->db->join('student as s', 'ihs.id_student = s.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('mark_distribution as md', 'ihs.id_course_registered_landscape = md.id_course_registered');
        $this->db->where('md.id_program', $data['id_program']);
        $this->db->where('md.id_intake', $data['id_intake']);
        $this->db->where('md.id_course_registered', $data['id_course']);
        $this->db->where('md.id_programme_landscape', $data['id_programme_landscape']);
        $this->db->where('md.status', 1);
        $this->db->where('ihs.is_exam_registered !=', 0);
        $this->db->where('ihs.is_result_announced', 0);
        $this->db->where('ihs.is_bulk_withdraw', 0);
        $this->db->where('ihs.id_programme', $data['id_program']);
        $this->db->where('ihs.id_intake', $data['id_intake']);
        $this->db->where('ihs.id_course_registered_landscape', $data['id_course']);
        $query = $this->db->get();
        return $query->result();
    }

    

    function getMarkDistributionByProgNIntakeNIdCourseRegLandscape($id_program, $id_intake, $id_course_registered_landscape)
    {
        $this->db->select('md.*');
        $this->db->from('mark_distribution as md');
        // $this->db->join('staff as adv', 's.id_advisor = adv.id');
        $this->db->where('md.id_program', $id_program);
        $this->db->where('md.id_intake', $id_intake);
        $this->db->where('md.id_course_registered', $id_course_registered_landscape);
        $this->db->where('md.status', '1');
        $query = $this->db->get();
        $result = $query->row(); 
                
        // echo "<Pre>";print_r($result);exit;
        
        $details= array();
        if($result != '')
        {
            $details = $this->getMarkDistributionDetailsByIdMarkDistribution($result->id);
        }

        return $details;
    }

    function getMarkDistributionDetailsByIdMarkDistribution($id_mark_distribution)
    {
        $this->db->select('tctd.*, ec.name as component_name, ec.code as component_code,');
        $this->db->from('mark_distribution_details as tctd');
        $this->db->join('examination_components as ec', 'tctd.id_component = ec.id');
        $this->db->where('tctd.id_mark_distribution', $id_mark_distribution);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCourseRegisteredDetails($id_course_registered_landscape)
    {
        $this->db->select('md.*, c.name as course_name, c.code as course_code');
        $this->db->from('add_course_to_program_landscape as md');
        $this->db->join('course as c', 'md.id_course = c.id');
        $this->db->where('md.id', $id_course_registered_landscape);
        $this->db->order_by('md.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function addStudentMarksEntry($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_marks_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addStudentMarksEntryDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_marks_entry_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateCourseRegistration($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_registration', $data);
        return TRUE;
    }

    function combineDataAddMarkHistoryOfStudent($data,$id_course_registration,$id_student,$id_student_marks_entry)
    {
        $data['id_course_registration'] = $id_course_registration;
        $data['id_student'] = $id_student;
        $data['id_student_marks_entry'] = $id_student_marks_entry;
        $data['created_by'] = $data['updated_by'];
        unset($data['is_result_announced']);


        $inserted = $this->addMarkHistoryOfStudent($data);
        return $inserted;
    }

    function addMarkHistoryOfStudent($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_marks_entry_history', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getStudentList()
    {
        $this->db->select('er.*');
        $this->db->from('student as er');
        $this->db->where('er.applicant_status !=', 'Graduated');
        $this->db->order_by("er.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function marksEntryListSearch($data)
    {
        $this->db->select('ssr.*, stu.full_name as student_name, stu.nric, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code');
        $this->db->from('student_marks_entry as ssr');
        $this->db->join('student as stu', 'ssr.id_student = stu.id');
        $this->db->join('intake as i', 'ssr.id_intake = i.id');
        $this->db->join('programme as p', 'ssr.id_program = p.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('ssr.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('ssr.id_intake', $data['id_intake']);
        }
        if ($data['id_student'] != '')
        {
            // echo "<Pre>";print_r($status);exit();
            $this->db->where('ssr.id_student', $data['id_student']);
        }
        if ($data['status'] != '')
        {
            // echo "<Pre>";print_r($status);exit();
            $this->db->where('ssr.status', $data['status']);
        }
        $this->db->order_by("ssr.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function updateStudentMarksEntry($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_marks_entry', $data);
        return TRUE;
    }

    function getStudentMarksEntry($id)
    {
        $this->db->select('ssr.*, stu.full_name as student_name, stu.nric, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code');
        $this->db->from('student_marks_entry as ssr');
        $this->db->join('student as stu', 'ssr.id_student = stu.id');
        $this->db->join('intake as i', 'ssr.id_intake = i.id');
        $this->db->join('programme as p', 'ssr.id_program = p.id');
        $this->db->where('ssr.id', $id);

        $query = $this->db->get();
         $result = $query->row();  
         return $result;

    }

    function getStudentMarksEntryDetailsByMasterId($id_marks_entry)
    {

        $this->db->select('md.*, c.name as component_name, c.code as component_code');
        $this->db->from('student_marks_entry_details as md');
        $this->db->join('examination_components as c', 'md.id_component = c.id');
        $this->db->where('md.id_student_marks_entry', $id_marks_entry);
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }



















    function deleteTempAmountDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_receipt_details');
    }
}