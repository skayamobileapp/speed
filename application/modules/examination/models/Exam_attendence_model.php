<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_attendence_model extends CI_Model
{
    function examAttendenceList($data)
    {
        $this->db->select('a.*, ecl.name as location');
        $this->db->from('exam_attendence as a');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
        // $this->db->join('programme as p', 'a.id_programme = p.id');
        // $this->db->join('student as std', 'a.id_student = std.id');
        // $this->db->join('semester as sem', 'a.id_semester = sem.id');

        // if($data['first_name']) {
        //     $likeCriteria = "(std.full_name  LIKE '%" . $data['first_name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        //  if($data['email_id']) {
        //     $likeCriteria = "(std.email_id  LIKE '%" . $data['email_id'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        //  if($data['nric']) {
        //     $likeCriteria = "(std.nric  LIKE '%" . $data['nric'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        //  if($data['id_program']) {
        //     $likeCriteria = "(std.id_program  LIKE '%" . $data['id_program'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
         if($data['id_location'] != '')
         {
            $this->db->where('a.id_location', $data['id_location']);
         }
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }


    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
         $this->db->where('status', $status);
         $this->db->order_by("name", "ASC");
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }

    function searchStudentForAttendence($data)
    {
        // $date = 
        $this->db->select('a.*, stu.full_name, stu.nric, stu.email_id, stu.phone, stu.gender, p.name as program_name, p.code as program_code, i.name as intake, i.year as intake_year, s.code as semester_code, s.name as semester_name');
        $this->db->from('exam_register as a');
        $this->db->join('student as stu', 'a.id_student = stu.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('semester as s', 'a.id_semester = s.id');
        if($data['id_location'] != '')
         {
            $this->db->where('a.id_location', $data['id_location']);
         }
         if($data['id_semester'] != '')
         {
            $this->db->where('a.id_semester', $data['id_semester']);
         }
         if($data['id_program'] != '')
         {
            $this->db->where('a.id_programme', $data['id_program']);
         }
         if($data['id_intake'] != '')
         {
            $this->db->where('a.id_intake', $data['id_intake']);
         }
        $this->db->where('a.is_attended', '0');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function addNewExamAttendence($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_attendence', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamRegistration($id)
    {
        $this->db->select('a.*, ec.name as location');
        $this->db->from('exam_register as a');
        $this->db->join('exam_center_location as ec', 'a.id_location = ec.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result; 
    }

     function addExamAttendenceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_attendence_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;      
    }

     function editExamRegister($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('exam_register', $data);
        return TRUE;
    }


    function getExamAttendence($id)
    {
        $this->db->select('a.*, ecl.name as location');
        $this->db->from('exam_attendence as a');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $result = $query->row();
         return $result; 
    }

     function getExamAttendenceDetails($id)
    {
        $this->db->select('a.*, stu.full_name, stu.nric, stu.email_id, stu.phone, stu.gender, p.name as program_name, p.code as program_code, i.name as intake, i.year as intake_year, s.code as semester_code, s.name as semester_name');
        $this->db->from('exam_attendence_details as a');
        $this->db->join('student as stu', 'a.id_student = stu.id');
        $this->db->join('programme as p', 'stu.id_program = p.id');
        $this->db->join('intake as i', 'stu.id_intake = i.id');
        $this->db->join('semester as s', 'a.id_semester = s.id');
         $this->db->where('a.id_exam_attendence', $id);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }

    function examLocationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('exam_center_location as a');
        $this->db->where('a.status', $status);
        // $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examLocationList()
    {
        $this->db->select('a.*');
        $this->db->from('exam_center_location as a');
        // $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

}