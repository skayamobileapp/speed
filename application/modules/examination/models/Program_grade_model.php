<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Program_grade_model extends CI_Model
{
    function programGradeList()
    {
        $this->db->select('pg.*, in.name as intake, p.name as program, s.name as semester, g.name as grade');
        $this->db->from('program_grade as pg');
        $this->db->join('intake as in', 'pg.id_intake = in.id');
        $this->db->join('programme as p', 'pg.id_program = p.id');
        $this->db->join('semester as s', 'pg.id_semester = s.id');
        $this->db->join('grade as g', 'pg.id_grade = g.id');
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programGradeListSearch($formData)
    {
        $this->db->select('pg.*, p.name as program, g.name as grade');
        $this->db->from('program_grade as pg');
        // $this->db->join('intake as in', 'pg.id_intake = in.id');
        $this->db->join('programme as p', 'pg.id_program = p.id');
        // $this->db->join('semester as s', 'pg.id_semester = s.id');
        $this->db->join('grade as g', 'pg.id_grade = g.id');
        // if($formData['id_intake']) {
        //     $likeCriteria = "(pg.id_intake  LIKE '%" . $formData['id_intake'] . "%')";
        //     $this->db->where($likeCriteria);
        // }

        if($formData['id_program']) {
            $likeCriteria = "(pg.id_program  LIKE '%" . $formData['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }

        // if($formData['id_semester']) {
        //     $likeCriteria = "(pg.id_semester  LIKE '%" . $formData['id_semester'] . "%')";
        //     $this->db->where($likeCriteria);
        // }

        if($formData['id_grade']) {
            $likeCriteria = "(pg.id_grade  LIKE '%" . $formData['id_grade'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['description']) {
            $likeCriteria = "(pg.description  LIKE '%" . $formData['description'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getProgramGradeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('program_grade');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgramGrade($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_grade', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgramGradeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_grade', $data);
        return TRUE;
    }


    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }
}

