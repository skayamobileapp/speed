<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mode_of_program_model extends CI_Model
{
    function programModeList()
    {
        $this->db->select('*');
        $this->db->from('mode_of_program');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getProgramModeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('mode_of_program');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgramMode($data)
    {
        $this->db->trans_start();
        $this->db->insert('mode_of_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgramModeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('mode_of_program', $data);
        return TRUE;
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }
}

