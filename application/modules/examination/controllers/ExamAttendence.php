<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamAttendence extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_attendence_model');
        $this->load->model('setup/programme_model');
        $this->load->model('setup/intake_model');
        $this->load->model('admission/student_model');
        $this->load->model('registration/exam_center_model');
        $this->load->model('registration/course_registration_model');
        $this->isLoggedIn();
    }

    function comingSoon()
    {

        $this->global['pageTitle'] = 'Campus Management System : Coming Soon';
        $this->loadViews("exam_attendence/coming_soon", $this->global, NULL, NULL);
    }

    function list()
    {
        if ($this->checkAccess('exam_attendence.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location')); 
            $data['searchParam'] = $formData;

            $data['examAttendenceList'] = $this->exam_attendence_model->examAttendenceList($formData);
            $data['examLocationList'] = $this->exam_attendence_model->examLocationList();


            $this->global['pageTitle'] = 'Campus Management System : Exam Attendence';
            $this->loadViews("exam_attendence/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_attendence.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                // echo "<Pre>";print_r($this->input->post());exit();

                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $room = $this->security->xss_clean($this->input->post('room'));
                $id_exam = $this->security->xss_clean($this->input->post('id_exam'));


                 $master = array(
                    'room' => $room,
                    'id_location' => $id_location,
                    // 'id_intake' => $id_intake,
                    // 'id_programme' => $id_programme,
                    // 'id_student' => $id_student,
                    'status' => '1',
                    'created_by' => $user_id
                );

                $insert_master_id = $this->exam_attendence_model->addNewExamAttendence($master);


                // echo "<Pre>";print_r($id_exam);exit();
                if($insert_master_id)
                {
                    for($i=0;$i<count($id_exam);$i++)
                    {

                        $id_exam_register = $id_exam[$i];

                    $exam_register_details = $this->exam_attendence_model->getExamRegistration($id_exam_register);

                         $data = array(
                        'id_exam_attendence' => $insert_master_id,
                        'id_exam_register' => $id_exam_register,
                        'id_student' => $exam_register_details->id_student,
                        'id_semester' => $exam_register_details->id_semester,
                        'status' => '1',
                        'created_by' => $user_id
                    );

                    $insert_id = $this->exam_attendence_model->addExamAttendenceDetails($data);
                        if($insert_id)
                        {
                            $update = array(
                                'is_attended' => $insert_master_id
                            );

                         // echo "<Pre>";print_r($update);exit();
                            $updated = $this->exam_attendence_model->editExamRegister($update,$id_exam_register);
                        }
                    }
                }              

                redirect('/examination/examAttendence/list');
            }
            $data['programList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['semesterList'] = $this->exam_attendence_model->semesterListByStatus('1');
            $data['examLocationList'] = $this->exam_attendence_model->examLocationListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Exam Attendence';
            $this->loadViews("exam_attendence/add", $this->global, $data, NULL);
        }
    }


    function edit( $id)
    {
        if ($this->checkAccess('exam_attendence.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/examAttendence/list');
            }
            if($this->input->post())
            {

                redirect('/examination/examAttendence/list');
            }
          
            $data['examAttendence'] = $this->exam_attendence_model->getExamAttendence($id);
            $data['examAttendenceDetails'] = $this->exam_attendence_model->getExamAttendenceDetails($id);
            // $data['courseList'] = $this->exam_attendence_model->getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student,$id);
            
            // echo "<Pre>";print_r($data); exit();

            $this->global['pageTitle'] = 'Campus Management System : Edit Course Attendence';
            $this->loadViews("exam_attendence/edit", $this->global, $data, NULL);
        }
    }

    function searchStudentForAttendence()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $student_attendence_list = $this->exam_attendence_model->searchStudentForAttendence($tempData);
        // echo "<pre>";print_r($student_attendence_list);exit();


        // echo $temp_details;
        $table = "
        <br>
               ";


        if($student_attendence_list==NULL)
        {
             $table.= "


                <h4 class='sub-title'>No Students Exam Registered For Selected Search</h4>

                ";
        }
        else
        {



                $table.= "



        <script type='text/javascript'>
            $('select').select2();
        </script>


        <table class='table' id='list-table'>
                  <tr>
                    <th>Attended</th>
                    <th>Student Name</th>
                    <th>Gender</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Semester</th>
                    <th>Email</th>
                    <th>Phone</th>
                </tr>";
                
        
            

                    for($i=0;$i<count($student_attendence_list);$i++)
                    {
                    $id = $student_attendence_list[$i]->id;
                    $full_name = $student_attendence_list[$i]->full_name;
                    $nric = $student_attendence_list[$i]->nric;
                    $email_id = $student_attendence_list[$i]->email_id;
                    $phone = $student_attendence_list[$i]->phone;
                    $gender = $student_attendence_list[$i]->gender;
                    $program_name = $student_attendence_list[$i]->program_name;
                    $program_code = $student_attendence_list[$i]->program_code;
                    $intake = $student_attendence_list[$i]->intake;
                    $intake_year = $student_attendence_list[$i]->intake_year;
                    $semester_name = $student_attendence_list[$i]->semester_name;
                    $semester_code = $student_attendence_list[$i]->semester_code;

                        $table.= "
                        <tr>
                            <td>
                                <input type='checkbox' name='id_exam[]' id='id_exam' value='$id' checked>
                            </td>

                            <td>
                                $nric - $full_name
                            </td>
                            <td>
                                $gender
                            </td>
                            <td>
                                $program_code - $program_name
                            </td>
                            <td>
                                $intake_year - $intake
                            </td>
                            <td>
                                $semester_code - $semester_name
                            </td>
                            <td>
                                $email_id
                            </td>
                            <td>
                                $phone
                            </td>
                        </tr>";
                    }
                $table.= "</table>";

        }
        
        echo $table;
    }
}
