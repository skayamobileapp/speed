<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PaymentType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_type_model');
        $this->isLoggedIn();
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Speed Management System : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {
        if ($this->checkAccess('payment_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['paymentTypeList'] = $this->payment_type_model->paymentTypeList();

            $this->global['pageTitle'] = 'Speed Management System : Payment Type List';
            $this->global['pageCode'] = 'payment_type.list';
            
            $this->loadViews("payment_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('payment_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $description_optional_language = $this->security->xss_clean($this->input->post('description_optional_language'));
                $payment_group = $this->security->xss_clean($this->input->post('payment_group'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'description' => $description,
                    'description_optional_language' => $description_optional_language,
                    'payment_group' => $payment_group,
                    'status' => $status
                );

                $result = $this->payment_type_model->addNewPaymentType($data);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'Payment Type added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Payment Type add failed');
                }
                redirect('/finance/paymentType/list');
            }
            //print_r($data['stateList']);exit;
            $this->global['pageTitle'] = 'Speed Management System : Add Payment Type';
            $this->global['pageCode'] = 'payment_type.add';

            $this->loadViews("payment_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('payment_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/paymentType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $description_optional_language = $this->security->xss_clean($this->input->post('description_optional_language'));
                $payment_group = $this->security->xss_clean($this->input->post('payment_group'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'description' => $description,
                    'description_optional_language' => $description_optional_language,
                    'payment_group' => $payment_group,
                    'status' => $status
                );
                
                $result = $this->payment_type_model->editPaymentType($data,$id);
                redirect('/finance/paymentType/list');
            }
            $data['paymentType'] = $this->payment_type_model->getPaymentType($id);

            $this->global['pageTitle'] = 'Speed Management System : Edit Payment Type';
            $this->global['pageCode'] = 'payment_type.edit';

            $this->loadViews("payment_type/edit", $this->global, $data, NULL);
        }
    }
}
