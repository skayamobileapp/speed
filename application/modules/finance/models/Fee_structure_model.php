<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fee_structure_model extends CI_Model
{
    function feeSetupListByStatus($status)
    {
        $this->db->select('ac.*');
        $this->db->from('fee_setup as ac');
        $this->db->where('ac.status', $status);
        $this->db->order_by("ac.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('ac.*');
        $this->db->from('course as ac');
        $this->db->where('ac.status', $status);
        $this->db->order_by("ac.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function categoryListByStatus($status)
    {
        $this->db->select('ac.*');
        $this->db->from('category as ac');
        $this->db->where('ac.status', $status);
        $this->db->order_by("ac.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function feeStructureListSearch($data)
    {
        $this->db->select('fc.*, c.name as course_name, c.code as course_code, cat.name as category_name');
        $this->db->from('fee_structure_main as fc');
        $this->db->join('category as cat', 'fc.id_category = cat.id');
        $this->db->join('course as c', 'fc.id_course = c.id');
        if ($data['id_course'] != '')
        {
            $this->db->where('fc.id_course', $data['id_course']);
        }
        if ($data['id_category'] != '')
        {
            $this->db->where('fc.id_category', $data['id_category']);
        }
        // $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addFeeStructureMain($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure_main', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getFeeeStructure($id)
    {
        $this->db->select('tfs.*');
        $this->db->from('fee_structure_main as tfs');
        $this->db->where("tfs.id", $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function saveTempFeeStructure($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_fee_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function deleteTempFeeStructureDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_fee_structure');
        return TRUE;
    }

    function getTempFeeStructureDetailsByIdSession($id_session)
    {
        $this->db->select('tfs.*, fs.name as fee_item_name, fs.code as fee_item_code');
        $this->db->from('temp_fee_structure as tfs');
        $this->db->join('fee_setup as fs', 'tfs.id_fee_item = fs.id');
        $this->db->where("tfs.id_session", $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function feeSetupList()
    {
        $this->db->select('fs.*, fc.name as fee_category, ac.code as account_code, ac.type as account_type');
        $this->db->from('fee_setup as fs');
        $this->db->join('fee_category as fc', 'fs.id_fee_category = fc.id');
        $this->db->join('financial_account_code as ac', 'fs.account_code = ac.id','left');
        $this->db->order_by("fs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFeeSetup($id)
    {
       $this->db->select('fs.*, fc.name as fee_category');
        $this->db->from('fee_setup as fs');
        $this->db->join('fee_category as fc', 'fs.id_fee_category = fc.id');
        // $this->db->join('account_code as ac', 'fs.id_account_code = ac.code');
        $this->db->where('fs.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editFeeSetup($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('fee_setup', $data);
        return TRUE;
    }

    function financialAccountCodeListByStatus($status)
    {
        // $query = 'SELECT ac.* FROM  account_code ac';
        // $sql = $this->db->query($query);
        // $result = $sql->result();

        // $this->db->select('ac.*, CONCAT(ac.code, '.', ac.name) AS code_name');
        $this->db->select('ac.*');
        $this->db->from('financial_account_code as ac');
        $this->db->where('ac.status', '1');
        $this->db->order_by("ac.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCourseByCategory($id_category)
    {
        $this->db->select('ac.*');
        $this->db->from('course as ac');
        $this->db->where('ac.status', '1');
        $this->db->where('ac.id_category', $id_category);
        $this->db->order_by("ac.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getTempFeeStructureDetailsByIdSessionToMove($id_session)
    {
        $this->db->select('tfs.*');
        $this->db->from('temp_fee_structure as tfs');
        $this->db->join('fee_setup as fs', 'tfs.id_fee_item = fs.id');
        $this->db->where("tfs.id_session", $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function moveTempToDetails($data)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempFeeStructureDetailsByIdSessionToMove($id_session);

        foreach ($temp_details as $detail)
        {
            unset($detail->id);
            unset($detail->id_session);

            $detail->id_category = $data['id_category'];
            $detail->id_course = $data['id_course'];
            $detail->id_fee_structure = $data['id'];

            $temp_details = $this->addFeeStructureDetails($detail);
        }
        $temp_deleted = $this->deleteTempFeeStructureDetailsByIdSession($id_session);

    }

    function addFeeStructureDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function deleteTempFeeStructureDetailsByIdSession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_fee_structure');
        return TRUE;
    }

    function editFeeStructure($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('fee_structure_main', $data);
        return TRUE;
    }

    function deleteFeeStructureDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fee_structure');
        return TRUE;
    }

    function getFeeStructureDetailsByIdFeeStructure($id_fee_structure)
    {
        $this->db->select('tfs.*, fs.name as fee_item_name, fs.code as fee_item_code');
        $this->db->from('fee_structure as tfs');
        $this->db->join('fee_setup as fs', 'tfs.id_fee_item = fs.id');
        $this->db->where("tfs.id_fee_structure", $id_fee_structure);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}

