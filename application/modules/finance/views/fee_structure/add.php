<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Fee Structure</h3>
            </div>


    <form id="form_main" action="" method="post">

        <div class="form-container">

            <h4 class="form-group-title">Fee Structure Details</h4>

        
                
                <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Category <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_category" id="id_category" class="form-control" onchange="getCourseByCategory(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($categoryList))
                            {
                                foreach ($categoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>




                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Module <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <span id="view_course">
                                <select class="form-control" id='id_course' name='id_course'>
                                    <option value=''></option>
                                  </select>
                            </span>
                          </div>
                        </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Total Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                          <input type="number" class="form-control" id="total_amount" name="total_amount" placeholder="Amount" readonly="">
                          </div>
                        </div>
                    </div>

                    
                </div>




                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <a href="list" class="btn btn-link">Cancel</a>
                  </div>

                </div> 

            </div> 

    </form>


    <br>




    <form id="form_detail" action="" method="post">

        <div class="form-container">

            <h4 class="form-group-title">FFee Structure details</h4>

        
                
                <div class="row">


                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Fee Item</label>
                          <div class="col-sm-8">
                            <select name="id_fee_item" id="id_fee_item" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($feeSetupList))
                            {
                                foreach ($feeSetupList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                      </div>   

                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                          <input type="number" class="form-control" id="details_amount" name="details_amount" placeholder="Amount">
                          </div>
                        </div>
                      </div>

                  </div>       


                    
                  <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="button" class="btn btn-primary" onclick="saveTempFeeStructure()">Save</button>
                        <!-- <button type="button" class="btn btn-primary" onclick="generate()">Save</button> -->
                    </div>
                  </div> 





            </div> 


            <div id="view_fee_details">
            
            </div>



    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    $('select').select2();


    function reloadPage()
    {
      window.location.reload();
    }

    function getCourseByCategory(id)
    {
        $.get("/finance/feeStructure/getCourseByCategory/"+id, function(data, status){
       
            $("#view_course").html(data);
        });
    }

    function generate()
    {
       var tempPR = {};
        tempPR['id_student'] = 1;
        tempPR['id_category'] = 1;
        tempPR['id_course'] = 1;

            $.ajax(
            {
               url: '/profile/dashboard/invoice',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_fee_details").html(result);
               }
            });
    }



    function saveTempFeeStructure()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#details_amount").val();
        // tempPR['status'] = $("#supervisor_status").val();
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/finance/feeStructure/saveTempFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_fee_details").html(result);

                var total_master = $("#calculated_total_amount").val();
                $("#total_amount").val(total_master);
               }
            });
        }
    }


    function deleteTempFeeStructureDetails(id) {
        // alert(id);
         $.ajax(
            {
               url: '/finance/feeStructure/deleteTempFeeStructureDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_fee_details").html(result);
               }
            });
    }


    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                details_amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                details_amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_category: {
                    required: true
                },
                id_course: {
                    required: true
                },
                total_amount: {
                    required: true
                }
            },
            messages: {
                id_category: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Module</p>",
                },
                total_amount: {
                    required: "<p class='error-text'>Add Details For Total Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>