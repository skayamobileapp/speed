<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Currency Rate Setup List</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Currency</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">

                  <div class="col-lg-6">
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Code / Name</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $searchParam['name'] ?>">
                      </div>
                    </div>
                  </div>


                </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Code</th>
            <th>Description</th>
            <th>Symbol Prefix</th>
            <th>Decimal Places</th>
            <th>Default Currency</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($currencyList)) {
            $i=1;
            foreach ($currencyList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->prefix ?></td>
                <td><?php echo $record->decimal_place ?></td>
                <td><?php if( $record->default == '1')
                {
                  echo "Default";
                }
                else
                {
                  echo "";
                } 
                ?></td>
                <td style="text-align: center;"><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td style="text-align: center;">
                    <a href="<?php echo 'edit/'.$record->id; ?>" title="Add Details">Add Details</a> 
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  $('select').select2();

  function clearSearchForm()
  {
    window.location.reload();
  }

  function makeDefault(id)
  {
     $.ajax(
        {
           url: '/finance/currency/makeDefault/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            window.location.reload();
           }
        });
  }
</script>