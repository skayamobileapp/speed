<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Bank Registration</h3>
            </div>


    <form id="form_main" action="" method="post">

        <div class="form-container">

            <h4 class="form-group-title">Bank Registration Details</h4>

        
                
                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Swift Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code">
                        </div>
                      </div>
                    </div>
                    
                </div>


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Bank ID <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="bank_id" name="bank_id" placeholder="Bank ID">
                        </div>
                      </div>
                    </div>



                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Country</label>
                          <div class="col-sm-8">
                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>                    

                </div>



                <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">State <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <span id="view_state">
                                <select class="form-control" id='id_state' name='id_state'>
                                    <option value=''></option>
                                  </select>
                            </span>
                          </div>
                        </div>
                    </div>                    

                

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Land Mark <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="landmark" name="landmark" placeholder="Landmark">
                        </div>
                      </div>
                    </div>


                </div>

                

                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">City <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="city" name="city" placeholder="City">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Zipcode <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode">
                        </div>
                      </div>
                    </div>


                </div>



                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Account No. <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="account_no" name="account_no" placeholder="Ex. 88888888">
                        </div>
                      </div>
                    </div>

               
                    
                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>

                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <a href="list" class="btn btn-link">Cancel</a>
                  </div>

                </div> 

            </div> 

    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function reloadPage()
    {
      window.location.reload();
    }

    $('select').select2();



    function getStateByCountry(id)
    {
        $.get("/finance/bankRegistration/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }



    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                bank_id: {
                    required: true
                },
                address: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                landmark: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                cr_fund: {
                    required: true
                },
                cr_department: {
                    required: true
                },
                cr_activity: {
                    required: true
                },
                cr_account: {
                    required: true
                },
                account_no: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Swift Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Bank Name Required</p>",
                },
                bank_id: {
                    required: "<p class='error-text'>Bank ID Required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                landmark: {
                    required: "<p class='error-text'>Enter Landmark</p>",
                },
                city: {
                    required: "<p class='error-text'>Enter City Name</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                cr_fund: {
                    required: "<p class='error-text'>Select Credit Fund Code</p>",
                },
                cr_department: {
                    required: "<p class='error-text'>Select Credit Department Code</p>",
                },
                cr_activity: {
                    required: "<p class='error-text'>Select Credit Activity Code</p>",
                },
                cr_account: {
                    required: "<p class='error-text'>Select Credit Account Code</p>",
                },
                account_no: {
                    required: "<p class='error-text'>Account No Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>