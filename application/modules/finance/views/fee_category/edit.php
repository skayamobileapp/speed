<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Edit Fee Category</h3>
            </div>


    <form id="form_main" action="" method="post">

        <div class="form-container">

            <h4 class="form-group-title">Fee Category Details</h4>

        
                
                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo $feeCategory->code ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $feeCategory->name ?>">
                        </div>
                      </div>
                    </div>

                    
                </div>


                <div class="row">
                  
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name Optional Language</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" placeholder="" value="<?php echo $feeCategory->name_optional_language ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Fee Group <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="fee_group" id="fee_group" class="form-control">
                                <option value="">Select</option>
                                <option value="<?php echo "Rental";  ?>"
                                    <?php 
                                    if ($feeCategory->fee_group == 'Rental')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo "Rental";  ?>
                                </option>

                                <option value="<?php echo "Statement Of Account";?>"
                                    <?php 
                                    if ($feeCategory->fee_group == 'Statement Of Account')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo "Statement Of Account";  ?>
                                </option>
                          </select>
                          </div>
                        </div>
                    </div>      



                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Sequence <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" id="sequence" name="sequence" placeholder="1,2,3 ..." value="<?php echo $feeCategory->sequence ?>">
                        </div>
                      </div>
                    </div>

                    
                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($feeCategory->status == 1){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($feeCategory->status == 0){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>



                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <a href="../list" class="btn btn-link">Back</a>
                  </div>

                </div> 

            </div> 

    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();
    
    
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                status: {
                    required: true
                },
                fee_group: {
                    required: true
                },
                sequence: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Category Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                fee_group: {
                    required: "<p class='error-text'>Select Fee Group</p>",
                },
                sequence: {
                    required: "<p class='error-text'>Enter Sequence</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>