<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Edit Currency</h3>
            </div>


    <form id="form_main" action="" method="post">

        <div class="form-container">

            <h4 class="form-group-title">Currency Details</h4>

        
                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo $currency->code ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $currency->name ?>">
                        </div>
                      </div>
                    </div>

                    
                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Default Language</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" placeholder=" Default Language Name" value="<?php echo $currency->name_optional_language ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Symbol Prefix <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="prefix" name="prefix" placeholder="Symbol Prefix" value="<?php echo $currency->prefix ?>">
                        </div>
                      </div>
                    </div>

                    

                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Symbol Syffix <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="suffix" name="suffix" placeholder="Symbol Suffix" value="<?php echo $currency->suffix ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Decimal Places <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="decimal_place" name="decimal_place" placeholder="Ex. 2 (Like .00)" value="<?php echo $currency->decimal_place ?>">
                        </div>
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($currency->status == 1){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($currency->status == 0){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <a href="../list" class="btn btn-link">Back</a>
                  </div>

                </div> 

            </div> 

    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                prefix: {
                    required: true
                },
                suffix: {
                    required: true
                },
                decimal_place: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                prefix: {
                    required: "<p class='error-text'>Prefix Required</p>",
                },
                suffix: {
                    required: "<p class='error-text'>Suffix Required</p>",
                },
                decimal_place: {
                    required: "<p class='error-text'>Decimal Places Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>