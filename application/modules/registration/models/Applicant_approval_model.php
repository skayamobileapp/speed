<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Applicant_approval_model extends CI_Model
{
   function applicantList($applicantList)
    {
        $status = 'Draft';
        $this->db->select('a.*');
        $this->db->from('applicant as a');

        if($applicantList['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        //  if($applicantList['last_name']) {
        //     $likeCriteria = "(a.last_name  LIKE '%" . $applicantList['last_name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if($applicantList['applicant_status']) {
        //     $likeCriteria = "(a.applicant_status  LIKE '%" . $applicantList['applicant_status'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        $this->db->where('a.applicant_status', $status);
        $this->db->where('a.email_verified', '1');
        $this->db->where('a.is_updated', '1');
        $this->db->where('a.is_submitted', '1');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function applicantListForApproval($applicantList)
    {
        $status = 'Draft';
        $this->db->select('a.*');
        $this->db->from('applicant as a');

        if($applicantList['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        $likeCriteria = "(a.is_sibbling_discount  != '0' and a.is_employee_discount  != '0')";
        $this->db->where($likeCriteria);
        $this->db->where('a.applicant_status', $status);
        $this->db->where('a.is_updated', '1');
        $this->db->where('a.email_verified', '1');
        $this->db->where('a.is_submitted', '1');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function addNewStudent($id)
    {
        $query = $this->db->select('*')->from('applicant')->where('id',$id)->get();
        foreach ($query->result() as $row)
        {
            // echo "<Pre>";print_r($row);exit();
            unset($row->id);
            unset($row->is_sibbling_discount);
            unset($row->is_employee_discount);
            unset($row->is_alumni_discount);
            unset($row->is_apeal_applied);
            unset($row->id_apeal_status);
            unset($row->approved_by);
            unset($row->email_verified);
            unset($row->is_updated);
            unset($row->is_submitted);
            unset($row->submitted_date);
            unset($row->apel_reject_reason);
            unset($row->created_dt_tm);
            
            
            
            $row->status = '1';
            $row->current_semester = '1';
            $row->applicant_status = 'Approved';
            $row->id_applicant = $id;
            $row->phd_duration = 1;
            $row->current_deliverable = date('M-Y');

            $this->db->insert('student',$row);
            $insert_id = $this->db->insert_id();

            if($insert_id)
            {
                $this->addNewDeliverableHistory($insert_id);
            }
        }

        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewDeliverableHistory($id_student)
    {
        $id_user = $this->session->userId;

        $data['id_student'] = $id_student;
        $data['old_deliverable_term'] = '';
        $data['new_deliverable_term'] = date('M-Y');
        $data['created_by'] = $id_user;

        $this->db->insert('student_deliverable_history',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addStudentProfileDetail($id)
    {
        $data = ['id_student'=>$id];

        $this->db->insert('profile_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

        // $this->db->insert('visa_details', $data);
    }


    function getApplicantDetails($id)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant', $data);
        return TRUE;
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }

    function createNewMainInvoiceForStudent($id)
    {
        $id_student = $id;
        $user_id = $this->session->userId;

        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $applicant_data = $query->row();

        // echo "<Pre>";print_r($applicant_data);exit;

        $id_applicant = $applicant_data->id;
        $id_program = $applicant_data->id_program;
        $id_intake = $applicant_data->id_intake;
        $id_program_scheme = $applicant_data->id_program_scheme;
        $is_sibbling_discount = $applicant_data->is_sibbling_discount;
        $is_employee_discount = $applicant_data->is_employee_discount;


        // echo "<Pre>";print_r($applicant_data);exit;

        $invoice_number = $this->generateMainInvoiceNumber();

        $invoice['invoice_number'] = $invoice_number;
        $invoice['type'] = 'Applicant';
        $invoice['remarks'] = 'Applicant Payable Amount';
        $invoice['id_application'] = '1';
        $invoice['id_student'] = $id_student;
        $invoice['id_program'] = $id_program;
        $invoice['id_intake'] = $id_intake;
        $invoice['total_amount'] = '0';
        $invoice['balance_amount'] = '0';
        $invoice['paid_amount'] = '0';
        $invoice['status'] = '1';
        $invoice['created_by'] = $user_id;

        // $detail_data = $this->getFeeStructure('13','5');
        $detail_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);
        
        // echo "<Pre>";print_r($detail_data);exit;


        $inserted_id = $this->addNewMainInvoice($invoice);
        $total_amount = 0;
        $total_discount_amount = 0;
        $sibling_discount_amount = 0;
        $employee_discount_amount = 0;
        foreach ($detail_data as $fee_structure)
        {
            $data = array(
                    'id_main_invoice' => $inserted_id,
                    'id_fee_item' => $fee_structure->id_fee_item,
                    'amount' => $fee_structure->amount,
                    'status' => '1',
                    'created_by' => $user_id
                );
            $total_amount = $total_amount + $fee_structure->amount;
            $this->addNewMainInvoiceDetails($data);

        }

        $total_invoice_amount = $total_amount;

        if($is_sibbling_discount == '1')
        {
            $this->db->select('*');
            $this->db->from('sibbling_discount');
            // $likeCriteria = "(date(start_date)  <= '" . date('Y-m-d') . "')";
            // $this->db->where($likeCriteria);
            // $likeCriteria = "(date(end_date)  <= '" . date('Y-m-d') . "')";
            // $this->db->where($likeCriteria);

        //     $SQL = "Select * From sibbling_discount where date(start_date) <= 'getdate()' and date(end_date) >= 'getdate()' order by id DESC limit 0,1";
        //     $query = $this->db->query($SQL);
           
        // echo "<Pre>";print_r($query->row());exit;

            $this->db->where('status', '1');
            $query = $this->db->get();
            $sibling_discount_data = $query->row();

            if($sibling_discount_data)
            {
                $amount = $sibling_discount_data->amount;
                $id_discount = $sibling_discount_data->id;

                $sibling_insert = array(
                    'id_main_invoice' => $inserted_id,
                    'id_student' => $id_student,
                    'name' => 'Sibbling Discount Applied',
                    'amount' => $amount,
                    'id_reference' => $id_discount,
                );
                $discount_inserted_id = $this->addNewMainInvoiceDiscountDetail($sibling_insert);
                if($discount_inserted_id)
                {
                    $total_amount = $total_amount - $sibling_discount_data->amount;
                    $sibling_discount_amount = $sibling_discount_data->amount;
                }
            }

        // echo "<Pre>";print_r($amount);exit;
        }

        if($is_employee_discount == '1')
        {

            $this->db->select('*');
            $this->db->from('employee_discount');
            // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
            // $this->db->where('date(start_date) >=', date('Y-m-d'));
            // $this->db->where('date(end_date) <=', date('Y-m-d'));
            $this->db->where('status', '1');
            $query = $this->db->get();
            $employee_discount_data = $query->row();
            if($employee_discount_data)
            {
                $amount = $employee_discount_data->amount;
                $id_discount = $employee_discount_data->id;

                $employee_insert = array(
                    'id_main_invoice' => $inserted_id,
                    'id_student' => $id_student,
                    'name' => 'Employee Discount Applied',
                    'amount' => $amount,
                    'id_reference' => $id_discount,
                );
                $sibbling_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
                if($sibbling_inserted_id)
                {
                    $total_amount = $total_amount - $employee_discount_data->amount;
                    $employee_discount_amount = $employee_discount_data->amount;
                }
            }
        }


        $total_discount_amount = $sibling_discount_amount + $employee_discount_amount;
        // $total_amount = number_format($total_amount, 2, '.', ',');
        // echo "<Pre>";print_r($total_amount);exit;

        $invoice_update['total_amount'] = $total_amount;
        $invoice_update['balance_amount'] = $total_amount;
        $invoice_update['invoice_total'] = $total_invoice_amount;
        $invoice_update['total_discount'] = $total_discount_amount;
        $invoice_update['paid_amount'] = '0';
        // $invoice_update['inserted_id'] = $inserted_id;
        // echo "<Pre>";print_r($invoice_update);exit;
        $this->editMainInvoice($invoice_update,$inserted_id);
        return TRUE;
        
    }

    function getFeeStructure($id_programme,$id_intake,$id_program_scheme)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_scheme', $id_program_scheme);
        $query = $this->db->get();
        $fee_structure = $query->result();
        // $detail_data = $this->getFeeStructureDetails($fee_structure->id);
        return $fee_structure;
    }

    function getFeeStructureDetails($id_fee_structure)
    {
        $this->db->select('tfsd.*, fs.name as fee_structure, fm.name as frequency_mode');
        $this->db->from('fee_structure_details as tfsd');
        $this->db->join('fee_setup as fs', 'tfsd.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'tfsd.id_frequency_mode = fm.id');   
        $this->db->where('tfsd.id_fee_structure', $id_fee_structure);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function addNewMainInvoiceDiscountDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_discount_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function getApplicantSibblingDiscountDetails($id_applicant)
    {
        $this->db->select('ahsd.*, usr.name as user_name');
        $this->db->from('applicant_has_sibbling_discount as ahsd');
        $this->db->join('tbl_users as usr', 'ahsd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantEmployeeDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_employee_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

     function getApplicantAlumniDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_alumni_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }    

    function raceList()
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionList()
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }
    
}