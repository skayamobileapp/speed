<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approval List Credit Transfer</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Credit Transfer</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Application ID</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester</label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control selitemIcon">
                            <option value="">Select</option>
                              <?php
                            if (!empty($semesterList)) {
                              foreach ($semesterList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_semester']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo $record->code . " - " . $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Excemption Type</label>
                      <div class="col-sm-8">
                        <select name="application_type" id="application_type" class="form-control">
                          <option value=''>Select</option>
                            <option value='CREDIT TRANSFER' <?php if($searchParam['application_type'] =='CREDIT TRANSFER')
                              { echo "selected=selected";} ?> >CREDIT TRANSFER
                            </option>
                            <option value='CREDIT EXCEMPTION' <?php if($searchParam['application_type'] =='CREDIT EXCEMPTION')
                            { echo "selected=selected";} ?> >CREDIT EXCEMPTION
                            </option>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button href="list" class="btn btn-link">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Application ID</th>
            <th>Application Date</th>
            <th>Application Type</th>
            <th>Institution Type</th>
            <th>Student ID</th>
            <th>Student Name</th>
            <th>Program</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($creditTransferList))
          {
            $i=1;
            foreach ($creditTransferList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->application_id ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>
                <td><?php echo $record->application_type ?></td>
                <td><?php echo $record->institution_type ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->full_name ?></td>
                <td><?php echo $record->program_code . "" . $record->program_name ?></td>
                <td style="text-align: center;"><?php if( $record->status == '0')
                {
                  echo "Pending";
                }
                elseif( $record->status == '1')
                {
                  echo "Approved";
                } 
                elseif( $record->status == '2')
                {
                  echo "Rejected";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'view/' . $record->id; ?>" title="Approve">Approve</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    function clearSearchForm()
    {
      window.location.reload();
    }
    $('select').select2();
</script>