<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Attendence Setup</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Attendence Setup Details</h4>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code. " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Scheme <span class='error-text'>*</span></label>
                        <select name="program_scheme" id="program_scheme" class="form-control">
                            <option value="">Select</option>
                            <option value="Full Time">Full Time</option>
                        </select>
                    </div>
                </div>
                
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Warning Requirement (%) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_warning" name="min_warning" min="1" max="100">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Warning Requirement (%) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_warning" name="max_warning" min="1" max="100">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Is Barring Applicable <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_barring_applicable" id="is_barring_applicable" value="1" checked="checked" onclick="showBarring()"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_barring_applicable" id="is_barring_applicable" value="0" onclick="hideBarring()"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>

            </div>


            <div class="row">

                
                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

                <div id="view_barring">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Min. Barring Requirement (%) <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="min_barring" name="min_barring" min="1" max="100">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Max. Barring Requirement (%) <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="max_barring" name="max_barring" min="1" max="100">
                        </div>
                    </div>


                </div>


            </div>

            

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();



     function showBarring(){
            $("#view_barring").show();
    }

    function hideBarring(){
            $("#view_barring").hide();
    }


    function getStateByCountry(id)
    {

        $.get("/registration/examCenter/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }




    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                id_program: {
                    required: true
                },
                 date_time: {
                    required: true
                },
                 program_scheme: {
                    required: true
                },
                 min_warning: {
                    required: true
                },
                 max_warning: {
                    required: true
                },
                 is_barring_applicable: {
                    required: true
                },
                 min_barring: {
                    required: true
                },
                max_barring : {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Effective Date</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                min_warning: {
                    required: "<p class='error-text'>Min. Warning Required</p>",
                },
                max_warning: {
                    required: "<p class='error-text'>Max. Warning Required</p>",
                },
                is_barring_applicable: {
                    required: "<p class='error-text'>Select Is Barring Applicable</p>",
                },
                min_barring: {
                    required: "<p class='error-text'>Min. Barring Required</p>",
                },
                max_barring: {
                    required: "<p class='error-text'>Max. Barring Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

      $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );
</script>