<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssignExamCenterToSemester extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('assign_exam_center_to_semester_model');
        $this->load->model('setup/semester_model');
        $this->load->model('setup/programme_model');
        $this->load->model('setup/intake_model');
        $this->load->model('registration/exam_center_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('assign_exam_center_to_semester.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $data['examCenterList'] = $this->assign_exam_center_to_semester_model->assignedExamCenterList();
            
            // $name = $this->security->xss_clean($this->input->post('name'));

            // $data['searchName'] = $name;
            // $data['examCenterList'] = $this->exam_center_model->examCenterListSearch();


            $this->global['pageTitle'] = 'Campus Management System : Assigned Exam Centers To Semester';
            $this->loadViews("assign_exam_center_to_semester/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('assign_exam_center_to_semester.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                
                $data = array(
                   'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'id_programme' => $id_programme,
                    'id_exam_center' => $id_exam_center
                );

                $result = $this->assign_exam_center_to_semester_model->addExamCenterToSemester($data);
                redirect('/registration/assignExamCenterToSemester/list');
            }
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['examCenterList'] = $this->exam_center_model->examCenterList();
            
            $this->global['pageTitle'] = 'Campus Management System : Add Exam Center';
            $this->loadViews("assign_exam_center_to_semester/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('assign_exam_center_to_semester.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/registration/assignExamCenterToSemester/list');
            }
            if($this->input->post())
            {
                
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                
                $data = array(
                   'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'id_programme' => $id_programme,
                    'id_exam_center' => $id_exam_center
                );

                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->assign_exam_center_to_semester_model->editExamCenterToSemester($data,$id);
                redirect('/registration/assignExamCenterToSemester/list');
            }
            $data['getExamCenterList'] = $this->assign_exam_center_to_semester_model->getAssignedExamCenterList($id);
            
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['examCenterList'] = $this->exam_center_model->examCenterList();
            $data['programmeList'] = $this->programme_model->programmeList();
            
            $this->global['pageTitle'] = 'Campus Management System : Edit Assigned Exam Center To Semester';
            $this->loadViews("assign_exam_center_to_semester/edit", $this->global, $data, NULL);
        }
    }
}
