<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseCount extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_count_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('report.course_count') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            $formData['id_university'] = $this->security->xss_clean($this->input->post('id_university'));
            $formData['id_branch'] = $this->security->xss_clean($this->input->post('id_branch'));
            $formData['id_program_scheme'] = $this->security->xss_clean($this->input->post('id_program_scheme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_learning_mode'] = $this->security->xss_clean($this->input->post('id_learning_mode'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));

            $data['searchParam'] = $formData;

            $data['courseCountList'] = $this->course_count_model->courseCountListSearch($formData);
            $data['branchList'] = $this->course_count_model->branchListByStatus('1');
            $data['schemeList'] = $this->course_count_model->schemeListByStatus('1');
            $data['intakeList'] = $this->course_count_model->intakeListByStatus('1');
            $data['learningModeList'] = $this->course_count_model->learningModeListByStatus('1');
            $data['programList'] = $this->course_count_model->programListByStatus('1');
            $data['partnerUniversityList'] = $this->course_count_model->getUniversityListByStatus('1');  
            // $name = $this->security->xss_clean($this->input->post('name'));

            // $data['searchName'] = $name;
            // $data['attendenceSetupList'] = $this->course_count_model->attendenceSetupListSearch();

            // echo "<Pre>";print_r($formData);exit();

            $this->global['pageTitle'] = 'Campus Management System : Report Course Count';
            $this->loadViews("course_count/list", $this->global, $data, NULL);
        }
    }


    function getProgramSchemeByProgramId()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $id_program = $tempData['id_program'];
        $id_learning_mode = $tempData['id_learning_mode'];

        $intake_data = $this->course_count_model->getProgramSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_learning_mode' id='id_learning_mode' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;

            $table.="<option value='".$id . "'";
            if($id_learning_mode == $id)
            {
               $table.="selected";
            } $table.= ">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function getBranchesByPartnerUniversity()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $id_partner_university = $tempData['id_university'];
        $id_branch = $tempData['id_branch'];

            // echo "<Pre>"; print_r($tempData);exit;
            
            $intake_data = $this->course_count_model->getBranchesByPartnerUniversity($id_partner_university);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_branch' id='id_branch' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            // $table.="<option value=".$id.">". $code . " - " .  $name .
            //         "</option>";


            $table.="<option value='".$id . "'";
            if($id_branch == $id)
            {
               $table.="selected";
            }
            $table.= ">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }
}