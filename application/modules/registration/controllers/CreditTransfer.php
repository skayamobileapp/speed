<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CreditTransfer extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('credit_transfer_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('credit_transfer.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['application_type'] = $this->security->xss_clean($this->input->post('application_type'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['creditTransferList'] = $this->credit_transfer_model->creditTransferListSearch($formData);
            $data['semesterList'] = $this->credit_transfer_model->getSemesterListByStatus('1');
                // echo "<Pre>";print_r($data['creditTransferList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Credit Transfer';
            $this->loadViews("credit_transfer/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('credit_transfer.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                // echo "<Pre>";print_r($this->input->post());exit();

                $institution_type = $this->security->xss_clean($this->input->post('institution_type'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $application_type = $this->security->xss_clean($this->input->post('application_type'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));


                // $to_tm_12hr  = date("g:i a", strtotime($to_tm));
                // $from_tm_12hr  = date("g:i a", strtotime($from_tm));
                // echo "<Pre>";print_r($time_in_12_hour_format);exit();

                $generated_number = $this->credit_transfer_model->generateCreditTransferNumber();
                
                $data = array(
                   'application_id' => $generated_number,
                   'institution_type' => $institution_type,
                    'id_student' => $id_student,
                    'application_type' => $application_type,
                    'id_semester' => $id_semester,
                    'status' => 0,
                    'created_by' => $user_id
                );

                $result = $this->credit_transfer_model->addCreditTransfer($data);

                // echo "<Pre>";print_r($result);exit();

                if($result)
                {
                    $inserted = $this->credit_transfer_model->moveDetailDataFromTempToMain($result);
                }

                $student_data = $this->credit_transfer_model->getStudent($id_student);
                $id_program = $student_data->id_program;


                $check_apply_status = $this->credit_transfer_model->getFeeStructureActivityType($application_type,'Application Level',$id_program);

                // echo "<Pre>";print_r($check_apply_status);exit();


                if($check_apply_status)
                {
                    $data['add'] = 1;
                    $this->credit_transfer_model->generateMainInvoice($data,$result);
                }

                redirect('/registration/creditTransfer/list');
            }
            else
            {
                    $result = $this->credit_transfer_model->deleteTempCreditTransferBySession($id_session);
            }

            $data['semesterList'] = $this->credit_transfer_model->getSemesterListByStatus('1');
            $data['courseList'] = $this->credit_transfer_model->courseListByStatus('1');
            $data['equivalentCourseList'] = $this->credit_transfer_model->equivalentCourseListByStatus('1');
            $data['gradeList'] = $this->credit_transfer_model->gradeListByStatus('1');
            $data['studentList'] = $this->credit_transfer_model->studentListByStatus('1');


                // echo "<Pre>";print_r($time_in_12_hour_format);exit();


            $this->global['pageTitle'] = 'Campus Management System : Add Credit Transfer';
            $this->loadViews("credit_transfer/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('credit_transfer.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/registration/creditTransfer/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $to_tm = $this->security->xss_clean($this->input->post('to_tm'));
                $from_tm = $this->security->xss_clean($this->input->post('from_tm'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_count = $this->security->xss_clean($this->input->post('max_count'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $to_tm_12hr  = date("g:i a", strtotime($to_tm));
                $from_tm_12hr  = date("g:i a", strtotime($from_tm));
                // echo "<Pre>";print_r($time_in_12_hour_format);exit();
                
                $data = array(
                   'name' => $name,
                    'from_dt' => date('Y-m-d', strtotime($from_dt)),
                    'id_location' => $id_location,
                    'id_exam_center' => $id_exam_center,
                    'to_tm' => $to_tm_12hr,
                    'from_tm' => $from_tm_12hr,
                    'type' => $type,
                    'max_count' => $max_count,
                    'status' => $status,
                    'updated_by' => $user_id
                );


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->credit_transfer_model->editCreditTransfer($data,$id);
                redirect('/registration/creditTransfer/list');
            }

            $data['semesterList'] = $this->credit_transfer_model->getSemesterListByStatus('1');
            $data['courseList'] = $this->credit_transfer_model->courseListByStatus('1');
            $data['equivalentCourseList'] = $this->credit_transfer_model->equivalentCourseListByStatus('1');
            $data['gradeList'] = $this->credit_transfer_model->gradeListByStatus('1');
            $data['studentList'] = $this->credit_transfer_model->studentListByStatus('1');



            $data['creditTransferData'] = $this->credit_transfer_model->getCreditTransfer($id);
            $data['creditTransferDetails'] = $this->credit_transfer_model->getCreditTransferDetailsByIdCreditTransfer($id);

            if($data['creditTransferData'])
            {
                $data['studentDetails'] = $this->credit_transfer_model->getStudentByStudentId($data['creditTransferData']->id_student);
            }


            
            $data['getCreditTransfer'] = $this->credit_transfer_model->getCreditTransfer($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Credit Transfer';
            $this->loadViews("credit_transfer/edit", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('credit_transfer.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['application_type'] = $this->security->xss_clean($this->input->post('application_type'));
            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['creditTransferList'] = $this->credit_transfer_model->creditTransferListSearch($formData);
            $data['semesterList'] = $this->credit_transfer_model->getSemesterListByStatus('1');
                // echo "<Pre>";print_r($data['creditTransferList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Credit Transfer';
            $this->loadViews("credit_transfer/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('credit_transfer.approve') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/registration/creditTransfer/approvallist');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                
                $data = array(
                   'status' => $status,
                    'reason' => $reason,
                    'updated_by' => $user_id
                );


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->credit_transfer_model->editCreditTransfer($data,$id);

                if($result)
                   {

                        $ctedit_transfer = $this->credit_transfer_model->getCreditTransfer($id);

                        $id_student = $ctedit_transfer->id_student;
                        $application_type = $ctedit_transfer->application_type;

                        $student_data = $this->credit_transfer_model->getStudent($id_student);
                        $id_program = $student_data->id_program;

                        $check_apply_status = $this->credit_transfer_model->getFeeStructureActivityType($application_type,'Approval Level',$id_program);

                        // echo "<Pre>"; print_r($id_program);exit;
                        if($check_apply_status)
                        {
                            $data['add'] = 0;
                            $data['id_student'] = $id_student;
                            $this->credit_transfer_model->generateMainInvoice($data,$id);
                        }
                    }

                redirect('/registration/creditTransfer/approvalList');
            }

            $data['semesterList'] = $this->credit_transfer_model->getSemesterListByStatus('1');
            $data['courseList'] = $this->credit_transfer_model->courseListByStatus('1');
            $data['equivalentCourseList'] = $this->credit_transfer_model->equivalentCourseListByStatus('1');
            $data['gradeList'] = $this->credit_transfer_model->gradeListByStatus('1');
            $data['studentList'] = $this->credit_transfer_model->studentListByStatus('1');



            $data['creditTransferData'] = $this->credit_transfer_model->getCreditTransfer($id);
            $data['creditTransferDetails'] = $this->credit_transfer_model->getCreditTransferDetailsByIdCreditTransfer($id);

            if($data['creditTransferData'])
            {
                $data['studentDetails'] = $this->credit_transfer_model->getStudentByStudentId($data['creditTransferData']->id_student);
            }


            
            $data['getCreditTransfer'] = $this->credit_transfer_model->getCreditTransfer($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Credit Transfer';
            $this->loadViews("credit_transfer/view", $this->global, $data, NULL);
        }
    }

    function saveTempDetailData()
    {
        $id_session = $this->session->my_session_id;
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;
        $tempData['status'] = 1;

        if($tempData['institution_type'] == 'Internal')
        {
            $tempData['credit_hours'] = $this->credit_transfer_model->getCourseCreditHours($tempData['id_course']);
        }
        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->credit_transfer_model->saveTempDetailData($tempData);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    function displayTempData()
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->credit_transfer_model->getTempCreditTransferDetailsBySessionId($id_session);


        if(!empty($temp_details))
        {
            
        // echo "<Pre>";print_r($details);exit;
       $table ="
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Institution</th>
                    <th>Subject/Course</th>
                    <th>Course Equivalent</th>
                    <th>Credit Hours</th>
                    <th>Grade</th>
                    <th>Remarks</th>
                    <th class='text-center'>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                        $id = $temp_details[$i]->id;
                        $subject_course = $temp_details[$i]->subject_course;
                        $institution_type = $temp_details[$i]->institution_type;
                        $credit_hours = $temp_details[$i]->credit_hours;
                        $course_name = $temp_details[$i]->course_name;
                        $course_code = $temp_details[$i]->course_code;
                        $e_course_name = $temp_details[$i]->e_course_name;
                        $e_course_code = $temp_details[$i]->e_course_code;
                        $grade_code = $temp_details[$i]->grade_code;
                        $grade_name = $temp_details[$i]->grade_name;
                        $remarks = $temp_details[$i]->remarks;
                        if($subject_course == '')
                        {
                            $subject_course = $course_code . " - " . $course_code;
                        }


                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$institution_type</td>
                            <td>$subject_course</td>
                            <td>$e_course_code - $e_course_name</td>
                            <td>$credit_hours</td>
                            <td>$grade_code - $grade_name</td>
                            <td>$remarks</td>
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table ="";

        }


        return $table;
    }

    function deleteTempData($id)
    {
        $id_session = $this->session->my_session_id;

        $deleted = $this->credit_transfer_model->deleteTempCreditTransfer($id);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    function getStudentById($id_student)
    {

        $student_details = $this->credit_transfer_model->getStudentByStudentId($id_student);

        // echo "<pre>";print_r($student_details);exit();
        if(!empty($student_details))
        {

            $full_name = $student_details->full_name;;
           $nric = $student_details->nric;
           $phone = $student_details->phone;
           $email_id = $student_details->email_id;
           $date_of_birth = $student_details->date_of_birth;
           $martial_status = $student_details->martial_status;
           $religion = $student_details->religion;
           $mail_address1 = $student_details->mail_address1;
           $mail_address2 = $student_details->mail_address2;
           $mailing_country = $student_details->mailing_country;
           $mailing_city = $student_details->mailing_city;
           $mailing_zipcode = $student_details->mailing_zipcode;
           $permanent_address1 = $student_details->permanent_address1;
           $permanent_address2 = $student_details->permanent_address2;
           $permanent_city = $student_details->permanent_city;
           $permanent_zipcode = $student_details->permanent_zipcode;
           $programme_name = $student_details->programme_name;
           $intake_name = $student_details->intake_name;
           $qualification_name = $student_details->qualification_name;
           $qualification_code = $student_details->qualification_code;
           $gender = $student_details->gender;
           $program_scheme = $student_details->program_scheme;




            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$full_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email_id</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd>$mail_address1 <br>
                        $mail_address2 <br>
                        $mailing_city <br>
                        $mailing_zipcode <br></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd>$program_scheme</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Phone No.</dt>
                                <dd>$phone</dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address.</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd>$permanent_address1 <br>
                        $permanent_address2 <br>
                        $permanent_city <br>
                        $permanent_zipcode <br></dd>
                            </dl>
                            <dl>
                                <dt>Gender.</dt>
                                <dd>$gender</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";

        }
        else
        {
            echo '';exit;
        }

        print_r($table);exit();
    }


    function getCentersByLocatioin($id_location)
    {
            $results = $this->credit_transfer_model->getCentersByLocatioin($id_location);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_center' id='id_exam_center' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function saveDetailData()
    {

        $id_session = $this->session->my_session_id;
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        if($tempData['institution_type'] == 'Internal')
        {
            $tempData['credit_hours'] = $this->credit_transfer_model->getCourseCreditHours($tempData['id_course']);
        }
        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->credit_transfer_model->addCreditTransferDetails($tempData);

        // $data = $this->displayTempData();

        echo "success";exit();
    }

    function deleteDetailData($id)
    {
        $deleted = $this->credit_transfer_model->deleteDetailData($id);
        echo 'success';exit;
    }
}
