<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Race extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('race_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('race.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['raceList'] = $this->race_model->raceListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : Race List';
            $this->loadViews("race/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('race.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->race_model->addNewRace($data);
                redirect('/setup/race/list');
            }
            $this->global['pageTitle'] = 'Campus Management System : Add Race';
            $this->loadViews("race/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('race.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/race/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->race_model->editRace($data,$id);
                redirect('/setup/race/list');
            }
            $data['race'] = $this->race_model->getRace($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Race';
            $this->loadViews("race/edit", $this->global, $data, NULL);
        }
    }
}
