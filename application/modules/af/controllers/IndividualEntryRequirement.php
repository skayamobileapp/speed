<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class IndividualEntryRequirement extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('individual_entry_requirement_model');
        $this->isLoggedIn();
    }


    function list()
    {
        if ($this->checkAccess('programme_entry_requirement.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['programmeList'] = $this->individual_entry_requirement_model->programListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Program List';
            $this->loadViews("individual_entry_requirement/list", $this->global, $data, NULL);
        }
    }
    
    function addRequirement($id_program = NULL)
    {
        if ($this->checkAccess('programme_entry_requirement.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_program == null)
            {
                redirect('/setup/individualEntryRequirement/list');
            }

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {                

                $age = $this->security->xss_clean($this->input->post('age'));
                $education = $this->security->xss_clean($this->input->post('education'));
                $work_experience = $this->security->xss_clean($this->input->post('work_experience'));
                $other = $this->security->xss_clean($this->input->post('other'));
                $min_age = $this->security->xss_clean($this->input->post('min_age'));
                $max_age = $this->security->xss_clean($this->input->post('max_age'));
                $id_education_qualification = $this->security->xss_clean($this->input->post('id_education_qualification'));
                $education_description = $this->security->xss_clean($this->input->post('education_description'));
                $min_work_experience = $this->security->xss_clean($this->input->post('min_work_experience'));
                $id_work_specialisation = $this->security->xss_clean($this->input->post('id_work_specialisation'));
                $work_description = $this->security->xss_clean($this->input->post('work_description'));
                $other_description = $this->security->xss_clean($this->input->post('other_description'));
            
                $data = array(
                    'id_program' => $id_program,
                    'age' => $age,
                    'education' => $education,
                    'work_experience' => $work_experience,
                    'other' => $other,
                    'min_age' => $min_age,
                    'max_age' => $max_age,
                    'id_education_qualification' => $id_education_qualification,
                    'education_description' => $education_description,
                    'min_work_experience' => $min_work_experience,
                    'id_work_specialisation' => $id_work_specialisation,
                    'work_description' => $work_description,
                    'other_description' => $other_description,
                    'status' => 1,
                    'created_by' => $id_user
                );

                $inserted_id = $this->individual_entry_requirement_model->addIndividualEntryRequirement($data);
                // if ($inserted_id)
                // {
                //     $inserted_document_reuirements = $this->individual_entry_requirement_model->addNewProgrammeDocumentRequirements($inserted_id);
                // }
                redirect('/setup/individualEntryRequirement/addRequirement/'.$id_program);
            }

            $data['programEntryRequirementList'] = $this->individual_entry_requirement_model->programEntryRequirementList($id_program);
            $data['programDetails'] = $this->individual_entry_requirement_model->getProgram($id_program);
            $data['qualificationList'] = $this->individual_entry_requirement_model->qualificationListByStatus('1');
            $data['workSpecialisationList'] = $this->individual_entry_requirement_model->workSpecialisationListByStatus('1');
            
            $this->global['pageTitle'] = 'Scholarship Management System : Add Individual Entry Requirement';
            $this->loadViews("individual_entry_requirement/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('programme_entry_requirement.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/individualEntryRequirement/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_award' => $id_award,
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->individual_entry_requirement_model->editProgrammeDetails($data,$id);
                
                redirect('/setup/individualEntryRequirement/list');
            }
            $data['id_individual_entry_requirement'] = $id;
            // echo "<Pre>";print_r($data['individual_entry_requirementHasDeanList']);exit;
            $data['awardList'] = $this->individual_entry_requirement_model->awardListByStatus('1');
            $data['individual_entry_requirementDetails'] = $this->individual_entry_requirement_model->getProgrammeDetails($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Programme';
            $this->loadViews("individual_entry_requirement/edit", $this->global, $data, NULL);
        }
    }

    function deleteRequirement($id)
    {
        $deleted = $this->individual_entry_requirement_model->deleteRequirement($id);
        echo "Success";
    }
}