<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StatementOfAccount extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('staff_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('staff.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 
            $data['searchParam'] = $formData;
            $data['departmentList'] = $this->staff_model->getDepartmentByStatus('1');
            // echo "<Pre>";print_r($data['countryList']);exit;
            // $data['departmentList'] = $this->staff_model->getStateByStatus('1');
            $data['staffDetails'] = $this->staff_model->staffListSearch($formData);
            $this->global['pageTitle'] = 'Campus Management System : Staff List';
            $this->loadViews("statement_of_account/list_staff", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {
        if ($this->checkAccess('staff.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/af/staff/list');
            }


            $tab = 0;

            $resultprint = $this->input->post();

            // echo "<Pre>"; print_r($resultprint);exit();
            
            if($resultprint)
            {
            // echo "<Pre>"; print_r($resultprint);exit();

            $tab = $resultprint['btn_submit'];


            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;



             switch ($resultprint['btn_submit'])
             {

                case '1':


                $formData = $this->input->post();



                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $ic_no = $this->security->xss_clean($this->input->post('ic_no'));
                $staff_id = $this->security->xss_clean($this->input->post('staff_id'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $address_two = $this->security->xss_clean($this->input->post('address_two'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                $phone_number = $this->security->xss_clean($this->input->post('phone_number'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email')); 
                $job_type = $this->security->xss_clean($this->input->post('job_type'));
                $id_department = $this->security->xss_clean($this->input->post('id_department'));
                $id_faculty_program = $this->security->xss_clean($this->input->post('id_faculty_program'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $dob = $this->security->xss_clean($this->input->post('dob'));
                $academic_type = $this->security->xss_clean($this->input->post('academic_type'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                     
                $salutationInfo = $this->staff_model->getSalutation($salutation);
                
                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'name' => $salutationInfo->name . ". " . $first_name . " " . $last_name,
                    'ic_no' => $ic_no,
                    'staff_id' => $staff_id,
                    'gender' => $gender,
                    'mobile_number' => $mobile_number,
                    'phone_number' => $phone_number,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'zipcode' => $zipcode,
                    'email' => $email,
                    'address' => $address,
                    'address_two' => $address_two,
                    'job_type' => $job_type,
                    'id_department' => $id_department,
                    'id_faculty_program' => $id_faculty_program,
                    'dob' =>  date('Y-m-d',strtotime($dob)),
                    'academic_type' => $academic_type,
                    'id_education_level' => $id_education_level,
                    'status' => $status
                );              
                $result = $this->staff_model->editStaff($data,$id);
                redirect($_SERVER['HTTP_REFERER']);
                // redirect('/af/staff/list');

                break;



                case '2':


                $formData = $this->input->post();



                $id_teaching_semester = $this->security->xss_clean($this->input->post('id_teaching_semester'));
                $id_teaching_programme = $this->security->xss_clean($this->input->post('id_teaching_programme'));
                $id_teaching_course = $this->security->xss_clean($this->input->post('id_teaching_course'));
                $id_teaching_mode_of_study = $this->security->xss_clean($this->input->post('id_teaching_mode_of_study'));
                $id_teaching_learning_center = $this->security->xss_clean($this->input->post('id_teaching_learning_center'));

                
                $data = array(
                    'id_staff' => $id,
                    'id_semester' => $id_teaching_semester,
                    'id_programme' => $id_teaching_programme,
                    'id_course' => $id_teaching_course,
                    'id_mode_of_study' => $id_teaching_mode_of_study,
                    'id_learning_center' => $id_teaching_learning_center,
                    'status' => 1,
                    'created_by' => $id_user,
                );              
                $result = $this->staff_model->addStaffTeachingDetails($data);
                redirect($_SERVER['HTTP_REFERER']);
                // redirect('/af/staff/list');

                break;






                case '3':


                $formData = $this->input->post();



                $leave_name = $this->security->xss_clean($this->input->post('leave_name'));
                $leave_from_dt = $this->security->xss_clean($this->input->post('leave_from_dt'));
                $leave_to_dt = $this->security->xss_clean($this->input->post('leave_to_dt'));

                
                $data = array(
                    'id_staff' => $id,
                    'name' => $leave_name,
                    'from_dt' => date('Y-m-d',strtotime($leave_from_dt)),
                    'to_dt' => date('Y-m-d',strtotime($leave_to_dt)),
                    'status' => 1,
                    'created_by' => $id_user,
                );              

                $result = $this->staff_model->addStaffLeaveDetails($data);

                
                redirect($_SERVER['HTTP_REFERER']);
                // redirect('/af/staff/list');

                break;





                case '5':


                $formData = $this->input->post();



                $id_change_status = $this->security->xss_clean($this->input->post('id_change_status'));

                
                $data = array(
                    'id_staff' => $id,
                    'id_change_status' => $id_change_status,
                    'status' => 1,
                    'created_by' => $id_user,
                );              
                $result = $this->staff_model->addStaffChangeStatus($data);


                redirect($_SERVER['HTTP_REFERER']);
                // redirect('/af/staff/list');

                break;



                // case '4':

                // // echo "<Pre>"; print_r($_FILES);exit();

                //     if($_FILES['moa_file'])
                //     {  


                //         $certificate_name = $_FILES['moa_file']['name'];
                //         $certificate_size = $_FILES['moa_file']['size'];
                //         $certificate_tmp =$_FILES['moa_file']['tmp_name'];
                        
                //         // echo "<Pre>"; print_r($certificate_tmp);exit();

                //         $certificate_ext=explode('.',$certificate_name);
                //         $certificate_ext=end($certificate_ext);
                //         $certificate_ext=strtolower($certificate_ext);


                //         $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'MOA File');

                //         $moa_file = $this->uploadFile($certificate_name,$certificate_tmp,'MOA File');

                //     }

                //     // echo "<Pre>"; print_r($moa_file);exit();

                //     $start_date = $this->security->xss_clean($this->input->post('moa_start_date'));
                //     $end_date = $this->security->xss_clean($this->input->post('moa_end_date'));
                //     $name = $this->security->xss_clean($this->input->post('moa_name'));
                //     $id_currency = $this->security->xss_clean($this->input->post('moa_id_currency'));
                //     $reminder_months = $this->security->xss_clean($this->input->post('moa_reminder_months'));

                //     $moa = array(

                //             'name' => $name,
                //             'id_currency' => $id_currency,
                //             'reminder_months' => $reminder_months,
                //             'start_date' => date('Y-m-d', strtotime($start_date)),
                //             'end_date' => date('Y-m-d', strtotime($end_date)),
                //             'id_partner_university' => $id
                //         );


                //     if($moa_file != '')
                //     {
                //         $moa['file'] = $moa_file;
                //     }

                //     // echo "<Pre>"; print_r($moa_file);exit();

                //     $result = $this->partner_university_model->addNewAggrement($moa);

                    
                //     redirect($_SERVER['HTTP_REFERER']);
                    
                //     break;


                }

             }



            $data['tab'] = $tab;
            $data['id_staff'] = $id;
            $data['countryList'] = $this->staff_model->getCountryByStatus('1');
            $data['stateList'] = $this->staff_model->getStateByStatus('1');

            $data['staffDetails'] = $this->staff_model->getStaff($id);
            $data['getStaffCourse'] = $this->staff_model->getStaffCourse($id);
            $data['getStaffTeachingDetails'] = $this->staff_model->getStaffTeachingDetails($id);
            $data['getStaffChangeStatusDetails'] = $this->staff_model->getStaffChangeStatusDetails($id);
            $data['getStaffLeaveDetails'] = $this->staff_model->getStaffLeaveDetails($id);

            // echo "<Pre>"; print_r($data['getStaffTeachingDetails']);exit();

            $data['facultyProgramList'] = $this->staff_model->getFacultyProgramListByStatus('1');
            $data['qualificationList'] = $this->staff_model->qualificationListByStatus('1');
            $data['salutationList'] = $this->staff_model->salutationListByStatus('1');

            $data['semesterList'] = $this->staff_model->semesterListByStatus('1');
            $data['programmeList'] = $this->staff_model->programmeListByStatus('1');
            $data['courseList'] = $this->staff_model->courseListByStatus('1');
            $data['modeOfStudyList'] = $this->staff_model->modeOfStudyListByStatus('1');
            $data['learningCenterList'] = $this->staff_model->learningCenterListByStatus('1');


            $data['changeStatusList'] = $this->staff_model->changeStatusListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Edit Staff';
            $this->loadViews("statement_of_account/edit", $this->global, $data, NULL);
            // $this->loadViews("staff/edit_tab", $this->global, $data, NULL);
        }
    }

    function delete()
    {
        if ($this->checkAccess('staff.delete') == 0)
        {
            echo (json_encode(array('status' => 'access')));
        }
        else
        {
            $countryId = $this->input->post('countryId');
            $countryInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));
            $result = $this->staff_model->deleteSemmester($countryId, $countryInfo);
            if ($result > 0)
            {
                echo (json_encode(array('status' => TRUE)));
            }
            else
            {
                echo (json_encode(array('status' => FALSE)));
            }
        }
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->staff_model->updateTempDetails($tempData,$id);
        }
        else
        {
            unset($tempData['id']);
            $inserted_id = $this->staff_model->addTempDetails($tempData);
// echo "<Pre>";  print_r($tempData);exit;
        }
        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->staff_model->getTempStaff($id_session); 
        // echo "<Pre>";print_r($details);exit;
         if(!empty($temp_details))
        {
            $table = "<table  class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Course Name</th>
                        <th>Action</th>
                    </tr>";
                        for($i=0;$i<count($temp_details);$i++)
                        {
                        $id = $temp_details[$i]->id;
                        $fee_name = $temp_details[$i]->name;
                        $j = $i+1;
                            $table .= "
                            <tr>
                                <td>$j</td>
                                <td>$fee_name</td>                       
                                <td>
                                    <span onclick='deleteTempData($id)'>Delete</a>
                                <td>
                            </tr>";
                        }
            $table.= "</table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->staff_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    }

    function getStateByCountry($id_country)
    {
            $results = $this->staff_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="<select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
            exit;
    }

    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $data['id_course'] =  $tempData['id_course'];
        $data['id_staff'] =  $tempData['id'];
        $inserted_id = $this->staff_model->addNewStaffCourse($data);
        
        echo $inserted_id;exit;
         // $temp_details = $this->staff_model->getStaffCourse($tempData['id']);

        // if(!empty($temp_details))
        // {

        //     $table = "<table  class='table' id='list-table'>
        //               <tr>
        //                 <th>Sl. No</th>
        //                 <th>Course Name</th>
        //                 <th>Action</th>
        //             </tr>";
        //                 for($i=0;$i<count($temp_details);$i++)
        //                 {
        //                 $id = $temp_details[$i]->id;
        //                 $coursename = $temp_details[$i]->coursename;
        //                 $j = $i+1;
        //                     $table .= "
        //                     <tr>
        //                         <td>$j</td>
        //                         <td>$coursename</td>                         
        //                         <td>
        //                             <span onclick='deleteCourseDetailData($id)'>Delete</a>
        //                         <td>
        //                     </tr>";
        //                 }
        //     $table.= "</table>";
        // }
        // else
        // {
        //     $table="";
        // }
        // echo $table;           
    }

     function getSchemeByProgramId($id_program)
    {
        // It's A Learning Mode After Flow Change
         $intake_data = $this->staff_model->getProgramSchemeByProgramId($id_program);
        
        // Multiple Programme Mode Ignored For Demo On 09-11-2020
        // $intake_data = $this->applicant_model->getProgramLandscapeSchemeByProgramId($id_program);

        // echo "<Pre>"; print_r($intake_data);exit;
        
        $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_teaching_mode_of_study' id='id_teaching_mode_of_study' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;

            $table.="<option value=".$id.">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function deleteCourseDetailData($id_details)
    {
        $inserted_id = $this->staff_model->deleteCourseData($id_details);
        echo "Success"; 
    }

    function deleteTeachingDetails($id)
    {
        $inserted_id = $this->staff_model->deleteTeachingDetails($id);
        echo "Success"; 
    }

    function deleteStaffChangeStatus($id)
    {
        $inserted_id = $this->staff_model->deleteStaffChangeStatus($id);
        echo "Success";
    }

    function deleteStaffLeaveDetails($id)
    {
        $inserted_id = $this->staff_model->deleteStaffLeaveDetails($id);
        echo "Success";
    }
}