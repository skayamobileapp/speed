<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_offered_model extends CI_Model
{
    function schemeListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('scheme as s');
        $this->db->where('s.status', $status);
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }
    
    function semesterListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('semester as s');
        $this->db->where('s.status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function facultyProgramListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('faculty_program as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function semesterListSearch($data)
    {
        $this->db->select('s.*, ay.name as academic_year');
        $this->db->from('semester as s');
        $this->db->join('academic_year as ay', 'ay.id = s.id_academic_year');
        if($data['id_scheme'] != '')
        {
            $this->db->join('semester_has_scheme as shs', 's.id = shs.id_semester');  
        }
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.name  LIKE '%" . $data['name'] . "%' or s.code  LIKE '%" . $data['name'] . "%' or ay.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_scheme'] != '')
        { 
            $this->db->where('shs.id_scheme', $data['id_scheme']);
        }
         $query = $this->db->get();
         $result = $query->result();   
         // print_r($data);exit();     
         return $result;
    }

    function searchCourseByFacultyProgram($data)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->where('c.id_faculty_program', $data['id_faculty_program']);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function addCourseOffered($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_offered', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function courseOfferedListSearch($data)
    {
        $this->db->select('co.*, c.name as course_name, c.code as course_code, c.credit_hours, fp.code as faculty_code, fp.name as faculty_name');
        $this->db->from('course_offered as co');
        $this->db->join('course as c', 'co.id_course = c.id');
        $this->db->join('faculty_program as fp', 'co.id_faculty_program = fp.id');
        if($data['id_semester'] != '')
        { 
            $this->db->where('co.id_semester', $data['id_semester']);
        }
        if($data['id_faculty_program'] != '')
        { 
            $this->db->where('co.id_faculty_program', $data['id_faculty_program']);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function deleteCourseOffered($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('course_offered');
        return TRUE;
    }











    

    function getSemester($id)
    {
        $this->db->select('s.*, ay.name as academic_year');
        $this->db->from('semester as s');
        $this->db->join('academic_year as ay', 'ay.id = s.id_academic_year');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    

    function editSemester($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('semester', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($countryId, $countryInfo)
    {
        $this->db->where('id', $countryId);
        $this->db->update('semester', $countryInfo);
        return $this->db->affected_rows();
    }


   

    function tempSemesterHasScheme($data)
    {
         $this->db->trans_start();
        $this->db->insert('temp_semester_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempSemesterHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_semester_has_scheme');
        return TRUE;
    }

    function deleteTempSemesterHasSchemeBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_semester_has_scheme');
        return TRUE;
    }

    function addSemesterHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('semester_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteSemesterHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('semester_has_scheme');
        return TRUE;
    }

    function getTempSemesterHasSchemeBySessionId($id_session)
    {
        $this->db->select('s.*');
        $this->db->from('temp_semester_has_scheme as s');
        $this->db->where('s.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempSemesterHasSchemeBySessionIdDisplay($id_session)
    {
        $this->db->select('s.*, n.description as scheme_name, n.code as scheme_code');
        $this->db->from('temp_semester_has_scheme as s');
        $this->db->join('scheme as n','s.id_scheme = n.id');
        $this->db->where('s.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function moveTempToDetailsTable($id_semester)
    {
        $id_session = $this->session->my_session_id;

        $details = $this->getTempSemesterHasSchemeBySessionId($id_session);

        foreach ($details as $detail)
        {
            unset($detail->id);
            unset($detail->id_session);
            $detail->id_semester = $id_semester;

            $added_details = $this->addSemesterHasScheme($detail);
        }

        $this->deleteTempSemesterHasSchemeBySession($id_session);
    }


}