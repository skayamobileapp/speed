<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit staff Has Course</h3>
        </div>
        <form id="form_staff_has_course" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Staff Has Courses Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Staff *</label>
                        <select name="id_staff" id="id_staff" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $staffHasCourse->id_staff)
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo $record->name;  ?>
                                </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>       

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Course *</label>
                        <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $staffHasCourse->id_course)
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo $record->name;  ?>
                                </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>        

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status *</p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($staffHasCourse->status=='1')
                          {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio">    
                          </span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($staffHasCourse->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>                              
                    </div>                         
                </div>       
            </div>

        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_staff_has_course").validate(
        {
            rules:
            {
                id_staff:
                {
                    required: true
                },
                id_course:
                {
                    required: true
                },
                status:
                {
                    required: true
                }
            },
            messages:
            {
                id_staff:
                {
                    required: "Select Staff",
                },
                id_course:
                {
                    required: "Select Course",
                },
                status:
                {
                    required: "Select Status",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>