<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Payment Type</h3>
            </div>


    <form id="form_programme" action="" method="post">
        
    <div class="form-container">
        <h4 class="form-group-title">Payment Type Details</h4>
            
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Award Level <span class='error-text'>*</span></label>
                        <select name="id_award" id="id_award" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($awardLevelList))
                            {
                                foreach ($awardLevelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Learning Mode <span class='error-text'>*</span></label>
                        <select name="learning_mode" id="learning_mode" class="form-control">
                            <option value="">Select</option>
                            <option value="Full Time">Full Time</option>
                            <option value="Part Time - Blended">Part Time - Blended</option>
                            <option value="Online">Online</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Teaching Component <span class='error-text'>*</span></label>
                        <select name="teaching_component" id="teaching_component" class="form-control">
                            <option value="">Select</option>
                            <option value="Face to face lecture">Face to face lecture</option>
                            <option value="Face to face tutorial">Face to face tutorial</option>
                            <option value="Online Facilitation">Online Facilitation</option>
                            <option value="Online tutorial">Online tutorial</option>
                        </select>
                    </div>
                </div>



            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Currency <span class='error-text'>*</span></label>
                        <select name="id_currency" id="id_currency" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($currencyList))
                            {
                                foreach ($currencyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>





                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="amount" name="amount">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Calculation type <span class='error-text'>*</span></label>
                        <select name="calculation_type" id="calculation_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Per hour">Per hour</option>
                            <option value="Per Subject">Per Subject</option>
                            <option value="Per Semester">Per Semester</option>
                            <option value="Per Assignment">Per Assignment</option>
                        </select>
                    </div>
                </div>



            </div>

            <div class="row">

                


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>


            </div>

            <!-- <div class="row">

               

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            </div> -->

        </div>
            
    

    <!-- <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div> -->

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    $('select').select2();

    function validateDetailsData()
    {
        if($('#form_programme').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam == '')
            {
                alert("Add Head Of Department to the Program");
            }
            else
            {
                $('#form_programme').submit();
            }
        }    
    }

    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                id_award: {
                    required: true
                },
                learning_mode: {
                    required: true
                },
                teaching_component: {
                    required: true
                },
                id_currency: {
                    required: true
                },
                amount: {
                    required: true
                },
                calculation_type: {
                    required: true
                }
            },
            messages: {
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                learning_mode: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                },
                teaching_component: {
                    required: "<p class='error-text'>Select Teaching Component</p>",
                },
                id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                },
                calculation_type: {
                    required: "<p class='error-text'>Select Calculation Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>