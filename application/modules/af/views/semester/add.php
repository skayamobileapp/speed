<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Semester</h3>
        </div>
        <form id="form_semester" action="" method="post">
            
        <div class="form-container">
            <h4 class="form-group-title">Semester Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language</label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language">
                    </div>
                </div>

                
                
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Academic Year <span class='error-text'>*</span></label>
                        <select name="id_academic_year" id="id_academic_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($academicYearList))
                            {
                                foreach ($academicYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                    </div>
                </div>

                
                
            </div>

            <div class="row">

              <!--   <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Scheme <span class='error-text'>*</span></label>
                        <select name="id_scheme" id="id_scheme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($schemeList))
                            {
                                foreach ($schemeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->description;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Type <span class='error-text'>*</span></label>
                        <select name="semester_type" id="semester_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Long">Long</option>
                            <option value="Short">Short</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Sequence <span class='error-text'>*</span></label>
                        <select name="semester_sequence" id="semester_sequence" class="form-control">
                            <option value="">Select</option>
                            <option value='January'>January</option>
                            <option value='February'>February</option>
                            <option value='March'>March</option>
                            <option value='April'>April</option>
                            <option value='May'>May</option>
                            <option value='June'>June</option>
                            <option value='July'>July</option>
                            <option value='August'>August</option>
                            <option value='September'>September</option>
                            <option value='October'>October</option>
                            <option value='November'>November</option>
                            <option value='December'>December</option>
                        </select>
                        </select>
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Is Countable <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_countable" id="is_countable" value="1" checked="checked"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_countable" id="is_countable" value="0"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>
                

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Is Special Semister <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="special_semester" id="special_semester" value="1"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="special_semester" id="special_semester" value="0" checked="checked"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>

            

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>


                
                
            </div>

        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>





        <div class="form-container">
            <h4 class="form-group-title"> Semester Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Scheme Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_scheme_nationality" action="" method="post">
                            <div class="form-container">
                                <h4 class="form-group-title">Scheme Details</h4>


                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Scheme <span class='error-text'>*</span></label>
                                            <select name="id_scheme_detail" id="id_scheme_detail" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($schemeList))
                                                {
                                                    foreach ($schemeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code . " - " . $record->description; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                  
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="tempSemesterHasScheme()">Add</button>
                                    </div>

                                </div>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            </div>
                        </form>


                        </div> 
                    </div>







                </div>

            </div>
        </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>

 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });



  function tempSemesterHasScheme()
    {
        if($('#form_scheme_nationality').valid())
        {

        var tempPR = {};
        tempPR['id_scheme'] = $("#id_scheme_detail").val();
            $.ajax(
            {
               url: '/setup/semester/tempSemesterHasScheme',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                // alert(result);
                // window.location.reload();
                // $("#view").html(result);
               }
            });
        }
    }



    function deleteTempSemesterHasScheme(id)
    {
        // alert(id);
            $.ajax(
            {
               url: '/setup/semester/deleteTempSemesterHasScheme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);

                // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                // alert(result);
                // window.location.reload();
               }
            });
    }


    

    $(document).ready(function()
    {
        $("#form_scheme_nationality").validate({
            rules: {
                id_scheme_detail: {
                    required: true
                }
            },
            messages: {
                id_scheme_detail: {
                    required: "<p class='error-text'>Select Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function()
    {
        $("#form_semester").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                id_academic_year:
                {
                    required: true
                },
                code:
                {
                    required: true
                },
                start_date:
                {
                    required: true
                },
                end_date:
                {
                    required: true
                },
                semester_type:
                {
                    required: true
                },
                semester_sequence:
                {
                    required: true
                },
                id_scheme:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_academic_year:
                {
                    required: "<p class='error-text'>Academic Year Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                start_date:
                {
                    required: "<p class='error-text'>Start Date Required</p>",
                },
                end_date:
                {
                    required: "<p class='error-text'>End Date Required</p>",
                },
                semester_type:
                {
                    required: "<p class='error-text'>Type Required</p>",
                },
                semester_sequence:
                {
                    required: "<p class='error-text'>Sequence Required</p>",
                },
                id_scheme:
                {
                    required: "<p class='error-text'>Select Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>