<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Program Landscape</h3>
            </div>
        
        <form id="form_programme_landscape" action="" method="post">

        <br>
        

        <div class="form-container">
            <h4 class="form-group-title">Program Details</h4>

            

          <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Name <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="name1" name="name1" value="<?php echo $programme->name; ?>" readonly="readonly">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Code <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="code" name="code" value="<?php echo $programme->code; ?>" readonly="readonly">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Name In Other Language <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programme->name_optional_language; ?>" readonly="readonly">
                </div>
            </div>
            
          </div>



          <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Educatoin Level <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="name1" name="name1" value="<?php echo $programme->education_level_name; ?>" readonly="readonly">
                </div>
            </div>


          </div>

        </div>

        <br>

        <div class="form-container">
        <h4 class="form-group-title">Program Landscape Details</h4>



            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="landscape_code" name="landscape_code">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

              
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select From Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->year . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select To Intake </label>
                        <select name="id_intake_to" id="id_intake_to" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->year . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>




                <!-- <?php
                if($programme->mode != 0)
                {
                ?>


               

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Learning Mode <span class='error-text'>*</span></label>
                        <select name="learning_mode" id="learning_mode" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmelearningMode))
                            {
                                foreach ($programmelearningMode as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    
                                    ><?php echo $record->mode_of_program . " - " . $record->mode_of_study;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <?php
                }
                ?> -->

            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Type <span class='error-text'>*</span></label>
                        <select name="program_landscape_type" id="program_landscape_type" class="form-control" onchange="getProgramLandscapeType(this.value)">
                            <option value="">Select</option>
                            <option value="Block">Block</option>
                            <option value="Level">Level</option>
                            <option value="Semester">Semester</option>
                        </select>
                    </div>
                </div>




                
                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Scheme <span class='error-text'>*</span></label>
                        <select name="program_scheme" id="program_scheme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeSchemeList))
                            {
                                foreach ($programmeSchemeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                                                       ><?php echo $record->description;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->



                <div class="col-sm-4">
                    <div class="form-group">
                        <label> <div id="show_type"></div>Total <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_semester" name="total_semester">
                    </div>
                </div>



            
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Cr. Hrs <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_total_cr_hrs" name="min_total_cr_hrs">
                    </div>
                </div>




            <!-- </div>


            <div class="row"> -->


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min Pass Subject <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_pass_subject" name="min_pass_subject">
                    </div>
                </div> -->

                

            


            <!-- </div>


            <div class="row"> -->


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Repeat Hrs <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_repeat_course" name="min_repeat_course">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Repeat Exams <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_repeat_exams" name="max_repeat_exams">
                    </div>
                </div> -->

                

            <!-- </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Minimum Total Score <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="min_total_score" name="min_total_score">
                        </div>
                </div>

            </div>

            <div class="row"> -->


                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Copy Landscape From Another <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="copy_landscape" value="0" onclick="hidePLField()"  checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="copy_landscape" value="1" onclick="showPLField()"><span class="check-radio"></span> Yes
                        </label>
                    </div>
                </div>


                <div class="col-sm-4" id="view_program_landscape_dropdown" style="display: none">
                    <div class="form-group">
                        <label>Program Landscape <span class='error-text'>*</span></label>
                        <select name="id_programme_landscape_for_copy" id="id_programme_landscape_for_copy" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeLandscapeListByProgrammeId))
                            {
                                foreach ($programmeLandscapeListByProgrammeId as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>">
                                        <?php echo $record->code . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>





            </div>

        </div>


    </form>

            
        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetails()">Save</button>
                <a href="<?php echo '../programmeLandscapeList/' . $programme->id; ?>" class="btn btn-link">Cancel</a>
            </div>
        </div>
        




    <form id="form_details" action="" method="post">
        <div class="form-container" style="display: none;">
        <h4 class="form-group-title">Semester Course Registration Info</h4>

            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Semester Type <span class='error-text'>*</span></label>
                        <select name="semester_type" id="semester_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Short Semester">Short Semester</option>
                            <option value="Long Semester">Long Semester</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Registration Rule <span class='error-text'>*</span></label>
                        <select name="registration_rule" id="registration_rule" class="form-control">
                            <option value="">Select</option>
                            <option value="No Of Courses">No Of Courses</option>
                            <option value="No Of Credit Hours">No Of Credit Hours</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                        <div class="form-group">
                            <label>Minimum <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="minimum" name="minimum">
                        </div>
                </div>

                 <div class="col-sm-3">
                        <div class="form-group">
                            <label>Maximum <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="maximum" name="maximum">
                        </div>
                </div>



            </div>


            <div class="row">
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>
            </div>


            <div class="row">
                <div id="view"></div>
            </div>

        </div>
    </form>









    <form id="form_credit_hr_details" action="" method="post">
        <div class="form-container">
        <h4 class="form-group-title">Course Credit Hour Info</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Type <span class='error-text'>*</span></label>
                        <select name="id_landscape_course_type" id="id_landscape_course_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($landscapeCourseTypeList))
                            {
                                foreach ($landscapeCourseTypeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>">
                                        <?php echo $record->code . " - " .  $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                 <div class="col-sm-3">
                        <div class="form-group">
                            <label>Hours <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="hours" name="hours">
                        </div>
                </div>



            </div>


            <div class="row">
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveCreditHourDetails()">Add</button>
                </div>
            </div>


            <div class="row">
                <div id="view_credit_hours"></div>
            </div>

        </div>
    </form>






        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    function showPLField(){
            $("#view_program_landscape_dropdown").show();
    }

    function hidePLField(){
            $("#view_program_landscape_dropdown").hide();
    }



    function getProgramLandscapeType(type)
    {
        // alert(type);
        $("#show_type").val(type);
    }

    function saveData()
    {
        if($('#form_details').valid())
        {

        var tempPR = {};
        tempPR['semester_type'] = $("#semester_type").val();
        tempPR['registration_rule'] = $("#registration_rule").val();
        tempPR['minimum'] = $("#minimum").val();
        tempPR['maximum'] = $("#maximum").val();
            $.ajax(
            {
               url: '/setup/programmeLandscape/tempSemesterInfoAdd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
               }
            });
        }
    }

    function deleteTempSemesterInfo(id) {
        // alert(id);
         $.ajax(
            {
               url: '/setup/programmeLandscape/deleteTempSemesterInfo/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }


    function saveCreditHourDetails()
    {
        if($('#form_credit_hr_details').valid())
        {

        var tempPR = {};
        tempPR['id_landscape_course_type'] = $("#id_landscape_course_type").val();
        tempPR['hours'] = $("#hours").val();
            $.ajax(
            {
               url: '/setup/programmeLandscape/saveCreditHourDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_credit_hours").html(result);
               }
            });
        }
    }


    function deleteTempCreditHourDetails(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/setup/programmeLandscape/deleteTempCreditHourDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_credit_hours").html(result);
               }
            });
    }




    function validateDetails()
    {
        if($('#form_programme_landscape').valid())
        {
         console.log($("#view_credit_hours").html());
         var detailAdded = $("#view_credit_hours").html();
         if(detailAdded=='')
         {
            alert("Add Course Credit Hour Info.");
         }
         else
         {
            $('#form_programme_landscape').submit();
         }
        }     
    }






    $(document).ready(function() {
        $("#form_programme_landscape").validate({
            rules: {
                name:
                {
                    required: true
                },
                landscape_code:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                id_intake:
                {
                    required: true
                },
                min_total_cr_hrs:
                {
                    required: true
                },
                min_repeat_course:
                {
                    required: true
                },
                max_repeat_exams:
                {
                    required: true
                },
                total_semester:
                {
                    required: true
                },
                total_block:
                {
                    required: true
                },
                total_level:
                {
                    required: true
                },
                min_total_score:
                {
                    required: true
                },
                min_pass_subject:
                {
                    required: true
                },
                program_scheme:
                {
                    required: true
                },
                program_landscape_type: {
                    required: true
                },
                learning_mode: {
                    required: true
                },
                id_programme_landscape_for_copy: {
                    required: true
                }
            },
            messages:
            {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                landscape_code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                min_total_cr_hrs: {
                    required: "<p class='error-text'>Enter Min Total Cr. Hours</p>",
                },
                min_repeat_course: {
                    required: "<p class='error-text'>Enter Repeat Course</p>",
                },
                max_repeat_exams: {
                    required: "<p class='error-text'>Enter Repeat Exams</p>",
                },
                total_semester: {
                    required: "<p class='error-text'>Enter Total Semester</p>",
                },
                total_block: {
                    required: "<p class='error-text'>Enter Total Block</p>",
                },
                total_level: {
                    required: "<p class='error-text'>Enter Total Level</p>",
                },
                min_total_score: {
                    required: "<p class='error-text'>Enter Min Total Score</p>",
                },
                min_pass_subject: {
                    required: "<p class='error-text'>Enter Minimum Pass Subject</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                program_landscape_type: {
                    required: "<p class='error-text'>Select Program Landscape Type</p>",
                },
                learning_mode: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                },
                id_programme_landscape_for_copy: {
                    required: "<p class='error-text'>Select Programme Landscape For Copy</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



$(document).ready(function() {
        $("#form_details").validate({
            rules: {
                semester_type: {
                    required: true
                },
                registration_rule: {
                    required: true
                },
                minimum: {
                    required: true
                },
                maximum: {
                    required: true
                }
            },
            messages: {
                semester_type: {
                    required: "<p class='error-text'>Select Semester Type</p>",
                },
                registration_rule: {
                    required: "<p class='error-text'>Select Registration Rule</p>",
                },
                minimum: {
                    required: "<p class='error-text'>Minimum Required</p>",
                },
                maximum: {
                    required: "<p class='error-text'>Maximum Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_credit_hr_details").validate({
            rules: {
                id_landscape_course_type: {
                    required: true
                },
                hours: {
                    required: true
                }
            },
            messages: {
                id_landscape_course_type: {
                    required: "<p class='error-text'>Select Course Type</p>",
                },
                hours: {
                    required: "<p class='error-text'>Hours Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>