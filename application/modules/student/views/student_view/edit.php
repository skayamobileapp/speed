<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Exam Center</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Center details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $getExamCenterList->name ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Address <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?php echo $getExamCenterList->address ?>">
                        </div>
                      </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact Person <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Contact Person" value="<?php echo $getExamCenterList->contact_person ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact Number <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Contact Person" value="<?php echo $getExamCenterList->contact_number ?>">
                        </div>
                      </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Country</label>
                          <div class="col-sm-8">
                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                               <?php 
                                if($record->id == $getExamCenterList->id_country)
                                    { echo "selected"; }
                                ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Country</label>
                          <div class="col-sm-8">
                            <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php 
                                if($record->id == $getExamCenterList->id_state)
                                    { echo "selected"; }
                                ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6" style="display: none;">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">State</label>
                          <div class="col-sm-8">
                            <span id='view_state'></span>
                          </div>
                        </div>
                    </div>

                  </div>

                  <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">City <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo $getExamCenterList->city ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Zipcode <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode" value="<?php echo $getExamCenterList->zipcode ?>">
                        </div>
                      </div>
                    </div>

                  </div>


                  <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Location</label>
                          <div class="col-sm-8">
                            <select name="id_location" id="id_location" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($locationList))
                            {
                                foreach ($locationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                               <?php 
                                if($record->id == $getExamCenterList->id_location)
                                    { echo "selected"; }
                                ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Exam Type</label>
                          <div class="col-sm-8">
                          <select name="exam_type" id="exam_type" class="form-control" >
                            <option value="">Select</option>
                            <option value="Face to Face"
                            <?php
                            if($getExamCenterList->exam_type == 'Face To Face')
                            {
                              echo 'selected';
                            }
                             ?> >Face to Face</option>
                            <option value="Online"
                            <?php
                            if($getExamCenterList->exam_type == 'Online')
                            {
                              echo 'selected';
                            }
                             ?> >Online</option>
                            <option value="Both"
                            <?php
                            if($getExamCenterList->exam_type == 'Both')
                            {
                              echo 'selected';
                            }
                             ?> >Both</option>
                          </select>
                          </div>
                        </div>
                    </div>


                </div>




                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Email <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $getExamCenterList->email ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Username <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Username" value="<?php echo $getExamCenterList->user_name ?>">
                        </div>
                      </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Password <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo $getExamCenterList->password ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Rooms <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="rooms" name="rooms" placeholder="Total No Of Rooms" value="<?php echo $getExamCenterList->rooms ?>">
                        </div>
                      </div>
                    </div>

                </div>



                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">TOS</label>
                          <div class="col-sm-8">
                            <select name="id_tos" id="id_tos" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($tosList))
                            {
                                foreach ($tosList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                               <?php 
                                if($record->id == $getExamCenterList->id_tos)
                                    { echo "selected"; }
                                ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1"  
                            <?php if($getExamCenterList->status==1)
                            {
                                 echo "checked=checked";
                            };?>
                              >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0"
                            <?php if($getExamCenterList->status==0)
                            {
                                 echo "checked=checked";
                            };?>

                            >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>       


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

    $('select').select2();

     function getStateByCountry(id)
    {

        $.get("/exam/examCenter/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                 id_country: {
                    required: true
                },
                 id_state: {
                    required: true
                },
                 city: {
                    required: true
                },
                 zipcode: {
                    required: true
                },
                 address: {
                    required: true
                },
                 status: {
                    required: true
                },
                id_location : {
                    required: true
                },
                 exam_type: {
                    required: true
                },
                 email: {
                    required: true
                },
                 user_name: {
                    required: true
                },
                 password: {
                    required: true
                },
                rooms : {
                    required: true
                },
                id_tos : {
                    required: true
                },
                address : {
                    required: true
                },
                contact_person : {
                    required: true
                },
                contact_number : {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Country required</p>",
                },
                id_state: {
                    required: "<p class='error-text'>State required</p>",
                },
                city: {
                    required: "<p class='error-text'>City required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_location: {
                    required: "<p class='error-text'>Select Location</p>",
                },
                exam_type: {
                    required: "<p class='error-text'>Select Exam Type</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                user_name: {
                    required: "<p class='error-text'>Username Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                },
                rooms: {
                    required: "<p class='error-text'>No. Of Rooms Required</p>",
                },
                id_tos: {
                    required: "<p class='error-text'>Select TOS</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                contact_person: {
                    required: "<p class='error-text'>Contact Person Required</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>