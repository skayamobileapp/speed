<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Register_model extends CI_Model
{
	 function addNewRegistration($data) {
	 	$result = $this->db->insert('student', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
	 }


	 function duplicateCheck($email) {
	 	 $this->db->select('fc.*');
        $this->db->from('student as fc');
        $this->db->where('fc.email', $email);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
	 }

   
     function getCategory() {
        $this->db->select('ct.*');
        $this->db->from('category as ct');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function getCourses($id) {
        $this->db->select('tc.*,ct.name as categoryname, c.name as coursename');
        $this->db->from('temp_cart as tc');
        $this->db->join('course as c', 'tc.id_course = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


       function getCategoryById($id) {
        $this->db->select('ct.*');
        $this->db->from('category as ct');
        $this->db->where('ct.id', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
       }
       function getAllCourses($id) {
        $this->db->select('tc.*,f.amount');
        $this->db->from('course as tc');
        $this->db->join('fee_structure_main as f', 'f.id_course = tc.id');
        $this->db->where('tc.id_category', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function getCoursesById($id) {
        $this->db->select('ct.*');
        $this->db->from('course as ct');       
        $this->db->where('ct.id', $id); 
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


     function getCourseAmount($id) {
         $this->db->select('f.*');
        $this->db->from('fee_structure_main as f');
        $this->db->where('f.id_course', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


}

