<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Index extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        $this->load->model('register_model');

        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {

        $data['categoryList'] = $this->register_model->getCategory();

        $data['firstCourseList'] = $this->register_model->getAllCourses(1);
        $data['secondCourseList'] = $this->register_model->getAllCourses(2);
        $data['thirdCourseList'] = $this->register_model->getAllCourses(3);

        $this->loadViews('index/index',$this->global,$data,NULL);
        
    }


     public function login()
    {
        $this->load->view('index/login');
    }

    public function logout()
    {

        $sessionArray = array(
            'id_student'=> '',
            'student_name'=> '',
            'student_first_name'=> '',
            'student_last_name'=> '',
            'student_email'=> '',
            'studentLoggedIn' => FALSE
            );
        
        $this->session->set_userdata($sessionArray);
        
        redirect('index');
    }
    
}