<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Login extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {

/*session is started if you don't write this line can't use $_Session  global variable*/

        parent::__construct();
        $this->load->model('login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {



        if($this->session->studentLoggedIn==1)
        {
            redirect('/profile/dashboard/index');
        }

        if($this->input->post())
        {
            
            // echo "<Pre>";print_r($this->input->post());exit;
            
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');



            $result = $this->login_model->loginMe($email, $password);

            // echo "<Pre>";print_r($result);exit;


            if($result)
            {
                $sessionArray = array(
                    'id_student'=>$result->id,
                    'student_name'=>$result->full_name,
                    'student_first_name'=>$result->first_name,
                    'student_last_name'=>$result->last_name,
                    'student_email'=>$result->email,
                    'studentLoggedIn' => TRUE
                    );

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_student'], $sessionArray['isStudentLoggedIn'], $sessionArray['student_last_login']);

                $loginInfo = array("id_student"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());


                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("student_session_id", md5($uniqueId));

                $this->login_model->addStudentLastLogin($loginInfo);

                redirect('/profile/dashboard/index');
            }
        }

        $data['name'] = 'asdf';

        $this->loadViews('login/index',$this->global,$data,NULL);
     
    }
}
?>