<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class ScholarApplicantLogin extends BaseController
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('scholar_applicant_login_model');
    }

    
    public function index()
    {
        $this->checkScholarApplicantLoggedIn();
    }

    function checkScholarApplicantLoggedIn()
    {
        $isScholarApplicantLoggedIn = $this->session->userdata('isScholarApplicantLoggedIn');
        
        if(!isset($isScholarApplicantLoggedIn) || $isScholarApplicantLoggedIn != TRUE)
        {
            // echo "Not Login";exit();
            $this->load->view('scholar_applicant_login');
        }
        else
        {
            // echo "Scholar Login";exit();
            redirect('scholarship_applicant/profile');
        }
    }

    public function applicantLogin()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            $result = $this->scholar_applicant_login_model->loginScholarApplicant($email, $password);
            
            
            if(!empty($result))
            {
                if($result->email_verified == 0)
                {
                    echo "Email Verification Pending Check Your Email To Verify";exit();
                }
                $lastLogin = $this->scholar_applicant_login_model->scholarApplicantLastLoginInfo($result->id_scholar_applicant);

                if($lastLogin == '')
                {
                    $applicant_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $applicant_login = $lastLogin->created_dt_tm;
                }

                $sessionArray = array('id_scholar_applicant'=>$result->id_scholar_applicant,                    
                                        'scholar_applicant_name'=>$result->scholar_applicant_name,
                                        'scholar_applicant_email_id'=>$result->email_id,
                                        'scholar_applicant_last_login'=> $applicant_login,
                                        'isScholarApplicantLoggedIn' => TRUE
                                );
        // echo "<Pre>";print_r($sessionArray);exit();

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_scholar_applicant'], $sessionArray['isScholarApplicantLoggedIn'], $sessionArray['scholar_applicant_last_login']);

                $loginInfo = array("id_scholar_applicant"=>$result->id_scholar_applicant, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());


                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("my_scholar_applicant_session_id", md5($uniqueId));


                $this->scholar_applicant_login_model->addScholarApplicantLastLogin($loginInfo);

                // echo "Login To Scholar Applicant";exit();
                // echo md5($uniqueId);exit();
                redirect('/scholarship_applicant/profile');
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                $this->index();
            }
        }
    }

    public function applicantRegistration()
    {
        
        if($this->input->post())
        {
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $password = $this->security->xss_clean($this->input->post('password'));
            // $id_program = $this->security->xss_clean($this->input->post('id_program'));

            $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation.". ".$first_name." ".$last_name,
                    'phone' => $phone,
                    'nric' => $nric,
                    'email_id' => $email_id,
                    'password' => $password,
                    'email_verified' => 1
            );
                    // 'id_program' => $id_program,
            $duplicate = $this->scholar_applicant_login_model->checkDuplicateScholarApplicantRegistration($data);
            if($duplicate)
            {
                echo "Email / Phone / NRIC Already Exist";exit();
            }

            $inserted_id = $this->scholar_applicant_login_model->addNewScholarApplicantRegistration($data);
            redirect('/scholarApplicantLogin');
        }

        $data['programmeList'] = $this->scholar_applicant_login_model->programmeListByStatus('1');

        
        // $this->loadViews("scholar_applicant_registration", NULL, $data, NULL);

        $this->load->view('scholar_applicant_registration', $data);
    }
}

?>