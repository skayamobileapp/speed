<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Register extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
                $this->load->model('register_model');
        $this->load->model('login_model');

    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {

        if($this->input->post())
        {
            $data = array();
                $data['first_name'] = $this->input->post('firstName');
                $data['last_name'] = $this->input->post('lastName');
                $data['nric'] = $this->input->post('NRIC');
                $data['email'] = $this->input->post('email');
                $data['password'] = $this->input->post('password');
                $data['mail_address1'] = $this->input->post('address');
                $data['mail_address2'] = $this->input->post('address2');
                $data['mailing_country'] = $this->input->post('country');
                $data['mailing_state'] = $this->input->post('state');
                $data['mailing_zipcode'] = $this->input->post('zip');
                $data['mailing_city'] = $this->input->post('city');
                $this->register_model->addNewRegistration($data);


                           $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            $result = $this->login_model->loginMe($email, $password);


                            $sessionArray = array('id_student'=>$result->id_student,
                                        'first_name'=>$result->first_name,
                                        'last_name'=>$result->last_name,
                                        'studentLoggedIn' => TRUE
                                );
                $this->session->set_userdata($sessionArray);


            redirect('/profile/dashboard/index');


                
        }
                $data['name'] = 'asdf';

                $this->loadViews('register/index',$this->global,$data,NULL);

    }


    public function duplicateemail($email = null) {


        $student_list_data = $this->register_model->duplicateCheck($email);
        if($student_list_data) {
           $return =  "1";            
        } else {
            $return =  "0";
        }
        echo $return;exit;
    }


    
}