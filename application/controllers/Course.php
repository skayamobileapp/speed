<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Course extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        $this->load->model('register_model');

        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index($id)
    {

$data['categoryDetails'] = $this->register_model->getCategoryById($id);

$data['courseList'] = $this->register_model->getAllCourses($id);


                $this->loadViews('courses/index',$this->global,$data,NULL);

        
    }

    
}