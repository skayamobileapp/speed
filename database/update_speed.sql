
CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT 0,
  `default` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `currency_rate_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_currency` int(20) DEFAULT 0,
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `financial_account_code` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(2) DEFAULT '0',
  `id_parent` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `financial_account_code` ADD `type` VARCHAR(512) NULL DEFAULT '' AFTER `name`;




CREATE TABLE `bank_registration` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `account_no` varchar(512) DEFAULT '',
  `bank_id` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `cr_fund` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_account` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `payment_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `description_optional_language` varchar(2048) DEFAULT '',
  `payment_group` varchar(512) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `payment_type` (`id`, `name`, `description`, `description_optional_language`, `payment_group`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Cash Payment', '', 'Cash', 'Cash', '', 1, NULL, '2020-08-20 10:08:10', NULL, '2020-08-20 10:08:10'),
(2, '', 'Bank Checue Transfer', '', 'Checque', 'Bank Transfer', '', 1, NULL, '2020-08-20 10:09:30', NULL, '2020-08-20 10:09:30');



CREATE TABLE `fee_category` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(520) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `fee_group` varchar(200) DEFAULT '',
  `sequence` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `fee_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(50) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `id_fee_category` int(10) DEFAULT NULL,
  `id_amount_calculation_type` int(10) DEFAULT NULL,
  `id_frequency_mode` int(10) DEFAULT NULL,
  `account_code` varchar(50) DEFAULT '',
  `is_refundable` int(2) DEFAULT NULL,
  `is_non_invoice` int(2) DEFAULT NULL,
  `is_gst` int(2) DEFAULT NULL,
  `gst_tax` varchar(50) DEFAULT '',
  `effective_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `category` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `category_has_module` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `course` ADD `id_category` INT(20) NULL DEFAULT '0' AFTER `id`;

ALTER TABLE `course` ADD `file` VARCHAR(512) NULL DEFAULT '' AFTER `name_optional_language`;


CREATE TABLE `fee_structure_main` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_currency` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `fee_structure_main` ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `id_currency`;




CREATE TABLE `temp_fee_structure` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(512) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_fee_item` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `fee_structure` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_fee_structure` int(20) DEFAULT 0,
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_fee_item` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `applicant` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `id_type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT '0',
  `id_degree_type` int(20) DEFAULT '0',
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `passport_number` varchar(50) DEFAULT '',
  `is_updated` int(2) DEFAULT '0',
  `is_submitted` int(2) DEFAULT '0',
  `submitted_date` datetime DEFAULT NULL,
  `email_verified` int(11) DEFAULT '0',
  `initial` int(2) DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `applicant_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_applicant` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `race_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `race_setup`
--

INSERT INTO `race_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malays', '', 1, NULL, '2020-07-10 23:24:07', NULL, '2020-07-10 23:24:07'),
(2, 'Chinese', '', 1, NULL, '2020-07-10 23:24:14', NULL, '2020-07-10 23:24:14'),
(3, 'Indian', '03', 1, NULL, '2020-08-27 17:14:35', NULL, '2020-08-27 17:14:35'),
(4, 'jawa', '04', 1, NULL, '2020-08-27 17:15:24', NULL, '2020-08-27 17:15:24');


CREATE TABLE `religion_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `religion_setup`
--

INSERT INTO `religion_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Islam', 'ISLAM', 1, NULL, '2020-07-10 23:24:44', NULL, '2020-07-10 23:24:44'),
(2, 'Buddhism', 'Buddhism', 1, NULL, '2020-07-10 23:24:56', NULL, '2020-07-10 23:24:56');


ALTER TABLE `student` ADD `id_category` INT(20) NULL DEFAULT '0' AFTER `passport_number`, ADD `id_course` INT(20) NULL DEFAULT '0' AFTER `id_category`;

ALTER TABLE `applicant` ADD `approved_dt_tm` DATETIME NULL DEFAULT NULL AFTER `approved_by`;


DROP TABLE student;


CREATE TABLE `student` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `first_name` varchar(500) DEFAULT NULL,
  `last_name` varchar(500) DEFAULT NULL,
  `nric` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `mail_address1` varchar(500) DEFAULT NULL,
  `mail_address2` varchar(500) DEFAULT NULL,
  `mailing_country` int(20) DEFAULT NULL,
  `mailing_state` int(20) DEFAULT NULL,
  `mailing_zipcode` varchar(500) DEFAULT NULL,
  `mailing_city` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `unit` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(1024) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `id_category` int(20) NULL DEFAULT 0,
  `id_course` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `marks_distribution` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_unit` bigint(20) NULL DEFAULT 0,
  `module` varchar(1024) DEFAULT '',
  `max_marks` int(20) NULL DEFAULT 0,
  `pass_marks` int(20) NULL DEFAULT 0,
  `is_pass_compulsary` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `main_invoice` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `invoice_number` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_application` int(10) DEFAULT NULL,
  `id_student` int(10) DEFAULT NULL,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `invoice_total` float(20,2) DEFAULT 0.00,
  `total_discount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `main_invoice_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_main_invoice` int(10) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `price` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT NULL,
  `id_reference` int(20) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `receipt` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT 0,
  `receipt_number` varchar(50) DEFAULT '',
  `receipt_amount` float(20,2) DEFAULT NULL,
  `remarks` varchar(520) DEFAULT '',
  `currency` varchar(520) DEFAULT '',
  `receipt_date` datetime DEFAULT current_timestamp,
  `approval_status` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `receipt_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_receipt` int(10) DEFAULT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `invoice_amount` float DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `course` ADD `best_selling` INT(2) NULL DEFAULT '0' AFTER `code`, ADD `best_trending` INT(2) NULL DEFAULT '0' AFTER `best_selling`;


ALTER TABLE `student` ADD `full_name` VARCHAR(1024) NULL DEFAULT '' AFTER `id`;


CREATE TABLE `student_has_course` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT NULL,
  `id_category` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_invoice` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `course` ADD `monthd` INT(20) NULL DEFAULT '0' AFTER `code`;

ALTER TABLE `course` CHANGE `monthd` `months` INT(20) NULL DEFAULT '0';

ALTER TABLE `student_has_course` ADD `expiry_date` DATETIME NULL DEFAULT NULL AFTER `id_invoice`;

ALTER TABLE `student_has_course` ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `expiry_date`;

CREATE TABLE `student_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE student;

CREATE TABLE `student` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `approved_dt_tm` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--------------------- Update Here From Server -------------------------

CREATE TABLE `award` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `level` int(20) DEFAULT 0,
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `award`
--

INSERT INTO `award` (`id`, `name`, `level`, `description`, `code`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Foundation', 1, 'Foundation', 'FDN', NULL, 1, 1, '2020-07-10 23:44:20', NULL, '2020-07-10 23:44:20'),
(2, 'Diploma', 2, 'Diploma', 'DIP', NULL, 1, 1, '2020-08-09 05:03:14', NULL, '2020-08-09 05:03:14'),
(3, 'Degree', 3, 'Degree', 'DEG', NULL, 1, 1, '2020-08-09 05:03:46', NULL, '2020-08-09 05:03:46'),
(4, 'Master', 4, 'Master', 'MAS', NULL, 1, 1, '2020-08-09 05:04:15', NULL, '2020-08-09 05:04:15'),
(5, 'PHD', 5, 'PHD', 'PHD', NULL, 1, 1, '2020-08-09 05:05:13', NULL, '2020-08-09 05:05:13'),
(6, 'Graduate Diploma', 4, 'Graduate Diploma', 'GDIP', NULL, 1, 1, '2020-08-09 05:06:39', NULL, '2020-08-09 05:06:39');



CREATE TABLE `payment_rate` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_award` int(20) DEFAULT 0,
  `learning_mode` varchar(200) DEFAULT '',
  `teaching_component` varchar(200) DEFAULT '',
  `id_currency` int(20) DEFAULT 0,
  `calculation_type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `payment_rate` ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `calculation_type`;


CREATE TABLE `staff` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `salutation` varchar(20) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `first_name` varchar(2048) DEFAULT '',
  `last_name` varchar(2048) DEFAULT '',
  `ic_no` varchar(50) DEFAULT '',
  `dob` date DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT '',
  `phone_number` varchar(20) DEFAULT '',
  `id_country` int(20) DEFAULT NULL,
  `id_state` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `gender` varchar(20) DEFAULT '',
  `staff_id` varchar(50) DEFAULT '',
  `email` varchar(180) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `job_type` varchar(20) DEFAULT '',
  `academic_type` varchar(20) DEFAULT '',
  `id_department` int(10) DEFAULT NULL,
  `id_faculty_program` int(20) DEFAULT 0 COMMENT 'as similar to department',
  `id_education_level` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `staff_status` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `staff_status` (`id`, `code`, `name`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Active', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(2, '', 'Inactive', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(3, '', 'Terminated', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(4, '', 'Suspended', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(5, '', 'Deceased', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(6, '', 'Quit', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(7, '', 'Sabbatical Leave', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(8, '', 'Long MC', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(9, '', 'Maternity', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37');




CREATE TABLE `temp_staff_has_course` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(1024) DEFAULT NULL,
  `id_course` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `staff_has_course` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(10) DEFAULT NULL,
  `id_course` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `staff_teaching_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_mode_of_study` int(20) DEFAULT 0,
  `id_learning_center` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `staff_change_status_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `id_change_status` int(20) DEFAULT 0,
  `from_dt` varchar(200) DEFAULT '0',
  `to_dt` varchar(200) DEFAULT '0',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `staff_leave_records` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `from_dt` varchar(200) DEFAULT '',
  `to_dt` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `staff_bank_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `id_bank` int(20) DEFAULT 0,
  `bank_account_name` varchar(500) DEFAULT '',
  `bank_code` varchar(500) DEFAULT '',
  `bank_account_number` varchar(500) DEFAULT '',
  `bank_address` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `online_claim` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(256) NOT NULL,
  `id_staff` bigint(20) NOT NULL,
  `id_university` bigint(20) NOT NULL,
  `id_branch` bigint(20) NOT NULL,
  `id_programme` bigint(20) NOT NULL,
  `id_semester` bigint(20) NOT NULL,
  `total_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `temp_online_claim_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(256) NOT NULL,
  `id_course` bigint(20) NOT NULL,
  `id_payment_rate` bigint(20) NOT NULL,
  `hours` bigint(20) NOT NULL,
  `date_time` varchar(2048) DEFAULT '',
  `amount` float(20,2) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `online_claim_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_online_claim` int(20) NOT NULL,
  `id_course` bigint(20) NOT NULL,
  `id_payment_rate` bigint(20) NOT NULL,
  `hours` bigint(20) NOT NULL,
  `date_time` varchar(2048) DEFAULT '',
  `amount` float(20,2) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `department` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Department', 'Setup', 'Organisation Setup', '1', 'department', 'list', '2');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Faculty', 'Setup', 'Faculty Setup', '1', 'staff', 'list', '3'), (NULL, 'Partner University', 'Setup', 'Organisation Setup', '2', 'partnerUniversity', 'list', '2');


CREATE TABLE `department_has_staff` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEy,
  `id_staff` int(20) DEFAULT 0,
  `id_department` int(20) DEFAULT 0,
  `role` varchar(1024) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `faculty_program` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `id_partner_university` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `partner_university` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `id_partner_category` int(20) DEFAULT 0,
  `id_partner_university` int(20) DEFAULT 0,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `certificate` varchar(200) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_registrar` int(20) DEFAULT 0,
  `pay_as_agent` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(512) DEFAULT '',
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `login_id` varchar(500) DEFAULT '',
  `password` varchar(500) DEFAULT '',
  `billing_to` varchar(200) DEFAULT '',
  `id_bank` int(20) DEFAULT 0,
  `account_number` varchar(512) DEFAULT '',
  `swift_code` varchar(512) DEFAULT '',
  `bank_address` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `partner_category` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner_category`
--

INSERT INTO `partner_category` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Franchise', 'F', 1, NULL, '2020-07-13 04:46:25', NULL, '2020-07-13 04:46:25'),
(2, 'Joint Award', 'J', 1, NULL, '2020-07-13 04:46:44', NULL, '2020-07-13 04:46:44'),
(3, 'Learning Centre', 'LC', 1, NULL, '2020-08-08 23:38:31', NULL, '2020-08-08 23:38:31'),
(4, 'Dual Award', 'DUAL', 1, NULL, '2020-08-10 20:23:43', NULL, '2020-08-10 20:23:43');




CREATE TABLE `scholarship_module_type_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(200) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_module_type_setup`
--

INSERT INTO `scholarship_module_type_setup` (`id`, `code`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'COM', 'Compulsory', 1, NULL, '2020-07-23 07:09:30', NULL, '2020-07-23 07:09:30'),
(2, 'ELE', 'Elective', 1, NULL, '2020-07-23 18:09:01', NULL, '2020-07-23 18:09:01'),
(3, 'MQA', 'MQA', 1, NULL, '2020-07-23 18:09:15', NULL, '2020-07-23 18:09:15'),
(4, 'UNI', 'University Module', 1, NULL, '2020-07-23 18:09:30', NULL, '2020-07-23 18:09:30');



CREATE TABLE `partner_university_comitee` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `organisation_has_training_center` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_organisation` int(20) DEFAULT 0 COMMENT 'Traeted as id_partner_university in Case Of Partner_university_has_training_center',
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `complete_code` varchar(500) DEFAULT '',
  `id_contact_person` int(20) DEFAULT 0,
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `fax` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(512) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `partner_university_has_aggrement` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_partner_university` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `id_currency` int(20) DEFAULT 0,
  `reminder_months` int(20) DEFAULT 0,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `file` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `test_booking_upload_files` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_test_booking` int(20) DEFAULT 0,
  `upload_file` varchar(512) DEFAULT '',
  `comments` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `education_level` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `short_name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `education_level` (`id`, `name`, `short_name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'POSTGRADUATE', 'POSTGRADUATE', '', 1, NULL, '2020-07-24 23:33:52', NULL, '2020-07-24 23:33:52'),
(2, 'UNDERGRADUATE', 'UNDERGRADUATE', '', 1, NULL, '2020-08-27 04:52:35', NULL, '2020-08-27 04:52:35'),
(7, 'MASTER', 'M', '', 1, NULL, '2020-09-13 14:22:20', NULL, '2020-09-13 14:22:20'),
(8, 'Bachelor\'s Degree', '-', '', 1, NULL, '2020-11-13 08:12:44', NULL, '2020-11-13 08:12:44');



CREATE TABLE `race_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `race_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malays', '05', 1, NULL, '2020-07-10 23:24:07', NULL, '2020-07-10 23:24:07'),
(2, 'Chinese', '02', 1, NULL, '2020-07-10 23:24:14', NULL, '2020-07-10 23:24:14'),
(3, 'Indian', '03', 1, NULL, '2020-08-27 17:14:35', NULL, '2020-08-27 17:14:35'),
(4, 'jawa', '04', 1, NULL, '2020-08-27 17:15:24', NULL, '2020-08-27 17:15:24'),
(5, 'Asian', '01', 1, NULL, '2020-11-13 09:06:53', NULL, '2020-11-13 09:06:53');



CREATE TABLE `religion_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `religion_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Islam', 'ISLAM', 1, NULL, '2020-07-10 23:24:44', NULL, '2020-07-10 23:24:44'),
(2, 'Buddhism', 'Buddhism', 1, NULL, '2020-07-10 23:24:56', NULL, '2020-07-10 23:24:56'),
(3, 'Hindu', 'Hindu', 1, NULL, '2020-11-13 09:10:41', NULL, '2020-11-13 09:10:41');



CREATE TABLE `nationality` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `nationality` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysian', 1, NULL, '2020-08-31 19:56:50', NULL, '2020-08-31 19:56:50'),
(2, 'Non-Malaysian', 1, NULL, '2020-09-01 05:33:51', NULL, '2020-09-01 05:33:51');


CREATE TABLE `organisation` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_registrar` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation`
--

INSERT INTO `organisation` (`id`, `name`, `short_name`, `name_in_malay`, `url`, `id_country`, `id_registrar`, `date_time`, `contact_number`, `email`, `address1`, `address2`, `id_state`, `city`, `zipcode`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Agile E-University', 'AEU', '', 'https://aeu.edu.my/', 1, 1, '2020-07-10 00:00:00', 2147483647, 'conta@gmail.com', 'KL', '', 1, 'KL', 345465, 'b970663167d4463f446a12be19e3281d.png', 1, 1, '2020-07-10 23:25:36', NULL, '2020-07-10 23:25:36');




CREATE TABLE `organisation_comitee` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_organisation` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation_comitee`
--

INSERT INTO `organisation_comitee` (`id`, `id_organisation`, `role`, `name`, `nric`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Vice Chancellor', 'Emeritus Prof Datuk Dr Hassan Said', '740501026919', '2020-08-01 00:00:00', NULL, NULL, '2020-08-08 23:35:59', NULL, '2020-08-08 23:35:59');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Salutation', 'Setup', 'General Setup', '1', 'salutation', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Education Level', 'Setup', 'General Setup', '5', 'educationLevel', 'list', '1');


UPDATE `menu` SET `order` = '2' WHERE `menu`.`id` = 1;
UPDATE `menu` SET `order` = '3' WHERE `menu`.`id` = 2;
UPDATE `menu` SET `order` = '4' WHERE `menu`.`id` = 3;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Organisation', 'Setup', 'Organisation Setup', '1', 'organisation', 'edit', '2')



CREATE TABLE `temp_programme_has_dean` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(100) DEFAULT NULL,
  `id_staff` int(10) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `effective_start_date` datetime DEFAULT current_timestamp,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `programme_has_dean` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(10) DEFAULT NULL,
  `id_staff` int(10) DEFAULT NULL,
  `effective_start_date` datetime DEFAULT current_timestamp,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `award_level` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `from_cgpa` varchar(20) DEFAULT NULL,
  `to_cgpa` varchar(20) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `award_level` (`id`, `name`, `description`, `code`, `from_cgpa`, `to_cgpa`, `id_programme`, `id_intake`, `type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Degree', 'Degree', 'Degree', '0', '0', 1, NULL, '', 1, 1, '2020-07-11 11:58:57', NULL, '2020-07-11 11:58:57'),
(2, 'Third Class', 'Third Class', 'Third Class', '2.00', '2.49', 3, 1, '', 1, 1, '2020-08-21 01:32:10', NULL, '2020-08-21 01:32:10'),
(3, 'Second Class (Lower)', 'Second Class (Lower)', 'Second Class (Lower)', '2.50', '3.29', 3, 1, '', 1, 1, '2020-08-21 01:35:59', NULL, '2020-08-21 01:35:59'),
(4, 'Second Class (Upper)', 'Second Class (Upper)', 'Second Class (Upper)', '3.30', '3.74', 3, 1, '', 1, 1, '2020-08-21 01:37:28', NULL, '2020-08-21 01:37:28'),
(5, 'First Class', 'First Class', 'First Class', '3.75', '4.00', 3, 1, '', 1, 1, '2020-08-21 01:37:52', NULL, '2020-08-21 01:37:52');


CREATE TABLE `scheme` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `description` varchar(580) DEFAULT '',
  `description_in_optional_language` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme`
--

INSERT INTO `scheme` (`id`, `description`, `description_in_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysian', 'Malaysian', '01', 1, 1, '2020-07-13 08:36:42', 1, '2020-07-13 08:36:42'),
(2, 'Corporate ', 'Corporate', '04', 0, 1, '2020-08-09 20:11:38', 1, '2020-08-09 20:11:38'),
(3, 'International Students', 'International students', '02', 1, 1, '2020-08-09 20:12:00', 1, '2020-08-09 20:12:00'),
(4, 'International Partner', 'International Partner', '03', 0, 1, '2020-08-10 20:26:02', 1, '2020-08-10 20:26:02');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Scheme', 'setup', 'Operation Setup', '3', 'scheme', 'list', '5');



INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Faculty / Program', 'Setup', 'Organisation Setup', '4', 'facultyProgram', 'list', '2');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'File Type', 'Setup', 'Document', '1', 'fileType', 'list', '4');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Documents', 'Setup', 'Document', '2', 'documents', 'list', '4');



CREATE TABLE `documents` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `documents` (`id`, `name`, `description`, `code`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'NRIC', '', 'NRIC', NULL, 1, 1, '2020-07-11 00:16:40', NULL, '2020-07-11 00:16:40'),
(2, 'English certificate', '', 'English certificate', NULL, 1, 1, '2020-07-11 00:17:34', NULL, '2020-07-11 00:17:34'),
(3, 'Batchelor Degreee Convocation', '', 'B. Convocation', NULL, 1, 1, '2020-09-13 14:25:11', NULL, '2020-09-13 14:25:11'),
(4, 'Master Convocation', '', 'MFA01', NULL, 1, 1, '2020-11-14 11:45:34', NULL, '2020-11-14 11:45:34');


ALTER TABLE `documents` ADD `file_size` VARCHAR(200) NULL DEFAULT '' AFTER `id_programme`;
ALTER TABLE `documents` ADD `is_required` int(20) NULL DEFAULT 0 AFTER `id_programme`;
ALTER TABLE `documents` ADD `file_format` VARCHAR(2048) NULL DEFAULT '' AFTER `file_size`;


INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Academic Year', 'Setup', 'Operation Setup', '1', 'academicYear', 'list', '5');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Schemer', 'scheme', 'Operation Setup', '2', 'scheme', 'list', '5')

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Program Offered', 'setup', 'Operation Setup', '3', 'programme', 'list', '5');


CREATE TABLE `academic_year` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(120) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `from_year` varchar(20) DEFAULT '',
  `to_year` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_year`
--

INSERT INTO `academic_year` (`id`, `name`, `start_date`, `end_date`, `from_year`, `to_year`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '2020', '2020-01-01 00:00:00', '2020-12-31 00:00:00', '2020', '2020', 1, 1, '2020-07-10 23:37:30', NULL, '2020-07-10 23:37:30'),
(2, '2021', '2021-01-01 00:00:00', '2021-12-31 00:00:00', '', '', 1, 1, '2020-08-27 17:13:00', NULL, '2020-08-27 17:13:00'),
(3, '2014', '2014-01-01 00:00:00', '2014-12-31 00:00:00', '', '', 1, 1, '2020-08-31 22:57:54', NULL, '2020-08-31 22:57:54'),
(4, '2016', '2016-01-01 00:00:00', '2016-12-31 00:00:00', '', '', 1, 1, '2020-08-31 23:01:07', NULL, '2020-08-31 23:01:07'),
(5, '2017', '2017-01-01 00:00:00', '2017-12-31 00:00:00', '', '', 1, 1, '2020-08-31 23:53:03', NULL, '2020-08-31 23:53:03'),
(6, '2019', '2019-01-01 00:00:00', '2019-12-31 00:00:00', '', '', 1, 1, '2020-11-13 08:19:24', NULL, '2020-11-13 08:19:24');



CREATE TABLE `programme` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(250) DEFAULT '',
  `id_award` int(10) DEFAULT 0,
  `id_education_level` int(20) DEFAULT 0,
  `id_scheme` int(20) DEFAULT 0,
  `id_partner_category` int(20) DEFAULT 0,
  `id_partner_university` int(20) DEFAULT 0,
  `internal_external` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `total_cr_hrs` varchar(20) DEFAULT '',
  `graduate_studies` varchar(100) DEFAULT '',
  `foundation` varchar(50) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `certificate` varchar(200) DEFAULT '',
  `is_apel` int(2) DEFAULT 0,
  `mode` varchar(10) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programme`
--s

INSERT INTO `programme` (`id`, `name`, `name_optional_language`, `id_award`, `id_education_level`, `id_scheme`, `id_partner_category`, `id_partner_university`, `internal_external`, `code`, `total_cr_hrs`, `graduate_studies`, `foundation`, `start_date`, `end_date`, `certificate`, `is_apel`, `mode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(10, 'Master of IT Management', '', 4, 1, 0, 0, 9, 'External', 'USAS_MITM', '10', '', '', NULL, NULL, '', 1, '', 1, 1, '2021-01-12 05:55:51', 1, '2021-01-12 05:55:51'),
(11, 'Master of Management', '', 4, 1, 0, 0, 9, 'External', 'USAS_MOM', '12', '', '', NULL, NULL, '', 1, '', 1, 1, '2021-01-12 05:57:31', 1, '2021-01-12 05:57:31');


CREATE TABLE `category_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `category_name` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Category Type Set-Up', 'prdtm', 'Product Management', '2', 'categoryType', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Category Type', 'prdtm', 'Product Management', '1', 'category', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Programme type set-up', 'prdtm', 'Product Management', '3', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Add product', 'prdtm', 'Product Management', '4', 'welcome', 'list', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Create campaign', 'mrktngm', 'Marketing Management', '1', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Archived campaign', 'mrktngm', 'Marketing Management', '2', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Registration record', 'mrktngm', 'Marketing Management', '3', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Create contact', 'mrktngm', 'Marketing Management', '4', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Reports', 'mrktngm', 'Marketing Management', '5', 'welcome', 'list', '1');


INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Partner type set-up', 'pm', 'Partner Management', '1', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Add partner', 'pm', 'Partner Management', '2', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Partner SOA', 'pm', 'Partner Management', '3', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Invoicing', 'pm', 'Partner Management', '4', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Payments', 'pm', 'Partner Management', '5', 'welcome', 'list', '1');


INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Register bulk students', 'registration', 'Registration', '1', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Registration list', 'registration', 'Registration', '2', 'welcome', 'list', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Defer Registration', 'registration', 'Registration', '3', 'welcome', 'list', '1');

---------------- UPDATE Here From Server --------------------------


CREATE TABLE `course` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `id_staff_coordinator` int(10) DEFAULT NULL,
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `credit_hours` varchar(20) DEFAULT '',
  `id_department` int(10) DEFAULT NULL,
  `id_faculty_program` int(20) DEFAULT 0,
  `id_course_type` int(20) DEFAULT 0,
  `id_course_description` int(20) DEFAULT 0,
  `class_total_hr` int(20) DEFAULT 0,
  `class_recurrence` varchar(50) DEFAULT '',
  `class_recurrence_period` varchar(50) DEFAULT '',
  `tutorial_total_hr` int(20) DEFAULT 0,
  `tutorial_recurrence` varchar(50) DEFAULT '',
  `tutorial_recurrence_period` varchar(50) DEFAULT '',
  `lab_total_hr` int(20) DEFAULT 0,
  `lab_recurrence` varchar(50) DEFAULT '',
  `lab_recurrence_period` varchar(50) DEFAULT '',
  `exam_total_hr` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `id_staff_coordinator`, `name_in_malay`, `code`, `credit_hours`, `id_department`, `id_faculty_program`, `id_course_type`, `id_course_description`, `class_total_hr`, `class_recurrence`, `class_recurrence_period`, `tutorial_total_hr`, `tutorial_recurrence`, `tutorial_recurrence_period`, `lab_total_hr`, `lab_recurrence`, `lab_recurrence_period`, `exam_total_hr`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
('IT SECURITY POLICY AND STRATEGY', 0, 'IT SECURITY POLICY AND STRATEGY', 'MIT 7083', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:19:32', NULL, '2021-01-12 11:19:32'),
('KNOWLEDGE MANAGEMENT', 0, 'KNOWLEDGE MANAGEMENT', 'MIT 7093', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:20:19', NULL, '2021-01-12 11:20:19'),
('DATA SCIENCE', 0, 'DATA SCIENCE', 'MIT 7123', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:20:47', NULL, '2021-01-12 11:20:47'),
('E-COMMERCE STRATEGY AND DEVELOPMENT', 0, 'E-COMMERCE STRATEGY AND DEVELOPMENT', 'MIT 7103', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:21:16', NULL, '2021-01-12 11:21:16'),
('PROJECT AND CHANGE MANAGEMENT', 0, 'PROJECT AND CHANGE MANAGEMENT', 'MIT 7063', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:21:41', NULL, '2021-01-12 11:21:41'),
('SYSTEM DEVELOPMENT METHODOLOGIES', 0, 'SYSTEM DEVELOPMENT METHODOLOGIES', 'MIT 7043', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:22:10', NULL, '2021-01-12 11:22:10'),
('DATA MANAGEMENT', 0, 'DATA MANAGEMENT', 'MIT 7033', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:22:40', NULL, '2021-01-12 11:22:40'),
('RESEARCH METHODOLOGY FOR IT', 0, 'RESEARCH METHODOLOGY FOR IT', 'MIT 7013', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:23:08', NULL, '2021-01-12 11:23:08'),
('PROJECT PAPER', 0, 'PROJECT PAPER', 'MIT 7130', '9', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:23:35', NULL, '2021-01-12 11:23:35'),
('BUSINESS DATA COMMUNICATION', 0, 'BUSINESS DATA COMMUNICATION', 'MIT 7053', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:24:17', NULL, '2021-01-12 11:24:17'),
('MOBILE COMMUNICATIONS', 0, 'MOBILE COMMUNICATIONS', 'MIT 7113', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:25:05', NULL, '2021-01-12 11:25:05'),
('ISLAMIC WORLDVIEW, IT AND SOCIETY', 0, 'ISLAMIC WORLDVIEW, IT AND SOCIETY', 'MIT 7023', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:25:45', NULL, '2021-01-12 11:25:45'),
('ORGANIZATIONAL BEHAVIOUR', 0, 'ORGANIZATIONAL BEHAVIOUR', 'MGT 7013', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:26:34', NULL, '2021-01-12 11:26:34'),
('STRATEGIC MANAGEMENT', 0, 'STRATEGIC MANAGEMENT', 'MGT 7033', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 11:27:00', NULL, '2021-01-12 11:27:00'),
('Elective Courses', 0, 'Elective Courses', 'ELEC', '3', 2, 7, 0, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2021-01-12 20:51:44', NULL, '2021-01-12 20:51:44');




















