-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2021 at 11:38 PM
-- Server version: 5.7.32-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `speed`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_last_login`
--

CREATE TABLE `admin_last_login` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `id` int(20) NOT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `id_type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT '0',
  `id_degree_type` int(20) DEFAULT '0',
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `passport_number` varchar(50) DEFAULT '',
  `is_updated` int(2) DEFAULT '0',
  `is_submitted` int(2) DEFAULT '0',
  `submitted_date` datetime DEFAULT NULL,
  `email_verified` int(11) DEFAULT '0',
  `initial` int(2) DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT '0',
  `approved_dt_tm` datetime DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_last_login`
--

CREATE TABLE `applicant_last_login` (
  `id` bigint(20) NOT NULL,
  `id_applicant` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `applicant_last_login`
--

INSERT INTO `applicant_last_login` (`id`, `id_applicant`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(0, 0, '{"applicant_name":"Miss. kiran as","email_id":"1@1.com"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-25 21:15:32'),
(1, 1, '{"applicant_name":"Mr. Vinay Kiran","email_id":"vk@cms.com"}', '157.49.105.34', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-25 07:27:45');

-- --------------------------------------------------------

--
-- Table structure for table `award`
--

CREATE TABLE `award` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `level` int(20) DEFAULT '0',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `award`
--

INSERT INTO `award` (`id`, `name`, `level`, `description`, `code`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Foundation', 1, 'Foundation', 'FDN', NULL, 1, 1, '2020-07-10 23:44:20', NULL, '2020-07-10 23:44:20'),
(2, 'Diploma', 2, 'Diploma', 'DIP', NULL, 1, 1, '2020-08-09 05:03:14', NULL, '2020-08-09 05:03:14'),
(3, 'Degree', 3, 'Degree', 'DEG', NULL, 1, 1, '2020-08-09 05:03:46', NULL, '2020-08-09 05:03:46'),
(4, 'Master', 4, 'Master', 'MAS', NULL, 1, 1, '2020-08-09 05:04:15', NULL, '2020-08-09 05:04:15'),
(5, 'PHD', 5, 'PHD', 'PHD', NULL, 1, 1, '2020-08-09 05:05:13', NULL, '2020-08-09 05:05:13'),
(6, 'Graduate Diploma', 4, 'Graduate Diploma', 'GDIP', NULL, 1, 1, '2020-08-09 05:06:39', NULL, '2020-08-09 05:06:39');

-- --------------------------------------------------------

--
-- Table structure for table `bank_registration`
--

CREATE TABLE `bank_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `account_no` varchar(512) DEFAULT '',
  `bank_id` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `cr_fund` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_account` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_registration`
--

INSERT INTO `bank_registration` (`id`, `name`, `code`, `account_no`, `bank_id`, `address`, `landmark`, `city`, `id_state`, `id_country`, `zipcode`, `cr_fund`, `cr_department`, `cr_activity`, `cr_account`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 'B1', 'B2', '8888', 'dasda', '', 'Land', 'KL', 2, 1, 123, '', '', '', '', 1, 1, '2020-09-25 01:10:31', NULL, '2020-09-25 01:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `image`) VALUES
(1, 'EARLY CHILDHOOD EDUCATION', '', '', 1, 1, '2020-09-25 01:55:49', NULL, '2020-09-25 01:55:49', '1.jpeg'),
(2, 'KURSUS ASUHAN PERMATA ', '', '', 1, 1, '2020-09-25 02:21:27', NULL, '2020-09-25 02:21:27', '1.jpeg'),
(3, 'MANAGEMENT AND LEADERSHIP Courses', '', '', 1, 1, '2020-09-25 02:21:27', NULL, '2020-09-25 02:21:27', '2.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `category_has_module`
--

CREATE TABLE `category_has_module` (
  `id` int(20) NOT NULL,
  `id_category` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_has_module`
--

INSERT INTO `category_has_module` (`id`, `id_category`, `id_course`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, '2020-09-25 01:56:03', NULL, '2020-09-25 01:56:03'),
(2, 1, 4, 1, 1, '2020-09-25 01:56:08', NULL, '2020-09-25 01:56:08'),
(4, 2, 2, 1, 1, '2020-09-25 02:21:56', NULL, '2020-09-25 02:21:56'),
(5, 2, 4, 1, 1, '2020-09-25 02:22:01', NULL, '2020-09-25 02:22:01');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group`
--

CREATE TABLE `communication_group` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT '0',
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message`
--

CREATE TABLE `communication_group_message` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT '0',
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message_recepients`
--

CREATE TABLE `communication_group_message_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT '0',
  `id_recepient` int(20) DEFAULT '0',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_recepients`
--

CREATE TABLE `communication_group_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT '0',
  `id_recepient` int(20) DEFAULT '0',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_template_message`
--

CREATE TABLE `communication_template_message` (
  `id` int(20) NOT NULL,
  `name` text,
  `subject` text,
  `message` text,
  `status` int(2) DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(2) DEFAULT '0',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(2) DEFAULT '0',
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'India', 1, 0, '2020-02-27 15:59:29', 0, '2020-02-27 15:59:29');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(20) NOT NULL,
  `id_category` int(20) DEFAULT '0',
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `months` int(20) DEFAULT '0',
  `name_optional_language` varchar(1024) DEFAULT '',
  `file` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `best_selling` varchar(10) NOT NULL,
  `best_trending` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `id_category`, `name`, `code`, `months`, `name_optional_language`, `file`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `best_selling`, `best_trending`) VALUES
(1, 1, 'Children Learning Science', 'CIIF1', 0, '', 'c1.jpg', 1, 1, '2020-08-14 18:04:41', 1, '2020-08-14 18:04:41', '', '1'),
(2, 1, 'Socioemotional Development of Children', 'CIIF2', 0, '', 'c2.jpg', 1, 1, '2020-08-14 18:04:49', 1, '2020-08-14 18:04:49', '1', ''),
(3, 1, 'Creative & Aesthetic Development', 'CIIF3', 0, '', 'c3.jpg', 1, 1, '2020-08-14 18:04:56', 1, '2020-08-14 18:04:56', '', '1'),
(4, 1, 'Language & Early Literacy', 'CIIF4', 0, '', 'c4.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', '1'),
(5, 1, 'Mathematics & Logic for Children', 'CIIF4', 0, '', 'c5.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(6, 1, 'Curriculum & Teaching methods for children', 'CIIF4', 0, '', 'c6.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(7, 1, 'Home-School Partnership', 'CIIF4', 0, '', 'c7.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(10, 2, 'Falsafah & Matlamat Program PERMATA', 'CIIF4', 0, '', 's1.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(11, 2, 'Undang-Undang Berkaitan Dengan Kanak-Kanak\n', 'CIIF4', 0, '', 's2.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(12, 2, 'Perkembangan & Pertumbuhan  Kanak-Kanak', 'CIIF4', 0, '', 's3.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(13, 2, 'Pengasuhan, Etika  & Profesionalisme', 'CIIF4', 0, '', 's4.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(14, 2, 'Kaedah Pengajaran Kanak-Kanak TASKA', 'CIIF4', 0, '', 's5.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(15, 2, 'Pentaksiran & Pemerhatian Kanak-Kanak di TASKA', 'CIIF4', 0, '', 's6.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(16, 2, 'Pengurusan & Persekitaran Kanak-Kanak', 'CIIF4', 0, '', 's7.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(17, 2, 'Penjadualan & Kolaborasi dengan Ibu Bapa', 'CIIF4', 0, '', 's8.jpg', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07', '', ''),
(18, 1, 'dasda', 'dsad', 6, '', '', 1, 1, '2021-01-02 22:16:47', NULL, '2021-01-02 22:16:47', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `currency_rate_setup`
--

CREATE TABLE `currency_rate_setup` (
  `id` int(20) NOT NULL,
  `id_currency` int(20) DEFAULT '0',
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_rate_setup`
--

INSERT INTO `currency_rate_setup` (`id`, `id_currency`, `exchange_rate`, `min_rate`, `max_rate`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, '12', '12', '12', '2020-09-24 00:00:00', 1, 1, '2020-09-25 00:51:19', NULL, '2020-09-25 00:51:19'),
(2, 2, '', '', '', '1970-01-01 00:00:00', 1, 1, '2021-01-02 23:08:18', NULL, '2021-01-02 23:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `currency_setup`
--

CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT '0',
  `default` int(2) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_setup`
--

INSERT INTO `currency_setup` (`id`, `code`, `name`, `name_optional_language`, `prefix`, `suffix`, `decimal_place`, `default`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 'COde ', 'Name', 'Default', 'Pref', 'Suff', 3, 1, 1, 1, '2020-09-24 17:33:56', NULL, '2020-09-24 17:33:56');

-- --------------------------------------------------------

--
-- Table structure for table `fee_category`
--

CREATE TABLE `fee_category` (
  `id` int(20) NOT NULL,
  `code` varchar(520) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `fee_group` varchar(200) DEFAULT '',
  `sequence` int(2) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_category`
--

INSERT INTO `fee_category` (`id`, `code`, `name`, `name_optional_language`, `fee_group`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Fee Cat 1', 'Fee Cat 1', '`', 'Rental', 1, 1, NULL, '2020-09-25 01:41:10', NULL, '2020-09-25 01:41:10');

-- --------------------------------------------------------

--
-- Table structure for table `fee_setup`
--

CREATE TABLE `fee_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(50) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `id_fee_category` int(10) DEFAULT NULL,
  `id_amount_calculation_type` int(10) DEFAULT NULL,
  `id_frequency_mode` int(10) DEFAULT NULL,
  `account_code` varchar(50) DEFAULT '',
  `is_refundable` int(2) DEFAULT NULL,
  `is_non_invoice` int(2) DEFAULT NULL,
  `is_gst` int(2) DEFAULT NULL,
  `gst_tax` varchar(50) DEFAULT '',
  `effective_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_setup`
--

INSERT INTO `fee_setup` (`id`, `code`, `name`, `name_optional_language`, `id_fee_category`, `id_amount_calculation_type`, `id_frequency_mode`, `account_code`, `is_refundable`, `is_non_invoice`, `is_gst`, `gst_tax`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(2, 'Code 1', 'Name', '', 1, 0, 0, 'Code', 0, 0, 0, 'Code', '2020-09-16 00:00:00', 1, NULL, '2020-09-25 01:58:39', NULL, '2020-09-25 01:58:39'),
(3, 'REG', 'Registration Fee', '', 1, 0, 0, '1', 0, 0, 0, 'aaa', '2020-09-03 00:00:00', 1, NULL, '2020-09-25 05:51:12', NULL, '2020-09-25 05:51:12');

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure`
--

CREATE TABLE `fee_structure` (
  `id` int(20) NOT NULL,
  `id_fee_structure` int(20) DEFAULT '0',
  `id_category` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `id_fee_item` int(20) DEFAULT '0',
  `amount` float(20,2) DEFAULT '0.00',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_structure`
--

INSERT INTO `fee_structure` (`id`, `id_fee_structure`, `id_category`, `id_course`, `id_fee_item`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(5, 4, 1, 2, 3, 800.00, 1, NULL, '2020-09-26 22:36:01', NULL, '2020-09-26 22:36:01'),
(6, 3, 1, 1, 3, 800.00, 1, NULL, '2020-09-26 22:36:29', NULL, '2020-09-26 22:36:29'),
(7, 5, 1, 3, 3, 800.00, 1, NULL, '2020-09-26 22:36:50', NULL, '2020-09-26 22:36:50'),
(8, 6, 1, 4, 3, 800.00, 1, NULL, '2020-09-26 22:37:18', NULL, '2020-09-26 22:37:18'),
(9, 7, 1, 5, 3, 800.00, 1, NULL, '2020-09-26 22:37:24', NULL, '2020-09-26 22:37:24'),
(10, 8, 1, 6, 3, 800.00, 1, NULL, '2020-09-26 22:37:29', NULL, '2020-09-26 22:37:29'),
(11, 9, 1, 7, 3, 800.00, 1, NULL, '2020-09-26 22:37:34', NULL, '2020-09-26 22:37:34'),
(12, 12, 2, 10, 3, 800.00, 1, NULL, '2020-09-26 22:37:39', NULL, '2020-09-26 22:37:39'),
(13, 13, 2, 11, 3, 800.00, 1, NULL, '2020-09-26 22:37:44', NULL, '2020-09-26 22:37:44'),
(14, 14, 2, 12, 3, 800.00, 1, NULL, '2020-09-26 22:37:49', NULL, '2020-09-26 22:37:49'),
(15, 15, 2, 13, 3, 800.00, 1, NULL, '2020-09-26 22:37:54', NULL, '2020-09-26 22:37:54'),
(16, 16, 2, 14, 3, 800.00, 1, NULL, '2020-09-26 22:37:59', NULL, '2020-09-26 22:37:59'),
(17, 17, 2, 15, 3, 800.00, 1, NULL, '2020-09-26 22:38:04', NULL, '2020-09-26 22:38:04'),
(18, 18, 2, 16, 3, 800.00, 1, NULL, '2020-09-26 22:38:17', NULL, '2020-09-26 22:38:17'),
(19, 19, 2, 17, 3, 800.00, 1, NULL, '2020-09-26 22:38:23', NULL, '2020-09-26 22:38:23');

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure_main`
--

CREATE TABLE `fee_structure_main` (
  `id` int(20) NOT NULL,
  `id_category` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `id_currency` int(20) DEFAULT '0',
  `amount` float(20,2) DEFAULT '0.00',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_structure_main`
--

INSERT INTO `fee_structure_main` (`id`, `id_category`, `id_course`, `id_currency`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 1, 1, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(4, 1, 2, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(5, 1, 3, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(6, 1, 4, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(7, 1, 5, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(8, 1, 6, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(9, 1, 7, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(10, 1, 8, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(11, 1, 9, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(12, 2, 10, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(13, 2, 11, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(14, 2, 12, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(15, 2, 13, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(16, 2, 14, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(17, 2, 15, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(18, 2, 16, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(19, 2, 17, 1, 800.00, 1, NULL, '2020-09-25 05:50:16', NULL, '2020-09-25 05:50:16'),
(20, 0, 0, 1, 0.00, 1, NULL, '2021-01-02 23:26:03', NULL, '2021-01-02 23:26:03');

-- --------------------------------------------------------

--
-- Table structure for table `financial_account_code`
--

CREATE TABLE `financial_account_code` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `type` varchar(512) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(2) DEFAULT '0',
  `id_parent` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financial_account_code`
--

INSERT INTO `financial_account_code` (`id`, `name`, `type`, `name_optional_language`, `short_code`, `code`, `level`, `id_parent`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Type', '', '', 'Code', 0, 0, 1, 1, '2020-09-24 19:15:07', 1, '2020-09-24 19:15:07');

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice`
--

CREATE TABLE `main_invoice` (
  `id` int(20) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `invoice_number` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `remarks` varchar(520) DEFAULT '',
  `id_application` int(10) DEFAULT NULL,
  `id_student` int(10) DEFAULT NULL,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT '0.00',
  `invoice_total` float(20,2) DEFAULT '0.00',
  `total_discount` float(20,2) DEFAULT '0.00',
  `balance_amount` float(20,2) DEFAULT '0.00',
  `paid_amount` float(20,2) DEFAULT '0.00',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_invoice`
--

INSERT INTO `main_invoice` (`id`, `type`, `id_category`, `id_course`, `invoice_number`, `date_time`, `remarks`, `id_application`, `id_student`, `currency`, `total_amount`, `invoice_total`, `total_discount`, `balance_amount`, `paid_amount`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Student', 1, 2, 'INV000001/2020', '2020-09-27 11:42:20', '', NULL, 1, '1', 800.00, 800.00, 0.00, 0.00, 800.00, '', 1, NULL, '2020-09-27 11:42:20', NULL, '2020-09-27 11:42:20'),
(2, 'Student', 1, 3, 'INV000002/2020', '2020-09-27 11:43:31', '', NULL, 1, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-09-27 11:43:31', NULL, '2020-09-27 11:43:31'),
(3, 'Student', 1, 2, 'INV000003/2020', '2020-09-27 12:13:33', '', NULL, 1, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-09-27 12:13:33', NULL, '2020-09-27 12:13:33'),
(4, 'Student', 1, 2, 'INV000004/2020', '2020-09-27 12:14:58', '', NULL, 1, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-09-27 12:14:58', NULL, '2020-09-27 12:14:58'),
(5, 'Student', 1, 2, 'INV000005/2020', '2020-09-27 12:15:43', '', NULL, 1, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-09-27 12:15:43', NULL, '2020-09-27 12:15:43'),
(6, 'Student', 1, 2, 'INV000006/2020', '2020-09-27 12:16:12', '', NULL, 1, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-09-27 12:16:12', NULL, '2020-09-27 12:16:12'),
(7, 'Student', 1, 7, 'INV000007/2020', '2020-09-27 12:31:46', '', NULL, 1, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-09-27 12:31:46', NULL, '2020-09-27 12:31:46'),
(8, 'Student', 1, 2, 'INV000008/2020', '2020-10-01 21:54:30', '', NULL, NULL, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-10-01 21:54:30', NULL, '2020-10-01 21:54:30'),
(9, 'Student', 1, 2, 'INV000009/2020', '2020-10-02 12:04:19', '', NULL, NULL, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-10-02 12:04:19', NULL, '2020-10-02 12:04:19'),
(10, 'Student', 1, 3, 'INV000010/2020', '2020-10-06 06:02:23', '', NULL, NULL, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-10-06 06:02:23', NULL, '2020-10-06 06:02:23'),
(11, 'Student', 1, 3, 'INV000011/2020', '2020-10-06 06:04:10', '', NULL, NULL, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-10-06 06:04:10', NULL, '2020-10-06 06:04:10'),
(12, 'Student', 1, 3, 'INV000012/2020', '2020-10-06 06:05:07', '', NULL, NULL, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-10-06 06:05:07', NULL, '2020-10-06 06:05:07'),
(13, 'Student', 1, 3, 'INV000013/2020', '2020-10-06 06:05:46', '', NULL, NULL, '1', 800.00, 800.00, 0.00, 800.00, 0.00, '', 1, NULL, '2020-10-06 06:05:46', NULL, '2020-10-06 06:05:46'),
(14, 'Student', 1, 6, 'INV000014/2020', '2020-10-06 06:46:58', '', NULL, 1, '1', 800.00, 800.00, 0.00, 0.00, 800.00, '', 1, NULL, '2020-10-06 06:46:58', NULL, '2020-10-06 06:46:58'),
(15, 'Student', 1, 4, 'INV000015/2020', '2020-10-06 07:19:40', '', NULL, 1, '1', 800.00, 800.00, 0.00, 0.00, 800.00, '', 1, NULL, '2020-10-06 07:19:40', NULL, '2020-10-06 07:19:40'),
(16, 'Student', 2, 12, 'INV000016/2020', '2020-10-06 07:44:46', '', NULL, 1, '1', 800.00, 800.00, 0.00, 0.00, 800.00, '', 1, NULL, '2020-10-06 07:44:46', NULL, '2020-10-06 07:44:46'),
(17, 'Student', 1, 2, 'INV000017/2020', '2020-10-06 10:43:44', '', NULL, 1, '1', 800.00, 800.00, 0.00, 0.00, 800.00, '', 1, NULL, '2020-10-06 10:43:44', NULL, '2020-10-06 10:43:44'),
(18, 'Student', 1, 2, 'INV000018/2020', '2020-10-25 00:53:10', '', NULL, 1, '1', 0.00, 0.00, 0.00, 0.00, 0.00, '', 1, NULL, '2020-10-25 00:53:10', NULL, '2020-10-25 00:53:10');

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice_details`
--

CREATE TABLE `main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `price` int(20) DEFAULT '0',
  `quantity` int(20) DEFAULT '0',
  `amount` float(20,2) DEFAULT NULL,
  `id_reference` int(20) DEFAULT '0',
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_invoice_details`
--

INSERT INTO `main_invoice_details` (`id`, `id_main_invoice`, `id_fee_item`, `price`, `quantity`, `amount`, `id_reference`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-09-27 11:42:20', NULL, '2020-09-27 11:42:20'),
(2, 2, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-09-27 11:43:31', NULL, '2020-09-27 11:43:31'),
(3, 3, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-09-27 12:13:33', NULL, '2020-09-27 12:13:33'),
(4, 4, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-09-27 12:14:58', NULL, '2020-09-27 12:14:58'),
(5, 5, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-09-27 12:15:43', NULL, '2020-09-27 12:15:43'),
(6, 6, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-09-27 12:16:12', NULL, '2020-09-27 12:16:12'),
(7, 7, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-09-27 12:31:46', NULL, '2020-09-27 12:31:46'),
(8, 8, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-01 21:54:30', NULL, '2020-10-01 21:54:30'),
(9, 9, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-02 12:04:19', NULL, '2020-10-02 12:04:19'),
(10, 10, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-06 06:02:23', NULL, '2020-10-06 06:02:23'),
(11, 11, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-06 06:04:10', NULL, '2020-10-06 06:04:10'),
(12, 12, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-06 06:05:07', NULL, '2020-10-06 06:05:07'),
(13, 13, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-06 06:05:47', NULL, '2020-10-06 06:05:47'),
(14, 14, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-06 06:46:58', NULL, '2020-10-06 06:46:58'),
(15, 15, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-06 07:19:40', NULL, '2020-10-06 07:19:40'),
(16, 16, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-06 07:44:46', NULL, '2020-10-06 07:44:46'),
(17, 17, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-06 10:43:44', NULL, '2020-10-06 10:43:44'),
(18, 18, 3, 800, 1, 800.00, 0, '', NULL, NULL, '2020-10-25 00:53:10', NULL, '2020-10-25 00:53:10');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(20) NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL,
  `parent_name` varchar(200) DEFAULT NULL,
  `order` int(20) DEFAULT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  `parent_order` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES
(1, 'User', 'Setup', 'General Setup', 1, 'user', 'list', 1),
(2, 'Role', 'Setup', 'General Setup', 2, 'role', 'list', 1),
(3, 'Permission', 'Setup', 'General Setup', 3, 'permission', 'list', 1),
(10, 'Category', 'cm', 'Functional Setup', 1, 'category', 'list', 1),
(11, 'Courses', 'cm', 'Functional Setup', 2, 'course', 'list', 1),
(12, 'Unit', 'cm', 'Functional Setup', 3, 'unit', 'list', 1),
(56, 'Barring Type', 'Records', NULL, NULL, NULL, NULL, NULL),
(57, 'Barring', 'Records', NULL, NULL, NULL, NULL, NULL),
(58, 'Release', 'Records', NULL, NULL, NULL, NULL, NULL),
(59, 'Student Profile', 'Records', NULL, NULL, NULL, NULL, NULL),
(60, 'Student Records', 'Records', 'Student Record', 1, 'student', 'list', 1),
(61, 'Change Status', 'Records', NULL, NULL, NULL, NULL, NULL),
(62, 'Apply Change Status', 'Records', NULL, NULL, NULL, NULL, NULL),
(63, 'Apply Change Status Approval', 'Records', NULL, NULL, NULL, NULL, NULL),
(64, 'Visa Details', 'Records', NULL, NULL, NULL, NULL, NULL),
(65, 'Apply Change Program', 'Records', NULL, NULL, NULL, NULL, NULL),
(66, 'Apply Change Program Approval', 'Records', NULL, NULL, NULL, NULL, NULL),
(67, 'Apply Change Scheme', 'Records', NULL, NULL, NULL, NULL, NULL),
(68, 'Apply Change Scheme Approval', 'Records', NULL, NULL, NULL, NULL, NULL),
(69, 'Change Branch', 'Records', NULL, NULL, NULL, NULL, NULL),
(70, 'Change Branch Approval', 'Records', NULL, NULL, NULL, NULL, NULL),
(100, 'Currency Setup', 'Finance', 'Finance Setup', 1, 'currency', 'list', 1),
(101, 'Currency Rate Setup', 'Finance', 'Finance Setup', 2, 'currencyRateSetup', 'list', 1),
(102, 'Credit Note Type', 'Finance', 'Finance Setup', NULL, NULL, NULL, NULL),
(103, 'Fee Structure Activity', 'Finance', 'Finance Setup', NULL, NULL, NULL, NULL),
(104, 'Account Code', 'Finance', 'General Setup', 1, 'accountCode', 'list', 2),
(105, 'Bank Registration', 'Finance', 'General Setup', 2, 'bankRegistration', 'list', 2),
(108, 'Payment Type', 'Finance', 'General Setup', 3, 'paymentType', 'list', 2),
(109, 'Fee Category', 'Finance', 'General Setup', 4, 'feeCategory', 'list', 2),
(110, 'Fee Setup', 'Finance', 'General Setup', 5, 'feeSetup', 'list', 2),
(111, 'Fee Structure', 'Finance', 'General Setup', 6, 'feeStructure', 'list', 2),
(153, 'Templates', 'Communication', 'Communication', 1, 'template', 'list', 1),
(154, 'Group', 'Communication', 'Communication', 2, 'group', 'list', 1),
(155, 'Group Recepients', 'Communication', 'Communication', 3, 'group', 'recepientList', 1),
(156, 'Messaging Templates', 'Communication', 'Messaging', 1, 'templateMessage', 'list', 2),
(157, 'Messaging Group', 'Communication', 'Messaging', 2, 'groupMessage', 'list', 2),
(158, 'Group Recepients (Messaging)', 'Communication', 'Messaging', 3, 'groupMessage', 'recepientList', 2),
(190, 'Main Invoice', 'Finance', 'Invoice', 1, 'mainInvoice', 'list', 3),
(191, 'Receipt Invoice', 'Finance', 'Receipt', 1, 'receipt', 'list', 4),
(192, 'Payment Rate', 'af', 'Setup', 1, 'paymentRate', 'list', 1),
(193, 'Academic Facilitator profile', 'af', 'Setup', 2, 'staff', 'list', 1),
(194, 'Change Of Status', 'af', 'Records management', 1, 'staff', 'changeStatusList', 2),
(195, 'Online claim', 'af', 'Online claim', 2, 'onlineClaim', 'list', 2),
(196, 'Statement of account', 'af', 'Statement Of Account', 3, 'statementOfAccount', 'list', 3);

-- --------------------------------------------------------

--
-- Table structure for table `online_claim`
--

CREATE TABLE `online_claim` (
  `id` bigint(20) NOT NULL,
  `type` varchar(256) NOT NULL,
  `id_staff` bigint(20) NOT NULL,
  `id_university` bigint(20) NOT NULL,
  `id_branch` bigint(20) NOT NULL,
  `id_programme` bigint(20) NOT NULL,
  `id_semester` bigint(20) NOT NULL,
  `total_amount` float(20,2) DEFAULT '0.00',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `online_claim`
--

INSERT INTO `online_claim` (`id`, `type`, `id_staff`, `id_university`, `id_branch`, `id_programme`, `id_semester`, `total_amount`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(4, 'Academic Facilitator', 1, 0, 0, 0, 0, 240.00, '', 1, 1, '2021-01-02 22:10:12', NULL, '2021-01-02 22:10:12');

-- --------------------------------------------------------

--
-- Table structure for table `online_claim_details`
--

CREATE TABLE `online_claim_details` (
  `id` bigint(20) NOT NULL,
  `id_online_claim` int(20) NOT NULL,
  `id_course` bigint(20) NOT NULL,
  `id_payment_rate` bigint(20) NOT NULL,
  `hours` bigint(20) NOT NULL,
  `date_time` varchar(2048) DEFAULT '',
  `amount` float(20,2) NOT NULL,
  `status` int(20) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `online_claim_details`
--

INSERT INTO `online_claim_details` (`id`, `id_online_claim`, `id_course`, `id_payment_rate`, `hours`, `date_time`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 4, 4, 1, 12, '2021-01-28', 120.00, 1, NULL, '2021-01-02 22:09:30', NULL, '2021-01-02 22:09:30'),
(4, 4, 4, 1, 12, '2021-01-28', 120.00, 1, NULL, '2021-01-02 22:09:31', NULL, '2021-01-02 22:09:31');

-- --------------------------------------------------------

--
-- Table structure for table `payment_rate`
--

CREATE TABLE `payment_rate` (
  `id` int(20) NOT NULL,
  `id_award` int(20) DEFAULT '0',
  `learning_mode` varchar(200) DEFAULT '',
  `teaching_component` varchar(200) DEFAULT '',
  `id_currency` int(20) DEFAULT '0',
  `calculation_type` varchar(200) DEFAULT '',
  `amount` float(20,2) DEFAULT '0.00',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_rate`
--

INSERT INTO `payment_rate` (`id`, `id_award`, `learning_mode`, `teaching_component`, `id_currency`, `calculation_type`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 'Full Time', 'Face to face tutorial', 2, 'Per hour', 12.00, 1, 1, '2020-12-31 23:08:41', NULL, '2020-12-31 23:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `description_optional_language` varchar(2048) DEFAULT '',
  `payment_group` varchar(512) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `name`, `description`, `description_optional_language`, `payment_group`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, '', 'Cash Payment', '', 'Cash', 'Cash', '', 1, NULL, '2020-08-20 10:08:10', NULL, '2020-08-20 10:08:10'),
(2, '', 'Bank Checue Transfer', 'dasda', 'Checque', 'Bank Transfer', '', 1, NULL, '2020-08-20 10:09:30', NULL, '2020-08-20 10:09:30'),
(3, '', '', '', '', '', '', 1, NULL, '2020-09-25 01:26:05', NULL, '2020-09-25 01:26:05'),
(4, '', 'Dadsa', '', 'Checque', 'Desc', '', 0, NULL, '2020-09-25 01:27:07', NULL, '2020-09-25 01:27:07'),
(5, '', '', '', '', '', '', 1, NULL, '2021-01-02 23:16:18', NULL, '2021-01-02 23:16:18');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `code`, `description`, `status`) VALUES
(5, 'student.studentexams', 'Student Exam page view', 1),
(6, 'role.list', 'List Role', 1),
(7, 'permission.add', 'Add Permission', 1),
(8, 'permission.list', 'View Permission', 1),
(9, 'permission.edit', 'Edit Permission', 1),
(10, 'user.edit', 'Edit User', 1),
(11, 'user.add', 'Add User', 1),
(12, 'user.list', 'View User', 1),
(13, 'course.list', 'View Course', 1),
(14, 'course.add', 'Add Course', 1),
(15, 'course.edit', 'Edit Course', 1),
(16, 'topic.edit', 'Edit Topic', 1),
(17, 'topic.add', 'Add Topic', 1),
(18, 'topic.list', 'View Topic', 1),
(19, 'learningobjective.list', 'View Learning Objective', 1),
(20, 'learningobjective.add', 'Add Learning Objective', 1),
(21, 'learningobjective.list', 'View Learning Objective', 1),
(22, 'taxonomy.list', 'View Blood Taxonomy', 1),
(23, 'taxonomy.add', 'Add Blood Taxonomy', 1),
(24, 'taxonomy.edit', 'Edit Blood Taxonomy', 1),
(25, 'difficultylevel.edit', 'Edit Difficulty Level', 1),
(26, 'difficultylevel.add', 'Add Difficulty Level', 1),
(27, 'difficultylevel.list', 'View Difficulty Level', 1),
(28, 'question.list', 'View Question', 1),
(29, 'question.add', 'Add Question', 1),
(30, 'question.edit', 'Edit Question', 1),
(31, 'questionhasoption.add', 'Add Question Options', 1),
(33, 'exam_name.list', 'List Exam Name', 1),
(34, 'exam_name.add', 'Add Exam Name', 1),
(35, 'exam_name.edit', 'Edit Exam Name', 1),
(36, 'exam_center.list', 'View Exam Center', 1),
(37, 'exam_center.add', 'Add Exam Center', 1),
(38, 'exam_center.edit', 'Edit Exam Center', 1),
(39, 'exam_event.edit', 'Edit Exam Event', 1),
(40, 'exam_event.list', 'View Exam Event', 1),
(41, 'exam_event.add', 'Add Exam Event', 1),
(42, 'exam_has_question.add', 'Add Exam Has Question', 1),
(43, 'exam_has_question.edit', 'Edit Exam Has Question', 1),
(44, 'role.add', 'Add Role', 1),
(45, 'exam_has_question.list', 'View Exam Has Question', 1),
(46, 'grade.list', 'View Grade', 1),
(47, 'grade.add', 'Add Grade', 1),
(48, 'grade.edit', 'Edit Grade', 1),
(49, 'location.edit', 'Edit Location', 1),
(50, 'location.add', 'Add Location', 1),
(51, 'location.list', 'View Location', 1),
(52, 'questionpool.list', 'List Question Pool', 1),
(53, 'questionpool.add', 'Add Question Pool', 1),
(54, 'questionpool.edit', 'Edit Question Pool', 1),
(55, 'tos.add', 'Add TOS', 1),
(56, 'tos.list', 'View TOS', 1),
(57, 'tos.edit', 'Edit TOS', 1),
(58, 'examset.add', 'Add Exam Set', 1),
(59, 'examset.list', 'View Exam Set', 1),
(60, 'examset.edit', 'Edit Exam Set', 1),
(61, 'student.list', 'List Student', 1),
(62, 'student.add', 'Add Student', 1),
(63, 'student.edit', 'Edit Student', 1),
(64, 'exam_student_tagging.add', 'Add Exam Student Tagging', 1),
(65, 'exam_student_tagging.list', 'View Exam Student Tagging', 1),
(66, 'exam_student_tagging.edit', 'Edit Exam Student Tagging', 1),
(67, 'role.edit', 'Role Edit', 1),
(68, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` int(20) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT '0',
  `id_student` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT '0',
  `receipt_number` varchar(50) DEFAULT '',
  `receipt_amount` float(20,2) DEFAULT NULL,
  `remarks` varchar(520) DEFAULT '',
  `currency` varchar(520) DEFAULT '',
  `receipt_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `approval_status` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `type`, `id_category`, `id_student`, `id_course`, `receipt_number`, `receipt_amount`, `remarks`, `currency`, `receipt_date`, `approval_status`, `status`, `reason`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Student', 1, 1, 2, 'REC000001/2020', 800.00, '', '1', '2020-09-27 11:43:32', '', 1, '', NULL, '2020-09-27 11:43:32', NULL, '2020-09-27 11:43:32'),
(2, 'Student', 1, 1, 2, 'REC000002/2020', 800.00, '', '1', '2020-09-27 12:31:46', '', 1, '', NULL, '2020-09-27 12:31:46', NULL, '2020-09-27 12:31:46'),
(3, 'Student', 1, 1, 2, 'REC000003/2020', 800.00, '', '1', '2020-10-01 21:54:30', '', 1, '', NULL, '2020-10-01 21:54:30', NULL, '2020-10-01 21:54:30'),
(4, 'Student', 1, 1, 2, 'REC000004/2020', 800.00, '', '1', '2020-10-02 12:04:19', '', 1, '', NULL, '2020-10-02 12:04:19', NULL, '2020-10-02 12:04:19'),
(5, 'Student', 1, 1, 6, 'REC000005/2020', 800.00, '', '1', '2020-10-06 06:46:58', '', 1, '', NULL, '2020-10-06 06:46:58', NULL, '2020-10-06 06:46:58'),
(6, 'Student', 1, 1, 4, 'REC000006/2020', 800.00, '', '1', '2020-10-06 07:19:40', '', 1, '', NULL, '2020-10-06 07:19:40', NULL, '2020-10-06 07:19:40'),
(7, 'Student', 2, 1, 12, 'REC000007/2020', 800.00, '', '1', '2020-10-06 07:44:46', '', 1, '', NULL, '2020-10-06 07:44:46', NULL, '2020-10-06 07:44:46'),
(8, 'Student', 1, 1, 2, 'REC000008/2020', 800.00, '', '1', '2020-10-06 10:43:45', '', 1, '', NULL, '2020-10-06 10:43:45', NULL, '2020-10-06 10:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE `receipt_details` (
  `id` int(20) NOT NULL,
  `id_receipt` int(10) DEFAULT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `invoice_amount` float DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt_details`
--

INSERT INTO `receipt_details` (`id`, `id_receipt`, `id_main_invoice`, `invoice_amount`, `paid_amount`, `approval_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 800, 800, NULL, 1, NULL, '2020-09-27 11:43:32', NULL, '2020-09-27 11:43:32'),
(2, 2, 1, 800, 800, NULL, 1, NULL, '2020-09-27 12:31:46', NULL, '2020-09-27 12:31:46'),
(3, 3, 1, 800, 800, NULL, 1, NULL, '2020-10-01 21:54:30', NULL, '2020-10-01 21:54:30'),
(4, 4, 1, 800, 800, NULL, 1, NULL, '2020-10-02 12:04:19', NULL, '2020-10-02 12:04:19'),
(5, 5, 14, 800, 800, NULL, 1, NULL, '2020-10-06 06:46:58', NULL, '2020-10-06 06:46:58'),
(6, 6, 15, 800, 800, NULL, 1, NULL, '2020-10-06 07:19:40', NULL, '2020-10-06 07:19:40'),
(7, 7, 16, 800, 800, NULL, 1, NULL, '2020-10-06 07:44:46', NULL, '2020-10-06 07:44:46'),
(8, 8, 17, 800, 800, NULL, 1, NULL, '2020-10-06 10:43:45', NULL, '2020-10-06 10:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(20) NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `status`) VALUES
(1, 'Administrator', 1),
(13, 'Examination Chief', 1),
(14, 'Permision View', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint(20) NOT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `id_role`, `id_permission`) VALUES
(84, 12, 5),
(85, 12, 7),
(459, 1, 5),
(460, 1, 6),
(461, 1, 7),
(462, 1, 8),
(463, 1, 9),
(464, 1, 10),
(465, 1, 11),
(466, 1, 12),
(467, 1, 13),
(468, 1, 14),
(469, 1, 15),
(470, 1, 16),
(471, 1, 17),
(472, 1, 18),
(473, 1, 19),
(474, 1, 20),
(475, 1, 21),
(476, 1, 22),
(477, 1, 23),
(478, 1, 24),
(479, 1, 25),
(480, 1, 26),
(481, 1, 27),
(482, 1, 28),
(483, 1, 29),
(484, 1, 30),
(485, 1, 31),
(486, 1, 33),
(487, 1, 34),
(488, 1, 35),
(489, 1, 36),
(490, 1, 37),
(491, 1, 38),
(492, 1, 39),
(493, 1, 40),
(494, 1, 41),
(495, 1, 42),
(496, 1, 43),
(497, 1, 44),
(498, 1, 45),
(499, 1, 46),
(500, 1, 47),
(501, 1, 48),
(502, 1, 49),
(503, 1, 50),
(504, 1, 51),
(505, 1, 52),
(506, 1, 53),
(507, 1, 54),
(508, 1, 55),
(509, 1, 56),
(510, 1, 57),
(511, 1, 58),
(512, 1, 59),
(513, 1, 60),
(514, 1, 61),
(515, 1, 62),
(516, 1, 63),
(517, 1, 64),
(518, 1, 65),
(519, 1, 66),
(520, 1, 67),
(532, 13, 14),
(548, 14, 10),
(549, 14, 11),
(550, 14, 12),
(551, 14, 13),
(552, 14, 14),
(553, 14, 15),
(554, 14, 7),
(555, 14, 8),
(556, 14, 9);

-- --------------------------------------------------------

--
-- Table structure for table `salutation_setup`
--

CREATE TABLE `salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salutation_setup`
--

INSERT INTO `salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Mr', 1, 1, NULL, '2020-07-30 21:10:18', NULL, '2020-07-30 21:10:18'),
(2, 'Ms', 2, 1, NULL, '2020-07-30 21:10:51', NULL, '2020-07-30 21:10:51'),
(3, 'Mrs', 3, 1, NULL, '2020-07-30 21:10:51', NULL, '2020-07-30 21:10:51');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_last_login`
--

CREATE TABLE `scholar_last_login` (
  `id` bigint(20) NOT NULL,
  `id_scholar` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(20) NOT NULL,
  `salutation` varchar(20) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `first_name` varchar(2048) DEFAULT '',
  `last_name` varchar(2048) DEFAULT '',
  `ic_no` varchar(50) DEFAULT '',
  `dob` date DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT '',
  `phone_number` varchar(20) DEFAULT '',
  `id_country` int(20) DEFAULT NULL,
  `id_state` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `gender` varchar(20) DEFAULT '',
  `staff_id` varchar(50) DEFAULT '',
  `email` varchar(180) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `job_type` varchar(20) DEFAULT '',
  `academic_type` varchar(20) DEFAULT '',
  `id_department` int(10) DEFAULT NULL,
  `id_faculty_program` int(20) DEFAULT '0' COMMENT 'as similar to department',
  `id_education_level` int(20) DEFAULT '0',
  `status` int(20) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `salutation`, `name`, `first_name`, `last_name`, `ic_no`, `dob`, `mobile_number`, `phone_number`, `id_country`, `id_state`, `zipcode`, `gender`, `staff_id`, `email`, `address`, `address_two`, `job_type`, `academic_type`, `id_department`, `id_faculty_program`, `id_education_level`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '1', 'Mr. qqq qwqw', 'qqq', 'qwqw', 'wqerwerw', '2020-12-03', '1121', '879789', 1, 2, 12345, 'Male', 'wertyu', 'sfs@ghgj.com', 'gjgjgj', 'hjgjgjgj', '0', '1', 0, 0, 0, 1, NULL, '2020-12-31 23:35:24', 1, '2020-12-31 23:35:24'),
(2, '1', 'Mr. qwerty asdfg', 'qwerty', 'asdfg', 'asdfgh', '2021-01-15', '123456', '345678', 1, 1, 23456, 'Male', 'asdfghj', 'asdfg@sdfgh.com', 'vhhhhhjh', 'ghhjhj', '0', '1', NULL, 0, 0, 3, NULL, '2021-01-01 00:01:19', 1, '2021-01-01 00:01:19'),
(3, '1', 'Mr. sadacna dasnda', 'sadacna', 'dasnda', 'danskda', '2021-01-21', '34567', '34567', 1, 2, 1234, 'Male', 'dasdhi', 'sad@dasdsa.com', 'dasdada', 'adasdsa', '0', '1', NULL, 0, 0, 1, NULL, '2021-01-02 22:03:54', NULL, '2021-01-02 22:03:54');

-- --------------------------------------------------------

--
-- Table structure for table `staff_bank_details`
--

CREATE TABLE `staff_bank_details` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT '0',
  `id_bank` int(20) DEFAULT '0',
  `bank_account_name` varchar(500) DEFAULT '',
  `bank_code` varchar(500) DEFAULT '',
  `bank_account_number` varchar(500) DEFAULT '',
  `bank_address` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_bank_details`
--

INSERT INTO `staff_bank_details` (`id`, `id_staff`, `id_bank`, `bank_account_name`, `bank_code`, `bank_account_number`, `bank_address`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 2, 3, 'wdasas', 'asasajs', '234567', 'ksakadka', 1, 1, '2021-01-02 22:04:31', NULL, '2021-01-02 22:04:31');

-- --------------------------------------------------------

--
-- Table structure for table `staff_change_status_details`
--

CREATE TABLE `staff_change_status_details` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT '0',
  `id_change_status` int(20) DEFAULT '0',
  `from_dt` varchar(200) DEFAULT '0',
  `to_dt` varchar(200) DEFAULT '0',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_change_status_details`
--

INSERT INTO `staff_change_status_details` (`id`, `id_staff`, `id_change_status`, `from_dt`, `to_dt`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 2, '1970-01-01', '1970-01-01', '1', 1, 1, '2021-01-01 01:06:16', NULL, '2021-01-01 01:06:16'),
(2, 2, 1, '1970-01-01', '1970-01-01', 'asdasdsada', 1, 1, '2021-01-02 22:04:50', NULL, '2021-01-02 22:04:50'),
(3, 2, 3, '1970-01-01', '1970-01-01', 'dsadasdasd', 1, 1, '2021-01-02 22:05:08', NULL, '2021-01-02 22:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `staff_has_course`
--

CREATE TABLE `staff_has_course` (
  `id` int(20) NOT NULL,
  `id_staff` int(10) DEFAULT NULL,
  `id_course` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_has_course`
--

INSERT INTO `staff_has_course` (`id`, `id_staff`, `id_course`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 2, 7, NULL, NULL, '2021-01-01 00:33:58', NULL, '2021-01-01 00:33:58'),
(3, 1, 14, NULL, NULL, '2021-01-01 01:20:54', NULL, '2021-01-01 01:20:54'),
(4, 3, 7, NULL, NULL, '2021-01-02 22:03:54', NULL, '2021-01-02 22:03:54');

-- --------------------------------------------------------

--
-- Table structure for table `staff_leave_records`
--

CREATE TABLE `staff_leave_records` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT '0',
  `name` varchar(2048) DEFAULT '',
  `from_dt` varchar(200) DEFAULT '',
  `to_dt` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_status`
--

CREATE TABLE `staff_status` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_status`
--

INSERT INTO `staff_status` (`id`, `code`, `name`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Active', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(2, '', 'Inactive', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(3, '', 'Terminated', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(4, '', 'Suspended', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(5, '', 'Deceased', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(6, '', 'Quit', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(7, '', 'Sabbatical Leave', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(8, '', 'Long MC', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(9, '', 'Maternity', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37');

-- --------------------------------------------------------

--
-- Table structure for table `staff_teaching_details`
--

CREATE TABLE `staff_teaching_details` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT '0',
  `id_semester` int(20) DEFAULT '0',
  `id_programme` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `id_mode_of_study` int(20) DEFAULT '0',
  `id_learning_center` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(10) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Karnataka', 1, 1, NULL, NULL, NULL, '2020-02-10 23:10:24'),
(2, 'Andra Pradesh', 1, 1, NULL, NULL, NULL, '2020-02-10 23:22:35'),
(3, 'Arunachal Pradesh', 1, 1, NULL, NULL, NULL, '2020-02-27 17:13:26'),
(4, 'KL', 4, 1, NULL, NULL, NULL, '2020-03-05 16:38:14'),
(5, 'Sidney', 5, 1, NULL, NULL, NULL, '2020-03-29 19:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(20) NOT NULL,
  `full_name` varchar(1024) DEFAULT '',
  `first_name` varchar(500) DEFAULT NULL,
  `last_name` varchar(500) DEFAULT NULL,
  `nric` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `mail_address1` varchar(500) DEFAULT NULL,
  `mail_address2` varchar(500) DEFAULT NULL,
  `mailing_country` int(20) DEFAULT NULL,
  `mailing_state` int(20) DEFAULT NULL,
  `mailing_zipcode` varchar(500) DEFAULT NULL,
  `mailing_city` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `full_name`, `first_name`, `last_name`, `nric`, `email`, `password`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_zipcode`, `mailing_city`) VALUES
(1, '', 'firstname', 'lastname', 'passport', 'a', 'a', 'address one', 'address tow', 0, 0, '123456', NULL),
(2, '', 'a', 'a', 'a', ' b', 'a', 'a', 'a', 0, 0, '12345', NULL),
(3, '', 'kiran', 'kiran', 'kiran', 'kiran@a.com', 'kiran', 'kiran', 'kiran', 0, 0, '1234', NULL),
(4, '', 'rut', 'rut', 'ru', 'ruu@u.com', 'ruu', 'ruu', 'ruu', 0, 0, '1234', NULL),
(5, '', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 0, 0, 'o', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_has_course`
--

CREATE TABLE `student_has_course` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_category` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_invoice` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `expiry_date` date DEFAULT NULL,
  `amount` float(20,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_has_course`
--

INSERT INTO `student_has_course` (`id`, `id_student`, `id_category`, `id_course`, `id_invoice`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `expiry_date`, `amount`) VALUES
(1, NULL, 1, 3, 10, 1, NULL, '2020-10-06 06:02:23', NULL, '2020-10-06 06:02:23', NULL, 0.00),
(2, NULL, 1, 3, 11, 1, NULL, '2020-10-06 06:04:10', NULL, '2020-10-06 06:04:10', NULL, 0.00),
(3, NULL, 1, 3, 12, 1, NULL, '2020-10-06 06:05:07', NULL, '2020-10-06 06:05:07', NULL, 0.00),
(4, NULL, 1, 3, 13, 1, NULL, '2020-10-06 06:05:47', NULL, '2020-10-06 06:05:47', NULL, 0.00),
(5, 1, 1, 6, 14, 1, NULL, '2020-10-06 06:46:58', NULL, '2020-10-06 06:46:58', '2020-10-31', 0.00),
(6, 1, 1, 4, 15, 1, NULL, '2020-10-06 07:19:40', NULL, '2020-10-06 07:19:40', NULL, 0.00),
(7, 1, 2, 12, 16, 1, NULL, '2020-10-06 07:44:46', NULL, '2020-10-06 07:44:46', NULL, 0.00),
(8, 1, 1, 2, 17, 1, NULL, '2020-10-06 10:43:45', NULL, '2020-10-06 10:43:45', NULL, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `student_last_login`
--

CREATE TABLE `student_last_login` (
  `id` bigint(20) NOT NULL,
  `id_student` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `temp_cart`
--

CREATE TABLE `temp_cart` (
  `id` int(20) NOT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_session` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `amount` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_cart`
--

INSERT INTO `temp_cart` (`id`, `id_course`, `id_session`, `created_date`, `amount`) VALUES
(1, 1, '1601103551', '2020-09-26 12:30:55', NULL),
(2, 1, '1601126854', '2020-09-26 12:31:07', NULL),
(3, 1, '1601127885', '2020-09-26 12:34:17', NULL),
(4, 1, '1601127885', '2020-09-26 12:34:26', NULL),
(5, 1, '1601126854', '2020-09-26 16:07:11', NULL),
(6, 1, 'fgam2e9opbnsfgci6a9vrs3nlujr2qcm', '2020-09-26 19:56:19', NULL),
(7, 11, 'kld0a43qhdt5juq4pi2eo09j5nrahdro', '2020-09-27 01:01:30', NULL),
(8, 2, 'i1s7hma2bnkrp021pca500e14e889c2f', '2020-10-25 00:00:47', '800.00'),
(9, 11, 'pp51g9v0majjpl64dtcpsbds9nul5g6f', '2020-12-27 11:27:55', '800.00'),
(10, 2, 'pp51g9v0majjpl64dtcpsbds9nul5g6f', '2020-12-27 11:29:01', '800.00'),
(11, 2, '09ofgdrse2dekeu3f6va7n84v6r7sduo', '2020-12-30 13:20:48', '800.00');

-- --------------------------------------------------------

--
-- Table structure for table `temp_fee_structure`
--

CREATE TABLE `temp_fee_structure` (
  `id` int(20) NOT NULL,
  `id_session` varchar(512) DEFAULT '',
  `id_category` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `id_fee_item` int(20) DEFAULT '0',
  `amount` float(20,2) DEFAULT '0.00',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_online_claim_details`
--

CREATE TABLE `temp_online_claim_details` (
  `id` bigint(20) NOT NULL,
  `id_session` varchar(256) NOT NULL,
  `id_course` bigint(20) NOT NULL,
  `id_payment_rate` bigint(20) NOT NULL,
  `hours` bigint(20) NOT NULL,
  `date_time` varchar(2048) DEFAULT '',
  `amount` float(20,2) NOT NULL,
  `status` int(20) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `temp_staff_has_course`
--

CREATE TABLE `temp_staff_has_course` (
  `id` int(20) NOT NULL,
  `id_session` varchar(1024) DEFAULT NULL,
  `id_course` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_course` bigint(20) NOT NULL,
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`id`, `name`, `code`, `id_course`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Topic 1', 'Topic 1', 2, '', 1, 1, '2020-08-14 18:10:00', 1, '2020-08-14 18:10:00'),
(2, 'Topic 2', 'Topic 2', 1, '', 1, 1, '2020-08-14 18:10:56', NULL, '2020-08-14 18:10:56'),
(3, 'Topic 3', 'Topic 3', 1, '', 1, 1, '2020-08-14 18:11:09', NULL, '2020-08-14 18:11:09'),
(4, 'Topic 4', 'Topic 4', 1, '', 1, 1, '2020-08-14 18:11:20', NULL, '2020-08-14 18:11:20'),
(5, 'Topic 1', 'Topic 1', 1, '', 1, 1, '2020-08-14 18:12:16', 1, '2020-08-14 18:12:16'),
(6, 'Topic 2', 'Topic 2', 2, '', 1, 1, '2020-08-14 18:14:49', NULL, '2020-08-14 18:14:49'),
(7, 'Topic 1', 'Topic 1', 3, '', 1, 1, '2020-08-14 18:15:20', NULL, '2020-08-14 18:15:20'),
(8, 'Topic 2', 'Topic 2', 3, '', 1, 1, '2020-08-14 18:15:34', NULL, '2020-08-14 18:15:34'),
(9, 'Topic 3', 'Topic 3', 3, '', 1, 1, '2020-08-14 18:15:44', NULL, '2020-08-14 18:15:44'),
(10, 'Topic 4', 'Topic 4', 3, '', 1, 1, '2020-08-14 18:16:03', NULL, '2020-08-14 18:16:03'),
(11, 'Topic 1', 'Topic 1', 4, '', 1, 1, '2020-08-14 18:16:14', NULL, '2020-08-14 18:16:14'),
(12, 'Topic 2', 'Topic 2', 4, '', 1, 1, '2020-08-14 18:16:25', NULL, '2020-08-14 18:16:25');

-- --------------------------------------------------------

--
-- Table structure for table `tos`
--

CREATE TABLE `tos` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_pool` varchar(50) NOT NULL,
  `question_count` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt_tm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tos`
--

INSERT INTO `tos` (`id`, `name`, `id_pool`, `question_count`, `status`, `created_by`, `updated_by`, `created_dt_tm`, `updated_dt_tm`) VALUES
(6, 'Test TOS', '1,2', 6, 1, 1, 1, '2020-08-15 00:28:05', '2020-08-15 00:28:05');

-- --------------------------------------------------------

--
-- Table structure for table `tos_details`
--

CREATE TABLE `tos_details` (
  `id_details` bigint(20) NOT NULL,
  `id_course` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  `id_bloom_taxonomy` int(11) NOT NULL,
  `id_difficult_level` int(11) NOT NULL,
  `id_pool` int(11) NOT NULL,
  `questions_available` int(11) NOT NULL,
  `questions_selected` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_tos` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tos_details`
--

INSERT INTO `tos_details` (`id_details`, `id_course`, `id_topic`, `id_bloom_taxonomy`, `id_difficult_level`, `id_pool`, `questions_available`, `questions_selected`, `status`, `id_tos`) VALUES
(6, 1, 2, 1, 2, 1, 3, 2, 1, 6),
(7, 1, 2, 1, 3, 1, 1, 1, 1, 6),
(8, 1, 3, 1, 3, 1, 1, 1, 1, 6),
(9, 1, 5, 1, 1, 1, 2, 1, 1, 6),
(10, 2, 1, 4, 3, 2, 1, 1, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(1024) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `id_category` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `name`, `code`, `name_optional_language`, `id_category`, `id_course`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'dasda', 'adasd', '', 2, 14, 1, 1, '2021-01-02 22:19:46', NULL, '2021-01-02 22:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `email` varchar(1024) DEFAULT '',
  `password` varchar(500) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `salutation` varchar(20) DEFAULT '',
  `first_name` varchar(1024) DEFAULT '',
  `last_name` varchar(1024) DEFAULT '',
  `mobile` varchar(50) DEFAULT '',
  `id_role` int(20) DEFAULT NULL,
  `is_deleted` int(20) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `salutation`, `first_name`, `last_name`, `mobile`, `id_role`, `is_deleted`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'virat@admin.com', '202cb962ac59075b964b07152d234b70', 'Mr. Virat Kohli', '1', 'Virat', 'Kohli', '88888888', 14, 0, 1, 1, '2020-07-26 23:20:26', NULL, '2020-07-26 23:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `user_exams`
--

CREATE TABLE `user_exams` (
  `Id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `examset_code` varchar(50) DEFAULT NULL,
  `exam_id` bigint(20) DEFAULT NULL,
  `exam_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `correct` bigint(20) DEFAULT NULL,
  `wrong` bigint(20) DEFAULT NULL,
  `grade` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_exams`
--

INSERT INTO `user_exams` (`Id`, `user_id`, `examset_code`, `exam_id`, `exam_date`, `correct`, `wrong`, `grade`, `status`) VALUES
(1, 3, 'EXM20200904084547', 6, '2020-09-04 21:13:31', 0, 0, 0, 1),
(2, 3, 'EXM20200904084547', 6, '2020-09-04 21:35:36', 0, 0, 0, 1),
(3, 3, 'EXM20200904084547', 6, '2020-09-04 21:41:02', 0, 0, 0, 1),
(4, 3, 'EXM20200904084547', 6, '2020-09-04 21:42:29', 0, 0, 0, 1),
(5, 3, 'EXM20200904084547', 6, '2020-09-04 21:43:53', 0, 0, 0, 1),
(6, 3, 'EXM20200904084547', 6, '2020-09-04 22:20:51', 0, 0, 0, 1),
(7, 3, 'EXM20200904084547', 6, '2020-09-04 22:22:30', 0, 0, 0, 1),
(8, 3, 'EXM20200904084547', 6, '2020-09-04 22:22:57', 0, 0, 0, 1),
(9, 3, 'EXM20200904084547', 6, '2020-09-04 22:25:50', 0, 0, 0, 1),
(10, 3, 'EXM20200904084547', 6, '2020-09-04 22:27:25', 0, 0, 0, 1),
(11, 3, 'EXM20200904084547', 6, '2020-09-04 22:30:55', 0, 0, 0, 1),
(12, 3, 'EXM20200906011804', 6, '2020-09-06 13:18:06', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_exam_details`
--

CREATE TABLE `user_exam_details` (
  `Id` bigint(20) NOT NULL,
  `id_question` bigint(20) DEFAULT NULL,
  `id_answer` bigint(20) DEFAULT NULL,
  `valid` tinyint(1) NOT NULL,
  `id_userexam` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_exam_details`
--

INSERT INTO `user_exam_details` (`Id`, `id_question`, `id_answer`, `valid`, `id_userexam`) VALUES
(1, 8, 3, 0, 2),
(2, 6, 3, 0, 4),
(3, 6, 3, 0, 5),
(4, 8, 5, 0, 5),
(5, 6, 3, 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `user_last_login`
--

CREATE TABLE `user_last_login` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_last_login`
--

INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(2, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(3, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(4, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(5, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(6, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(7, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(8, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(9, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(10, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(11, 1, '{"role":"2","roleText":"Ex","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(12, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(13, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(14, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(15, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(16, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(17, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(18, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(19, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(20, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '117.230.180.35', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(21, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(22, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(23, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(24, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(25, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(26, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(27, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(28, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(29, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(30, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(31, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(32, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(33, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(34, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(35, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(36, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(37, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(38, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '157.49.159.118', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(39, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(40, 1, '{"role":"1","roleText":null,"name":"Mr. Virat Kohli"}', '157.49.104.95', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(41, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '157.49.104.95', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(42, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '157.49.98.97', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(43, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(44, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(45, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(46, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(47, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(48, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(49, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(50, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(51, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(52, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(53, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(54, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(55, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(56, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(57, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_last_login`
--
ALTER TABLE `admin_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicant_last_login`
--
ALTER TABLE `applicant_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `award`
--
ALTER TABLE `award`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_registration`
--
ALTER TABLE `bank_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_has_module`
--
ALTER TABLE `category_has_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group`
--
ALTER TABLE `communication_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_setup`
--
ALTER TABLE `currency_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_category`
--
ALTER TABLE `fee_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_setup`
--
ALTER TABLE `fee_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structure`
--
ALTER TABLE `fee_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structure_main`
--
ALTER TABLE `fee_structure_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice`
--
ALTER TABLE `main_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_claim`
--
ALTER TABLE `online_claim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_claim_details`
--
ALTER TABLE `online_claim_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_rate`
--
ALTER TABLE `payment_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_last_login`
--
ALTER TABLE `scholar_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_bank_details`
--
ALTER TABLE `staff_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_change_status_details`
--
ALTER TABLE `staff_change_status_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_has_course`
--
ALTER TABLE `staff_has_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_leave_records`
--
ALTER TABLE `staff_leave_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_status`
--
ALTER TABLE `staff_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_teaching_details`
--
ALTER TABLE `staff_teaching_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_has_course`
--
ALTER TABLE `student_has_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_last_login`
--
ALTER TABLE `student_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_cart`
--
ALTER TABLE `temp_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_fee_structure`
--
ALTER TABLE `temp_fee_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_online_claim_details`
--
ALTER TABLE `temp_online_claim_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_staff_has_course`
--
ALTER TABLE `temp_staff_has_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tos`
--
ALTER TABLE `tos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tos_details`
--
ALTER TABLE `tos_details`
  ADD PRIMARY KEY (`id_details`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_exams`
--
ALTER TABLE `user_exams`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `user_exam_details`
--
ALTER TABLE `user_exam_details`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `user_last_login`
--
ALTER TABLE `user_last_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_last_login`
--
ALTER TABLE `admin_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_last_login`
--
ALTER TABLE `applicant_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `award`
--
ALTER TABLE `award`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bank_registration`
--
ALTER TABLE `bank_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `category_has_module`
--
ALTER TABLE `category_has_module`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `communication_group`
--
ALTER TABLE `communication_group`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `currency_setup`
--
ALTER TABLE `currency_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fee_category`
--
ALTER TABLE `fee_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fee_setup`
--
ALTER TABLE `fee_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `fee_structure`
--
ALTER TABLE `fee_structure`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `fee_structure_main`
--
ALTER TABLE `fee_structure_main`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `main_invoice`
--
ALTER TABLE `main_invoice`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;
--
-- AUTO_INCREMENT for table `online_claim`
--
ALTER TABLE `online_claim`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `online_claim_details`
--
ALTER TABLE `online_claim_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `payment_rate`
--
ALTER TABLE `payment_rate`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `receipt_details`
--
ALTER TABLE `receipt_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `scholar_last_login`
--
ALTER TABLE `scholar_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `staff_bank_details`
--
ALTER TABLE `staff_bank_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `staff_change_status_details`
--
ALTER TABLE `staff_change_status_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `staff_has_course`
--
ALTER TABLE `staff_has_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `staff_leave_records`
--
ALTER TABLE `staff_leave_records`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `staff_status`
--
ALTER TABLE `staff_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `staff_teaching_details`
--
ALTER TABLE `staff_teaching_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `student_has_course`
--
ALTER TABLE `student_has_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `student_last_login`
--
ALTER TABLE `student_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_cart`
--
ALTER TABLE `temp_cart`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `temp_fee_structure`
--
ALTER TABLE `temp_fee_structure`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_online_claim_details`
--
ALTER TABLE `temp_online_claim_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `temp_staff_has_course`
--
ALTER TABLE `temp_staff_has_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tos`
--
ALTER TABLE `tos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tos_details`
--
ALTER TABLE `tos_details`
  MODIFY `id_details` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_exams`
--
ALTER TABLE `user_exams`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user_exam_details`
--
ALTER TABLE `user_exam_details`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_last_login`
--
ALTER TABLE `user_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
